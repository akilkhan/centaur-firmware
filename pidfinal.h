#ifndef __PF_PID_H__
#define __PF_PID_H__

//#include "fixedMath.h"
//Change these as per the data types of the framework ...
#define __PF_PID_PARAM_IS_DWORD__	1
// #define USE_INTMATH_CAL			1
#define USE_FIXEDMATH_CAL					1

#define USE_PID						1
#define FPS  						200
#define MAX_ENCODER_COUNT_RATE  	13000
// #define MAX_ENCODER_COUNT_RATE  	10000
#define MIN_ENCODER_COUNT_RATE  	0
#define MAX_PWM_COUNT  				500 
#define MIN_PWM_COUNT  				45 
#define MAX_ENCODER_COUNT			6400
#define MAX_PWM_FREQ				10000
#define FACTOR_SEC 					100

/* Weight constant of filter */
#define K_PV_NUM					2
#define K_PV_DENOM					10
#define K_CO_NUM					1
#define K_CO_DENOM					1




#ifdef USE_FIXEDMATH_CAL

#undef ERR_DIMENSIONLESS_FRACT		0x028f
#undef K_PV_NUM					
#undef K_PV_DENOM					
#undef K_CO_NUM					
#undef K_CO_DENOM					
#undef __PF_PID_PARAM_IS_DWORD__ 

#define K_CO_INTPART_VAL					1
#define K_CO_FRACTPART_VAL					0
#define K_CO_FRACTION_PRECISION				10

#define K_PV_INTPART_VAL					0
#define K_PV_FRACTPART_VAL					2
#define K_PV_FRACTION_PRECISION				10


#if ( BIT_PRECISION == 4)
#define K_CO_INTPART			0x10
#define K_CO_FRACTPART			0x10
#define K_CO_DECIMAL_PRECISION			0x10

#define K_PV 			0x03

#endif

#if ( BIT_PRECISION == 8)
#define K_CO			0x100
#define K_PV 			0x033
#endif

#if ( BIT_PRECISION == 12)
#define K_CO			0x1000
#define K_PV 			0x0333
#endif

#if ( BIT_PRECISION == 16)
#define K_CO			0x10000
#define K_PV 			0x03333
#endif

#endif

#define MAX_PID_NO 			2



#ifdef __PF_PID_PARAM_IS_FLOAT__
typedef PFfloat_t pfPidData_t;
typedef PFfloat_t pfPidConstant_t;

#elif __PF_PID_PARAM_IS_BYTE__
typedef uint8_t pfPidData_t;
typedef uint8_t pfPidConstant_t;

#elif __PF_PID_PARAM_IS_WORD__
typedef uint16_t pfPidData_t;
typedef uint16_t pfPidConstant_t;

#elif __PF_PID_PARAM_IS_DWORD__
typedef int32_t pfPidData_t;
typedef int32_t pfPidConstant_t;

#elif USE_FIXEDMATH_CAL
typedef pfFixedDataType pfPidData_t;
typedef pfFixedDataType pfPidConstant_t;

#endif


typedef struct 
{
		int64_t errEachLoop;
		int64_t errAccumalated;
}pfPidErrors;

typedef int pidLoopTime_t;

/* PID Config only PID parameters */

#ifdef USE_INTMATH_CAL

typedef struct
{
	pfPidConstant_t kpNum; 				//defines the proportional constant Numerator
	pfPidConstant_t kpDenom; 			//defines the proportional constant Denominator 
	pfPidConstant_t tiNum; 				//defines the integral Time constant Numerator 
	pfPidConstant_t tiDenom; 			//defines the integral Time constant Denominator 
	pfPidConstant_t tdNum; 				//defines the differential constant Numerator 
	pfPidConstant_t tdDenom; 			//defines the differential constant Denominator 
	pfPidData_t maxPidOutputNum; 		// maximum limit for the PID output Numerator
	pfPidData_t maxPidOutputDenom; 		// maximum limit for the PID output	Denominator
	pfPidData_t minPidOutputNum; 		// minimum limit for the PID output(threshold)
	pfPidData_t minPidOutputDenom; 		// minimum limit for the PID output(threshold)
	pfPidData_t outputSpan;				// Span of effective output
	pfPidData_t maxPidInputNum; 			// maximum limit for the PID error input per pid loop
	pfPidData_t maxPidInputDenom; 			// maximum limit for the PID error input per pid loop       
	pfPidData_t minPidInputNum; 			// minimum limit for the PID error input per pid loop 	
	pfPidData_t minPidInputDenom; 			// minimum limit for the PID error input per pid loop       
	pfPidData_t inputSpan;				// Span of effective input 
		
} PFPidConfig; //Pid control configuration

/* PID context Filtered */
typedef struct
{
	PFPidConfig *configuration;
	pfPidData_t setPoint;	
	pfPidData_t accumulatedErrorNum;
	pfPidData_t accumulatedErrorDenom;
	pfPidData_t lastErrorInputNum; 
	pfPidData_t lastErrorInputDenom; 
	pfPidData_t lastFilteredCO; 	
} PFPidContext;	

PFEnStatus pfPidOpen(PFbyte* idArray,const PFPidConfig *context,PFbyte count);
pfPidData_t pfPidUpdate(PFbyte id, pfPidData_t currentOutput,pfPidErrors *errPrint);

	
#elif USE_FIXEDMATH_CAL
	
typedef struct
{
	pfPidConstant_t kp; 				//defines the proportional constant 
	pfPidConstant_t ti; 				//defines the integral Time constant  
	pfPidConstant_t td; 				//defines the differential constant  
	pfPidData_t maxPidOutput; 		// maximum limit for the PID output 
	pfPidData_t minPidOutput; 		// minimum limit for the PID output(threshold)
	pfPidData_t outputSpan;				// Span of effective output
	pfPidData_t maxPidInput; 			// maximum limit for the PID error input per pid loop       
	pfPidData_t minPidInput; 			// minimum limit for the PID error input per pid loop       
	pfPidData_t inputSpan;				// Span of effective input 
		
} PFPidConfig; //Pid control configuration

typedef struct
{
	PFPidConfig *configuration;
	pfPidData_t setPoint;	
	pfPidData_t lastErrorInput;
	pfPidData_t accumulatedError;
	pfPidData_t lastControlOut;	
} PFPidContext; //Pid context ...

PFEnStatus pfPidOpen(PFbyte* idArray,const PFPidConfig *context,PFbyte count);
int64_t pfPidUpdate(PFbyte id, int64_t controlInput,pfPidErrors *errPrint);
	
#endif



PFEnStatus pfPidControlReset(PFbyte id); //inline

PFEnStatus pfPidControlSetParams(PFbyte id, pfPidConstant_t kp, pfPidConstant_t ki, pfPidConstant_t kd, pidLoopTime_t sampleWindow);//inline

PFEnStatus pfPidChangeSetPoint(PFbyte id, pfPidData_t* setPoint);

#endif /*__PF_PID_H__*/
