/* centaur.h */

#pragma once

#define CENTAUR_TEST 							 	0

#define CENTAUR_USE_CAN                         	1   	/** Use CAN for communication between Baseboard & Expansion board */
#define CENTAUR_USE_SPI                         	0   	/** Use SPI for communication between Baseboard & Expansion board */
#define CENTAUR_USE_UART                        	0   	/** Use UART for communication between Baseboard & Expansion board */
#define CENTAUR_CAN_MSG_ID                      	0x07	/** Can message ID */
#define CENTAUR_MAX_CMD_RETRY                   	3   	/** Number of times driver tries to send command */
#define CENTAUR_STX                            	 	'$' 	/** Start of packet */
#define CENTAUR_ETX                             	'@' 	/** End of packet */

#define CENTAUR_OPEN_CAN_RCV_ACK_TIMEOUT        	100000
#define CENTAUR_CMD_CAN_RCV_ACK_TIMEOUT         	100000
	
#define CENTAUR_OPEN_UART_RCV_ACK_TIMEOUT       	100000
#define CENTAUR_CMD_UART_RCV_ACK_TIMEOUT        	100000

/* delay after which baseboard read response of Expansion board via SPI */

#define CENTAUR_LYNX_OPEN_SPI_RCV_ACK_TIMEOUT      	400
#define CENTAUR_SERVO_OPEN_SPI_RCV_ACK_TIMEOUT     	400
#define CENTAUR_SMARTLYNX_OPEN_SPI_RCV_ACK_TIMEOUT 	1300

#define CENTAUR_LYNX_CMD_SPI_RCV_ACK_TIMEOUT       	500
#define CENTAUR_LYNX_SLEEP_CMD_SPI_RCV_ACK_TIMEOUT 	1100
#define CENTAUR_SERVO_CMD_SPI_RCV_ACK_TIMEOUT      	200
#define CENTAUR_SMARTLYNX_CMD_SPI_RCV_ACK_TIMEOUT  	500

#define CMD_CENTAUR_OPEN   				        	4   /** Command ID for CENTAUR-CB open */
#define CMD_CENTAUR_MOTOR_CLOSE    				    5   /** Command ID for motor close */
#define CMD_CENTAUR_MOTOR_RESET_DEVICE          	6   /** Command ID for device reset */
#define CMD_CENTAUR_MOTOR_RESET_POSITION        	7   /** Command ID for position reset */
#define CMD_CENTAUR_MOTOR_GOHOME    			    8   /** Command ID for Go home position */
#define CMD_CENTAUR_MOTOR_SOFTSTOP    			    9   /** Command ID for soft stop */
#define CMD_CENTAUR_MOTOR_HARDSTOP    			    10  /** Command ID for hard stop */
#define CMD_CENTAUR_MOTOR_MOVE   				    11  /** Command ID for motor move */
#define CMD_CENTAUR_MOTOR_SET_FULLSTEP_SPEED    	12  /** command ID for setting full step speed */
#define CMD_CENTAUR_MOTOR_RUN    				    13  /** command ID to run motor */
#define CMD_CENTAUR_MOTOR_READY    				    14  /** command ID to test motor ready */
#define CMD_CENTAUR_MOTOR_SET_ACC_PROFILE     		15  /** command ID to set acc profile */
#define CMD_CENTAUR_MOTOR_GET_ACC_PROFILE      		16  /** command ID to get acc profile */
#define CMD_CENTAUR_GET_MOTOR_COUNT    		  		17  /** command ID to get position */
#define CMD_CENTAUR_MOTOR_SLEEP   				    18  /** command ID to put driver in sleep mode */
#define CMD_CENTAUR_MOTOR_SET_STEPCLOCK_MODE    	19  /** command ID for setting motor in stepclock mode */
#define CMD_CENTAUR_MOTOR_TAKE_SINGLE_STEP      	20  /** command ID to take single step movement */
#define CMD_CENTAUR_MOTOR_GET_SPEED   			    21  /** command ID for reading speed */
#define CMD_CENTAUR_MOTOR_GOMARK    			    22  /** command ID to go to mark position */
#define CMD_CENTAUR_MOTOR_GOUNTIL    			    23  /** command ID to go until switch press */
#define CMD_CENTAUR_MOTOR_RELEASE_SW   			    24  /** command ID to go until switch release */
#define CMD_CENTAUR_SET_MOTOR_SPEED    		  		25  /** command ID for setting minimum speed */
#define CMD_CENTAUR_SET_MOTOR_COUNTRATE   		    26  /** command ID for setting minimum speed */
#define CMD_CENTAUR_SET_MOTOR_DIRECTION   		    27  /** command ID for setting minimum speed */
#define CMD_CENTAUR_OPEN_RESPONSE         	        31  /** Init Response **/
#define CMD_CENTAUR_HEADER_ERROR                	41  /** Init Response **/
#define CMD_CENTAUR_SET_MOTOR_SPEED_RESPONSE    	32  /** Init Response **/
#define CMD_CENTAUR_GET_MOTOR_COUNT_RESPONSE    	33

#define CMD_CENTAUR_BOARD_RESET						0xff
#define CMD_CENTAUR_BOARD_RESET_RESPONSE			0xff

#define CENTAUR_MAX_SUPPORTED       				1

typedef struct
{
#if (PF_USE_FIXEDMATH_CAL == 0)
	PFdword pvConstantNum;
	PFdword pvConstantDenom;
	PFdword coConstantNum;
	PFdword coConstantDenom;
#elif (PF_USE_FIXEDMATH_CAL == 1)
	pfFixedDataType pvConstant;
	pfFixedDataType coConstant;
#endif
}PFCfgPidFilter;

typedef struct
{
	PFEnBoolean useEncoder;
	PFdword motorMaxSpeed; 	/** Max change in encoder count per sec  */
	PFdword encoderCpr;
	PFdword motorGearRatio;
	PFEnBoolean usePid;
	PFCfgPid pidCfgData;
}PFCfgCentaur;

typedef PFCfgCentaur* PFpCfgCentaur;

typedef struct
{
	PFbyte command;
}PFCentaurHeader;

typedef struct
{
	PFbyte checksum;
}PFCentaurFooter;

typedef struct
{
	PFCentaurHeader header;
	PFbyte data[86];
	PFCentaurFooter footer;
}PFCentaurInitMsg;

typedef struct
{
	PFCentaurHeader header;
	PFbyte data[14];
	PFCentaurFooter footer;
}PFCentaurCmdMsg;

typedef struct
{
	PFEnStatus status;
	PFdword encoderCount;
	PFdword encoderTimeStamp;
}PFCentaurMotorCountResponse;

/* Enumeration for different states of board */
typedef enum
{
	enCentaurStateInit = 0,   /** board's initialization state */
	enCentaurStateIdle,       /** board waiting for command	*/
	enCentaurStateRunning,    /** board parsing received command */
	enCentaurStateExecution,  /** board executing command */
	enCentaurStateReset,      /** board's reset state */
	enCentaurStateError       /** board is in error state, when error log exceeds specified limit */
}PFEnCentaurState;

PFEnStatus pfCentaurHwInit(void);

PFEnStatus pfCentaurOpen(PFpCfgCentaur cbChannel);

PFEnStatus pfCentaurSetSpeed(PFsdword Speed);

PFEnStatus pfCentaurSetDir(PFbyte dir);

PFEnStatus pfCentaurGetEncoderCount(PFdword* count);

// PFEnStatus pfCentaurGetBatteryVolt(PFbyte channel, PFdword* volt);

