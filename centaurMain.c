#include "prime_framework.h"
#include "prime_centaurBoardConfig.h"
#include "prime_gpio.h"
#include "a3941.h"
#include "prime_sysClk.h"
#include "prime_uart0.h"
#include "prime_timer1.h"
#include "prime_pwm.h"
#include "prime_spi0.h"
#include "prime_canCommon.h"
#include "prime_can1.h"
#include "prime_qei.h"

#include "fixedMath.h"

#include "prime_pidHwconfig.h"
#include "prime_pid.h"

#include "centaur.h"
#include "centaurMain.h"

#if (CENTAUR_CB_STANDALONE_TEST == 1)
	#include "centaurStandaloneTest.h"
#endif

extern PFEnBoolean channelInitFlag;
extern PFbyte idCentaurPid;

PFbyte pidId[MAX_PID_NO];
PFbyte printBuff[512] = {0};
PFbyte cmd_count = 0, cmd_flag_90 = 0;

volatile PFdword encoderCount = 0;
volatile PFdword prev_encoderCount = 0;
volatile static PFdword elapsedtime = 0;
PFdword printCount = 0;
PFdword intStatus = 0;

PFsdword setPointArray[10] = {1111, 2223, 3334, 4567, 5678, 3456, 6789, 7654, 4321, 9876};
volatile PFsdword error_current=0, error_prev=0, error_difference=0, error_sum=0, curr_velocity=0, prev_velocity=0;
volatile PFsdword  pid_I_term=0, count = 1, thresholdCount = FPS / 100 ;
volatile PFsdword step = 0;
PFsdword duty_cycle = 0;

int64_t last_filtered_countrate = 0;
static pfPidErrors errPrint;
pfPidData_t desired_countrate = {0};

void canReceiveIsr(void);
void allPIDTermUsed(void);

PFEnStatus writeCentaurResetMessage(void);
PFEnStatus writeCentaurCmdMessage(PFbyte cmd, PFbyte *data);
PFEnStatus writeCentaurInitMessage(PFbyte cmd, PFbyte *data);
PFEnStatus writeResponse(PFbyte cmd, PFbyte *data, PFdword data_length);
PFEnStatus createCheckSum(PFbyte *data, PFbyte* checksum, PFdword datalength);
PFEnBoolean isChecksumValid(PFbyte *data, PFbyte checksum, PFdword datalength);

PFCentaurInitMsg initMsg = {0};
PFCentaurCmdMsg cmdMsg = {0};
PFEnCentaurState centaurState = enCentaurStateInit;
PFEnBoolean isInitialized = enBooleanFalse;

//#define CAN_TEST 0

int main(void)
{	
	PFEnStatus status;
	PFEnBoolean timerStart = enBooleanFalse;
	PFEnBoolean initSuccess = enBooleanTrue;
	
	volatile PFbyte loop1 = 0, loop2 = 0;
	volatile PFbyte checksum=0, dir=0, dataBytes=0, no_of_packets =0;
	PFdword motorSpeed = 0;
	PFbyte buf[40] = {0};
	
	PFEnStatus initStatus = enStatusCrcError;
	volatile PFCanMsgHeader msgHeader = {CENTAUR_CAN_MSG_ID, enCanFrameStandard, enBooleanFalse};
	
	PFCfgCentaur configData;
	PFCentaurCmdMsg cmd;
	PFCentaurInitMsg init;
	PFCentaurMotorCountResponse motorCountResponse;

	pfMemSet(&configData, 0, sizeof(PFCfgCentaur));
	pfMemSet(&initMsg, 0, sizeof(PFCentaurInitMsg));
	pfMemSet(&init, 0, sizeof(PFCentaurInitMsg));
	pfMemSet(&cmdMsg, 0, sizeof(PFCentaurCmdMsg));
	pfMemSet(&cmd, 0, sizeof(PFCentaurCmdMsg));
	
	while(1)
	{ 
		switch(centaurState)
		{
			case enCentaurStateInit:
			{
				status = boardInit();
				
				#ifdef CAN_TEST
				while(1)
				{	
					do{
 					pfUart0GetRxBufferCount(&cnt);
 				}while(cnt<8);
 				
				pfUart0Read(dat,8,&cnt);	
 			
				pfUart0Write(dat,cnt);
 				
				pfCan1Write(&txMsg,dat,cnt);
				
				pfUart0WriteString("Message sent\r");			
				}
				#endif
				if(status != enStatusSuccess)
				{
					centaurState = enCentaurStateError;
				}
				else
				{
					centaurState = enCentaurStateIdle;
				}
				break;
			} // case enCentaurStateInit
			
			case enCentaurStateIdle:
			{
				// Do Nothing;
				break;
			}	// case enCentaurStateIdle
			
			case enCentaurStateReset:
			{
				writeCentaurResetMessage();
				pfUart0WriteString("\rReset ");
				centaurState = enCentaurStateIdle;
				break;
			}	// case enCentaurStateReset
			
			case enCentaurStateRunning:
			{
				if(!isInitialized)
				{
					// Copy init message to local buffer..
					pfMemCopy(&init, &initMsg, sizeof(PFCentaurInitMsg));
					//Verify checksum
					if(!isChecksumValid(init.data, init.footer.checksum , sizeof(PFCfgCentaur)))
					{
						// Not Valid;
						pfUart0WriteString("\r CS ERR");
						status = enStatusCrcError;
						writeResponse(CMD_CENTAUR_HEADER_ERROR, (PFbyte *)&status, sizeof(PFEnStatus));
						centaurState = enCentaurStateIdle;
						break;
					}
					// Valid
					if(init.header.command != CMD_CENTAUR_OPEN)
					{
						pfUart0WriteString("\r CMD ERR");
						status = enStatusAccessDenied;
						writeResponse(CMD_CENTAUR_HEADER_ERROR, (PFbyte *)&status, sizeof(PFEnStatus));
						centaurState = enCentaurStateIdle;
						break;
					}
					
					pfMemCopy(&configData, init.data, sizeof(PFCfgCentaur));
					initStatus = pfCentaurOpen(&configData);
					initSuccess = (!initStatus) & initSuccess;
					
					pfUart0WriteString("\r INIT ok");
					isInitialized = initSuccess;
					centaurState = enCentaurStateIdle;
					
					//Write Response.
					writeResponse(CMD_CENTAUR_OPEN_RESPONSE, (PFbyte *)&isInitialized, sizeof(PFEnBoolean));
					break;
				} 
				else 
				{
					// Copy cmd to local variable;
					pfMemCopy(&cmd, &cmdMsg, sizeof(PFCentaurCmdMsg));
					// Reset State to Idle.
					centaurState = enCentaurStateIdle;
					//Verify checksum
					if(!isChecksumValid(cmd.data, cmd.footer.checksum, sizeof(cmd.data) ))
					{
						status = enStatusCrcError;
						writeResponse(CMD_CENTAUR_HEADER_ERROR, (PFbyte *)&status,  sizeof(PFEnStatus));
						break;
					}
					
					switch(cmd.header.command)
					{
						case CMD_CENTAUR_SET_MOTOR_SPEED:
						{
							pfMemCopy(&motorSpeed, cmd.data, 4);	
							status = pfCentaurSetSpeed(motorSpeed);
							sprintf(buf,"SPEED : %d\r",motorSpeed);
							pfUart0WriteString(buf);
							
							if(timerStart == enBooleanFalse)
							{
								timerStart = enBooleanTrue;
								pfTimer1Start();
							}
							// writeResponse
							writeResponse(CMD_CENTAUR_SET_MOTOR_SPEED_RESPONSE, (PFbyte *)&status, sizeof(PFEnStatus));
							break;
						}// Set Speed
												
						case CMD_CENTAUR_GET_MOTOR_COUNT:
						{
							// Create data structure.
							motorCountResponse.status = status;
							motorCountResponse.encoderCount = encoderCount;
							motorCountResponse.encoderTimeStamp = elapsedtime;
							writeResponse(CMD_CENTAUR_GET_MOTOR_COUNT_RESPONSE, (PFbyte*)&motorCountResponse, sizeof(PFCentaurMotorCountResponse));
							break;
						}// Get encoderCount command

						default: 
						{ 
							// We should not come here. 
							// Send Error
							status = enStatusAccessDenied;
							writeResponse(CMD_CENTAUR_HEADER_ERROR, (PFbyte *)&status, sizeof(PFEnStatus));
						}
					}	//switch for Commands 
				}	// else for Command
			}	// case enCentaurStateRunning
		}	// Switch for State
				
		#if (ENABLE_PID_LOG == 1)
 		if (count >= STORED_DATA_SIZE)
 		{
 			pfTimer1Stop();
 			pfPwmUpdateMatchRegister(CENTAUR_PWM_CHANNEL,0);
 			for(i = 0; i < STORED_DATA_SIZE; i++)
 			{
 			#ifdef PF_USE_FIXEDMATH_CAL
 				pfFixedNum2Decimal(&deciSetpoint,setPointArray1[i] );
 				pfFixedNum2Decimal(&deciPv,pv_printdata[i] );
 				pfFixedNum2Decimal(&deciErr,errLoop_printdata[i] );
 				pfFixedNum2Decimal(&deciAccErr,errAccumated_printdata[i] );
 				pfFixedNum2Decimal(&pwm,duty_printdata[i]);
 				printCount=	sprintf( printBuff,"\n\r%4d \t%f \t%f \t%f\t%f \t%f", i, deciSetpoint, deciPv, pwm, deciErr, deciAccErr);
 			#else
			//	printCount=	sprintf( printBuff,"\n\r%4d \t%6d \t%6d \r\n", i, duty_printdata[i], pv_printdata[i]); // step response
			//	printCount=	sprintf( printBuff,"\n\r%x \t%x \t%x \t%x \t%x \t%x", i, setPointArray1[i], pv_printdata[i], duty_printdata[i], errLoop_printdata[i], errAccumated_printdata[i]);
			//	printCount=	sprintf( printBuff,"\n\r%4d \t%6d \t%6d \t%6d ", i, setPointArray1[i], pv_printdata[i], duty_printdata[i]);
  				printCount=	sprintf( printBuff,"\n\r%4d \t%6d \t%6d \t%6d \t%6d \t%6d", i, setPointArray1[i], pv_printdata[i], duty_printdata[i], errLoop_printdata[i], errAccumated_printdata[i]);	
 			#endif
 				pfUart0Write(printBuff,printCount);
 			}
 			count = 0;
  			elapsedtime =0;
 		}
	#endif	// #if (ENABLE_PID_LOG == 1)		
	}// while(1)
	return 0;
}

//
// PID Timer Callback
//
#ifdef PF_USE_INTMATH_CAL 
void allPIDTermUsed(void)
{
	int32_t delCount, countRate = 0, filteredCountRateNum = 0, filteredCountRateDenom = 1;
	int32_t finalCountRate = 0;
	int32_t duty_cycle=0;
	PFbyte buf[30];

	__asm__("cpsid i");
	pfTimer1GetIntStatus(&intStatus);
	if(intStatus & 0x01)		// Match reg 0
	{
 		pfTimer1ClearIntStatus(intStatus);
		
		// PID for channel 
		if(channelInitFlag == enBooleanTrue)
		{
			encoderCount = pfQeiGetPosition();
				
			#if (USE_COUNT_DIFF_FILTER == 0)
					// x(t) = cr = delcount * fps  
				delCount = encoderCount - prev_encoderCount;
				sprintf(buf,"del count : %d\r",delCount);
				pfUart0WriteString(buf);
				countRate = delCount * FPS;
			#else	
				countDiff = encoderCount - prev_encoderCount;
				if(countDiff > countDiffMax)
				{
					countDiff = countDiffPrev;
				}
				countDiffPrev = countDiff;
				countRate = countDiff * FPS;
			#endif	// USE_COUNT_DIFF_FILTER

			// y(t) = (1-K) * y(t-1) + K * x(t) 
			filteredCountRateNum = (K_PV_DENOM - K_PV_NUM) * last_filtered_countrate + K_PV_NUM * countRate;
			filteredCountRateDenom = K_PV_DENOM;
			
			finalCountRate = filteredCountRateNum / filteredCountRateDenom;
			last_filtered_countrate = finalCountRate;
			prev_encoderCount = encoderCount;
			duty_cycle = pfPidUpdate(idCentaurPid, finalCountRate, &errPrint);
			
			sprintf(buf,"cr: %d Duty Cycle : %d\r",finalCountRate,duty_cycle);
			pfUart0WriteString(buf);
						
			if(duty_cycle < 0)
			{
				pfGpioPinsClear(A3941_PHASE_PORT, A3941_PHASE_PIN);
				pfPwmUpdateMatchRegister(CENTAUR_PWM_CHANNEL,(500-(-duty_cycle)));
			}
			else
			{
				pfGpioPinsSet(A3941_PHASE_PORT, A3941_PHASE_PIN);
				pfPwmUpdateMatchRegister(CENTAUR_PWM_CHANNEL,(500-duty_cycle));
			}
			
			count = elapsedtime++ / thresholdCount;

			#if (ENABLE_PID_LOG == 1)	
			if(count  < STORED_DATA_SIZE)
			{
			setPointArray1[count] = desired_countrate;
 			pv_printdata[count]= last_filtered_countrate[1];
 			duty_printdata[count]=duty_cycle;
			errLoop_printdata[count] = errPrint.errEachLoop ;
			errAccumated_printdata[count] =errPrint.errAccumalated;
			}
			#endif	// #if (ENABLE_PID_LOG == 1)
		}
	}
	__asm__("cpsie i");	
}
#endif	// #ifdef PF_USE_INTMATH_CAL


#ifdef PF_USE_FIXEDMATH_CAL
void allPIDTermUsed(void)
{
	int32_t loop, delCount; 
	int32_t lastNthcount = 0;
	int64_t finalCountRate = 0;
	PFbyte printBuff[64]={0}, printCount;
	int64_t countRate=0,duty_cycle=0,filteredCountRateNum = 0, filteredCountRateDenom = 1;

	pfTimer1GetIntStatus(&intStatus);
	if(intStatus & 0x01)		// match reg 0
	{
		pfTimer1ClearIntStatus(intStatus);
		
		if(channelInitFlag == enBooleanTrue)
		{
			encoderCount = pfQeiGetPosition(); 		
			
		// 		x(t) = cr = delcount * fps  
			delCount = encoderCount - prev_encoderCount;
	
		// 		countRate = delCount * FPS;
			pfFixedSmul32(&countRate, INT_FIXED_CONVERSION(delCount), INT_FIXED_CONVERSION(FPS));
	
		
			pfFixedSmul32(&filteredCountRateNum ,((1 << BIT_PRECISION) - gKpv), last_filtered_countrate);
			pfFixedSmul32(&filteredCountRateDenom, gKpv , countRate);
		
			finalCountRate = filteredCountRateNum + filteredCountRateDenom;
		
			last_filtered_countrate = finalCountRate;
			prev_encoderCount = encoderCount;
			
			duty_cycle = pfPidUpdate(pidId[0], (finalCountRate), &errPrint);
		
			if(FIXED_INT_CONVERSION(duty_cycle) < 0)
			{
				pfGpioPinsClear(A3941_PHASE_PORT, A3941_PHASE_PIN);
				pfPwmUpdateMatchRegister(CENTAUR_PWM_CHANNEL, FIXED_INT_CONVERSION((500-(-duty_cycle))));
			}	
			else
			{
				pfGpioPinsSet(A3941_PHASE_PORT,A3941_PHASE_PIN);
				pfPwmUpdateMatchRegister(CENTAUR_PWM_CHANNEL, FIXED_INT_CONVERSION(500 - duty_cycle));
			
			}	
		
			count = elapsedtime++ / thresholdCount;
			if(count  < 1000)
			{
			setPointArray1[count] = (setPoint[0]);
 			pv_printdata[count]= (finalCountRate);
 			duty_printdata[count]=(duty_cycle);
			errLoop_printdata[count] = (errPrint.errEachLoop) ;
			errAccumated_printdata[count] =(errPrint.errAccumalated);
			}
		}
	}
}
#endif

//
// CAN RX Callack
//	
void canReceiveIsr(void)
{
	static PFbyte initMsgArrayIndex = 0;
	static PFbyte cmdMsgArrayIndex = 0;

	PFCanMessage msg;
	
	if(pfCan1Read(&msg) == enStatusSuccess)
	{	
		if(centaurState == enCentaurStateIdle)
		{
			if(!isInitialized)
			{
				pfMemCopy(((PFbyte*)&initMsg + (8*initMsgArrayIndex)), msg.data, 8);
				initMsgArrayIndex++;
				
				if(initMsgArrayIndex == 11)
				{
					centaurState = enCentaurStateRunning;
					initMsgArrayIndex = 0;
				}
			}
			else 
			{
				if((msg.data[0] & msg.data[1] & msg.data[2] & msg.data[3] & msg.data[4] & msg.data[5] & msg.data[6] & msg.data[7])\
					== CMD_CENTAUR_BOARD_RESET)
				{
					cmdMsgArrayIndex =0 ;
					centaurState = enCentaurStateReset;
					return;
				}
				pfMemCopy(((PFbyte*)&cmdMsg + (8*cmdMsgArrayIndex)), msg.data, 8);
				cmdMsgArrayIndex++;
				
				if(cmdMsgArrayIndex == 2)
				{
					centaurState = enCentaurStateRunning;
					cmdMsgArrayIndex = 0;
				}
			}	// if(!isInitialized)
		}	// if(centaurState == enCentaurStateIdle )
	}	// if(pfCan1Read(&msg) == enStatusSuccess)
}	// void canReceiveIsr(void)			

PFEnBoolean isChecksumValid(PFbyte *data, PFbyte checksum, PFdword datalength)
{
	PFdword loop;
	PFbyte calCheckSum = 0;	
	for(loop = 0; loop < datalength ;loop++)
	{
		calCheckSum ^= data[loop];
	}
	if(calCheckSum == checksum )
		return enBooleanTrue;
	else
		return enBooleanFalse;
}

PFEnStatus createCheckSum(PFbyte *data, PFbyte* checksum, PFdword datalength)
{
	PFdword loop;		
	for(loop = 0; loop <datalength ;loop++)
	{
		*checksum ^= data[loop];
	}	
	return enStatusSuccess;	
}

PFEnStatus writeResponse(PFbyte cmd, PFbyte *data, PFdword data_length)
{
	volatile PFCanMsgHeader msgHeader = {CENTAUR_CAN_MSG_ID, enCanFrameStandard, enBooleanFalse };
	PFCentaurCmdMsg cmdMsg;
	
	pfMemSet(&cmdMsg, 0 , sizeof(PFCentaurCmdMsg));
	cmdMsg.header.command = cmd;

	pfMemCopy(cmdMsg.data, data, data_length); 
	createCheckSum(data, &cmdMsg.footer.checksum, sizeof(cmdMsg.data));
	pfCan1Write((PFpCanMsgHeader)&msgHeader,(PFbyte*)&cmdMsg, sizeof(PFCentaurCmdMsg));
	return enStatusSuccess;
}

PFEnStatus writeCentaurInitMessage(PFbyte cmd, PFbyte *data)
{
	volatile PFCanMsgHeader msgHeader = {CENTAUR_CAN_MSG_ID, enCanFrameStandard, enBooleanFalse };
	PFCentaurInitMsg message;
	
	message.header.command = cmd;
	
	pfMemCopy(message.data, data, sizeof(message.data)); 
	createCheckSum(data, &message.footer.checksum, sizeof(message.data));
	
	return pfCan1Write((PFpCanMsgHeader)&msgHeader,(PFbyte*)&message, sizeof(PFCentaurInitMsg));
}

PFEnStatus writeCentaurCmdMessage(PFbyte cmd, PFbyte *data)
{
	volatile PFCanMsgHeader msgHeader = {CENTAUR_CAN_MSG_ID, enCanFrameStandard, enBooleanFalse };
	PFCentaurCmdMsg message;
	message.header.command = cmd;

	pfMemCopy(message.data, data, sizeof(message.data)); 
	createCheckSum(data, &message.footer.checksum, sizeof(message.data));
	
	return pfCan1Write((PFpCanMsgHeader)&msgHeader,(PFbyte*)&message, sizeof(PFCentaurCmdMsg));
}

PFEnStatus writeCentaurResetMessage(void)
{
	volatile PFCanMsgHeader msgHeader = {CENTAUR_CAN_MSG_ID, enCanFrameStandard, enBooleanFalse};
	PFCentaurCmdMsg message;

	pfMemSet(&message, 0xff, 8);
	
	createCheckSum(message.data, &message.footer.checksum, 8);
	return pfCan1Write((PFpCanMsgHeader)&msgHeader, (PFbyte*)&message, 8);
}


