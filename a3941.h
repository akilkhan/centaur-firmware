#pragma once

#include "prime_gpio.h"

/**  
 * Automatic Full Bridge MOSFET Driver (A3941) Pin Config 		
 */
 
#define 	A3941_PHASE_PORT		GPIO_PORT_2
#define 	A3941_PHASE_PIN			GPIO_PIN_0

#define		A3941_RESET_PORT		GPIO_PORT_2
#define		A3941_RESET_PIN			GPIO_PIN_5

#define		A3941_PWMH_PORT			GPIO_PORT_2
#define		A3941_PWMH_PIN			GPIO_PIN_4

#define		A3941_PWML_PORT			GPIO_PORT_2
#define		A3941_PWML_PIN			GPIO_PIN_3

#define		A3941_SR_PORT				GPIO_PORT_2
#define		A3941_SR_PIN				GPIO_PIN_1

#define		A3941_FF1_PORT			GPIO_PORT_2
#define		A3941_FF1_PIN				GPIO_PIN_6

#define		A3941_FF2_PORT			GPIO_PORT_2
#define		A3941_FF2_PIN				GPIO_PIN_7

#define 	A3941_LED_PORT			GPIO_PORT_1
#define		A3941_LED_PIN				GPIO_PIN_0

#define 	CENTAUR_PWM_CHANNEL			5


