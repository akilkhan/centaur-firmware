

#pragma once
#define OVERFLOW_VAL_32B 		0x80000000

#define BIT_PRECISION 					 16

#if (BIT_PRECISION == 16)
#define BIT_MASK 					0x0000ffffffffffff
#elif (BIT_PRECISION == 12)
#define BIT_MASK 					0x000fffffffffffff
#elif (BIT_PRECISION == 8)
#define BIT_MASK 					0x00ffffffffffffff
#elif (BIT_PRECISION == 4)
#define BIT_MASK 					0x0fffffffffffffff
#endif

#define INT_FIXED_CONVERSION(x)  	( (int64_t)x << (BIT_PRECISION )	)
#define FIXED_INT_CONVERSION(x)  	((int64_t)x >> (BIT_PRECISION )	)

/** Fixed Math structure */
typedef struct 
{
	uint32_t intPart;  /**< Integer part of the number */
	uint32_t fractPart; /**< Fraction val in*/
	uint32_t divFact;
}pfFixedDataType;

PFEnStatus pfPrecisionInit(PFbyte x);
PFEnStatus pfDecimal2FixedNum(int64_t *res, pfFixedDataType* num );
PFEnStatus pfFixedNum2Decimal(float *res, int64_t num );
PFEnStatus pfFixedUmul32(int64_t *res, int64_t x, int64_t y);
PFEnStatus pfFixedSmul32(int64_t *res, int64_t x, int64_t y);
PFEnStatus pfFixedUdiv32(int64_t *res, int64_t x, int64_t y);
PFEnStatus pfFixedSdiv32(int64_t *res, int64_t x, int64_t y);
