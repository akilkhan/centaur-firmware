//#include "fixedMath.h"
//Change these as per the data types of the framework ...

#define FPS  						200
#define MAX_ENCODER_COUNT_RATE  	13000
//#define MAX_ENCODER_COUNT_RATE  	10000
#define MIN_ENCODER_COUNT_RATE  	0
#define MAX_PWM_COUNT  				500 
#define MIN_PWM_COUNT  				45 
#define MAX_ENCODER_COUNT			6400
#define MAX_PWM_FREQ				10000
#define FACTOR_SEC 					100

