#include "prime_framework.h"
#include "prime_centaurBoardConfig.h"
#include "prime_gpio.h"
#include "a3941.h"
#include "prime_sysClk.h"
#include "prime_delay.h"
#include "prime_uart0.h"
#include "prime_timer1.h"
#include "prime_pwm.h"
#include "prime_pidHwconfig.h"
#include "prime_pid.h"
#include "prime_qei.h"

#include "centaur.h"

#if(PF_USE_FIXEDMATH_CAL == 1)
	#include "fixedMath.h"
#endif

extern PFCfgGpio driverGpioPins; 

#if (ENABLE_CENTAUR_PROFILING == 1)
typedef struct 
{
	PFsdword totalEncoderCount;
	PFsdword totalTimeStepCount;
	PFdword  currentTimeStep;
	PFsdword lastSetPoint;
	PFsdword rampingSlope;
	PFsdword rampingSlopeNum;
	PFsdword rampingSlopeDenom;
	PFsdword nRampLower;
	PFsdword nConstLower;
	PFsdword nRampUpper;
	PFsdword nConstUpper;
}genericRampProfile_t;

genericRampProfile_t profileValue[CENTAUR_MAX_SUPPORTED];
#endif	// #if (ENABLE_CENTAUR_PROFILING == 1)

//Variable Initialization
PFEnBoolean channelInitFlag = enBooleanFalse;
PFbyte idCentaurPid = 0;
PFdword maxMotorSpeed = 0;
PFdword encoderCpr = 0;
PFdword motorGearRatio = 0;
PFbyte qeiMode = 0;

#define LED			0
#define RESET 	5
#define PWMH		4
#define PWML		3
#define SR			1
#define PHASE		0
#define FF1			6
#define FF2			7

PFEnStatus pfCentaurHwInit(void)
{
	boardInit();
	//NVIC_SetPriority(CAN_IRQn,0);
	NVIC_SetPriority(TIMER1_IRQn,0);
	return enStatusSuccess;
}

PFEnStatus pfCentaurOpen(PFpCfgCentaur config)
{
	PFEnStatus status;
	
	status = pfGpioInit(&driverGpioPins,8);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	
	/* Settings for A3941 MOSFET Driver */
	pfDelayMilliSec(1000);
	PERIPH_GPIO2->FIOPIN &= ~(1<<RESET);	//Enable outputs
	pfDelayMilliSec(1000);  //Intial delay for Power Board Startup
	 
	if(config->useEncoder == enBooleanTrue)
	{
		maxMotorSpeed = config->motorMaxSpeed;
		encoderCpr = config->encoderCpr;
		motorGearRatio = config->motorGearRatio;
				
		if((config->usePid == enBooleanTrue))
		{
				#if( PF_USE_FIXEDMATH_CAL == 0 )						
					status = pfPidOpen(&idCentaurPid,(const PFCfgPid*)&config->pidCfgData, 1);
					if(status != enStatusSuccess)
						return status;
				#else
					pfDecimal2FixedNum(&gKpv,&gKpvStruct );
					status = pfPidOpen(&idCentaurPid, (const PFCfgPid*)&config->pidCfgData, 1);
					if(status != enStatusSuccess)
						return status;				
				#endif
		}	
	}
	
	channelInitFlag = enBooleanTrue;
	return enStatusSuccess;
}

PFEnStatus pfCentaurSetSpeed(PFsdword speed)
{
	PFEnStatus status;
	PFsdword desiredCountRate = 0;
	
	// for setting speed in RPM
	//desiredCountRate = (PFsdword)( speed * encoderCpr[channel]*motorGearRatio[channel]*(qeiMode[channel]+1))/240;	
	
	// for setting speed in count rate
	desiredCountRate = (PFsdword)speed;	
	
	if(channelInitFlag == enBooleanTrue)
	{
		status = pfPidChangeSetPoint(idCentaurPid, &desiredCountRate);
		return status;		
	}
	else
		return enStatusNotConfigured ;
}

#if (FUTURE_EXPANSION == 1)
PFEnStatus pfCentaurSetDir(PFbyte dir)
{
	PFEnStatus status;
	if(channelInitFlag == enBooleanTrue)
	{
		if(dir == 0)
		{
			pfGpioPinsClear(A3941_PHASE_PORT, A3941_PHASE_PIN);
		}
		else
		{
			pfGpioPinsSet(A3941_PHASE_PORT, A3941_PHASE_PIN);
		}
	}
	return enStatusSuccess;
}
#endif  // #if (FUTURE_EXPANSION == 1)

PFEnStatus pfCentaurGetEncoderCount(PFdword* count)
{
	if(channelInitFlag == enBooleanTrue)
	{
		*count = pfQeiGetPosition();
	}
	return enStatusSuccess;
}

PFEnStatus pfCentaurClose()
{
	if(channelInitFlag == enBooleanTrue)
	{
		pfGpioPinsClear(A3941_RESET_PORT, A3941_RESET_PIN);
		channelInitFlag = enBooleanFalse;
	}
	return enStatusSuccess;
}

#if (ENABLE_CENTAUR_PROFILING == 1)
int initGenericProfileR(PFbyte channel, genericRampProfile_t* profile)
{
	profileValue->totalEncoderCount = profile->totalEncoderCount ;
	profileValue->totalTimeStepCount = profile->totalTimeStepCount ;
	profileValue->currentTimeStep = profile->currentTimeStep ;
	profileValue->lastSetPoint = profile->lastSetPoint ;
	profileValue->rampingSlope = profile->rampingSlope ;
	
	return 0;
}

int getGenericProfileValue(PFbyte channel, genericRampProfile_t *profile)
{
	int currTimeStepVal = 0, deltaSetPoint = 0, n1 = 0, n2=0, n3 = 0, n4 = 0;
	PFsdword slope = 0;
	 
	slope = profile->rampingSlope ;
	currTimeStepVal = profile->currentTimeStep; 
	n1 = profile->nRampLower;
	n2 = n1 + profile->nConstLower;
	n3 = n2 + profile->nRampUpper;
	n4 = n3 + profile->nConstUpper;
	
	if(profile->totalEncoderCount < 0)
	{
		n1 = -n1;
		n2 = -n2;
		n3 = -n3;
		n4 = -n4;
	}
	
	if(currTimeStepVal < n1)
	{
		deltaSetPoint = slope/FPS;
	}
	if((currTimeStepVal >= n1 ) && (currTimeStepVal < n2))
	{
		deltaSetPoint = 0 ;
	}
	if((currTimeStepVal >= n2) && (currTimeStepVal < n3))
	{
		deltaSetPoint = slope/FPS ;
	}
	if((currTimeStepVal >= n3) && (currTimeStepVal < n4))
	{
		deltaSetPoint = 0 ;
	}
	if((currTimeStepVal >= n4) && (currTimeStepVal < profile->totalTimeStepCount))
	{
		deltaSetPoint = -(slope/FPS) ;
	}
	
	if(profile->totalEncoderCount < 0)
	deltaSetPoint = -deltaSetPoint;
	return deltaSetPoint;
}

void genericRampingCalc2(int count,genericRampProfile_t* rampingData)
{
	float temp, err1 =0 , err2 =0, e=0, zCount =0, extraCountZ =0 ;
	float y0;
	PFsdword xFactor = 0, zFactor = 0, yFactor = 0, constLowerCount = 0, constUpperCount = 0;
	volatile PFsdword extraAreaZ = 0, extraCountX = 0, rampUpperError = 0,AreaZ =0;
	PFbyte printBuff[64]={0}, printCount;
	PFsdword rampingSlopeNum = 0, rampingSlopeDenom = 1000, rampingSlope = 0;
	PFsdword maxRampCountNum = 0, maxRampCountDenom = 1, maxRampCount = 0;
	
	rampingData->totalEncoderCount = count;
	rampingSlope = rampingData->rampingSlope;
	maxRampCount = 2 * rampingSlope +  (MAX_ENCODER_COUNT_RATE - rampingSlope)*(MAX_ENCODER_COUNT_RATE - rampingSlope)/rampingSlope \
				+ 2 * (MAX_ENCODER_COUNT_RATE - rampingSlope);
	
	if(count < 0 )
	count = -count;

	if(count <=  rampingSlope)
	{
		temp = sqrt((double)((double)count/(double)rampingSlope));
		rampingData->nRampLower = temp*FPS;
		rampingData->nConstLower = 0 ;
		rampingData->nRampUpper =  0 ;
			
	}
	else if((count > rampingSlope) && (count <= (2 * rampingSlope)))
	{
		rampingData->nRampLower = FPS;
		rampingData->nConstLower = ((count - rampingSlope)*FPS)/rampingSlope;
		rampingData->nRampUpper =  0 ;
		rampingData->nConstUpper = 0 ;
	}
	else if((count > (2 * rampingSlope)) && ((count <= maxRampCount)))
	{
	//	y0 = (float)(((float)sqrt((float)(count -2000)/1000.0+(float)1.0) -(float)1.0);
		
		temp = (float) (count -rampingSlope)/(float)rampingSlope;
		y0 = (float)sqrt((float)temp);
		y0 = (float)(y0 - (float)1.0) ;
		
		if(y0 < 9.0)
		{
			rampingData->nRampUpper =   (PFsdword)(y0 * FPS)+1;
			e = ((double)((double)rampingData->nRampUpper - (y0 * FPS)));
			
		}
		else 
		{
			e = 0;
			rampingData->nRampUpper =   (PFsdword)(y0 * FPS);
		}
		err1 = ((float)e * 2 *rampingSlope)/FPS;
		err2 =  (e *( 2 * ((float)rampingData->nRampUpper) - e) * (float)rampingSlope)/(FPS *FPS);
		
		constLowerCount = ((err1 + err2)*FPS )/rampingSlope ;
		
		rampingData->nRampLower = FPS;
		rampingData->nConstLower =  FPS - constLowerCount ;
		rampingData->nConstUpper = 0 ;
			
	}
	else
	{
		AreaZ= (count - maxRampCount);
		zCount = ((float)AreaZ * FPS)/(MAX_SPEED) ;
		extraCountZ = (PFsdword)(zCount +1) - zCount;
		
		extraAreaZ = (extraCountZ * MAX_SPEED)/FPS;
		
		extraCountX = (extraAreaZ * FPS )/rampingSlope;
		
		rampingData->nRampLower = FPS ;	
		rampingData->nConstLower = FPS - extraCountX ;
		rampingData->nRampUpper =(9*FPS);
		rampingData->nConstUpper = (PFsdword)(zCount +1);

	}
	rampingData->totalTimeStepCount = rampingData->nRampLower 		\
									+ rampingData->nConstLower		\
									+ rampingData->nRampUpper 		\
									+ rampingData->nConstUpper 		\
									+ rampingData->nRampUpper		\
									+ rampingData->nRampLower ;
	
	if( rampingData->totalEncoderCount < 0 )
	{
		rampingData->nRampLower = -rampingData->nRampLower;
		rampingData->nConstLower = -rampingData->nConstLower;
		rampingData->nRampUpper = -rampingData->nRampUpper;
		rampingData->nConstUpper = -rampingData->nConstUpper;
		rampingData->totalTimeStepCount = -rampingData->totalTimeStepCount;
	}
	return;
}
#endif	// #if (ENABLE_CENTAUR_PROFILING == 1)

