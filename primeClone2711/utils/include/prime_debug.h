#pragma once

#define PRIME_DEBUG			1

#if (PRIME_DEBUG == 1)

// Native hardware debug macros
#define PF_ADC_DEBUG   1				

#define PF_EINT_DEBUG	1
#if (PF_EINT_DEBUG == 1)
	#define PF_EINT0_DEBUG   1			
	#define PF_EINT1_DEBUG   1			
	#define PF_EINT2_DEBUG   1			
	#define PF_EINT3_DEBUG   1			
#endif	// #if (PF_EINT_DEBUG == 1)

#define PF_UART_DEBUG	1
#if (PF_UART_DEBUG == 1)
	#define PF_UART0_DEBUG   1				
	#define PF_UART1_DEBUG   1				
	#define PF_UART2_DEBUG   1				
	#define PF_UART3_DEBUG   1				
#endif	// #if (PF_UART_DEBUG == 1)	
	
#define PF_SPI_DEBUG	1
#if (PF_SPI_DEBUG == 1)
	#define PF_SPI0_DEBUG   1				
	#define PF_SPI1_DEBUG   1				
#endif	// #if (PF_SPI_DEBUG == 1)

#define PF_I2C_DEBUG	1
#if (PF_I2C_DEBUG == 1)
	#define PF_I2C0_DEBUG   1		
	#define PF_I2C1_DEBUG   1				
	#define PF_I2C2_DEBUG   1				
#endif	// #if (PF_I2C_DEBUG == 1)

#define PF_CAN_DEBUG   1	
#if (PF_CAN_DEBUG == 1)
	#define PF_CAN1_DEBUG   1				
	#define PF_CAN2_DEBUG   1				
#endif	// #if (PF_USE_CAN == 1)	

#define PF_TIMER_DEBUG	1
#if (PF_TIMER_DEBUG == 1)
	#define PF_TIMER0_DEBUG   1				
	#define PF_TIMER1_DEBUG   1				
	#define PF_TIMER2_DEBUG   1
	#define PF_TIMER3_DEBUG   1				
	#define PF_TIMER4_DEBUG   1				
#endif	// #if (PF_TIEMR_DEBUG == 1)

#define PF_PWM_DEBUG   		1				
#define PF_WDT_DEBUG		1
#define PF_RIT_DEBUG   		1
#define PF_RTC_DEBUG   		1
#define PF_SYSCLK_DEBUG   	1				
#define PF_SYSTICK_DEBUG   	1

// Device driver debug macros
#define PF_A3977_DEBUG				1
#define PF_L6470_DEBUG				1
#define PF_CHEETAH_DEBUG			1
#define PF_LS7366_DEBUG				1
#define PF_GP2Y0A02_DEBUG			1
#define PF_GP2Y0A21_DEBUG			1
#define PF_GP2Y0A710_DEBUG			1
#define PF_GPS_DEBUG				1
#define PF_ULTRASONIC_DEBUG			1
#define PF_VCNL4000_DEBUG			1
#define PF_MPU6050_DEBUG			1
#define PF_LSM303_DEBUG				1
#define PF_BLUETOOTH_DEBUG			1
#define PF_TCS230_DEBUG				1


// Other debug macros
#define	PF_UTILS_DEBUG				1

/** defines whether to use assert functions (boolean). */
#define	PF_DEBUG_ASSERT				0

/** defines whether to print debug information (boolean). */
#define PF_DEBUG_PRINT          0

#if(PF_DEBUG_PRINT == 1)
/** defines function to call printf like function for debugging purpose */
#define PF_PRINTF(...)      pfPrintf(__VA_ARGS__) 
#else
/** defines function which will not print any debug information */
#define PF_PRINTF(...)      do{}while(0)
#endif

#if(PF_DEBUG_ASSERT == 1)
/** function to check condition.If condition holds true then the program execution will continue otherwise it will be terminated with return value 
and an error message with filename and function name will be printed */    
#define PF_ASSERT_RET(_e_, _r_) \
    if(!(_e_)) \
    { \
        PF_PRINTF("ASSERTION FAILED at %s:%s!", __FILE__, __FUNCTION__); \
        return (_r_); \
    }
/** function to check condition.If condition holds true then the program execution will continue otherwise it will be stopped 
and an error message with filename and function name will be printed */    
#define PF_ASSERT(_e_) \
    if(!(_e_)) \
    { \
        PF_PRINTF("ASSERTION FAILED at %s:%s!", __FILE__, __FUNCTION__); \
        while(1); \
    }
/** function to check whether pointer is valid or not.If pointer is valid then the program execution will continue otherwise it will be stopped 
and an error message with filename and function name will be printed */    
#define PF_ASSERT_PTR(_p_) \
    if((_p_) == PF_NULL_PTR) \
    { \
        PF_PRINTF("PTR ASSERTION FAILED at %s:%s!", __FILE__, __FUNCTION__); \
        while(1); \
    }
#else 
/** defines function which will not check any condition.  */   
#define PF_ASSERT_RET(_e_, _r_)     do{}while(0)
/** defines function which will not check any condition.  */   
#define PF_ASSERT(_e_)              do{}while(0)
/** defines function which will not check pointer  */       
#define PF_ASSERT_PTR(_p_)          do{}while(0)
#endif	// #if (PF_DEBUG_ASSERT == 1)

/** This function writes formatted output from the format string FORMAT. */
#if(PF_DEBUG_PRINT == 1)
PFdword pfPrintf(char * format, ...);
#endif	// #if(PF_DEBUG_PRINT == 1)
	
#endif	// #if (PRIME_DEBUG == 1)

