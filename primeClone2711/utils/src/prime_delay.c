#include "prime_framework.h"
#include "prime_delay.h"



PFdword delayTick = 0;
static PFdword timerPeriodUs = 10;

void pfDelaySetTimerPeriod(PFdword timerPeriod)
{
	timerPeriodUs = timerPeriod;
}

void pfDelayTickUpdate(void)
{
	++delayTick;
}

PFdword pfDelayGetTimerPeriod(void)
{
	return timerPeriodUs;
}

PFdword pfDelayGetTick(void)
{
	return delayTick;
}

void pfDelayTickReset(void)
{
	delayTick = 0;
}

PFdword pfDelaySetTimeout(PFdword time)
{
	if(time < timerPeriodUs)
	{
		time = timerPeriodUs;
	}
	else
		time /= timerPeriodUs;

	return(delayTick + time);
}

PFEnBoolean pfDelayCheckTimeout(PFdword timeoutTick)
{
	if(timeoutTick > delayTick)
		return enBooleanFalse;
	else
		return enBooleanTrue;
}

void pfDelayMicroSec(PFdword delayUs)
{
	PFdword delayTimeout,i=0,j=0;

	if(TIMER_CONFIGURED && (delayUs>timerPeriodUs))
	{
		delayTimeout = pfDelaySetTimeout(delayUs);
		while(pfDelayCheckTimeout(delayTimeout) == enBooleanFalse);
	}
	else
	{//approx delay of 1us
		for(i=0; i<delayUs; ++i)
		{
			for(j=0; j<22; ++j);
		}
	}
}

void pfDelayMilliSec(PFdword delayMs)
{
	PFdword delayTimeout=delayMs*1000;

	if(TIMER_CONFIGURED)
	{
		delayTimeout = pfDelaySetTimeout(delayTimeout);
		while(pfDelayCheckTimeout(delayTimeout) == enBooleanFalse);
	}
	else
	{
		pfDelayMicroSec(delayTimeout);
	}
}

