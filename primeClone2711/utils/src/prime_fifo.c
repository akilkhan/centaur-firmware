#include "prime_types.h"
#include "prime_fifo.h"

PFEnStatus pfFifoInit(PFpFifo pFifo, PFbyte* pBuf, PFdword uSize)
{
#if (PF_UTILS_DEBUG == 1)
	if((pFifo == 0) || (pBuf == 0))
	{
		return enStatusInvArgs;
	}
#endif	//	#if (PF_UTILS_DEBUG == 1)
	pFifo->pHead = pBuf;
	pFifo->pTail = pBuf;
	pFifo->pBegin = pBuf;
	pFifo->pEnd = pBuf + uSize - 1;
	pFifo->size = uSize;
	pFifo->count = 0;
	
	if(pFifo->pBegin < pFifo->pEnd)
	{
		return enStatusError;
	}
	return enStatusSuccess;
}

PFdword pfFifoLength(PFpFifo pFifo)
{
	return pFifo->size;
}

PFEnBoolean pfFifoPush(PFpFifo pFifo, PFbyte c)
{
	if(pFifo->count < pFifo->size)
	{
		*(pFifo->pTail) = c;
		pFifo->count++;
		if (pFifo->pTail == pFifo->pEnd)
		{
			pFifo->pTail = pFifo->pBegin;
		}
		else
		{
			pFifo->pTail++;
		}
	}
	else
	{
	#if(PF_FIFO_OVERWRITE_ENABLE == 0)
		return enBooleanFalse;
	#else
		*(pFifo->pTail) = c;
// 		pFifo->count++;
		if(pFifo->pTail == pFifo->pHead)
		{
			if (pFifo->pTail == pFifo->pEnd)
			{
				pFifo->pTail = pFifo->pBegin;
				pFifo->pHead = pFifo->pBegin;
			}
			else
			{
				pFifo->pTail++;
				pFifo->pHead++;
			}
		}
	#endif
	}
	
	return enBooleanTrue;
}

PFbyte pfFifoPop(PFpFifo pFifo)
{
	if(pFifo->count == 0)
	{
		return 0xFF;
	}
	
	pFifo->count--;
	
	if (pFifo->pHead == pFifo->pEnd)
	{
		pFifo->pHead = pFifo->pBegin;
		return *(pFifo->pEnd);
	}
	
	return *(pFifo->pHead++);
	
}

void pfFifoFlush(PFFifo *pFifo)
{
	pFifo->pHead = pFifo->pTail;
	pFifo->count = 0;
}

PFEnBoolean pfFifoIsEmpty(const PFpFifo pFifo)
{
	return ( (pFifo->count > 0) ? enBooleanFalse : enBooleanTrue); 
}

PFEnBoolean pfFifoIsFull(const PFpFifo pFifo)
{
	return ( (pFifo->count < pFifo->size) ? enBooleanFalse : enBooleanTrue);
}

PFdword pfFifoGetCount(const PFpFifo pFifo)
{
	PFsdword bCount = 0;
	
	bCount = pFifo->pHead - pFifo->pTail;
	if(bCount < 0)
	{
		bCount = pFifo->size + bCount;
	}
	return (PFdword)bCount;
}

