#include "prime_framework.h"
#include "prime_utils.h"

PFbyte* errorStrings[] = 
{
	"Success",
	"Error",
	"InvArgs",			
	"InvState",		
	"NotSupported",	
    "NotConfigured",	
	"Busy",			
	"NoMem",			
	"Timeout",		
	"ConfigConflict",	
	"Aborted",		
	"NotExist",		
	"Unknown",		
	"DiskError",			
	"InternalError",		
	"DiskNotReady",		
	"NoFile",				
	"NoPath",				
	"InvalidName",		
	"AccessDenied",		
	"FileAlreadyExist",	
	"InvalidObject",		
	"WriteProtected",		
	"InvalidDrive",		
	"NotEnabled",			
	"NoFilesystem",		
	"MkfsAborted",		
	"NetNoRouteFound"
};

PFEnEndian pfGetSystemEndian(void)
{
	PFword testWord = 0x1234;
	PFbyte* pByte;
	
	pByte = (PFbyte*)(&testWord);
	
	if(*pByte == 0x12)
	{
		return enEndianBig;
	}
	else
	{
		return enEndianLittle;
	}
}

PFbyte* pfGetErrorString(PFEnStatus errCode)
{
	if( errCode >= (sizeof(errorStrings)/sizeof(errorStrings[0])) )
	{
		return (PFbyte*)0;
	}
	
	return errorStrings[errCode];
}

