#Configurations

#Compiler
ifeq ($(CORE_FAMILY),arm)
TOOLCHAIN = "C:/Program Files/GNU Tools ARM Embedded/4.8 2014q2/bin/arm-none-eabi-"
else ifeq ($(CORE_FAMILY),avr)
TOOLCHAIN = C:/WinAVR-20100110/bin/avr-
endif

CC = $(TOOLCHAIN)gcc
AR = $(TOOLCHAIN)ar

#Project
PACKAGE_NAME = libprimeframework

#Compiler flags
INC_FLAG = -I
OBJ_FLAG = -c
DEFINE_FLAG = -D
