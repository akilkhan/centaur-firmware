
if [[ $1 == "--help" ]]; then
	echo Prime Framework v1.0
	echo Framework Build Shell Script
	echo Command Format: prime.sh Platform Build_Mode Build_Components
	echo Choose parameters from following options
	echo Platform: lpc1768, atmega2560, at90can128
	echo Build Mode: Debug, Release
	echo Build Components: Native, All
	exit 0
fi	

#Framework Version
export PF_VERSION=1.0
export PF_BUILD_CMD=$1
export MCU_CHIP=$2
export PF_BUILD_MODE=$3
export PF_BUILD_COMPONENT=$4
export PF_ROOT_DIR=$(pwd)


#Hardware platform
if [[ $MCU_CHIP == "lpc1768" ]]; then
	export CORE_FAMILY="arm"
	export CORE="cortex-m3"
	export MCU_FAMILY="lpc17xx"
else 
	if [[ $MCU_CHIP == "atmega2560" ]]; then
		export CORE_FAMILY="avr"
		export CORE="avr8"
	else 
		if [[ $MCU_CHIP == "at90can128" ]]; then
			export CORE_FAMILY="avr"
			export CORE="avr8"
		else
			echo Invalid input parameters
			exit 1
		fi
	fi
fi

echo "************************************"
echo Prime Framework Build Details
echo "************************************"
echo Version:	 		$PF_VERSION
echo Build:		 		$PF_BUILD_MODE
echo Build Components:	$PF_BUILD_COMPONENT
echo Core Family:		$CORE_FAMILY
echo Core:				$CORE
echo Device:			$MCU_CHIP
echo -------------------------------------
 
 
make -f $PF_ROOT_DIR/build/makefile $PF_BUILD_CMD &>logs/cmd-$MCU_CHIP-$PF_BUILD_MODE-$PF_BUILD_COMPONENT.txt 2>logs/err-$MCU_CHIP-$PF_BUILD_MODE-$PF_BUILD_COMPONENT.txt

if [[ $PF_BUILD_CMD == "Clean" ]]; then
	if [ $? == 0 ]
	then
		echo Prime Framework Clean Done!
		rm -f logs/*txt
	else
		echo Prime Framework Clean Failed!
	fi	
else
	if [ $? == 0 ]
	then
		echo Prime Framework Build Done!
	else
		echo Prime Framework Build Failed!
	fi	
fi
echo -------------------------------------
echo   
echo   