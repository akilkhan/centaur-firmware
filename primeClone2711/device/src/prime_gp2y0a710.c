#include "prime_framework.h"
#include "prime_adc.h"
#include "prime_gp2y0a710.h"
#include <math.h>

static volatile PFbyte gp2y0a710_InitFlag[MAX_GP2Y0A710_SENSORS_SUPPORTED]={enBooleanFalse};        /** to check for initialization of sensor */
static volatile PFbyte gp2y0a710_Count=0;
static volatile PFbyte gp2y0a710_adcChannel[MAX_GP2Y0A710_SENSORS_SUPPORTED]={0};

PFEnStatus pfGP2Y0A710Open(PFbyte* devId,PFbyte* adcChannel,PFbyte deviceCount)
{
	int i,j;
	
	#if (PF_GP2Y0A710_DEBUG == 1)
	if(devId == NULL)
		return enStatusInvArgs;
	if(adcChannel == NULL)
		return enStatusInvArgs;
	if(deviceCount == 0)
		return enStatusInvArgs;
	
	#endif	// #if (PF_GP2Y0A710_DEBUG == 1)
	
	for(i=0;i<deviceCount;i++)
	{
			for(j=0;j<MAX_GP2Y0A710_SENSORS_SUPPORTED;j++)
			{
				if(gp2y0a710_InitFlag[j]==enBooleanFalse)
				{
					gp2y0a710_adcChannel[j] = adcChannel[j];
					gp2y0a710_Count++;
					devId[i]=j;
					gp2y0a710_InitFlag[j] = enBooleanTrue;
					break;
				}
			}
			if(j == MAX_GP2Y0A710_SENSORS_SUPPORTED)
					return enStatusError;
	}	
	return enStatusSuccess;
}	
	
PFEnStatus pfGP2Y0A710GetDistance(PFbyte id, PFdword* distance)
{ 
    PFdword vtg;
    PFEnStatus status;
	
	#if (PF_GP2Y0A710_DEBUG == 1)
	if(gp2y0a710_InitFlag[id]==enBooleanFalse)
		return enStatusNotConfigured;
			
	if( id >= MAX_GP2Y0A710_SENSORS_SUPPORTED )
		return enStatusInvArgs;
	#endif //#if (PF_GP2Y0A710_DEBUG == 1)
	

    status = pfAdcGetVoltageSingleConversion(gp2y0a710_adcChannel[id], &vtg);
    if(status!=enStatusSuccess)
        return enStatusError;

    *distance = 1420000/(vtg-1091);

    return enStatusSuccess;
}

PFEnStatus pfGP2Y0A710GetRawValue(PFbyte id, PFword* value)
{
	#if (PF_GP2Y0A710_DEBUG == 1)
	if(gp2y0a710_InitFlag[id]==enBooleanFalse)
		return enStatusNotConfigured;
				
	if( id >= MAX_GP2Y0A710_SENSORS_SUPPORTED )
		return enStatusInvArgs;
  	#endif //#if (PF_GP2Y0A710_DEBUG == 1)
		
	pfAdcSingleConversion(gp2y0a710_adcChannel[id],value);
		
	return enStatusSuccess;	
}

PFEnStatus pfGP2Y0A710Close(PFbyte devId)
{
	#if (PF_GP2Y0A710_DEBUG == 1)
		/** check for initialization */
	if(gp2y0a710_InitFlag[devId]==enBooleanFalse)
		return enStatusNotConfigured;
	if( devId >= MAX_GP2Y0A710_SENSORS_SUPPORTED )
		return enStatusInvArgs;
	#endif	//#if (PF_GP2Y0A710_DEBUG == 1)
		
	gp2y0a710_Count--;
	gp2y0a710_InitFlag[devId] = enBooleanFalse;	
	return enStatusSuccess;
}

