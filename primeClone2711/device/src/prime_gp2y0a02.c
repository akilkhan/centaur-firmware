#include "prime_framework.h"
#include "prime_adc.h"
#include "prime_gp2y0a02.h"
#include <math.h>

static volatile PFbyte gp2y0a02_InitFlag[MAX_GP2Y0A02_SENSORS_SUPPORTED]={enBooleanFalse};        
static volatile PFbyte gp2y0a02_Count=0;
static volatile PFbyte gp2y0a02_adcChannel[MAX_GP2Y0A02_SENSORS_SUPPORTED]={0};

PFEnStatus pfGP2Y0A02Open(PFbyte* devId,PFbyte* adcChannel,PFbyte deviceCount)
{
		int i,j;
	
	#if (PF_GP2Y0A02_DEBUG == 1)
	if(devId == NULL)
		return enStatusInvArgs;
	if(adcChannel == NULL)
		return enStatusInvArgs;
	if(deviceCount == 0)
		return enStatusInvArgs;
	
	#endif	// #if (PF_GP2Y0A02_DEBUG == 1)
	
	for(i=0;i<deviceCount;i++)
	{
			for(j=0;j<MAX_GP2Y0A02_SENSORS_SUPPORTED;j++)
			{
				if(gp2y0a02_InitFlag[j]==enBooleanFalse)
				{
					gp2y0a02_adcChannel[j] = adcChannel[j];
					gp2y0a02_Count++;
					devId[i]=j;
					gp2y0a02_InitFlag[j] = enBooleanTrue;
					break;
				}
			}
			if(j == MAX_GP2Y0A02_SENSORS_SUPPORTED)
					return enStatusError;
	}	
	return enStatusSuccess;
}

PFEnStatus pfGP2Y0A02GetDistance(PFbyte id, PFdword* distance)
{
     PFdword vtg = 0;
     PFEnStatus status;

	#if (PF_GP2Y0A02_DEBUG == 1)
	if(gp2y0a02_InitFlag[id]==enBooleanFalse)
		return enStatusNotConfigured;
				
	if( id >= MAX_GP2Y0A02_SENSORS_SUPPORTED )
		return enStatusInvArgs;
	#endif //#if (PF_GP2Y0A02_DEBUG == 1)
	
	
    status = pfAdcGetVoltageSingleConversion(gp2y0a02_adcChannel[id], &vtg);
    if(status!=enStatusSuccess)
    return enStatusError;

    *distance = (PFword)( 600.495 * (pow(((float)vtg)/1000,-1.1904)) );

    return enStatusSuccess;
}

PFEnStatus pfGP2Y0A02GetRawValue(PFbyte id, PFword* value)
{
	#if (PF_GP2Y0A02_DEBUG == 1)
	if(gp2y0a02_InitFlag[id]==enBooleanFalse)
		return enStatusNotConfigured;
				
	if( id >= MAX_GP2Y0A02_SENSORS_SUPPORTED )
		return enStatusInvArgs;
  	#endif //#if (PF_GP2Y0A02_DEBUG == 1)
		
	pfAdcSingleConversion(gp2y0a02_adcChannel[id],value);
		
	return enStatusSuccess;	
}

PFEnStatus pfGP2Y0A02Close(PFbyte devId)
{
	#if (PF_GP2Y0A02_DEBUG == 1)
		/** check for initialization */
	if(gp2y0a02_InitFlag[devId]==enBooleanFalse)
		return enStatusNotConfigured;
	if( devId >= MAX_GP2Y0A02_SENSORS_SUPPORTED )
		return enStatusInvArgs;
	#endif	//#if (PF_GP2Y0A02_DEBUG == 1)
		
	gp2y0a02_Count--;
	gp2y0a02_InitFlag[devId] = enBooleanFalse;	
	return enStatusSuccess;
}

