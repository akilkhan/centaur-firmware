#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_adxl345.h"

static PFCfgAdxl345 adxl345Config[ADXL345_MAX_DEVICE_SUPPORTED]; // array to store configuration structures of ADXL345 devices connected
static PFEnBoolean adxl345InitFlag[ADXL345_MAX_DEVICE_SUPPORTED] = {enBooleanFalse};
static PFbyte adxl345DeviceCount=0;

static PFEnStatus pfAdxl345WriteReg(PFbyte* devId, PFbyte regAddr, PFbyte writeValue);
static PFEnStatus pfAdxl345ReadReg(PFbyte* devId, PFbyte regAddr, PFbyte* readValue);

PFEnStatus pfAdxl345Open(PFbyte* devId, PFpCfgAdxl345 sensorConfig,PFbyte deviceCount )
{
	PFbyte regVal=0, i=0, j=0;
	PFEnStatus ret;
	
	for(i=0; i<deviceCount; ++i)
	{
		for(j=0; j<ADXL345_MAX_DEVICE_SUPPORTED; ++j)
		{
			if( adxl345InitFlag[j] == enBooleanFalse )
			{
				devId[i]=j;
				
				pfMemCopy(&adxl345Config[j], &sensorConfig[i], sizeof(PFCfgAdxl345));
				
				if( (sensorConfig[i].act_ac_dc != enBooleanTrue)  && (sensorConfig[i].act_ac_dc != enBooleanFalse) )
						return enStatusInvArgs;
				else
						regVal |= 0x80;		//BIT_7

				if( (sensorConfig[i].inact_ac_dc != enBooleanTrue)  && (sensorConfig[i].inact_ac_dc!= enBooleanFalse) )
						return enStatusInvArgs;
				else
						regVal |= 0x08;		//BIT_3

				if(sensorConfig[i].actAxisComponent > 7)
						return enStatusInvArgs;
				else
				{
						if(sensorConfig[i].actAxisComponent != enAdxl345ComponentNone)
						regVal |= sensorConfig[i].actAxisComponent << 4;
				}

				if(sensorConfig[i].inactAxisComponent > 7)
						return enStatusInvArgs;
				else
				{
						if(sensorConfig[i].inactAxisComponent != enAdxl345ComponentNone)
						regVal |= sensorConfig[i].inactAxisComponent;
				}

				ret=pfAdxl345WriteReg(&devId[i], ADXL345_REG_ACT_INACT_CTL,regVal);
				if(ret!=enStatusSuccess)
						return ret;

				if(sensorConfig[i].tapAxisComponent > 7)
						return enStatusInvArgs;
				else
				{
						if(sensorConfig[i].tapAxisComponent != enAdxl345ComponentNone)
						regVal = sensorConfig[i].tapAxisComponent | (sensorConfig[i].suppressDoubleTap << 3 );
				}

				ret=pfAdxl345WriteReg(&devId[i], ADXL345_REG_TAP_AXES,regVal);
				if(ret!=enStatusSuccess)
				return ret;

				if(sensorConfig[i].operatingRange > enAdxl345Range_16g)
						return enStatusInvArgs;
				else
				{
						regVal = sensorConfig[i].operatingRange;

						if(ADXL345_ACC_INTERRUPTACTIVELOW)
								regVal |= 1 << 5;

						if(ADXL345_ACC_USEFULLRESOLUTION)
								regVal |= 1 << 3;
				}

				ret=pfAdxl345WriteReg(&devId[i], ADXL345_REG_DATA_FORMAT,regVal);
				if(ret!=enStatusSuccess)
						return ret;

				if(sensorConfig[i].dataRate > 15)
						return enStatusInvArgs;
				else
				{
						regVal = sensorConfig[i].dataRate;

						if(ADXL345_ACC_USELOWPOWEROPERATION)
								regVal |= 1 << 4;
				}

				ret=pfAdxl345WriteReg(&devId[i], ADXL345_REG_BW_RATE,regVal);
				if(ret!=enStatusSuccess)
						return ret;

				if(sensorConfig[i].fifoMode > enAdxl345FifoMode_Trigger )
						return enStatusInvArgs;
				else
				{
						regVal = sensorConfig[i].fifoMode << 6 ;

						if( (sensorConfig[i].fifoTriggerINT2 != enBooleanTrue)  && (sensorConfig[i].fifoTriggerINT2!= enBooleanFalse) )
								return enStatusInvArgs;
						else
						{
								if(sensorConfig[i].fifoTriggerINT2)
								regVal |= sensorConfig[i].fifoTriggerINT2 << 5;
						}

						if(sensorConfig[i].fifoSamples > 31)
								return enStatusInvArgs;

						regVal |= sensorConfig[i].fifoSamples;

				}

				ret=pfAdxl345WriteReg(&devId[i], ADXL345_REG_FIFO_CTL,regVal);
				if(ret!=enStatusSuccess)
						return ret;

				if( sensorConfig[i].intEnable != enAdxl345Int_None )
				{
						regVal = sensorConfig[i].intEnable;

						ret=pfAdxl345WriteReg(&devId[i], ADXL345_REG_INT_ENABLE,regVal);
						if(ret!=enStatusSuccess)
								return ret;
				}

				if( sensorConfig[i].intMap_on_INT2 != enAdxl345Int_None )
				{
						regVal = sensorConfig[i].intMap_on_INT2;

						ret=pfAdxl345WriteReg(&devId[i], ADXL345_REG_INT_MAP,regVal);
						if(ret!=enStatusSuccess)
								return ret;
				}

				ret=pfAdxl345WriteReg(&devId[i], ADXL345_REG_OFSX, ADXL345_ACC_OFFSETFORXAXIS);
				if(ret!=enStatusSuccess)
						return ret;

				ret=pfAdxl345WriteReg(&devId[i], ADXL345_REG_OFSY, ADXL345_ACC_OFFSETFORYAXIS);
				if(ret!=enStatusSuccess)
						return ret;

				ret=pfAdxl345WriteReg(&devId[i], ADXL345_REG_OFSZ, ADXL345_ACC_OFFSETFORZAXIS);
				if(ret!=enStatusSuccess)
				return ret;


		#if(USE_ACTIVITY_DETECTION != 0)
				/** to initialise Activity detection parameters */
				ret=pfAdxl345WriteReg(&devId[i], ADXL345_REG_THRESH_ACT, ADXL345_ACT_THRESHOLD_VAL);
				if(ret!=enStatusSuccess)
						return ret;
		#endif

		#if(USE_INACTIVITY_DETECTION != 0)
				/** to initialise Inactivity detection parameters */
				ret=pfAdxl345WriteReg(&devId[i], ADXL345_REG_THRESH_INACT, ADXL345_INACT_THRESHOLD_VAL);
				if(ret!=enStatusSuccess)
						return ret;

				ret=pfAdxl345WriteReg(&devId[i], ADXL345_REG_TIME_INACT, ADXL345_INACT_DURATION);
				if(ret!=enStatusSuccess)
						return ret;
		#endif

		#if(USE_FREEFALL_DETECTION != 0)
				/** to initialise freefall detection parameters */
				ret=pfAdxl345WriteReg(&devId[i], ADXL345_REG_THRESH_FF, ADXL345_FREEFALL_THRESHOLD_VAL);
				if(ret!=enStatusSuccess)
						return ret;

				ret=pfAdxl345WriteReg(&devId[i], ADXL345_REG_TIME_FF, ADXL345_FREEFALL_DURATION);
				if(ret!=enStatusSuccess)
						return ret;
		#endif

		#if(USE_TAP_DETECTION != 0)
				/** to initialize tap/double tap detection parameters */
				ret=pfAdxl345WriteReg(&devId[i], ADXL345_REG_THRESH_TAP, ADXL345_TAP_THRESHOLD_VAL);
				if(ret!=enStatusSuccess)
						return ret;

				ret=pfAdxl345WriteReg(&devId[i], ADXL345_REG_DUR, ADXL345_TAP_DURATION);
				if(ret!=enStatusSuccess)
						return ret;

				ret=pfAdxl345WriteReg(&devId[i], ADXL345_REG_LATENT, ADXL345_DOUBLE_TAP_LATENCY);
				if(ret!=enStatusSuccess)
						return ret;

				ret=pfAdxl345WriteReg(&devId[i], ADXL345_REG_WINDOW, ADXL345_DOUBLE_TAP_WINDOW);
				if(ret!=enStatusSuccess)
						return ret;
		#endif

				regVal = 0x08 | (ADXL345_ACC_LINKACTIVITYWITHINACTIVITY <<5);  /** to power on ADXL345 module */
				ret=pfAdxl345WriteReg(&devId[i], ADXL345_REG_POWER_CTL,regVal);
				if(ret!=enStatusSuccess)
				return ret;
				
				adxl345InitFlag[j] = enBooleanTrue;
				++adxl345DeviceCount;
				break;
			}
			
			if(j == ADXL345_MAX_DEVICE_SUPPORTED)
				return enStatusError;
		}
	}
  
	return enStatusSuccess;
}

PFEnStatus pfAdxl345GetID(PFbyte* devId, PFbyte* deviceId)
{
    volatile PFEnStatus ret;

    if( adxl345InitFlag[*devId] != enBooleanTrue )
    return enStatusNotConfigured;

		if( *devId >= ADXL345_MAX_DEVICE_SUPPORTED )
    return enStatusInvArgs;
		
    ret=pfAdxl345ReadReg(devId, ADXL345_REG_DEVID, deviceId);
    if(ret!=enStatusSuccess)
    return ret;

    return enStatusSuccess;
}

PFEnStatus pfAdxl345GetIntSource(PFbyte* devId, PFEnAdxl345Int *intSource)
{
    PFEnStatus ret;
    PFbyte i=1,j=0,source=0;
		
	if(*devId >= ADXL345_MAX_DEVICE_SUPPORTED)
			return enStatusInvArgs;
		
    if( adxl345InitFlag[*devId] != enBooleanTrue )
        return enStatusNotConfigured;

    ret=pfAdxl345ReadReg(devId, ADXL345_REG_INT_SOURCE, &source);
    if(ret!=enStatusSuccess)
        return ret;

    /** to find int source */
		i=1;
    while( i<=8 )
    {
        if( source & (1 << (8-i)) )
        {
            intSource[j] = (PFEnAdxl345Int)(1 << (i-1));
            ++j;
        }

        ++i;
    }

    while(j<8)
    {
        intSource[j]=(PFEnAdxl345Int)0;
        ++j;
    }

    return enStatusSuccess;
}

PFEnStatus pfAdxl345GetAcceleration(PFbyte *devId, PFword* Xcomp, PFword* Ycomp, PFword* Zcomp)
{
   PFEnStatus stat;
   PFbyte dataBuf[6]={0};
   PFbyte temp=0;
   PFword readBytes=0;
		
		if(*devId >= ADXL345_MAX_DEVICE_SUPPORTED)
			return enStatusInvArgs;
    if( adxl345InitFlag[*devId] != enBooleanTrue )
        return enStatusNotConfigured;

    /** poll for data ready bit set */
    stat=pfAdxl345ReadReg(devId, ADXL345_REG_INT_SOURCE, &temp);
    if(stat!=enStatusSuccess)
        return stat;

    while( !(temp & 0x80) )
    {
        pfAdxl345ReadReg(devId, ADXL345_REG_INT_SOURCE, &temp);
    }

    /** point to data reg X lsb */
    temp=ADXL345_REG_DATAX_0;
			
    stat=adxl345Config[*devId].i2cWrite(&temp, enBooleanTrue, adxl345Config[*devId].adxl345_slaveAddr, 1);
    if(stat!=enStatusSuccess)
        return stat;

    /** read 6 data registers */
    stat=adxl345Config[*devId].i2cRead(dataBuf, enBooleanTrue, adxl345Config[*devId].adxl345_slaveAddr, 6, &readBytes);
    if(stat!=enStatusSuccess)
        return stat;

    if(readBytes!=6)
        return enStatusError;

    /** scale into g as sensitivity is 4mg/LSB */
    *Xcomp= (PFword)( 4*( (dataBuf[1]<<8) | dataBuf[0] ));   //copy higher and lower data register of X
    *Ycomp= (PFword)( 4*( (dataBuf[3]<<8) | dataBuf[2] ));	//copy higher and lower data register of Y
    *Zcomp= (PFword)( 4*( (dataBuf[5]<<8) | dataBuf[4] ));	//copy higher and lower data register of Z

    return enStatusSuccess;
}

PFEnStatus pfAdxl345GetNoOfSamplesAfterTrigger(PFbyte *devId, PFbyte* samples)
{
    PFEnStatus ret;
		if(*devId >= ADXL345_MAX_DEVICE_SUPPORTED)
			return enStatusInvArgs;
    if( adxl345InitFlag[*devId] != enBooleanTrue )
			return enStatusNotConfigured;

    ret=pfAdxl345ReadReg(devId, ADXL345_REG_FIFO_STATUS, samples);
    if(ret!=enStatusSuccess)
    return ret;

    *samples &= 0x3F;

    return enStatusSuccess;
}

/**
 * To write value to register of ADXL345 Accelerometer pointed by regAddr
 *
 * \param devId specify id assigned to driver
 *
 * \param regAddr register address at which writeValue to be write
 *
 * \param writeValue value to be written
 *
 * \return status of write register operation
 */
static PFEnStatus pfAdxl345WriteReg(PFbyte *devId, PFbyte regAddr, PFbyte writeValue)
{
     PFEnStatus ret;
     PFbyte writeBuf[2]={0};
		if(*devId >= ADXL345_MAX_DEVICE_SUPPORTED)
			return enStatusInvArgs;
		
    writeBuf[0]=regAddr;
    writeBuf[1]=writeValue;

    ret=adxl345Config[*devId].i2cWrite(writeBuf, enBooleanTrue,adxl345Config[*devId].adxl345_slaveAddr, 2);
    if(ret!=enStatusSuccess)
        return ret;

    return enStatusSuccess;
}

/**
 * To read single register from ADXL345 Accelerometer
 *
 * \param devId specify id assigned to driver
 *
 * \param regAddr register address to read that Register
 *
 * \param readValue pointer to copy data from Register
 *
 * \return status of read register operation
 */
static PFEnStatus pfAdxl345ReadReg(PFbyte* devId, PFbyte regAddr, PFbyte* readValue)
{
    volatile PFEnStatus ret;
    volatile PFbyte writeVal=0;
    PFword readBytes=0;

		if(*devId >= ADXL345_MAX_DEVICE_SUPPORTED)
			return enStatusInvArgs;
		
    writeVal=regAddr;
		
    ret=adxl345Config[*devId].i2cWrite((PFbyte*)&writeVal, enBooleanTrue, adxl345Config[*devId].adxl345_slaveAddr, 1);
    if(ret!=enStatusSuccess)
        return ret;

    /** read register value */
    ret=adxl345Config[*devId].i2cRead(readValue, enBooleanTrue, adxl345Config[*devId].adxl345_slaveAddr, 1, &readBytes);
    if(ret!=enStatusSuccess)
        return ret;

    return enStatusSuccess;
}

PFEnStatus pfAdxl345Close(PFbyte* devId)
{
	PFEnStatus ret;
	if(*devId >= ADXL345_MAX_DEVICE_SUPPORTED)
		return enStatusInvArgs;
	if(adxl345InitFlag[*devId] != enBooleanTrue)
		return enStatusNotConfigured;

	ret=pfAdxl345WriteReg(devId, ADXL345_REG_POWER_CTL, 0x00);
	if(ret!=enStatusSuccess)
			return ret;	
	adxl345InitFlag[*devId] = enBooleanFalse;
	--adxl345DeviceCount;
	
	return enStatusSuccess;
}
