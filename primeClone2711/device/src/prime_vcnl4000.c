#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_vcnl4000.h"

static PFEnBoolean vcnlInitFlag[VCNL_MAX_DEVICE_CONNECTED] = {enBooleanFalse};  /** array to store initialization status */
static PFCfgVcnl vcnlConfig[VCNL_MAX_DEVICE_CONNECTED];      /** array to store configuration structures of VCNL4000 devices connected */
static PFbyte vcnlDeviceCount=0;

/*
 * To read individual Register of VCNL4000 
 *
 * \param devId, pointer to specify id assigned to driver
 * \param regAddr, address of VCNL4000's register to be read
 * \param readValue, pointer to copy value read from VCNL4000's register 
 *
 * \return status of read register 
 */
static PFEnStatus pfVcnlReadReg(PFbyte* devId, PFbyte regAddr, PFbyte* readValue);

/*
 * To write individual Register of VCNL4000 
 *
 * \param devId, pointer to specify id assigned to driver
 * \param regAddr, address of VCNL4000's register to which write to be performed
 * \param writeValue, value to be written to VCNL4000's register 
 *
 * \return status of write register 
 */
static PFEnStatus pfVcnlWriteReg(PFbyte* devId, PFbyte regAddr, PFbyte writeValue);

PFEnStatus pfVcnlOpen(PFbyte* devId, PFpCfgVcnl sensorConfig, PFbyte deviceCount)
{
    PFbyte regVal=0, i=0, j=0;
		PFEnStatus stat;
	
#if (PF_VCNL4000_DEBUG == 1)
	if(devId == NULL)
		return enStatusInvArgs;
	if(sensorConfig == NULL)
		return enStatusInvArgs;
#endif	// #if (PF_VCNL4000_DEBUG == 1)
	
	for(i=0; i<deviceCount; ++i)
	{
		for(j=0; j<VCNL_MAX_DEVICE_CONNECTED; ++j)
		{
			if( vcnlInitFlag[j] == enBooleanFalse )
			{
				devId[i]=j;
				
				pfMemCopy(&vcnlConfig[j], &sensorConfig[i], sizeof(PFCfgVcnl));
				
				//if( (sensorConfig[i].proxIrLedCurrent>enProxIRledCurrent_200mA) || (sensorConfig[i].proxIrLedCurrent<enProxIRledCurrent_0mA) )
				if(sensorConfig[i].proxIrLedCurrent>enProxIRledCurrent_200mA)
					return enStatusInvArgs;
				else
				{
					regVal= sensorConfig[i].proxIrLedCurrent;
					stat=pfVcnlWriteReg(&devId[i], VCNL_REG_LED_CURRENT, regVal);
					if(stat!=enStatusSuccess)
						return stat;
				}

			
				if(sensorConfig[i].ambAvgSamples>enAmbAvgSample_128) 
					return enStatusInvArgs;
				else
				{
					regVal=sensorConfig[i].ambAvgSamples;

					if(sensorConfig[i].ambContinousSensing)
						regVal |= 0x07;

					if(sensorConfig[i].ambOffsetCompensation)
						regVal |= 0x03;

					stat=pfVcnlWriteReg(&devId[i], VCNL_REG_AMB_PARAMETER,regVal);
					if(stat!=enStatusSuccess)
						return stat;
				}

				
				if( sensorConfig[i].proxIrLedFreq>enProxIRledFreq_3_125MHz) 	
					return enStatusInvArgs;
				else
				{
					regVal= ( ~sensorConfig[i].proxIrLedFreq ) & 0x03;

					stat=pfVcnlWriteReg(&devId[i], VCNL_REG_LED_SIG_FREQ, regVal);
					if(stat!=enStatusSuccess)
						return stat;
				}

				if( (sensorConfig[i].modDelayTime>enModDelayTime_7) 	|| (sensorConfig[i].modDeadTime>enModDeadTime_7) 	)
					return enStatusInvArgs;
				else
				{
					regVal=(sensorConfig[i].modDelayTime<<5) | sensorConfig[i].modDeadTime;

					stat=pfVcnlWriteReg(&devId[i], VCNL_REG_LED_MOD_TIMING, regVal);
					if(stat!=enStatusSuccess)
						return stat;
				}

				
				
				vcnlInitFlag[j]=enBooleanTrue;
				++vcnlDeviceCount;
				break;
			}
			
		}
		if(j == VCNL_MAX_DEVICE_CONNECTED)
			return enStatusError;
	}

    return enStatusSuccess;
}

PFEnStatus pfA3977Close(PFbyte* devId)
{

#if (PF_VCNL4000_DEBUG == 1)
	if(devId == NULL)
		return enStatusInvArgs;
#endif	// #if (PF_VCNL4000_DEBUG == 1)

	if(*devId > VCNL_MAX_DEVICE_CONNECTED)
		return enStatusInvArgs;
	if(vcnlInitFlag[*devId] != enBooleanTrue)
		return enStatusNotConfigured;
	
	vcnlInitFlag[*devId] = enBooleanFalse;
	--vcnlDeviceCount;

	return enStatusSuccess;
}

PFEnStatus pfVcnlGetProductId(PFbyte* devId, PFbyte* prodId)
{
	PFEnStatus retStat;

#if (PF_VCNL4000_DEBUG == 1)
	if(devId == NULL)
		return enStatusInvArgs;
	if(prodId == NULL)
		return enStatusInvArgs;
#endif	// #if (PF_VCNL4000_DEBUG == 1)
	
	if(*devId > VCNL_MAX_DEVICE_CONNECTED)
		return enStatusInvArgs;
	if(vcnlInitFlag[*devId] != enBooleanTrue)
		return enStatusNotConfigured;

    retStat=pfVcnlReadReg(devId, VCNL_REG_PROD_ID, prodId);
    if(retStat!=enStatusSuccess)
			return retStat;

    return enStatusSuccess;
}

PFEnStatus pfVcnlGetAmbience(PFbyte* devId, PFword* ambience)
{
    PFEnStatus retStat;
    PFbyte cmdReg=0,res=0;
	PFsword var=0;

#if (PF_VCNL4000_DEBUG == 1)
	if(devId == NULL)
		return enStatusInvArgs;
	if(ambience == NULL)
		return enStatusInvArgs;
#endif	// #if (PF_VCNL4000_DEBUG == 1)
    
    if(*devId > VCNL_MAX_DEVICE_CONNECTED)
			return enStatusInvArgs;
    if(vcnlInitFlag[*devId] != enBooleanTrue)
			return enStatusNotConfigured;

    retStat=pfVcnlReadReg(devId, VCNL_REG_CMD, &cmdReg);
    if(retStat!=enStatusSuccess)
			return retStat;

    cmdReg |= VCNL_START_AMBIENCE_CONV;

    retStat=pfVcnlWriteReg(devId, VCNL_REG_CMD, cmdReg);
    if(retStat!=enStatusSuccess)
			return retStat;
	
    /** wait for greater than 400us */
	// pfDelayMicroSec(10);
	
	
	retStat=pfVcnlReadReg(devId, VCNL_REG_CMD, &cmdReg);
    if(retStat!=enStatusSuccess)
			return retStat;

    while( !(cmdReg & VCNL_AMBIENCE_DATA_READY) )
    {
        pfVcnlReadReg(devId, VCNL_REG_CMD, &cmdReg);
    }

    retStat=pfVcnlReadReg(devId, VCNL_REG_AMB_DATA_H, &res);
    if(retStat!=enStatusSuccess)
			return retStat;

    var = res << 8;

    retStat=pfVcnlReadReg(devId, VCNL_REG_AMB_DATA_L, &res);
    if(retStat!=enStatusSuccess)
			return retStat;

    var |= res;
    *ambience = (PFword)( ( (float)var ) * 0.25 );

    return enStatusSuccess;
}

PFEnStatus pfVcnlGetProximity(PFbyte* devId, PFword* proximity)
{
    PFEnStatus retStat;
    PFbyte cmdReg=0,res=0;

 
#if (PF_VCNL4000_DEBUG == 1)
	if(devId == NULL)
		return enStatusInvArgs;
	if(proximity == NULL)
		return enStatusInvArgs;
	
	
#endif	// #if (PF_VCNL4000_DEBUG == 1)	
    if(*devId > VCNL_MAX_DEVICE_CONNECTED)
			return enStatusInvArgs;
    if(vcnlInitFlag[*devId] != enBooleanTrue)
			return enStatusNotConfigured;

    retStat=pfVcnlReadReg(devId, VCNL_REG_CMD,&cmdReg);
    if(retStat!=enStatusSuccess)
			return retStat;

    cmdReg |= VCNL_START_PROXIMITY_CONV;

    retStat=pfVcnlWriteReg(devId, VCNL_REG_CMD,cmdReg);
    if(retStat!=enStatusSuccess)
			return retStat;

    /** wait for greater than 400us */
	// pfDelayMicroSec(10);
	
	

    retStat=pfVcnlReadReg(devId, VCNL_REG_CMD, &cmdReg);
    if(retStat!=enStatusSuccess)
			return retStat;

    while( !(cmdReg & VCNL_PROXIMITY_DATA_READY) )
    {
        pfVcnlReadReg(devId, VCNL_REG_CMD,&cmdReg);
    }

    retStat=pfVcnlReadReg(devId, VCNL_REG_PROX_DATA_H, &res);
    if(retStat!=enStatusSuccess)
			return retStat;

    *proximity = res << 8;

    retStat=pfVcnlReadReg(devId, VCNL_REG_PROX_DATA_L,&res);
    if(retStat!=enStatusSuccess)
			return retStat;

    *proximity |= res;

    return enStatusSuccess;
}

PFEnStatus pfVcnlGetLastAmbience(PFbyte* devId, PFword* ambience)
{
    PFEnStatus retStat;
    PFbyte res=0;
    PFsword var;

#if (PF_VCNL4000_DEBUG == 1)
	if(devId == NULL)
		return enStatusInvArgs;
#endif	// #if (PF_VCNL4000_DEBUG == 1)    
   
		if(*devId > VCNL_MAX_DEVICE_CONNECTED)
			return enStatusInvArgs;
    if(vcnlInitFlag[*devId] != enBooleanTrue)
			return enStatusNotConfigured;

    retStat=pfVcnlReadReg(devId, VCNL_REG_AMB_DATA_H,&res);
    if(retStat!=enStatusSuccess)
			return retStat;

    var = res << 8;
    
    retStat=pfVcnlReadReg(devId, VCNL_REG_AMB_DATA_L,&res);
    if(retStat!=enStatusSuccess)
			return retStat;

    var |= res;

    *ambience = (PFword)( ( (float)var ) * 0.25 );

    return enStatusSuccess;
}

PFEnStatus pfVcnlGetLastProximity(PFbyte* devId, PFword* proximity)
{
    PFEnStatus retStat;
    PFbyte res=0;
    
#if (PF_VCNL4000_DEBUG == 1)
	if(devId == NULL)
		return enStatusInvArgs;
#endif	// #if (PF_VCNL4000_DEBUG == 1)
	
		if(*devId > VCNL_MAX_DEVICE_CONNECTED)
			return enStatusInvArgs;
    if(vcnlInitFlag[*devId] != enBooleanTrue)
			return enStatusNotConfigured;

    retStat=pfVcnlReadReg(devId, VCNL_REG_PROX_DATA_H, &res);
    if(retStat!=enStatusSuccess)
			return retStat;

    *proximity = res << 8;

    retStat=pfVcnlReadReg(devId, VCNL_REG_PROX_DATA_L, &res);
    if(retStat!=enStatusSuccess)
			return retStat;

    *proximity |= res;

    return enStatusSuccess;
}

PFEnStatus pfVcnlSetProxIRledCurrent(PFbyte* devId, PFEnProxIRledCurrent proxIRledCurrent)
{
    PFEnStatus retStat;
    
#if (PF_VCNL4000_DEBUG == 1)
	if(devId == NULL)
		return enStatusInvArgs;
#endif	// #if (PF_VCNL4000_DEBUG == 1)

	if(*devId > VCNL_MAX_DEVICE_CONNECTED)
		return enStatusInvArgs;
    if(vcnlInitFlag[*devId] != enBooleanTrue)
			return enStatusNotConfigured;

	retStat=pfVcnlWriteReg(devId, VCNL_REG_LED_CURRENT, proxIRledCurrent);
	if(retStat!=enStatusSuccess)
		return retStat;

    return enStatusSuccess;
}

PFEnStatus pfVcnlSetAvgSamples(PFbyte* devId, PFEnAmbAvgSamples avgSamples)
{
    PFEnStatus retStat;
    PFbyte regVal=0;

#if (PF_VCNL4000_DEBUG == 1)
	if(devId == NULL)
		return enStatusInvArgs;
#endif	// #if (PF_VCNL4000_DEBUG == 1)
	
    if(*devId > VCNL_MAX_DEVICE_CONNECTED)
			return enStatusInvArgs;
    if(vcnlInitFlag[*devId] != enBooleanTrue)
			return enStatusNotConfigured;

    if (avgSamples>enAmbAvgSample_128) 
        return enStatusInvArgs;
    else
    {
        retStat=pfVcnlReadReg(devId, VCNL_REG_AMB_PARAMETER, &regVal);
        if(retStat!=enStatusSuccess)
					return retStat;

        regVal &= 0xf8;
        regVal |= avgSamples;

        retStat=pfVcnlWriteReg(devId, VCNL_REG_AMB_PARAMETER, regVal);
        if(retStat!=enStatusSuccess)
            return retStat;
    }

    return enStatusSuccess;
}

/** to write individual Register */
static PFEnStatus pfVcnlWriteReg(PFbyte* devId, PFbyte regAddr, PFbyte writeValue)
{
    PFEnStatus ret;
    PFbyte writeBuf[2]={0};

#if (PF_VCNL4000_DEBUG == 1)
	if(devId == NULL)
		return enStatusInvArgs;
#endif	// #if (PF_VCNL4000_DEBUG == 1)
	
    writeBuf[0]=regAddr;
    writeBuf[1]=writeValue;

    /** i2cWrite function use 0x13 address and R/W bit so need to send only reg address and value */
    ret=vcnlConfig[*devId].i2cWrite(writeBuf, enBooleanTrue, VCNL_SLAVE_ADDR, 2);
    if(ret!=enStatusSuccess)
			return ret;

    return enStatusSuccess;
}

/** to read individual Register */
static PFEnStatus pfVcnlReadReg(PFbyte* devId, PFbyte regAddr, PFbyte* readValue)
{
    PFEnStatus ret;
    PFbyte writeAddr=0;
    PFword readByte=0;

#if (PF_VCNL4000_DEBUG == 1)
	if(devId == NULL)
		return enStatusInvArgs;
	if(readValue == NULL)
		return enStatusInvArgs;
#endif	// #if (PF_VCNL4000_DEBUG == 1)
    
		writeAddr=regAddr;

    /** 0x3C regAddr   to set pointer to reg with address regAddr */
    ret=vcnlConfig[*devId].i2cWrite(&writeAddr, enBooleanTrue, VCNL_SLAVE_ADDR, 1);
    if(ret!=enStatusSuccess)
        return ret;

    /** read register value */
    ret=vcnlConfig[*devId].i2cRead(readValue, enBooleanTrue, VCNL_SLAVE_ADDR, 1, &readByte);
    if(ret!=enStatusSuccess)
        return ret;

    return enStatusSuccess;
}


