#include "prime_framework.h"
#include "prime_config.h"
#include "prime_utils.h"
#include "prime_delay.h"
#include "prime_gpio.h"
#include "prime_l6470.h"

static PFCfgStepperL6470 l6470ConfigStruct[L6470_MAX_DAISY_CHAIN_DEVICES_SUPPORTED]; // structure to copy settings passed by user
static PFdword l6470InitFlag=0;                                                 // flag to check initialization
static PFbyte l6470deviceCount=0, l6470devicesConnected=0;

//
// To set/read registers of diferrent size
//
// \param index of stepper who has to pass value
//
// \param param register to be set/read
//
// \param value to be passed to register or dummy value for dummy read
//
// \return data(register value) read from driver
//
static PFdword pfL6470paramHandler(PFchar index, PFbyte param, PFdword value);

//
// To transmit/receive value through spi
//
// \param index of stepper who has to pass value
//
// \param value to be passed to register or dummy value for dummy read
//
// \param bit_len length of register
//
// \return data(register value) read from driver
//
static PFdword pfL6470dataTransfer(PFchar index, PFdword value, PFbyte bit_len);

PFEnStatus pfL6470open(PFbyte* deviceId, PFpCfgStepperL6470 sensorConfig, PFbyte deviceCount)
{
	volatile PFEnStatus status;	
	volatile PFword data=0,dummyRead=0;
	volatile static PFchar i=0,j=0;
	volatile PFbyte buf[2]={0};
	volatile PFsdword temp=0,conf=0,readBytes=0;

// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( sensorConfig == NULL )
		return enStatusInvArgs;
  if( deviceCount > L6470_MAX_DAISY_CHAIN_DEVICES_SUPPORTED )
		return enStatusInvArgs;	
#endif	// #if (PF_L6470_DEBUG == 1)

	l6470devicesConnected = deviceCount;

// Register SPI device
	for(i=0; i<deviceCount; ++i)
	{
		status = sensorConfig[i].pfSpiRegisterDevice(&sensorConfig[i].devId, &sensorConfig[i].chipSelectPortPin);
		if(status != enStatusSuccess)
		return status;
		
		deviceId[i] = sensorConfig[i].devId;
	}

	l6470deviceCount = deviceCount;
	
	for(i=0; i<	deviceCount; ++i)
	{
		status=pfMemCopy( &l6470ConfigStruct[i], &sensorConfig[i], sizeof(PFCfgStepperL6470) );
		if(status != enStatusSuccess)
		return status;
	}

// Reset device through reset pin
	pfGpioPinsClear(sensorConfig[0].resetPortPin.port,sensorConfig[0].resetPortPin.pin);
	pfDelayMicroSec(250);
	pfGpioPinsSet(sensorConfig[0].resetPortPin.port,sensorConfig[0].resetPortPin.pin);
	pfDelayMicroSec(250);
	
// set KVAL_DEC
	for(i=0; i<deviceCount; ++i)
	{
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,0);
		for(j=deviceCount-1; j>=0; --j)
		{
			if( j ==  i )
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_SET_PARAM | L6470_KVAL_DEC, &dummyRead);
			else
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_NOP, &dummyRead);
		}
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,1);

		pfL6470dataTransfer(i,(PFdword)L6470_VTG_KVAL_DEC, 8);
	}
	
// set KVAL_ACC
	for(i=0; i<deviceCount; ++i)
	{
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,0);
		for(j=deviceCount-1; j>=0; --j)
		{
			if( j ==  i )
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_SET_PARAM | L6470_KVAL_ACC, &dummyRead);
			else
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_NOP, &dummyRead);
		}
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,1);

		pfL6470dataTransfer(i,(PFdword)L6470_VTG_KVAL_ACC, 8);
	}
	
// set KVAL_RUN
	for(i=0; i<deviceCount; ++i)
	{
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,0);
		for(j=deviceCount-1; j>=0; --j)
		{
			if( j ==  i )
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_SET_PARAM | L6470_KVAL_RUN, &dummyRead);
			else
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_NOP, &dummyRead);
		}
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,1);

		pfL6470dataTransfer(i,(PFdword)L6470_VTG_KVAL_RUN, 8);
	}
	
// set KVAL_HOLD
	for(i=0; i<deviceCount; ++i)
	{
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,0);
		for(j=deviceCount-1; j>=0; --j)
		{
			if( j ==  i )
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_SET_PARAM | L6470_KVAL_HOLD, &dummyRead);
			else
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_NOP, &dummyRead);
		}
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,1);

		pfL6470dataTransfer(i,(PFdword)L6470_VTG_KVAL_HOLD, 8);
	}	
	
// set maximum speed of rotation
	for(i=0; i<deviceCount; ++i)
	{
		temp=(PFdword)(0.065536*L6470_MAX_SPEED_SUPPORTED); 
		if(temp<=0)
			temp = 1;
	
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,0);
		for(j=deviceCount-1; j>=0; --j)
		{
			if( j ==  i )
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_SET_PARAM | L6470_MAX_SPEED, &dummyRead);
			else
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_NOP, &dummyRead);
		}
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,1);

		pfL6470dataTransfer(i,temp, 10);
	}
	
// set minimum speed of rotation
	for(i=0; i<deviceCount; ++i)
	{
		temp=(PFdword)(4.1943*L6470_MIN_SPEED_OF_ROTATION); 
		if(temp<0)
			temp = 0;
	
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,0);
		for(j=deviceCount-1; j>=0; --j)
		{
			if( j ==  i )
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_SET_PARAM | L6470_MIN_SPEED, &dummyRead);
			else
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_NOP, &dummyRead);
		}
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,1);

		pfL6470dataTransfer(i,temp, 13);
	}

// set acceleration
	for(i=0; i<deviceCount; ++i)
	{
		temp=(PFdword)(0.0687*sensorConfig[i].acceleration);
		if(temp<=0)
			temp = 1;
	
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,0);
		for(j=deviceCount-1; j>=0; --j)
		{
			if( j ==  i )
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_SET_PARAM | L6470_ACC, &dummyRead);
			else
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_NOP, &dummyRead);
		}
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,1);
	
		pfL6470dataTransfer(i,temp, 12);
	}	
	
// set deceleration 
	for(i=0; i<deviceCount; ++i)
	{
		temp=(PFdword)(0.0687*sensorConfig[i].deceleration);
		if(temp<=0)
			temp = 1;
	
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,0);
		for(j=deviceCount-1; j>=0; --j)
		{
			if( j ==  i )
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_SET_PARAM | L6470_DEC, &dummyRead);
			else
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_NOP, &dummyRead);
		}
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,1);
	
		pfL6470dataTransfer(i,temp, 12);
	}	
	
// set step size
	for(i=0; i<deviceCount; ++i)
	{
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,0);
		for(j=deviceCount-1; j>=0; --j)
		{
			if( j ==  i )
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_SET_PARAM | L6470_STEP_MODE, &dummyRead);
			else
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_NOP, &dummyRead);
		}
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,1);
	
		if(L6470_SYNC_SELECT_ENABLE)
			pfL6470paramHandler(i, L6470_STEP_MODE, 0x80 | sensorConfig[i].stepSize );
		else
			pfL6470paramHandler(i, L6470_STEP_MODE, sensorConfig[i].stepSize );
	}	
	
// set full step threshold
	for(i=0; i<deviceCount; ++i)
	{
		temp=(PFdword)( (0.0655*L6470_FULL_STEP_THRESHOLD_SPEED) - 0.5 );
		if(temp<=0)
			temp = 1;
	
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,0);
		for(j=deviceCount-1; j>=0; --j)
		{
			if( j ==  i )
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_SET_PARAM | L6470_FS_SPD, &dummyRead);
			else
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_NOP, &dummyRead);
		}
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,1);
	
		pfL6470dataTransfer(i,temp, 10);
	}
	
// set overcurrent threshold
	for(i=0; i<deviceCount; ++i)
	{
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,0);
		for(j=deviceCount-1; j>=0; --j)
		{
			if( j ==  i )
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_SET_PARAM | L6470_OCD_TH, &dummyRead);
			else
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_NOP, &dummyRead);
		}
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,1);
	
		pfL6470dataTransfer(i, sensorConfig[i].overCurrentThreshold, 4);
	}
	
// set Alarm Enable
	for(i=0; i<deviceCount; ++i)
	{
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,0);
		for(j=deviceCount-1; j>=0; --j)
		{
			if( j ==  i )
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_SET_PARAM | L6470_ALARM_EN, &dummyRead);
			else
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_NOP, &dummyRead);
		}
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,1);
	
		pfL6470dataTransfer(i, sensorConfig[i].alarmEnable, 8);
	}	
	
// set configuration register
	for(i=0; i<deviceCount; ++i)
	{
		conf = (sensorConfig[i].divisor<<13) | (sensorConfig[i].multiplier<<10) | (sensorConfig[i].slewRate<<8) | (sensorConfig[i].oscSelect);
		if(L6470_OVERCURRENT_SHUTDOWN_ENABLE)
			conf |= 1<<7;
		if(L6470_VTG_COMPENSATION_ENABLE)
			conf |= 1<<5;
		if(L6470_HARD_STOP_DISABLE)
			conf |= 1<<4;
	
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,0);
		for(j=deviceCount-1; j>=0; --j)
		{
			if( j ==  i )
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_SET_PARAM | L6470_CONFIG, &dummyRead);
			else
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_NOP, &dummyRead);
		}
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,1);
	
		pfL6470dataTransfer(i, conf, 16);
	}	
	
// read status register is must to set ULV0(under-voltage lockout) flag
	for(i=0; i<deviceCount; ++i)
	{
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,0);
		for(j=deviceCount-1; j>=0; --j)
		{
			if( j ==  i )
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_GET_STATUS, &dummyRead);
			else
				l6470ConfigStruct[j].pfSpiExchangeByte(&sensorConfig[i].devId, L6470_NOP, &dummyRead);
		}
		l6470ConfigStruct[i].pfSpiChipSelect(&sensorConfig[i].devId,1);
	
		pfL6470dataTransfer(i, 0, 16);
	}
	
	l6470InitFlag=1;
	
	return enStatusSuccess;
}

PFEnStatus pfL6470close(PFbyte* devId)
{
	volatile PFchar i=0, index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}
	
	l6470ConfigStruct[i].pfSpiUnregisterDevice(&l6470ConfigStruct[i].devId);
	--l6470deviceCount;
	
	if(l6470deviceCount == 0)
		l6470InitFlag=0;
	
	return enStatusSuccess;	
}

PFEnStatus pfL6470closeAll(void)
{
	volatile PFchar i=0;
	
// check for initialization
#ifdef _PFSTEPPER6470_DEBUG_
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// _PFSTEPPER6470_DEBUG_
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		l6470ConfigStruct[i].pfSpiUnregisterDevice(&l6470ConfigStruct[i].devId);
	}

	l6470deviceCount=0;
	l6470InitFlag=0;
	
	return enStatusSuccess;	
}

PFEnStatus pfL6470run(PFbyte* devId, PFbyte dir, PFfloat speed)
{
	volatile PFword dummyRead=0;
	volatile PFdword status=0;
	volatile PFsdword temp=0;
	volatile PFchar i=0,j=0,index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
	if(speed > L6470_MAX_SPEED_SUPPORTED)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if( !(status & 0x0002) )
		return enStatusBusy;

	temp=(PFdword)(67.1*speed);
	if(temp<=0)
		temp = 1;
	
	if (temp > 0xFFFFF) 
		temp = 0xFFFFF;
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_RUN | dir, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	pfL6470dataTransfer(index, temp, 20);
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1); 
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if(status & 0x0180)
		return enStatusError;
	
	return enStatusSuccess;
}

PFEnStatus pfL6470move(PFbyte* devId, PFbyte dir, PFdword steps, PFfloat speed)
{
	volatile PFword dummyRead=0;
	volatile PFsdword status=0,data=0;
	volatile PFchar i=0,j=0,index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

 #if (PF_L6470_DEBUG == 1)  //no device with specified devId
 	if(i == l6470devicesConnected)
 		return enStatusInvArgs;
 #endif	// #if (PF_L6470_DEBUG == 1)

	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1); 
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if( status & 0x0060 )
		return enStatusBusy;
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, (L6470_MOVE | dir), &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);	

  if (steps > 0x3FFFFF) 
		steps = 0x3FFFFF;
	
	pfL6470dataTransfer(index, steps, 22);	 
	
	pfDelayMicroSec(10);
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1); 
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if(status & 0x0180)
		return enStatusError;
	
	return enStatusSuccess;
}

PFEnStatus pfL6470deviceReady(PFbyte* devId, PFEnBoolean* test)
{
	volatile PFword dummyRead=0;
	volatile PFsdword status=0;
	volatile PFchar i=0,j=0,index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if( status & 0x0002 )
		*test = enBooleanTrue;
	else
		*test = enBooleanFalse;
	
	return enStatusSuccess;
}

PFEnStatus pfL6470goUntil(PFbyte* devId, PFEnL6470actionForSwitchPress act, PFbyte dir, PFfloat speed)
{
	volatile PFword dummyRead=0;
	volatile PFdword status=0;
	volatile PFsdword temp=0;
	volatile PFchar i=0,j=0,index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
	if(speed > L6470_MAX_SPEED_SUPPORTED)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if( !(status & 0x0002) )
		return enStatusBusy;
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, dir | act | L6470_GO_UNTIL, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);	 

	temp=(PFdword)(67.1*speed);
	
	if(temp<=0)
		temp = 1;
	
	if (temp > 0xFFFFF) 
		temp = 0xFFFFF;
	
	pfL6470dataTransfer(index, speed, 20);
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if(status & 0x0180)
		return enStatusError;
	
	return enStatusSuccess;
}

PFEnStatus pfL6470releseSW(PFbyte* devId, PFEnL6470actionForSwitchPress act, PFbyte dir)
{
	volatile PFword dummyRead=0;
	volatile PFdword status=0;
	volatile PFchar i=0,j=0,index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if( !(status & 0x0002) )
		return enStatusBusy;
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, dir | act | L6470_RELEASE_SW, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	 
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if(status & 0x0180)
		return enStatusError;
	
	return enStatusSuccess;	
}

PFEnStatus pfL6470setMinimumSpeed(PFbyte* devId, PFfloat speed)
{
	volatile PFword dummyRead=0;
	volatile PFdword status=0;
	volatile PFchar i=0,j=0,index=0;
	volatile PFsdword temp=0;

// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
	if(speed > L6470_MAX_SUPPORTED_MIN_SPEED)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if( status & 0x0060 )
		return enStatusBusy;

	temp=(PFdword)(4.1943*speed); 
	if(temp<0)
		temp = 0;

	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_SET_PARAM | L6470_MIN_SPEED, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);

	pfL6470dataTransfer(index,temp, 13);
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1); 
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if(status & 0x0180)
		return enStatusError;
	
	return enStatusSuccess;
}

PFEnStatus pfL6470setFullStepSpeed(PFbyte* devId, float speed)
{
	volatile PFword dummyRead=0;
	volatile PFdword status=0;
	volatile PFchar i=0,j=0,index=0;
	volatile PFsdword temp=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
	if(speed > L6470_MAX_SPEED_SUPPORTED)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)

	temp=(PFdword)( (0.0655*speed) - 0.5 );
	if(temp<=0)
		temp = 1;

	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_SET_PARAM | L6470_FS_SPD, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);

	pfL6470dataTransfer(index,temp, 10);
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1); 
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if(status & 0x0180)
		return enStatusError;
	
	return enStatusSuccess;
}

PFEnStatus pfL6470GoHomePosition(PFbyte* devId)
{
	volatile PFword dummyRead=0;
	volatile PFdword status=0;
	volatile PFchar i=0,j=0,index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if( status & 0x0060 )
		return enStatusBusy;
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GO_HOME, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	 
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if(status & 0x0180)
		return enStatusError;
	
	return enStatusSuccess;	
}

PFEnStatus pfL6470resetPosition(PFbyte* devId)
{
	volatile PFword dummyRead=0;
	volatile PFdword status=0;
	volatile PFchar i=0,j=0,index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if( status & 0x0060 )
		return enStatusBusy;
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_RESET_POS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
//read command status	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if(status & 0x0180)
		return enStatusError;
	
	return enStatusSuccess;	
}

PFEnStatus pfL6470resetDevice(PFbyte* devId)
{
	volatile PFword dummyRead=0;
	volatile PFdword status=0;
	volatile PFchar i=0,j=0,index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_RESET_DEVICE, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	 
//read command status	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if(status & 0x0180)
		return enStatusError;
	
	return enStatusSuccess;
}

PFEnStatus pfL6470softHiZ(PFbyte* devId)
{
	volatile PFword dummyRead=0;
	volatile PFchar i=0,j=0,index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_SOFT_HIZ, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	return enStatusSuccess;
}

PFEnStatus pfL6470hardHiZ(PFbyte *devId)
{
	volatile PFword dummyRead=0;
	volatile PFchar i=0,j=0,index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_HARD_HIZ, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	return enStatusSuccess;
}

PFEnStatus pfL6470goMark(PFbyte* devId)
{
	volatile PFword dummyRead=0;
	volatile PFdword status=0;
	volatile PFchar i=0,j=0,index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if( status & 0x0060 )
		return enStatusBusy;
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GO_MARK, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	 
//read command status	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if(status & 0x0180)
		return enStatusError;
	
	return enStatusSuccess;
}

PFEnStatus pfL6470goTo(PFbyte* devId, PFdword pos)
{
	volatile PFword dummyRead=0;
	volatile PFdword status=0;
	volatile PFchar i=0,j=0,index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if( status & 0x0060 )
		return enStatusBusy;
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GOTO, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	if (pos > 0x3FFFFF) 
		pos = 0x3FFFFF;
	
	pfL6470dataTransfer(index, pos, 22);
	
//read command status	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if(status & 0x0180)
		return enStatusError;
	
	return enStatusSuccess;
}

PFEnStatus pfL6470goTo_DIR(PFbyte* devId, PFbyte dir, PFdword pos)
{
	volatile PFword dummyRead=0;
	volatile PFdword status=0;
	volatile PFchar i=0,j=0,index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if( status & 0x0060 )
		return enStatusBusy;
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GOTO_DIR, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	if (pos > 0x3FFFFF) 
		pos = 0x3FFFFF;
	
	pfL6470dataTransfer(index, pos, 22);
	
//read command status	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if(status & 0x0180)
		return enStatusError;
	
	return enStatusSuccess;
}

PFEnStatus pfL6470takeSingleStep(PFbyte* devId)
{
	volatile PFword dummyRead=0;
	volatile PFdword status=0;
	volatile PFdword k=0;
	volatile PFchar i=0,j=0,index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);

	if( status & 0x0060 )
		return enStatusBusy;

	pfGpioPinsClear(l6470ConfigStruct[index].stepPortPin.port, l6470ConfigStruct[index].stepPortPin.pin);
	pfDelayMicroSec(1);
	pfGpioPinsSet(l6470ConfigStruct[index].stepPortPin.port, l6470ConfigStruct[index].stepPortPin.pin);
	
	return enStatusSuccess;
}

PFEnStatus pfL6470setStepClockMode(PFbyte* devId, PFbyte dir)
{
	volatile PFword dummyRead=0;
	volatile PFdword status=0;
	volatile PFchar i=0,j=0,index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);

	if( status & 0x0060 )
		return enStatusBusy;

	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, (L6470_STEP_CLOCK | dir), &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);

//read command status	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	status = pfL6470dataTransfer(index, 0, 16);
	
	if(status & 0x0180)
		return enStatusError;
	
	return enStatusSuccess;
}

PFEnStatus pfL6470softStop(PFbyte* devId)
{
	volatile PFword dummyRead=0;
	volatile PFchar i=0,j=0,index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_SOFT_STOP, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	return enStatusSuccess;
}

// Stop the motor with infinite deceleration.
PFEnStatus pfL6470hardStop(PFbyte* devId)
{
	volatile PFword dummyRead=0;
	volatile PFchar i=0,j=0,index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
		for(j=l6470devicesConnected-1; j>=0; --j)
		{
			if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_HARD_STOP, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	return enStatusSuccess;
}

PFEnStatus pfL6470getStatus(PFbyte* devId, PFword *status)
{
	volatile PFword dummyRead=0;
	volatile PFchar i=0,j=0,index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	*status = (PFword)pfL6470dataTransfer(index, 0, 16);
	
	return enStatusSuccess;
}

PFEnStatus pfL6470nop(PFbyte* devId)
{
	volatile PFword dummyRead=0;
	volatile PFchar j=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(j=0; j<l6470devicesConnected; ++j)
	{
		if( l6470ConfigStruct[j].devId == *devId )
			break;
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(j == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[0].devId,0);
	for(j=0; j<l6470devicesConnected; ++j)
	{
		l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[j].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[0].devId,1);
	
	return enStatusSuccess;	
}

PFEnStatus pfL6470getCurrentPosition(PFbyte* devId, PFsdword* position)
{
	volatile PFword dummyRead=0;
	volatile PFsdword abbs=0;
	volatile PFchar i=0,j=0,index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_PARAM | L6470_ABS_POS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	abbs = pfL6470dataTransfer(index, 0, 22);

	if(abbs & 0x00200000)
		abbs |= 0xffc00000;

	*position = abbs;
	
	return enStatusSuccess;	
}

PFEnStatus pfL6470getAccelerationProfile(PFbyte* devId, PFfloat* acceleration, PFfloat* deceleration)
{
	volatile PFword dummyRead=0;
	volatile PFdword data=0;
	volatile PFchar i=0,j=0,index=0;
	
// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_PARAM | L6470_ACC, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	data = pfL6470dataTransfer(index, 0, 12);
	
	*acceleration = 14.552*((float)data);
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_PARAM | L6470_DEC, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	data = pfL6470dataTransfer(index, 0, 12);
	
	*deceleration = 14.552*((float)data);
	
	return enStatusSuccess;	
}

PFEnStatus pfL6470getSpeed(PFbyte* devId, PFfloat* speed)
{
	volatile PFword dummyRead=0;
	volatile PFdword data=0;
	volatile PFchar i=0,j=0,index=0;

// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_PARAM | L6470_SPEED, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
	
	data = pfL6470dataTransfer(index, 0, 20);
	
	*speed = ((float)data) * 0.0149;
	
	return enStatusSuccess;	
}

PFEnStatus pfL6470setAccelerationProfile(PFbyte* devId, PFfloat acceleration, PFfloat deceleration)
{
	volatile PFword dummyRead=0;
	volatile PFdword data=0;
	volatile PFchar i=0,j=0,index=0;

// check for initialization
#if (PF_L6470_DEBUG == 1)
  if( !l6470InitFlag )
		return enStatusNotConfigured;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	for(i=0; i<l6470devicesConnected; ++i)
	{
		if( l6470ConfigStruct[i].devId == *devId )
		{
			index = i;
			break;
		}
	}

#if (PF_L6470_DEBUG == 1)  //no device with specified devId
	if(i == l6470devicesConnected)
		return enStatusInvArgs;
#endif	// #if (PF_L6470_DEBUG == 1)
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1); 
	
	data = pfL6470dataTransfer(index, 0, 16);
	
	if(data & 0x0060)
		return enStatusBusy;
	
// set acceleration
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_SET_PARAM | L6470_ACC, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);

	data=(PFdword)(0.0687*acceleration);
	if(data<=0)
		data = 1;
	pfL6470dataTransfer(index, data, 12);
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1); 
	
	data = pfL6470dataTransfer(index, 0, 16);
	
	if(data & 0x0180)
		return enStatusError;
	
// set deceleration
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_SET_PARAM | L6470_DEC, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);

	data=(PFdword)(0.0687*deceleration);
	if(data<=0)
		data = 1;
	pfL6470dataTransfer(index, data, 12);
	
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
	for(j=l6470devicesConnected-1; j>=0; --j)
	{
		if( j ==  index )
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_GET_STATUS, &dummyRead);
		else
			l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
	}
	l6470ConfigStruct[i].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1); 
	
	data = pfL6470dataTransfer(index, 0, 16);
	
	if(data & 0x0180)
		return enStatusError;
	
	return enStatusSuccess;	
}

static PFdword pfL6470paramHandler(PFchar index, PFbyte param, PFdword value)
{
  volatile PFword dummyRead=0;
  volatile PFdword ret_val = 0;
  volatile PFchar j=0;
	
  switch (param)
  {
    case L6470_ABS_POS:
					ret_val = (PFdword)pfL6470dataTransfer(index, value, 22);
					break;
    
    case L6470_EL_POS:
					ret_val = (PFdword)pfL6470dataTransfer(index, value, 9);
					break;
    
    case L6470_MARK:
					ret_val = (PFdword)pfL6470dataTransfer(index, value, 22);
					break;
 
    case L6470_SPEED:
					ret_val = (PFdword)pfL6470dataTransfer(index, 0, 20);
					break; 

    case L6470_ACC: 
					ret_val = (PFdword)pfL6470dataTransfer(index, value, 12);
					break;
		
    case L6470_DEC: 
					ret_val = (PFdword)pfL6470dataTransfer(index, value, 12);
					break;
    
    case L6470_MAX_SPEED:
					ret_val = (PFdword)pfL6470dataTransfer(index, value, 10);
					break;
    
    case L6470_MIN_SPEED: 
					ret_val = (PFdword)pfL6470dataTransfer(index, value, 12);
					break;
   
    case L6470_FS_SPD:
					ret_val = (PFdword)pfL6470dataTransfer(index, value, 10);
					break;
    
    case L6470_KVAL_HOLD:
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
					for(j=l6470devicesConnected-1; j>=0; --j)
					{
						if( j ==  index )
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, ((PFbyte)value), (PFword*)&ret_val);
						else
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
					}
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
					break;
		
    case L6470_KVAL_RUN:
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
					for(j=l6470devicesConnected-1; j>=0; --j)
					{
						if( j ==  index )
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, ((PFbyte)value), (PFword*)&ret_val);
						else
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
					}
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
					break;
		
    case L6470_KVAL_ACC:
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
					for(j=l6470devicesConnected-1; j>=0; --j)
					{
						if( j ==  index )
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, ((PFbyte)value), (PFword*)&ret_val);
						else
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
					}
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
					break;
		
    case L6470_KVAL_DEC:
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
					for(j=l6470devicesConnected-1; j>=0; --j)
					{
						if( j ==  index )
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, ((PFbyte)value), (PFword*)&ret_val);
						else
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
					}
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
					break;

    case L6470_INT_SPD:
					ret_val = (PFdword)pfL6470dataTransfer(index, value, 14);
					break;
		
    case L6470_ST_SLP:
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
					for(j=l6470devicesConnected-1; j>=0; --j)
					{
						if( j ==  index )
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, ((PFbyte)value), (PFword*)&ret_val);
						else
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
					}
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
					break;
		
    case L6470_FN_SLP_ACC:
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
					for(j=l6470devicesConnected-1; j>=0; --j)
					{
						if( j ==  index )
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, ((PFbyte)value), (PFword*)&ret_val);
						else
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
					}
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
					break;
		
    case L6470_FN_SLP_DEC: 
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
					for(j=l6470devicesConnected-1; j>=0; --j)
					{
						if( j ==  index )
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, ((PFbyte)value), (PFword*)&ret_val);
						else
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
					}
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
					break;
    
    case L6470_K_THERM:
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
					for(j=l6470devicesConnected-1; j>=0; --j)
					{
						if( j ==  index )
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, ((PFbyte)value), (PFword*)&ret_val);
						else
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
					}
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
					break;
   
    case L6470_ADC_OUT:
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
					for(j=l6470devicesConnected-1; j>=0; --j)
					{
						if( j ==  index )
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, ((PFbyte)value), (PFword*)&ret_val);
						else
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
					}
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
					break;
    
    case L6470_OCD_TH:
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
					for(j=l6470devicesConnected-1; j>=0; --j)
					{
						if( j ==  index )
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, ((PFbyte)value), (PFword*)&ret_val);
						else
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
					}
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
					break;
    
    case L6470_STALL_TH:
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
					for(j=l6470devicesConnected-1; j>=0; --j)
					{
						if( j ==  index )
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, ((PFbyte)value), (PFword*)&ret_val);
						else
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
					}
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
					break;
    
    case L6470_STEP_MODE:
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
					for(j=l6470devicesConnected-1; j>=0; --j)
					{
						if( j ==  index )
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, ((PFbyte)value), (PFword*)&ret_val);
						else
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
					}
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
					break;
    
    case L6470_ALARM_EN:
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
					for(j=l6470devicesConnected-1; j>=0; --j)
					{
						if( j ==  index )
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, ((PFbyte)value), (PFword*)&ret_val);
						else
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
					}
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
					break;
    
    case L6470_CONFIG: 
					ret_val = (PFdword)pfL6470dataTransfer(index, value, 16);
					break;
    
    case L6470_STATUS:  // STATUS is a read-only register
					ret_val = (PFdword)pfL6470dataTransfer(index, 0, 16);
					break;
		
    default:
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
					for(j=l6470devicesConnected-1; j>=0; --j)
					{
						if( j ==  index )
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, ((PFbyte)value), (PFword*)&ret_val);
						else
							l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
					}
					l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);
					break;
  }
	
  return ret_val;
}

static PFdword pfL6470dataTransfer(PFchar index, PFdword value, PFbyte bit_len)
{
  volatile PFdword ret_val=0,mask; 
	volatile PFword temp=0,dummyRead=0;
	volatile PFchar j=0;
	
  PFbyte byte_len = bit_len/8;    
  if (bit_len%8 > 0) 
		byte_len++;  

  mask = 0xffffffff >> (32-bit_len);
  if (value > mask) 
		value = mask;

  if (byte_len == 3) 
	{
		l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
		for(j=l6470devicesConnected-1; j>=0; --j)
		{
			if( j ==  index )
				l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, ((PFbyte)(value>>16)), &temp);
			else
				l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
		}
		l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);

		temp &= 0x00ff;
		ret_val |= ((PFbyte)temp)<<16;
  } 
	
  if (byte_len >= 2) 
	{
		l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
		for(j=l6470devicesConnected-1; j>=0; --j)
		{
			if( j ==  index )
				l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, ((PFbyte)(value>>8)), &temp);
			else
				l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
		}
		l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);

		temp &= 0x00ff;
		ret_val |= ((PFbyte)temp)<<8;
  }
	
  if (byte_len >= 1) 
	{
		l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,0);
		for(j=l6470devicesConnected-1; j>=0; --j)
		{
			if( j ==  index )
				l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, ((PFbyte)value), &temp);
			else
				l6470ConfigStruct[j].pfSpiExchangeByte(&l6470ConfigStruct[index].devId, L6470_NOP, &dummyRead);
		}
		l6470ConfigStruct[0].pfSpiChipSelect(&l6470ConfigStruct[index].devId,1);

		temp &= 0x00ff;
		ret_val |= (PFbyte)temp;
  }

  return (ret_val & mask);
}

