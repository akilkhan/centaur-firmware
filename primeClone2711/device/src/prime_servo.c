#include "prime_framework.h"
#include "prime_servo.h"

static PFCfgServo servoConfigStruct [ MAX_SERVO_SUPPORTED ]={0};
static PFEnBoolean servoInitFlag [MAX_SERVO_SUPPORTED] = {0}; 
static PFdword servoCountPerDegree[MAX_SERVO_SUPPORTED] = {0};   // servo count per degree multiple of 100
static PFbyte servoCurrentPosition[MAX_SERVO_SUPPORTED] = {0};  // current servo position in degree
static volatile PFbyte servo_device_count = 0;

PFEnStatus pfServoOpen(PFbyte *devId, PFpCfgServo servoConfig, PFbyte device_count)
{
	int i,j;
	volatile PFEnStatus status;
	
#if (PF_SERVO_DEBUG == 1)
	if( (devId == NULL) || (servoConfig == NULL) )
		return enStatusInvArgs;
	if(device_count >= MAX_SERVO_SUPPORTED)
		return enStatusInvArgs;
	
#endif	// #if (PF_SERVO_DEBUG == 1)
		
	for ( i=0; i< device_count; i++)
	{
		for ( j=0; j< MAX_SERVO_SUPPORTED; j++)
		{
			if ( servoInitFlag[j] == enBooleanFalse)
			{
				status = pfMemCopy(&servoConfigStruct[j], &servoConfig[i], (sizeof(PFCfgServo)));
				status = servo_count_calculator ( &servoConfigStruct[j]);
				if(status != enStatusSuccess)
				{
					return status;
				}
				else
				{
					servoConfigStruct[j].pfPwmUpdateMatchRegister((servoConfigStruct[j].channel+1), servoConfigStruct[j].pulseDurationForPosition0degree);
					servoConfigStruct[j].pfPwmUpdateMatchRegister(0,servoConfigStruct[j].refreshRate);
	
					servoCountPerDegree[j] = ( ( servoConfigStruct[j].pulseDurationForPosition180degree - servoConfigStruct[j].pulseDurationForPosition0degree ) * 100 ) / 180;   //count per degree multiplied by 100
	
					servo_device_count++;
					devId[i]=j;
					servoInitFlag[j]= enBooleanTrue;
					servoCurrentPosition[j]=0;
					break;
				}
			}
		}
		if (j== MAX_SERVO_SUPPORTED)
			return enStatusError;
	}
	return enStatusSuccess;
}

PFEnStatus pfServoClose(PFbyte* devId)
{
	volatile PFEnStatus status;
	
	if(	servoInitFlag[*devId]!= enBooleanTrue )
		return enStatusNotConfigured;
	
	if( *devId >= MAX_SERVO_SUPPORTED )
		return enStatusInvArgs;	
	
	servoConfigStruct[*devId].pfPwmStop();
	if(status != enStatusSuccess)
	{
		return status;
	}
	else
	{
		servo_device_count--;
		servoInitFlag[*devId] = enBooleanFalse;
	}
	return enStatusSuccess;
}

PFEnStatus pfServoMove(PFbyte* devId, PFbyte angle)
{
	volatile PFEnStatus status;
	volatile PFdword temp=0;
	
	if(	servoInitFlag[*devId] != enBooleanTrue )
	{
		return enStatusNotConfigured;
	}
	
	if(angle>180)
	{
		angle=180;
	}
		
	temp = servoConfigStruct[*devId].pulseDurationForPosition0degree + ( (servoCountPerDegree[*devId] * (PFdword)angle)/100 );
	servoConfigStruct[*devId].pfPwmUpdateMatchRegister( (servoConfigStruct[*devId].channel+1),temp);
	
	servoCurrentPosition[*devId] = angle;
	
	return enStatusSuccess;
}

PFEnStatus pfServoResetPosition(PFbyte* devId)
{
	if(	servoInitFlag[*devId] != enBooleanTrue)
	{
		return enStatusNotConfigured;
	}
	
	servoConfigStruct[*devId].pfPwmUpdateMatchRegister( (servoConfigStruct[*devId].channel+1), servoConfigStruct[*devId].pulseDurationForPosition0degree);
	
	servoCurrentPosition[*devId] = 0;
	
	return enStatusSuccess;
}

PFEnStatus pfServoGetCurrentPosition(PFbyte* devId, PFbyte* angle)
{
	volatile PFEnStatus status;
	volatile PFdword temp=0;
	
	if(	servoInitFlag[*devId] != enBooleanTrue )
	{
		return enStatusNotConfigured;
	}
	
	*angle = servoCurrentPosition[*devId];
		
	return enStatusSuccess;	
}

PFEnStatus servo_count_calculator ( PFpCfgServo servoConfig)
{
	PFdword pwm_tick_clock=0, clk_time=0;
	
	servoConfig->pfPwmGetTickFreq( &pwm_tick_clock);
	clk_time = (1000000000/pwm_tick_clock);
	servoConfig->pulseDurationForPosition0degree = ((servoConfig->pulseDurationForPosition0degree *1000)/clk_time);
	servoConfig->pulseDurationForPosition180degree = ((servoConfig->pulseDurationForPosition180degree* 1000)/clk_time);
	servoConfig->refreshRate = ((servoConfig->refreshRate *1000)/clk_time);
	
	return enStatusSuccess;
}
