/*-----------------------------------------------------------------------*/
/* MMCv3/SDv1/SDv2 (in SPI mode) control module  (C)ChaN, 2007           */
/*-----------------------------------------------------------------------*/
/* Only pfMmcSpiRcv(), pfMmcSpiSend(), pfMmcTimerProc() and some macros         */
/* are platform dependent.                                               */
/*-----------------------------------------------------------------------*/

#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_gpio.h"
#include "prime_mmc.h"


#define RES_OK 		enStatusSuccess			/* 0: Successful */
#define RES_ERROR	enStatusDiskError		/* 1: R/W Error */
#define RES_WRPRT	enStatusWriteProtected	/* 2: Write Protected */
#define RES_NOTRDY	enStatusDiskNotReady	/* 3: Not Ready */
#define RES_PARERR	enStatusInvArgs			/* 4: Invalid Parameter */

/* Command code for disk_ioctrl function */
#define CTRL_SYNC			0	/* Mandatory for write functions */
#define GET_SECTOR_COUNT	1	/* Mandatory for only f_mkfs() */
#define GET_SECTOR_SIZE		2
#define GET_BLOCK_SIZE		3	/* Mandatory for only f_mkfs() */
#define CTRL_POWER			4
#define CTRL_LOCK			5
#define CTRL_EJECT			6
#define CTRL_GET_STATE      7
#define CTRL_DISK_INIT      8
/* MMC/SDC command */
#define MMC_GET_TYPE		10
#define MMC_GET_CSD			11
#define MMC_GET_CID			12
#define MMC_GET_OCR			13
#define MMC_GET_SDSTAT		14

/* MMC/SDC specific ioctl command */
#define MMC_GET_TYPE		10	/* Get card type */
#define MMC_GET_CSD			11	/* Get CSD */
#define MMC_GET_CID			12	/* Get CID */
#define MMC_GET_OCR			13	/* Get OCR */
#define MMC_GET_SDSTAT		14	/* Get SD status */

/* Card type flags (CardType) */
#define CT_MMC				0x01	/* MMC ver 3 */
#define CT_SD1				0x02	/* SD ver 1 */
#define CT_SD2				0x04	/* SD ver 2 */
#define CT_SDC				(CT_SD1|CT_SD2)	/* SD */
#define CT_BLOCK			0x08	/* Block addressing */

/* Definitions for MMC/SDC command */
#define MMC_CMD0	(0x40+0)	/* GO_IDLE_STATE */
#define MMC_CMD1	(0x40+1)	/* SEND_OP_COND (MMC) */
#define	MMC_ACMD41	(0xC0+41)	/* SEND_OP_COND (SDC) */
#define MMC_CMD8	(0x40+8)	/* SEND_IF_COND */
#define MMC_CMD9	(0x40+9)	/* SEND_CSD */
#define MMC_CMD10	(0x40+10)	/* SEND_CID */
#define MMC_CMD12	(0x40+12)	/* STOP_TRANSMISSION */
#define MMC_ACMD13	(0xC0+13)	/* SD_STATUS (SDC) */
#define MMC_CMD16	(0x40+16)	/* SET_BLOCKLEN */
#define MMC_CMD17	(0x40+17)	/* READ_SINGLE_BLOCK */
#define MMC_CMD18	(0x40+18)	/* READ_MULTIPLE_BLOCK */
#define MMC_CMD23	(0x40+23)	/* SET_BLOCK_COUNT (MMC) */
#define	MMC_ACMD23	(0xC0+23)	/* SET_WR_BLK_ERASE_COUNT (SDC) */
#define MMC_CMD24	(0x40+24)	/* WRITE_BLOCK */
#define MMC_CMD25	(0x40+25)	/* WRITE_MULTIPLE_BLOCK */
#define MMC_CMD55	(0x40+55)	/* APP_CMD */
#define MMC_CMD58	(0x40+58)	/* READ_OCR */

#define	MMC_FCLK_SLOW()					/* Set slow clock (100k-400k) */
#define	MMC_FCLK_FAST()					/* Set fast clock (depends on the CSD) */


#define MMC_WAIT_READY_TIMEOUT_MS			1000
#define MMC_DATA_RCV_TIMEOUT_MS				500
#define MMC_INIT_TIMEOUT_MS					200
#define MMC_INIT_RETRY						200

static PFbyte pfMmcWaitReady(PFbyte* id);

static void pfMmcDeselect(PFbyte* id);

static PFEnBoolean pfMmcSelect(PFbyte* id);

static void pfMmcPowerOn(PFbyte *id);

static void pfMmcPowerOff(PFbyte *id);

static PFEnBoolean pfMmcReceiveDatablock(PFbyte* id, PFbyte* pBuf, PFdword btr);

static PFEnBoolean pfMmcWriteDatablock(PFbyte* id, const PFbyte *pBuf, PFbyte token);

static PFbyte pfMmcSendCommand(PFbyte* id, PFbyte cmd, PFdword arg);

static PFEnStatus pfMmcSoftInit(PFbyte* id);


/*--------------------------------------------------------------------------

   Module Private Functions

---------------------------------------------------------------------------*/

static PFCfgMmc mmcConfig[PF_MMC_MAX_DEVICE_SUPPORTED];

static PFEnBoolean mmcInit[PF_MMC_MAX_DEVICE_SUPPORTED] = {enBooleanFalse};

static PFbyte mmcSpiId[PF_MMC_MAX_DEVICE_SUPPORTED] = {0};

//static volatile PFEnStatus Stat[PF_MMC_MAX_DEVICE_SUPPORTED] = enStatusNotExist;	/* Disk status */

//static volatile PFbyte Timer1, Timer2;	/* 100Hz decrement timer */

static PFbyte CardType;			/* Card type flags */

PFEnStatus pfMmcOpen(PFbyte* id, PFpCfgMmc config)
{
	PFbyte index, freeInd = 0xFF, indAssigned = 0;
	if( (config == 0) || (id == 0) )
	{
		return enStatusInvArgs;
	}
	
	for(index = 0; index < PF_MMC_MAX_DEVICE_SUPPORTED; index++)
	{
		if(mmcInit[index] == enBooleanTrue)
		{
			if( (mmcConfig[index].gpioChipSelect.port == config->gpioChipSelect.port) && (mmcConfig[index].gpioChipSelect.pin == config->gpioChipSelect.pin) )
			{
				indAssigned = 1;
				break;
			}
		}
		else
		{
			if(freeInd == 0xFF)
			{
				freeInd = index;
			}
		}
	}
	
	if(indAssigned == 0)
	{
		*id = freeInd;
		pfMemCopy(&mmcConfig[freeInd], config, sizeof(PFCfgMmc));
		mmcConfig[freeInd].spiRegister(&mmcSpiId[*id], &mmcConfig[freeInd].gpioChipSelect);
	}
	
	
	return pfMmcSoftInit(id);
	
}

/*-----------------------------------------------------------------------*/
/* Transmit a byte to MMC via SPI  (Platform dependent)                  */
/*-----------------------------------------------------------------------*/

static void pfMmcSpiSend(PFbyte* id, PFbyte dat)
{
	PFword data;
	
	mmcConfig[*id].spiExchangeByte(&mmcSpiId[*id], (PFword)dat, &data);
}


/*-----------------------------------------------------------------------*/
/* Receive a byte from MMC via SPI  (Platform dependent)                 */
/*-----------------------------------------------------------------------*/

static PFbyte pfMmcSpiRcv (PFbyte* id)
{
    PFword data = 0;

    mmcConfig[*id].spiExchangeByte(&mmcSpiId[*id], 0xFF, &data);

    return (PFbyte)data;
}

/* Alternative macro to receive data fast */

#define MMC_SPI_RCV(DEV_ID, DATA_BUF) mmcConfig[*DEV_ID].spiExchangeByte(&mmcSpiId[*DEV_ID], 0, &DATA_BUF)




/*-----------------------------------------------------------------------*/
/* Wait for card ready                                                   */
/*-----------------------------------------------------------------------*/

static PFbyte pfMmcWaitReady(PFbyte* id)
{
	PFbyte res;
	PFdword timeout;
	timeout = pfTickSetTimeoutMs(MMC_WAIT_READY_TIMEOUT_MS);	/* Wait for ready in timeout of 500ms */
	pfMmcSpiRcv(id);
	do
	{
		res = pfMmcSpiRcv(id);
	}while ((res != 0xFF) && (pfTickCheckTimeout(timeout) == enBooleanFalse));

	return res;
}



/*-----------------------------------------------------------------------*/
/* Deselect the card and release SPI bus                                 */
/*-----------------------------------------------------------------------*/

static void pfMmcDeselect(PFbyte* id)
{
	PFEnStatus res = mmcConfig[*id].spiChipSelect(&mmcSpiId[*id], 1);
	if(res != enStatusSuccess)
	{
		//printf("Failed to unselect SPI slave (%d)!\r\n", res);
	}
	pfMmcSpiRcv(id);
}



/*-----------------------------------------------------------------------*/
/* Select the card and wait ready                                        */
/*-----------------------------------------------------------------------*/

static PFEnBoolean pfMmcSelect(PFbyte* id)	/* enBooleanTrue:Successful, enBooleanFalse:Timeout */
{
	PFEnStatus res = mmcConfig[*id].spiChipSelect(&mmcSpiId[*id], 0);
	if(res != enStatusSuccess)
	{
		//printf("Failed to unselect SPI slave (%d)!\r\n", res);
		return enBooleanFalse;
	}

	if (pfMmcWaitReady(id) != 0xFF) 
	{
		pfMmcDeselect(id);
		return enBooleanFalse;
	}
	return enBooleanTrue;
}

/*-----------------------------------------------------------------------*/
/* Power Control  (Platform dependent)                                   */
/*-----------------------------------------------------------------------*/
/* When the target system does not support socket power control, there   */
/* is nothing to do in these functions and pfMmcCheckPower always returns 1.   */

static void pfMmcPowerOn(PFbyte *id)
{

}

static void pfMmcPowerOff(PFbyte *id)
{
	
}

static int pfMmcCheckPower(PFbyte* id)		/* Socket power state: 0=off, 1=on */
{
	return 1;
}



/*-----------------------------------------------------------------------*/
/* Receive a data packet from MMC                                        */
/*-----------------------------------------------------------------------*/

static PFEnBoolean pfMmcReceiveDatablock(PFbyte* id, PFbyte* pBuf, PFdword btr)
{
	PFEnStatus status;
	PFbyte token;
	PFdword timeout;
	PFdword bRead = 0;

	timeout = pfTickSetTimeoutMs(MMC_DATA_RCV_TIMEOUT_MS);
	do 							/* Wait for data packet in timeout of 200ms */
	{	
		token = pfMmcSpiRcv(id);
	}while ((token == 0xFF) && (pfTickCheckTimeout(timeout) == enBooleanFalse));
	if(token != 0xFE) 	/* If not valid data token, retutn with error */
	{
		return enBooleanFalse;
	}

	status = mmcConfig[*id].spiRead(&mmcSpiId[*id], pBuf, btr, &bRead);
	if( (status != enStatusSuccess) || (bRead != btr) )
	{
		return enBooleanFalse;
	}
#if 0	
	do 							/* Receive the data block into buffer */
	{
		mmcConfig[*id].spiExchangeByte(&mmcSpiId[*id], 0, (PFword*)pBuf);
		pBuf++;
		mmcConfig[*id].spiExchangeByte(&mmcSpiId[*id], 0, (PFword*)pBuf);
		pBuf++;
		mmcConfig[*id].spiExchangeByte(&mmcSpiId[*id], 0, (PFword*)pBuf);
		pBuf++;
		mmcConfig[*id].spiExchangeByte(&mmcSpiId[*id], 0, (PFword*)pBuf);
		pBuf++;
	} while (btr -= 4);
#endif	
	pfMmcSpiRcv(id);						/* Discard CRC */
	pfMmcSpiRcv(id);

	return enBooleanTrue;					/* Return with success */
}



/*-----------------------------------------------------------------------*/
/* Send a data packet to MMC                                             */
/*-----------------------------------------------------------------------*/

#if PF_MMC_READONLY == 0
static PFEnBoolean pfMmcWriteDatablock(PFbyte* id, const PFbyte *pBuf, PFbyte token)
{
	PFbyte resp, wc;

	if(pfMmcWaitReady(id) != 0xFF) 
	{
		return enBooleanFalse;
	}

	pfMmcSpiSend(id, token);					/* Xmit data token */
	if (token != 0xFD) 	/* Is data token */
	{
		wc = 0;
		do 							/* Xmit the 512 byte data block to MMC */
		{
			pfMmcSpiSend(id, *pBuf++);
			pfMmcSpiSend(id, *pBuf++);
		} while (--wc);
		pfMmcSpiSend(id, 0xFF);					/* CRC (Dummy) */
		pfMmcSpiSend(id, 0xFF);
		resp = pfMmcSpiRcv(id);				/* Reveive data response */
		if ((resp & 0x1F) != 0x05)		/* If not accepted, return with error */
		{
			return enBooleanFalse;
		}
	}

	return enBooleanTrue;
}
#endif /* PF_MMC_READONLY */



/*-----------------------------------------------------------------------*/
/* Send a command packet to MMC                                          */
/*-----------------------------------------------------------------------*/

static PFbyte pfMmcSendCommand(PFbyte* id, PFbyte cmd, PFdword arg)
{
	PFbyte n, res;

	if (cmd & 0x80) 	/* ACMD<n> is the command sequense of MMC_CMD55-CMD<n> */
	{
		cmd &= 0x7F;
		res = pfMmcSendCommand(id, MMC_CMD55, 0);
		if (res > 1) 
		{
			return res;
		}
	}

	/* Select the card and wait for ready */
	pfMmcDeselect(id);
	if (!pfMmcSelect(id)) 
	{
		return 0xFF;
	}

	/* Send command packet */
	pfMmcSpiSend(id, 0x40 | cmd);						/* Start + Command index */
	pfMmcSpiSend(id, (PFbyte)(arg >> 24));		/* Argument[31..24] */
	pfMmcSpiSend(id, (PFbyte)(arg >> 16));		/* Argument[23..16] */
	pfMmcSpiSend(id, (PFbyte)(arg >> 8));			/* Argument[15..8] */
	pfMmcSpiSend(id, (PFbyte)arg);				/* Argument[7..0] */
	n = 0x01;							/* Dummy CRC + Stop */
	if (cmd == MMC_CMD0) 			/* Valid CRC for MMC_CMD0(0) */
	{
		n = 0x95;
	}
	if (cmd == MMC_CMD8) 			/* Valid CRC for MMC_CMD8(0x1AA) */
	{
		n = 0x87;
	}
	pfMmcSpiSend(id, n);

	/* Receive command response */
	if (cmd == MMC_CMD12) 		/* Skip a stuff byte when stop reading */
	{
		pfMmcSpiRcv(id);
	}
	n = 10;								/* Wait for a valid response in timeout of 10 attempts */
	do
	{
		res = pfMmcSpiRcv(id);
	}while ((res & 0x80) && --n);

	return res;			/* Return with the response value */
}

static PFEnStatus pfMmcSoftInit(PFbyte* id)
{
	PFbyte n, cmd, ty, ocr[4], idleState = 0;
	PFdword timeout;
	PFEnBoolean toStat = enBooleanFalse;
	PFEnStatus status;
	PFdword retry;

	if (pfMmcGetStatus(id) == enStatusNotExist) 	/* No card in the socket */
	{
		return enStatusNotExist;
	}

	pfMmcPowerOn(id);							/* Force socket power on */
	MMC_FCLK_SLOW();
	for (n = 10; n; n--) 				/* 80 dummy clocks */
	{
		pfMmcSpiRcv(id);	
	}

	ty = 0;
	retry = MMC_INIT_RETRY;	
	do
	{
		idleState = pfMmcSendCommand(id, MMC_CMD0, 0);
		retry--;
	}while( (idleState != 1) && (retry != 0) );
	//if (pfMmcSendCommand(id, MMC_CMD0, 0) == 1) 						/* Enter Idle state */
	if (idleState == 1) 						/* Enter Idle state */
	{
		timeout = pfTickSetTimeoutMs(MMC_INIT_TIMEOUT_MS);						/* Initialization timeout of 1000 msec */
		if (pfMmcSendCommand(id, MMC_CMD8, 0x1AA) == 1) 				/* SDHC */
		{
			for (n = 0; n < 4; n++) ocr[n] = pfMmcSpiRcv(id);		/* Get trailing return value of R7 resp */
			if (ocr[2] == 0x01 && ocr[3] == 0xAA) 						/* The card can work at vdd range of 2.7-3.6V */
			{
				while ((toStat == enBooleanFalse) && pfMmcSendCommand(id, MMC_ACMD41, 1UL << 30)) /* Wait for leaving idle state (MMC_ACMD41 with HCS bit) */
				{
					toStat = pfTickCheckTimeout(timeout);
				}
				if ((toStat == enBooleanFalse) && pfMmcSendCommand(id, MMC_CMD58, 0) == 0) 				/* Check CCS bit in the OCR */
				{
					for (n = 0; n < 4; n++) ocr[n] = pfMmcSpiRcv(id);
					{
						ty = (ocr[0] & 0x40) ? CT_SD2 | CT_BLOCK : CT_SD2;	/* SDv2 */
					}
				}
			}
		} 
		else 									/* SDSC or MMC */
		{
			if (pfMmcSendCommand(id, MMC_ACMD41, 0) <= 1) 	
			{
				ty = CT_SD1; cmd = MMC_ACMD41;	/* SDv1 */
			} 
			else 
			{
				ty = CT_MMC; cmd = MMC_CMD1;	/* MMCv3 */
			}
			while ((toStat == enBooleanFalse) && pfMmcSendCommand(id, cmd, 0));			/* Wait for leaving idle state */
			{
				toStat = pfTickCheckTimeout(timeout);
			}
			if( (toStat == enBooleanFalse) || (pfMmcSendCommand(id, MMC_CMD16, 512) != 0) )	/* Set R/W block length to 512 */
			{
				ty = 0;
			}
		}
	}
	CardType = ty;
	pfMmcDeselect(id);

	if (ty) 								/* Initialization succeded */
	{
		mmcInit[*id] = enBooleanTrue;
		status = enStatusSuccess;			/* Clear MMC_STA_NOINIT */
		MMC_FCLK_FAST();
	} 
	else 									/* Initialization failed */
	{			
		pfMmcPowerOff(id);
		status = enStatusNotConfigured;
	}

	return status;
}


/*--------------------------------------------------------------------------

   Public Functions

---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------*/
/* Initialize Disk Drive                                                 */
/*-----------------------------------------------------------------------*/

PFEnStatus pfMmcInitialize(
	PFbyte* id		/* Pointer to spi and gpio drivers */
)
{
/*
	if (!id) return MMC_STA_NOINIT;

	PFEnStatus res = phihal_GpioPinConfigure(id->hGpioDev, id->uPinCd,
			phihal_gpio_enPinSpeedFast, phihal_gpio_enPinConfigSTANDARD, phihal_gpio_enPinDirIN);
	if(res != enStatusSuccess)
	{
		printf("Failed to configure MMC_CD (%d)!\r\n", res);
		return -1;
	}

	pfMmcTimerProc(id);			// we have to do that to have valid Stat variable. 
*/
	return pfMmcSoftInit(id);
}



/*-----------------------------------------------------------------------*/
/* Get Disk Status                                                       */
/*-----------------------------------------------------------------------*/

PFEnStatus pfMmcGetStatus(PFbyte* id)
{
	if(mmcConfig[*id].useCardDetect == enBooleanTrue)
	{
		if( (pfGpioPortRead(mmcConfig[*id].gpioCardDetect.port) & mmcConfig[*id].gpioCardDetect.pin) != 0 )
		{
			mmcInit[*id] = enBooleanFalse;
			return enStatusNotExist;
		}
	}

	if(mmcInit[*id] == enBooleanTrue)
	{
		return enStatusSuccess;
	}
	else
	{
		return enStatusNotConfigured;
	}
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

PFEnStatus pfMmcRead(PFbyte* id, PFbyte *pBuf, PFdword sector, PFbyte uCount)
{
	if (!id || !uCount) 
	{
		return RES_PARERR;
	}
	if (pfMmcGetStatus(id) != enStatusSuccess) 
	{
		return RES_NOTRDY;
	}

	if (!(CardType & CT_BLOCK))				/* Convert to byte address if needed */
	{
		sector *= 512;
	}

	if (uCount == 1) {	/* Single block read */
		if ((pfMmcSendCommand(id, MMC_CMD17, sector) == 0)	/* READ_SINGLE_BLOCK */
			&& pfMmcReceiveDatablock(id, pBuf, 512))
		{
		    uCount = 0;
		}
	}
	else 				/* Multiple block read */
	{				
		if (pfMmcSendCommand(id, MMC_CMD18, sector) == 0) 				/* READ_MULTIPLE_BLOCK */
		{	
			do 
			{
				if (!pfMmcReceiveDatablock(id, pBuf, 512)) 
				{
					break;
				}
				pBuf += 512;
			} while (--uCount);
			pfMmcSendCommand(id, MMC_CMD12, 0);				/* STOP_TRANSMISSION */
		}
	}
	pfMmcDeselect(id);

	return uCount ? RES_ERROR : RES_OK;
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if PF_MMC_READONLY == 0
PFEnStatus pfMmcWrite (PFbyte* id, const PFbyte *pBuf, PFdword sector, PFbyte uCount)
{
	PFEnStatus status;
	if (!id || !uCount) 
	{
		return RES_PARERR;
	}
	
	status = pfMmcGetStatus(id);
	if(status != enStatusSuccess)
	{
		return status;
	}
	
	if (!(CardType & CT_BLOCK)) 	/* Convert to byte address if needed */
	{
		sector *= 512;
	}		

	if (uCount == 1) 	/* Single block write */
	{
		if ((pfMmcSendCommand(id, MMC_CMD24, sector) == 0)	/* WRITE_BLOCK */
			&& pfMmcWriteDatablock(id, pBuf, 0xFE))
		{
		    uCount = 0;
		}
	}
	else 				/* Multiple block write */
	{
		if (CardType & CT_SDC) 
		{
			pfMmcSendCommand(id, MMC_ACMD23, uCount);
		}
		if (pfMmcSendCommand(id, MMC_CMD25, sector) == 0) 				/* WRITE_MULTIPLE_BLOCK */
		{
			do 
			{
				if (!pfMmcWriteDatablock(id, pBuf, 0xFC)) 
				{
					break;
				}
				pBuf += 512;
			} while (--uCount);
			if (!pfMmcWriteDatablock(id, 0, 0xFD))	/* STOP_TRAN token */
			{
			    uCount = 1;
			}
		}
	}
	pfMmcDeselect(id);

	return uCount ? RES_ERROR : RES_OK;
}
#endif /* PF_MMC_READONLY == 0 */



/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

#if PF_MMC_USE_IOCTL != 0
PFEnStatus pfMmcIoCtrl(PFbyte* id, PFbyte ctrl,	void *pBuf)
{
	PFEnStatus res;
	PFbyte n, csd[16], *ptr = pBuf;
	uint16_t csize;


	if (!id) return RES_PARERR;

	res = RES_ERROR;

	if (ctrl == CTRL_POWER) 
	{
		switch (*ptr) 
		{
			case 0:		/* Sub control code == 0 (POWER_OFF) */
				if (pfMmcCheckPower(id))
				{
					pfMmcPowerOff(id);		/* Power off */
				}
				res = RES_OK;
				break;
			case 1:		/* Sub control code == 1 (POWER_ON) */
				pfMmcPowerOn(id);				/* Power on */
				res = RES_OK;
				break;
			case 2:		/* Sub control code == 2 (POWER_GET) */
				*(ptr+1) = (PFbyte)pfMmcCheckPower(id);
				res = RES_OK;
				break;
			default :
				res = RES_PARERR;
		}
	}
	else if (ctrl == CTRL_GET_STATE)
	{
		*ptr = pfMmcGetStatus(id);
		res = RES_OK;
	}
	else if (ctrl == CTRL_DISK_INIT)
	{
		if (ptr != PF_NULL)
		{
			*ptr = pfMmcSoftInit(id);
		}
		else 
		{
			res = pfMmcSoftInit(id);
		}
		res = RES_OK;
	}
	else 
	{
		if (pfMmcGetStatus(id) != enStatusSuccess) 
		{
			return RES_NOTRDY;
		}

		switch (ctrl) 
		{
			case CTRL_SYNC :		/* Make sure that no pending write process. Do not remove this or written sector might not left updated. */
				if (pfMmcSelect(id)) 
				{
					res = RES_OK;
					pfMmcDeselect(id);
				}
				break;

			case GET_SECTOR_COUNT :	/* Get number of sectors on the disk (PFdword) */
				if ((pfMmcSendCommand(id, MMC_CMD9, 0) == 0) && pfMmcReceiveDatablock(id, csd, 16)) 
				{
					if ((csd[0] >> 6) == 1) 	/* SDC ver 2.00 */
					{
						csize = csd[9] + ((uint16_t)csd[8] << 8) + 1;
						*(PFdword*)pBuf = (PFdword)csize << 10;
					} 
					else 								/* SDC ver 1.XX or MMC*/
					{
						n = (csd[5] & 15) + ((csd[10] & 128) >> 7) + ((csd[9] & 3) << 1) + 2;
						csize = (csd[8] >> 6) + ((uint16_t)csd[7] << 2) + ((uint16_t)(csd[6] & 3) << 10) + 1;
						*(PFdword*)pBuf = (PFdword)csize << (n - 9);
					}
					res = RES_OK;
				}
				break;

			case GET_SECTOR_SIZE :	/* Get R/W sector size (uint16_t) */
				*(uint16_t*)pBuf = 512;
				res = RES_OK;
				break;

			case GET_BLOCK_SIZE :	/* Get erase block size in unit of sector (PFdword) */
				if (CardType & CT_SD2) 	/* SDC ver 2.00 */
				{
					if (pfMmcSendCommand(id, MMC_ACMD13, 0) == 0) 	/* Read SD status */
					{
						pfMmcSpiRcv(id);
						if (pfMmcReceiveDatablock(id, csd, 16)) 				/* Read partial block */
						{
							for (n = 64 - 16; n; n--) 						/* Purge trailing data */
							{
								pfMmcSpiRcv(id);	
							}
							*(PFdword*)pBuf = 16UL << (csd[10] >> 4);
							res = RES_OK;
						}
					}
				} 
				else 										/* SDC ver 1.XX or MMC */
				{
					if ((pfMmcSendCommand(id, MMC_CMD9, 0) == 0) && pfMmcReceiveDatablock(id, csd, 16)) 	/* Read CSD */
					{	
						if (CardType & CT_SD1) 	/* SDC ver 1.XX */
						{
							*(PFdword*)pBuf = (((csd[10] & 63) << 1) + ((uint16_t)(csd[11] & 128) >> 7) + 1) << ((csd[13] >> 6) - 1);
						} 
						else 									/* MMC */
						{
							*(PFdword*)pBuf = ((uint16_t)((csd[10] & 124) >> 2) + 1) * (((csd[11] & 3) << 3) + ((csd[11] & 224) >> 5) + 1);
						}
						res = RES_OK;
					}
				}
				break;

			case MMC_GET_TYPE :		/* Get card type flags (1 byte) */
				*ptr = CardType;
				res = RES_OK;
				break;

			case MMC_GET_CSD :		/* Receive CSD as a data block (16 bytes) */
				if (pfMmcSendCommand(id, MMC_CMD9, 0) == 0		/* READ_CSD */
					&& pfMmcReceiveDatablock(id, ptr, 16))
				{
					res = RES_OK;
				}
				break;

			case MMC_GET_CID :		/* Receive CID as a data block (16 bytes) */
				if (pfMmcSendCommand(id, MMC_CMD10, 0) == 0		/* READ_CID */
					&& pfMmcReceiveDatablock(id, ptr, 16))
				{
					res = RES_OK;
				}
				break;

			case MMC_GET_OCR :		/* Receive OCR as an R3 resp (4 bytes) */
				if (pfMmcSendCommand(id, MMC_CMD58, 0) == 0) 	/* READ_OCR */
				{
					for (n = 4; n; n--) 
					{
						*ptr++ = pfMmcSpiRcv(id);
					}
					res = RES_OK;
				}
				break;

			case MMC_GET_SDSTAT :	/* Receive SD statsu as a data block (64 bytes) */
				if (pfMmcSendCommand(id, MMC_ACMD13, 0) == 0) 	/* SD_STATUS */
				{
					pfMmcSpiRcv(id);
					if (pfMmcReceiveDatablock(id, ptr, 64))
					{
						res = RES_OK;
					}
				}
				break;

			default:
				res = RES_PARERR;
		}

		pfMmcDeselect(id);
	}

	return res;
}
#endif /* PF_MMC_USE_IOCTL != 0 */


/*-----------------------------------------------------------------------*/
/* Device Timer Interrupt Procedure  (Platform dependent)                */
/*-----------------------------------------------------------------------*/
/* This function must be called in period of 10ms                        */

void pfMmcTimerProc (PFbyte* id)
{
/*
	static PFbyte pv;
	
	PFbyte n, s;
	phihal_gpio_ENPinState eState;

	n = Timer1;						// 100Hz decrement timer 
	if (n) Timer1 = --n;
	n = Timer2;
	if (n) Timer2 = --n;

	n = pv;

	if ( enStatusSuccess == phihal_GpioPinStateGet(id->hGpioDev, id->uPinCd, &eState) )
	{
		pv = (eState != phihal_gpio_enPinStateLOW);

		if (n == pv) {					// Have contacts stabled? 
			s = Stat;

			// write protect NOT supported

			// check card detect 
			if (pv)			       // (Socket empty) 
				s = enStatusNotExist;
			else				       // (Card inserted) 
				s &= ~MMC_STA_NODISK;

			Stat = s;
		}
	}
	*/
}

