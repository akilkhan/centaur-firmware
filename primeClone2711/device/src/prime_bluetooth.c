#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_bluetooth.h"
//#include <string.h>
static PFCfgBluetooth deviceConfigs[PF_MAX_BLUETOOTH_SUPPORTED];
static PFEnBoolean bluetoothDevice[PF_MAX_BLUETOOTH_SUPPORTED] = {enBooleanFalse};	
static PFbyte idBluetooth[PF_MAX_BLUETOOTH_SUPPORTED];
PFbyte* pfstrtok(PFbyte * str, PFbyte *comp);
PFbyte pfbluetoothtoken(PFbyte *data,PFBluetoothInquiryName x[],PFbyte *cnt);

void btDelayUs(PFdword val)
{
	unsigned int loop1,loop2;
	
	for(loop1=0;loop1 < val;loop1++ )
	for(loop2 =0;loop2< 22;loop2++ );
}


void btDelay(PFdword del)
{
	PFdword del1, del2;
	for(del1 = 0; del1 < del; del1++)
	{
		for(del2 = 2000; del2 > 0; del2--);
	}
}

PFEnStatus pfBluetoothOpen(PFbyte *idArray,PFpCfgBluetooth config, PFbyte devicecnt)
{
    PFbyte i=0,j=0;
    PFdword count_md=0; PFdword read_count_md=0;
    PFbyte  md_data[7];
    PFdword count=0;	  
    PFdword read_count_iq=0;
    PFbyte  name_data[20];	//Make_Discoverable function received data
		PFbyte index=0;
		if(devicecnt > PF_MAX_BLUETOOTH_SUPPORTED)
		{
				return enStatusError;
		}
    for(index=0; index < devicecnt;index++)
    {
			if(bluetoothDevice[index]==enBooleanFalse)
			{
				config[index].comWrite((PFbyte*)"BC:MD=01",8);//Sending Command BC:MD=01 on UART2 to Bluetooth
				config[index].comWriteByte(0x0D);		 //Sending <CR>
				config[index].comWriteByte(0x0A);		 //Sending <LF>
				btDelay(1000);
				config[index].comGetRxBufferCount(&count_md);
				if(count_md>=1)
				{
					config[index].comRead(md_data,7,&read_count_md);
				}
				config[index].comWrite((PFbyte*)"BC:NM=",6);	//Sending Command BC:NM on UART2 to Bluetooth
				config[index].comWrite(config[i].name,10);
				config[index].comWriteByte(0x0D);	   //Sending <CR>
				config[index].comWriteByte(0x0A);	   //Sendinf <LF>
				btDelay(1900);
				config[index].comGetRxBufferCount(&count);
				if(count==4)
				{
					config[index].comRead(name_data,count,&read_count_iq);
				} 
				idArray[j]=index;
				idBluetooth[index]=index;
				bluetoothDevice[index] = enBooleanTrue;
			}
			  
		pfMemCopy(&deviceConfigs[index],&config[j],(sizeof(PFCfgBluetooth)));
		j++; 
		if(j==devicecnt)
		break;
	}
 	 return  enStatusSuccess;
}
PFEnStatus pfBluetoothClose(PFbyte id)
{
  PFbyte i=0;	
	#if (PF_BLUETOOTH_DEBUG == 1)
	if(id>=PF_MAX_BLUETOOTH_SUPPORTED)
		return enStatusInvArgs;
	if(bluetoothDevice[i]==enBooleanFalse)
		return enStatusNotConfigured;
	#endif
	
	for(i=0; i< PF_MAX_BLUETOOTH_SUPPORTED;i++)
	{
		if(idBluetooth[i]==id )
		{
			if(bluetoothDevice[i]==enBooleanFalse)
			{
				return enStatusNotConfigured;
			}
			else
			{
				break;
			}						
		}	
	}	
    bluetoothDevice[i]=enBooleanFalse;			
    return enStatusSuccess;
}	 

PFEnStatus pfBluetoothGetName(PFbyte id, PFbyte *data)
{
	PFbyte i=0;
	PFdword count=0;	  
	PFdword read_count_iq=0;
	#if (PF_BLUETOOTH_DEBUG == 1)
	if(id>=PF_MAX_BLUETOOTH_SUPPORTED)
		return enStatusInvArgs;
	if(bluetoothDevice[i]==enBooleanFalse)
		return enStatusNotConfigured;
   #endif
	
	for(i=0; i< PF_MAX_BLUETOOTH_SUPPORTED;i++)
	{
		if(idBluetooth[i]==id )
		{
			if(bluetoothDevice[i]==enBooleanFalse)
			{
				return enStatusNotConfigured;
			}
			else
			{
				break;
			}
		}
	}
	deviceConfigs[i].comWrite((PFbyte*)"BC:NM",5);   //Sending Command BC:NM on UART2 to Bluetooth
	deviceConfigs[i].comWriteByte(0x0D);	   //Sending <CR>
	deviceConfigs[i].comWriteByte(0x0A);	   //Sendinf <LF>
	btDelay(1900);
	deviceConfigs[i].comGetRxBufferCount(&count);
	if(count>=8)
	{
		deviceConfigs[i].comRead(data,count,&read_count_iq);
		return enStatusSuccess ;		 
	}
	return enStatusError;
}

PFEnStatus pfBluetoothSetName(PFbyte id,PFbyte *data,PFbyte len)
{  
	PFbyte i=0;
	#if (PF_BLUETOOTH_DEBUG == 1)
	if(id>=PF_MAX_BLUETOOTH_SUPPORTED)
		return enStatusInvArgs;
	if(bluetoothDevice[i]==enBooleanFalse)
		return enStatusNotConfigured;
  #endif
	
	for(i=0; i< PF_MAX_BLUETOOTH_SUPPORTED;i++)
	{
		if(idBluetooth[i]==id )
		{
			if(bluetoothDevice[i]==enBooleanFalse)
			{
				return enStatusNotConfigured;
			}
			else
			{
				break;
			}
		}
	}
	if(len<=20)
	{
		deviceConfigs[i].comWrite((PFbyte*)"BC:NM=",6);
		deviceConfigs[i].comWrite(data,len);
		deviceConfigs[i].comWriteByte(0x0D);
		deviceConfigs[i].comWriteByte(0x0A);
		return enStatusSuccess;		
	}
	else 
	{
		return enStatusError;
	}
} 
PFEnStatus pfBluetoothGetAddress(PFbyte id,PFbyte *data)
{
	PFbyte i=0;
	PFdword count=0;	  
	PFdword read_count=0;
	#if (PF_BLUETOOTH_DEBUG == 1)
	if(id>=PF_MAX_BLUETOOTH_SUPPORTED)
		return enStatusInvArgs;
	if(bluetoothDevice[i]==enBooleanFalse)
		return enStatusNotConfigured;
  #endif
	
	for(i=0; i< PF_MAX_BLUETOOTH_SUPPORTED;i++)
	{
		if(idBluetooth[i]==id )
		{
			if(bluetoothDevice[i]==enBooleanFalse)
			{
				return enStatusNotConfigured;
			}	
			else
			{
				break;
			}
		}
	}
	deviceConfigs[i].comWrite((PFbyte*)"BC:AD",5);
	deviceConfigs[i].comWriteByte(0x0D);
	deviceConfigs[i].comWriteByte(0X0A);
	btDelay(300000);
	deviceConfigs[i].comGetRxBufferCount(&count);
	if(count>=17 )
	{
		deviceConfigs[i].comRead(data,17,&read_count);
		return enStatusSuccess;
	}	
    return enStatusError;
}
PFEnStatus pfBluetoothInquiry( PFbyte id,PFBluetoothInquiryName data[], PFbyte* devcnt)
{  
	PFbyte i=0;
	PFdword count=0;
	PFdword read_count_iq=0;
	PFbyte  iq_data[256];	
#if (PF_BLUETOOTH_DEBUG == 1)
	if(id>=PF_MAX_BLUETOOTH_SUPPORTED)
		return enStatusInvArgs;
	if(bluetoothDevice[i]==enBooleanFalse)
		return enStatusNotConfigured;
#endif
	
	for(i=0; i< PF_MAX_BLUETOOTH_SUPPORTED;i++)
	{
		if(idBluetooth[i]==id )
		{
			if(bluetoothDevice[i]==enBooleanFalse)
			{	
				return enStatusNotConfigured;
			}
			else
			{
				break;
			}
		}
	}
	deviceConfigs[i].comWrite((PFbyte*)"BC:IQ",5);		  //Sending Command BC:NM on UART2 to Bluetooth
	deviceConfigs[i].comWriteByte(0x0D);			 //Sending <CR>
	deviceConfigs[i].comWriteByte(0x0A);			//Sending <LF>
 	btDelay(400000);				   
	deviceConfigs[i].comGetRxBufferCount(&count);
	if(count>=1)
	{
  		deviceConfigs[i].comRead(iq_data,count,&read_count_iq);  
        pfbluetoothtoken(iq_data,data,devcnt);
        return enStatusSuccess;				
	}	 
			
    return enStatusError;	       
}

PFEnStatus pfBluetoothConnect(PFbyte id,PFbyte *address)
{ 
	PFbyte i=0;
	PFdword count=0;
	PFdword read_count_ct=0;
	PFbyte  connect_data[256];
	#if (PF_BLUETOOTH_DEBUG == 1)
	if(id>=PF_MAX_BLUETOOTH_SUPPORTED)
		return enStatusInvArgs;
	if(bluetoothDevice[i]==enBooleanFalse)
		return enStatusNotConfigured;
  #endif
	
	for(i=0; i< PF_MAX_BLUETOOTH_SUPPORTED;i++)
	{
		if(idBluetooth[i]==id )
		{
			if(bluetoothDevice[i]==enBooleanFalse)
			{
					return enStatusNotConfigured;
			}				
			else
			{
				break;
			}
		}
	}
	deviceConfigs[i].comWrite((PFbyte*)"BC:CT=",6);		 //Sending Command BC:CT on UART2 to Bluetooth
	deviceConfigs[i].comWrite(address,12);		 //	Sending the address of the bluetooth device
	deviceConfigs[i].comWriteByte(0x0D);			 //Sending <CR>
	deviceConfigs[i].comWriteByte(0x0A);			 //Sending <LF>
	btDelay(300000);
	deviceConfigs[i].comGetRxBufferCount(&count);
	if(count>=1)
	{
		deviceConfigs[i].comRead(connect_data,count ,&read_count_ct);
		return enStatusSuccess;
	}
	return enStatusError;
}
 
PFEnStatus pfBluetoothSendData(PFbyte id,PFbyte* data,PFdword size)
{
   PFbyte i=0;	
	#if (PF_BLUETOOTH_DEBUG == 1)
	if(id>=PF_MAX_BLUETOOTH_SUPPORTED)
		return enStatusInvArgs;
	if(bluetoothDevice[i]==enBooleanFalse)
		return enStatusNotConfigured;
  #endif
	
	for(i=0; i< PF_MAX_BLUETOOTH_SUPPORTED;i++)
	{
		if(idBluetooth[i]==id )
		{
			if(bluetoothDevice[i]==enBooleanFalse)
			{
				return enStatusNotConfigured;
			}		 
			else
			{
				break;
			}	
		}
	}		
	deviceConfigs[i].comWrite(data,size);	
	return enStatusSuccess; 
}	 

PFEnStatus pfBluetoothReceiveData(PFbyte id,PFbyte *data,PFdword *ncount)
{
	PFdword count=0;
  PFdword read_count_iq=0;
	PFbyte i=0;	
	#if (PF_BLUETOOTH_DEBUG == 1)
	if(id>=PF_MAX_BLUETOOTH_SUPPORTED)
		return enStatusInvArgs;
	if(bluetoothDevice[i]==enBooleanFalse)
		return enStatusNotConfigured;
  #endif
	
	for(i=0; i< PF_MAX_BLUETOOTH_SUPPORTED;i++)
	{
		if(idBluetooth[i]==id )
		{
			if(bluetoothDevice[i]==enBooleanFalse)
			{
				return enStatusNotConfigured;
			}			
			else
			{
				break;
			}
		}
	}
    deviceConfigs[i].comGetRxBufferCount(&count);
    if(count>1)
    {
		deviceConfigs[i].comRead(data,count,&read_count_iq);
		*ncount=count;
		return enStatusSuccess;
	  }
	return enStatusError;
}

PFbyte pfbluetoothtoken(PFbyte* data,PFBluetoothInquiryName x[],PFbyte *cnt)
{
	PFword i;
	PFbyte *p;
//  char *y
	PFbyte *array[24];
	i = 0;
	p = pfstrtok(data,(PFbyte*)",\n\r");
	while (p != NULL)
	{
		array[i++] = p;
		p = pfstrtok(PF_NULL_PTR,(PFbyte*) ",\n\r");
	}
	if(data[11]=='0')
   {
		*cnt=(data[11]-'0');
		return 0;
   }
	if(data[11]=='1')
   {

		pfStrCopy(x[0].address,array[4]);
		pfStrCopy(x[0].name,array[6]);
		*cnt=(data[11]-'0');
		return 0;
   }

   if(data[11]=='2')
    {

		pfStrCopy(x[0].address,array[4]);
		pfStrCopy(x[1].address,array[8]);
		pfStrCopy(x[0].name,array[6]);
		pfStrCopy(x[1].name,array[10]);

		*cnt=(data[11]-'0');
		return 0;
    }
    if(data[11]=='3')
    {
		pfStrCopy(x[0].address,array[4]);
		pfStrCopy(x[1].address,array[8]);
		pfStrCopy(x[2].address,array[12]);
		pfStrCopy(x[0].name,array[6]);
		pfStrCopy(x[1].name,array[10]);
		pfStrCopy(x[2].name,array[14]);
		*cnt=(data[11]-'0');
		return 0;
	}
	return 0;	
}

PFbyte* pfstrtok(PFbyte * str, PFbyte *comp)
{
	PFword pos = 0;
	PFbyte *s;
	PFword start = pos;
	PFword j = 0;
	if(str!=NULL)
	s = str;
	while(s[pos] != '\0')
	{
		j = 0;	
		while(comp[j] != '\0')
		{

			if(s[pos] == comp[j])
			{

				s[pos] = '\0';
				pos = pos+1;

				if(s[start] != '\0')
					return &s[start];
				else
				{

					start = pos;
					pos--;
					break;
				}
			}
			j++;
		}
		pos++;
	}
	s[pos] = '\0';
	if(s[start] == '\0')
		return NULL;
	else
		return &s[start];
}



