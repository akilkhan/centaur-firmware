#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_hmc5883compass.h"
#include "math.h"

#define PF_HMC_PI		3.14159265358979323846

static PFCfgHMC5883compass hmc5883Config[PF_MAX_HMC5883_SUPPORTED];
static PFbyte hmc5883compassInitFlag[PF_MAX_HMC5883_SUPPORTED] = {0};                                                 /** compass initialization test flag */
static PFbyte hmc5883DeviceCount=0;

float hmc5883compassScalingFactor[]={ 0.73, 0.92, 1.22, 1.52, 2.27, 2.56, 3.03, 4.35 };   /** array of compass scaling factor */

volatile PFbyte hmc5883compassScalingFactorIndex=1;                                       /** to copy scaling factor index passed by user */
volatile PFEnHMC5883compassMode hmc5883compassMode;                                       /** to copy compass operating mode passed by user */

/**
 * To write value to register of HMC5883l pointed by regAddr
 *
 * \param regAddr register address at which writeValue to be write
 *
 * \param writeValue value to be written
 *
 * \return status of write register operation
 */
static PFEnStatus pfHMC5883compassWriteReg(PFbyte devId, PFbyte regAddr, PFbyte writeValue);           /** to write individual Register */

/**
 * To read single register from HMC5883l
 *
 * \param regAddr register address to read that Register
 *
 * \param readValue pointer to copy data from Register
 *
 * \return status of read register operation
 */
static PFEnStatus pfHMC5883compassReadReg(PFbyte devId, PFbyte regAddr, PFbyte* readValue);

PFEnStatus pfHMC5883compassOpen(PFbyte* devId, PPFCfgHMC5883compass sensorConfig,PFbyte deviceCount )
{
	volatile PFbyte regA=0,regB=0,regC=0, i=0, j=0;
	volatile PFEnStatus stat;
	
	for(i=0; i<deviceCount; ++i)
	{
		for(j=0; j<PF_MAX_HMC5883_SUPPORTED; ++j)
		{
			if( hmc5883compassInitFlag[j] == enBooleanFalse )
			{
				devId[i]=j;
				
				pfMemCopy(&hmc5883Config[j], &sensorConfig[i], sizeof(PFCfgHMC5883compass));
				
				if( (sensorConfig[i].bias>2) || (sensorConfig[i].dataRate>6) || (sensorConfig[i].avgSample>3) )
						return enStatusInvArgs;
				else
				{
						regA= sensorConfig[i].bias | (sensorConfig[i].dataRate<<2) | (sensorConfig[i].avgSample<<5) ;
						stat=pfHMC5883compassWriteReg(devId[i], HMC5883_COMPASS_CONFIG_A,regA);
						if(stat!=enStatusSuccess)
								return stat;
				}

				if( sensorConfig[i].gain>7 )
						return enStatusInvArgs;
				else
				{
						regB=sensorConfig[i].gain;
						stat=pfHMC5883compassWriteReg(devId[i], HMC5883_COMPASS_CONFIG_B,regB);
						if(stat!=enStatusSuccess)
								return stat;

						hmc5883compassScalingFactorIndex=sensorConfig[i].gain;
				}

				if( sensorConfig[i].mode>2 )
						return enStatusInvArgs;
				else
				{
						regC=sensorConfig[i].mode;
						stat=pfHMC5883compassWriteReg(devId[i], HMC5883_COMPASS_MODE,regC);
						if(stat!=enStatusSuccess)
								return stat;

						hmc5883compassMode=sensorConfig[i].mode;
				}
				
				hmc5883compassInitFlag[j] = enBooleanTrue;
				++hmc5883DeviceCount;
				break;
			}
			if(j == PF_MAX_HMC5883_SUPPORTED)
				return enStatusError;
		}
	}
	return enStatusSuccess;
}

PFEnStatus pfHMC5883compassGetHeading(PFbyte devId, PFword* headingX, PFword* headingY, PFword* headingZ)
{
    volatile PFEnStatus stat;
    volatile PFbyte dataBuf[6]={0};
    volatile PFbyte temp=0;
    PFword i,j,readBytes=0;

    if( hmc5883compassInitFlag[devId] != enBooleanTrue )
        return enStatusNotConfigured;

    if(hmc5883compassMode==enHMC5883compassModeSingle)
    {
        stat=pfHMC5883compassWriteReg(devId, HMC5883_COMPASS_MODE,hmc5883compassMode);
        if(stat!=enStatusSuccess)
            return stat;
    }

    /** ( 6ms ) conversion time to allow completion of data conversion */
    for(j=0;j<6;++j)
    {    for(i=0;i<325;++i);    }

    /** point to data reg X msb */
    temp=HMC5883_COMPASS_DATAX_H;

    /** 0x3C regAddr  to set pointer to reg with addresses regAddr */
    stat=hmc5883Config[devId].pfIicWrite((PFbyte*)&temp, enBooleanTrue, HMC5883_COMPASS_SLAVE_ADDR, 1);
    if(stat!=enStatusSuccess)
    return stat;

    /** read 6 data registers */
    stat=hmc5883Config[devId].pfIicRead((PFbyte*)dataBuf, enBooleanTrue, HMC5883_COMPASS_SLAVE_ADDR, 6, &readBytes);
    if(stat!=enStatusSuccess)
    return stat;

    if(readBytes!=6)
    return enStatusError;

    *headingX= (dataBuf[0]<<8) | dataBuf[1];                                             /** copy higher and lower data register of X */
    *headingY= (dataBuf[4]<<8) | dataBuf[5];                                             /** copy higher and lower data register of Y */
    *headingZ= (dataBuf[2]<<8) | dataBuf[3];                                             /** copy higher and lower data register of Z */

    return enStatusSuccess;
}

PFEnStatus pfHMC5883compassGetMagnetism(PFbyte devId, PFword* magnetismX, PFword* magnetismY, PFword* magnetismZ)
{
    volatile PFEnStatus stat;
    PFword headX=0,headY=0,headZ=0;

    if( hmc5883compassInitFlag[devId] != enBooleanTrue )
    return enStatusNotConfigured;

    stat=pfHMC5883compassGetHeading(devId,&headX, &headY, &headZ);
    if(stat!=enStatusSuccess)
    return stat;

    *magnetismX= (PFword)(( (float)headX ) * (hmc5883compassScalingFactor[hmc5883compassScalingFactorIndex]));
    *magnetismY= (PFword)(( (float)headY ) * (hmc5883compassScalingFactor[hmc5883compassScalingFactorIndex]));
    *magnetismZ= (PFword)(( (float)headZ ) * (hmc5883compassScalingFactor[hmc5883compassScalingFactorIndex]));

    return enStatusSuccess;
}

PFEnStatus pfHMC5883compassGetHeadingAngle(PFbyte devId, PFword* headingAngle)
{
    volatile PFEnStatus stat;
    PFword headX=0,headY=0,headZ=0;
    volatile double cal=0.0;

    if( hmc5883compassInitFlag[devId] != enBooleanTrue )
    return enStatusNotConfigured;

    stat=pfHMC5883compassGetHeading(devId, &headX, &headY, &headZ);
    if(stat!=enStatusSuccess)
    return stat;

    cal= atan2( (double)(headX), (double)(headY) );
    cal= ( ( cal * 180.0 ) / PF_HMC_PI ) + 180;
    *headingAngle=(PFword)cal;

    return enStatusSuccess;
}

PFEnStatus pfHMC5883compassGetID(PFbyte devId, PFbyte* ida, PFbyte* idb, PFbyte* idc)
{
    volatile PFEnStatus ret;

    if( hmc5883compassInitFlag[devId] != enBooleanTrue )
    return enStatusNotConfigured;

    ret=pfHMC5883compassReadReg(devId, HMC5883_COMPASS_ID_A, ida);
    if(ret!=enStatusSuccess)
    return ret;

    ret=pfHMC5883compassReadReg(devId, HMC5883_COMPASS_ID_B, idb);
    if(ret!=enStatusSuccess)
    return ret;

    ret=pfHMC5883compassReadReg(devId, HMC5883_COMPASS_ID_C, idc);
    if(ret!=enStatusSuccess)
    return ret;

    return enStatusSuccess;
}

static PFEnStatus pfHMC5883compassWriteReg(PFbyte devId, PFbyte regAddr, PFbyte writeValue)
{
    volatile PFEnStatus ret;
    volatile PFbyte writeBuf[2]={0};

    writeBuf[0]=regAddr;
    writeBuf[1]=writeValue;

    /** pfIicWrite function use 0x1E address and R/W bit so need to send only reg address and value */
    ret=hmc5883Config[devId].pfIicWrite((PFbyte*)writeBuf, enBooleanTrue, HMC5883_COMPASS_SLAVE_ADDR, 2);
    if(ret!=enStatusSuccess)
        return ret;

    return enStatusSuccess;
}

static PFEnStatus pfHMC5883compassReadReg(PFbyte devId, PFbyte regAddr, PFbyte* readValue)
{
    volatile PFEnStatus ret;
    volatile PFbyte writeVal=0;
    PFword readBytes=0;

    writeVal=regAddr;

    /** 0x3C regAddr to set pointer to reg with addresses regAddr */
    ret=hmc5883Config[devId].pfIicWrite((PFbyte*)&writeVal, enBooleanTrue, HMC5883_COMPASS_SLAVE_ADDR, 1);
    if(ret!=enStatusSuccess)
        return ret;

    /** read register value */
    ret=hmc5883Config[devId].pfIicRead(readValue, enBooleanTrue, HMC5883_COMPASS_SLAVE_ADDR, 1, &readBytes);
    if(ret!=enStatusSuccess)
        return ret;

    return enStatusSuccess;
}

PFEnStatus pfHmc5883close(PFbyte devId)
{
	if(devId > PF_MAX_HMC5883_SUPPORTED)
		return enStatusInvArgs;
	if(hmc5883compassInitFlag[devId] != enBooleanTrue)
		return enStatusNotConfigured;

	hmc5883compassInitFlag[devId] = enBooleanFalse;
	--hmc5883DeviceCount;
	
	return enStatusSuccess;
}
