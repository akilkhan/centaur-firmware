#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_mpu6050.h"

static PFEnStatus pfMPU6050_WriteReg(PFbyte* devId,PFbyte regAddr, PFbyte writeValue);
static PFEnStatus pfMPU6050_ReadReg(PFbyte* devId,PFbyte regAddr, PFbyte* readValue);


/** array to scale raw value from Accelerometer into Acceleration as per range selection */
static PFfloat mpu6050_accelerationScalingFactor[]={ 0.061035, 0.12207, 0.24414, 0.488281 };
volatile PFbyte  mpu6050_accelerationScallingIndex=0;

/** array to scale raw value from Gyroscope into Rotation as per range selection */
static PFfloat mpu6050_gyroscopeScalingFactor[]={ 0.007633, 0.015267, 0.030487, 0.060975 };
volatile PFbyte  mpu6050_gyroscopeScallingIndex=0;

/** flag to test for Initialization */
static PFEnBoolean  mpu6050InitFlag[PF_MAX_MPU6050_SUPPORTED]={enBooleanFalse};
static PFCfgMPU6050 mpu6050Config[PF_MAX_MPU6050_SUPPORTED];       /** array to store configuration structures of VCNL4000 devices connected */
static PFbyte mpu6050DeviceCount=0;


PFEnStatus pfMPU6050_open(PFbyte* devId,PPFCfgMPU6050 sensorConfig,PFbyte deviceCount)
{
    volatile PFbyte regVal=0,i,j;
    volatile PFEnStatus stat;

#if (PF_MPU6050_DEBUG == 1)
	if(devId == PF_NULL_PTR)
		return enStatusInvArgs;
	if(sensorConfig == PF_NULL_PTR)
		return enStatusInvArgs;
	if(( deviceCount > PF_MAX_MPU6050_SUPPORTED)|| (deviceCount <= 0 ))
		return enStatusInvArgs;
#endif	// #if (PF_MPU6050_DEBUG == 1)
	
	for(i=0; i<deviceCount; ++i)
	{
		for(j=0; j < PF_MAX_MPU6050_SUPPORTED ; ++j)
		{
			if( mpu6050InitFlag[j] == enBooleanFalse )
			{
				devId[i]=j;
				pfMemCopy(&mpu6050Config[j], &sensorConfig[i], sizeof(PFCfgMPU6050));
				regVal = sensorConfig[i].sampleRateDivider;

				stat=pfMPU6050_WriteReg(&devId[i],MPU6050_RA_SMPLRT_DIV, regVal);
				if(stat!=enStatusSuccess)
					return stat;

				if( (sensorConfig[i].extSync>7) || (sensorConfig[i].dlpfBandwidth>6) )
					return enStatusInvArgs;
				else
				{
					regVal = sensorConfig[i].extSync<<3;

					regVal |= sensorConfig[i].dlpfBandwidth;

					stat=pfMPU6050_WriteReg(&devId[i],MPU6050_RA_CONFIG, regVal);
					if(stat!=enStatusSuccess)
						return stat;
				}

				if(sensorConfig[i].gyroRange>3) 
					return enStatusInvArgs;
				else
				{
					regVal = sensorConfig[i].gyroRange<<3;

					mpu6050_gyroscopeScallingIndex = sensorConfig[i].gyroRange;

					stat=pfMPU6050_WriteReg(&devId[i],MPU6050_RA_GYRO_CONFIG, regVal);
					if(stat!=enStatusSuccess)
						return stat;
				}

				if( sensorConfig[i].accelRange>3) 
					return enStatusInvArgs;
				else
				{
					regVal = sensorConfig[i].accelRange<<3;

					mpu6050_accelerationScallingIndex = sensorConfig[i].accelRange;

					stat=pfMPU6050_WriteReg(&devId[i],MPU6050_RA_ACCEL_CONFIG, regVal);
					if(stat!=enStatusSuccess)
						return stat;
				}

				regVal = sensorConfig[i].freeFallDetectionThreshold;
				stat=pfMPU6050_WriteReg(&devId[i],MPU6050_RA_FF_THR, regVal);
				if(stat!=enStatusSuccess)
					return stat;

				regVal = sensorConfig[i].freeFallDuration;
				stat=pfMPU6050_WriteReg(&devId[i],MPU6050_RA_FF_DUR, regVal);
				if(stat!=enStatusSuccess)
					return stat;

				regVal = sensorConfig[i].fifoEnablefor;
				stat=pfMPU6050_WriteReg(&devId[i],MPU6050_RA_FIFO_EN, regVal);
				if(stat!=enStatusSuccess)
					return stat;

				/** interrupt configuration */
				regVal = 0;

				if(MPU6050_CONFIGINTACTIVELOW)
					regVal = 0x80;

				if(MPU6050_INTREADCLEAR)
					regVal |= 0x10;

				if(MPU6050_CONFIGFSYNCINTACTIVEHIGH)
					regVal |= 0x80;

				if(MPU6050_FSYNCINTENABLE)
					regVal |= 0x04;

				if(MPU6050_I2CBYPASSENABLE)
				regVal |= 0x20;

				stat=pfMPU6050_WriteReg(&devId[i],MPU6050_RA_INT_PIN_CFG, regVal);
				if(stat!=enStatusSuccess)
					return stat;

				regVal = sensorConfig[i].intEnable;
				stat=pfMPU6050_WriteReg(&devId[i],MPU6050_RA_INT_ENABLE, regVal);
				if(stat!=enStatusSuccess)
					return stat;

				if(sensorConfig[i].accelOnDelay>3) 
					return enStatusInvArgs;
				else
				{
					regVal = sensorConfig[i].accelOnDelay<<4;

					stat=pfMPU6050_WriteReg(&devId[i],MPU6050_RA_MOT_DETECT_CTRL, regVal);
					if(stat!=enStatusSuccess)
					   return stat;
				}

				if(MPU6050_USEFIFO)
				{
					regVal = 0x40;
					stat=pfMPU6050_WriteReg(&devId[i],MPU6050_RA_USER_CTRL, regVal );
					if(stat!=enStatusSuccess)
						return stat;		
				}

				stat=pfMPU6050_WriteReg(&devId[i],MPU6050_RA_FIFO_R_W, 0x00 );
				if(stat!=enStatusSuccess)
					return stat;

				stat=pfMPU6050_WriteReg(&devId[i],AUX_VDDIO, 0x00 );
				if(stat!=enStatusSuccess)
					return stat;

				stat=pfMPU6050_WriteReg(&devId[i],MPU6050_RA_SIGNAL_PATH_RESET, 0x07 );
				if(stat!=enStatusSuccess)
					return stat;

				if( sensorConfig[i].devClock>5) 
					return enStatusInvArgs;
				else
				{
					regVal = sensorConfig[i].devClock;

					stat=pfMPU6050_WriteReg(&devId[i],MPU6050_RA_PWR_MGMT_1, regVal );
					if(stat!=enStatusSuccess)
						return stat;
				}
				
				mpu6050InitFlag[j]=enBooleanTrue;
				++mpu6050DeviceCount;
				break;				
			}
		}
		if(j == PF_MAX_MPU6050_SUPPORTED)
			return enStatusError;
	}									

    return enStatusSuccess;
}

PFEnStatus pfMPU6050_close(PFbyte* devId)
{
	
#if (PF_MPU6050_DEBUG == 1)
	if(devId == PF_NULL_PTR)
		return enStatusInvArgs;
#endif	// #if (PF_MPU6050_DEBUG == 1)

	if(*devId > PF_MAX_MPU6050_SUPPORTED)
		return enStatusInvArgs;
		
	if(mpu6050InitFlag[*devId] != enBooleanTrue)
		return enStatusNotConfigured;
	
	mpu6050InitFlag[*devId] = enBooleanFalse;
	--mpu6050DeviceCount;
	return enStatusSuccess;
}
PFEnStatus pfMPU6050_readAccelerometer(PFbyte* devId,PFsword* Xcomp, PFsword* Ycomp, PFsword* Zcomp)
{
    volatile PFEnStatus stat;
    volatile PFbyte dataBuf[6]={0},var=0;
    volatile PFword readBytes=0;

#if (PF_MPU6050_DEBUG == 1)
	if(devId == PF_NULL_PTR)
		return enStatusInvArgs;
	if(Xcomp == PF_NULL_PTR)
		return enStatusInvArgs;
	if(Ycomp == PF_NULL_PTR)
		return enStatusInvArgs;
	if(Zcomp == PF_NULL_PTR)
		return enStatusInvArgs;
#endif	// #if (PF_MPU6050_DEBUG == 1)
	
		if(*devId > PF_MAX_MPU6050_SUPPORTED)
			return enStatusInvArgs;
		
		if(mpu6050InitFlag[*devId] != enBooleanTrue)
       return enStatusNotConfigured;

    var = MPU6050_RA_ACCEL_XOUT_H;

    stat=mpu6050Config[*devId].pfI2cWrite((PFbyte*)&var, enBooleanTrue, mpu6050Config[*devId].slaveaddr, 1);
    if(stat!=enStatusSuccess)
        return stat;

    stat=mpu6050Config[*devId].pfI2cRead((PFbyte*)dataBuf, enBooleanTrue, mpu6050Config[*devId].slaveaddr, 6, (PFword*)&readBytes);
    if(stat!=enStatusSuccess)
        return stat;

    if(readBytes!=6)
        return enStatusError;

    *Xcomp= (PFsword)( (dataBuf[0]<<8) | dataBuf[1] );        //copy higher and lower data register of X
    *Ycomp= (PFsword)( (dataBuf[2]<<8) | dataBuf[3] );        //copy higher and lower data register of Y
    *Zcomp= (PFsword)( (dataBuf[4]<<8) | dataBuf[5] );        //copy higher and lower data register of Z

    return enStatusSuccess;
}

PFEnStatus pfMPU6050_getAcceleration(PFbyte* devId,PFsword* Xcomp, PFsword* Ycomp, PFsword* Zcomp)
{
		volatile PFEnStatus stat;
    volatile PFsword xcomp=0,ycomp=0,zcomp=0;

#if (PF_MPU6050_DEBUG == 1)
	if(devId == PF_NULL_PTR)
		return enStatusInvArgs;
	if(Xcomp == PF_NULL_PTR)
		return enStatusInvArgs;
	if(Ycomp == PF_NULL_PTR)
		return enStatusInvArgs;
	if(Zcomp == PF_NULL_PTR)
		return enStatusInvArgs;
#endif	// #if (PF_MPU6050_DEBUG == 1)
		
		if(*devId > PF_MAX_MPU6050_SUPPORTED)
			return enStatusInvArgs;

    if(mpu6050InitFlag[*devId] != enBooleanTrue)
			return enStatusNotConfigured;

    stat=pfMPU6050_readAccelerometer(devId,(PFsword*)&xcomp,(PFsword*)&ycomp,(PFsword*)&zcomp);
    if(stat!=enStatusSuccess)
			return stat;

    *Xcomp= (PFsword)( (PFfloat)xcomp * mpu6050_accelerationScalingFactor[mpu6050_accelerationScallingIndex] );
    *Ycomp= (PFsword)( (PFfloat)ycomp * mpu6050_accelerationScalingFactor[mpu6050_accelerationScallingIndex] );
    *Zcomp= (PFsword)( (PFfloat)zcomp * mpu6050_accelerationScalingFactor[mpu6050_accelerationScallingIndex] );

    return enStatusSuccess;
}

PFEnStatus pfMPU6050_readGyroscope(PFbyte* devId,PFsword* Xcomp, PFsword* Ycomp, PFsword* Zcomp)
{
    volatile PFEnStatus stat;
    volatile PFbyte dataBuf[6]={0},var=0;
    volatile PFword readBytes=0;

#if (PF_MPU6050_DEBUG == 1)
	if(devId == PF_NULL_PTR)
		return enStatusInvArgs;
	if(Xcomp == PF_NULL_PTR)
		return enStatusInvArgs;
	if(Ycomp == PF_NULL_PTR)
		return enStatusInvArgs;
	if(Zcomp == PF_NULL_PTR)
		return enStatusInvArgs;
#endif	// #if (PF_MPU6050_DEBUG == 1)

    if(*devId > PF_MAX_MPU6050_SUPPORTED)
			return enStatusInvArgs;
		
		if(mpu6050InitFlag[*devId] != enBooleanTrue)
			return enStatusNotConfigured;

    var = MPU6050_RA_GYRO_XOUT_H;

    stat=mpu6050Config[*devId].pfI2cWrite((PFbyte*)&var, enBooleanTrue,mpu6050Config[*devId].slaveaddr,1);
    if(stat!=enStatusSuccess)
        return stat;

    stat=mpu6050Config[*devId].pfI2cRead((PFbyte*)dataBuf, enBooleanTrue, mpu6050Config[*devId].slaveaddr,6,(PFword*)&readBytes);
    if(stat!=enStatusSuccess)
			return stat;

    if(readBytes!=6)
			return enStatusError;

    *Xcomp= (PFsword)( (dataBuf[0]<<8) | dataBuf[1] );        //copy higher and lower data register of X
    *Ycomp= (PFsword)( (dataBuf[2]<<8) | dataBuf[3] );        //copy higher and lower data register of Y
    *Zcomp= (PFsword)( (dataBuf[4]<<8) | dataBuf[5] );        //copy higher and lower data register of Z

    return enStatusSuccess;
}

PFEnStatus pfMPU6050_getRotation(PFbyte* devId,PFsword* Xcomp, PFsword* Ycomp, PFsword* Zcomp)
{
    volatile PFEnStatus stat;
    volatile PFsword xcomp=0,ycomp=0,zcomp=0;

#if (PF_MPU6050_DEBUG == 1)
	if(devId == PF_NULL_PTR)
		return enStatusInvArgs;
	if(Xcomp == PF_NULL_PTR)
		return enStatusInvArgs;
	if(Ycomp == PF_NULL_PTR)
		return enStatusInvArgs;
	if(Zcomp == PF_NULL_PTR)
		return enStatusInvArgs;
#endif	// #if (PF_MPU6050_DEBUG == 1)
	
		if(*devId > PF_MAX_MPU6050_SUPPORTED)
			return enStatusInvArgs;
		
		if(mpu6050InitFlag[*devId] != enBooleanTrue)
        return enStatusNotConfigured;

    stat=pfMPU6050_readGyroscope(devId,(PFsword*)&xcomp,(PFsword*)&ycomp,(PFsword*)&zcomp);
    if(stat!=enStatusSuccess)
			return stat;

    *Xcomp= (PFsword)( (PFfloat)xcomp * mpu6050_gyroscopeScalingFactor[mpu6050_gyroscopeScallingIndex] );
    *Ycomp= (PFsword)( (PFfloat)ycomp * mpu6050_gyroscopeScalingFactor[mpu6050_gyroscopeScallingIndex] );
    *Zcomp= (PFsword)( (PFfloat)zcomp * mpu6050_gyroscopeScalingFactor[mpu6050_gyroscopeScallingIndex] );

    return enStatusSuccess;
}

PFEnStatus pfMPU6050_readTemperature(PFbyte* devId,PFsword* temp)
{
    volatile PFEnStatus stat;
    volatile PFbyte dataBuf[2]={0},var=0;
    volatile PFword readBytes=0;
    volatile PFsword calc=0;

#if (PF_MPU6050_DEBUG == 1)
	if(devId == PF_NULL_PTR)
		return enStatusInvArgs;
	if(temp == PF_NULL_PTR)
		return enStatusInvArgs;
#endif	// #if (PF_MPU6050_DEBUG == 1)
	
    if(*devId > PF_MAX_MPU6050_SUPPORTED)
			return enStatusInvArgs;
	
		if(mpu6050InitFlag[*devId] != enBooleanTrue)
      return enStatusNotConfigured;

    var = MPU6050_RA_TEMP_OUT_H;

    stat=mpu6050Config[*devId].pfI2cWrite((PFbyte*)&var, enBooleanTrue, mpu6050Config[*devId].slaveaddr, 1);
    if(stat!=enStatusSuccess)
        return stat;

    stat=mpu6050Config[*devId].pfI2cRead((PFbyte*)dataBuf, enBooleanTrue, mpu6050Config[*devId].slaveaddr, 2, (PFword*)&readBytes);
    if(stat!=enStatusSuccess)
        return stat;

    if(readBytes<2)
        return enStatusError;

    calc =  (dataBuf[0]<<8) | dataBuf[1];
    *temp = (PFword)( (calc / 340.0) + 36.53 );

    return enStatusSuccess;
}

PFEnStatus pfMPU6050_getDeviceID(PFbyte* devId,PFbyte* ida)
{
    volatile PFEnStatus ret;
	
#if (PF_MPU6050_DEBUG == 1)
	if(devId == PF_NULL_PTR)
		return enStatusInvArgs;
	if(ida == PF_NULL_PTR)
		return enStatusInvArgs;
#endif	// #if (PF_MPU6050_DEBUG == 1)

    if(*devId > PF_MAX_MPU6050_SUPPORTED)
			return enStatusInvArgs;
		
		if(mpu6050InitFlag[*devId] != enBooleanTrue)
			return enStatusNotConfigured;

    ret=pfMPU6050_ReadReg(devId,MPU6050_RA_WHO_AM_I, ida );
    if(ret!=enStatusSuccess)
        return ret;

    return enStatusSuccess;
}

static PFEnStatus pfMPU6050_WriteReg(PFbyte* devId,PFbyte regAddr, PFbyte writeValue)
{
	volatile PFEnStatus ret;
	volatile PFbyte writeBuf[2]={0};

#if (PF_MPU6050_DEBUG == 1)
	if(devId == PF_NULL_PTR)
		return enStatusInvArgs;
#endif	// #if (PF_MPU6050_DEBUG == 1)

	writeBuf[0]=regAddr;
	writeBuf[1]=writeValue;

	ret = mpu6050Config[*devId].pfI2cWrite((PFbyte*)writeBuf, enBooleanTrue, mpu6050Config[*devId].slaveaddr, 2);
	if(ret!=enStatusSuccess)
		return ret;

	return enStatusSuccess;
}


static PFEnStatus pfMPU6050_ReadReg(PFbyte* devId,PFbyte regAddr, PFbyte* readValue)
{
    volatile PFEnStatus ret;
    volatile PFbyte writeVal=0;
    volatile PFword readBytes=0;

#if (PF_MPU6050_DEBUG == 1)
	if(devId == PF_NULL_PTR)
		return enStatusInvArgs;
	if(readValue == PF_NULL_PTR)
		return enStatusInvArgs;
#endif	// #if (PF_MPU6050_DEBUG == 1)

    writeVal=regAddr;

    ret = mpu6050Config[*devId].pfI2cWrite((PFbyte*)&writeVal, enBooleanTrue, mpu6050Config[*devId].slaveaddr, 1);
    if(ret!=enStatusSuccess)
        return ret;

    /** read register value */
    ret = mpu6050Config[*devId].pfI2cRead(readValue, enBooleanTrue,mpu6050Config[*devId].slaveaddr, 1,(PFword*)&readBytes);
    if(ret!=enStatusSuccess)
        return ret;

    return enStatusSuccess;
}




