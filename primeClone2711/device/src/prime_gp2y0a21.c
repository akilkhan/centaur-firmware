#include "prime_framework.h"
#include "prime_adc.h"
#include "prime_gp2y0a21.h"
#include <math.h>

static volatile PFbyte gp2y0a21_InitFlag[MAX_GP2Y0A21_SENSORS_SUPPORTED]={enBooleanFalse};        
static volatile PFbyte gp2y0a21_Count=0;
static volatile PFbyte gp2y0a21_adcChannel[MAX_GP2Y0A21_SENSORS_SUPPORTED]={0};


PFEnStatus pfGP2Y0A21Open(PFbyte* devId,PFbyte* adcChannel,PFbyte deviceCount)
{
	int i,j;
	
	#if (PF_GP2Y0A21_DEBUG == 1)
	if(devId == NULL)
		return enStatusInvArgs;
	if(adcChannel == NULL)
		return enStatusInvArgs;
	if(deviceCount == 0)
		return enStatusInvArgs;
	
	#endif	// #if (PF_GP2Y0A21_DEBUG == 1)
	
	for(i=0;i<deviceCount;i++)
	{
			for(j=0;j<MAX_GP2Y0A21_SENSORS_SUPPORTED;j++)
			{
				if(gp2y0a21_InitFlag[j]==enBooleanFalse)
				{
					gp2y0a21_adcChannel[j] = adcChannel[j];
					gp2y0a21_Count++;
					devId[i]=j;
					gp2y0a21_InitFlag[j] = enBooleanTrue;
					break;
				}
			}
			if(j == MAX_GP2Y0A21_SENSORS_SUPPORTED)
					return enStatusError;
	}	
	return enStatusSuccess;
}

PFEnStatus pfGP2Y0A21GetDistance(PFbyte id, PFdword* distance)
{
    PFdword vtg=0;
    PFEnStatus status;
    float temp=0.0;

	#if (PF_GP2Y0A21_DEBUG == 1)
	if(gp2y0a21_InitFlag[id]==enBooleanFalse)
		return enStatusNotConfigured;
				
	if( id >= MAX_GP2Y0A21_SENSORS_SUPPORTED )
		return enStatusInvArgs;
	#endif //#if (PF_GP2Y0A21_DEBUG == 1)
			
    status = pfAdcGetVoltageSingleConversion(gp2y0a21_adcChannel[id], &vtg);
    if(status!=enStatusSuccess)
			return enStatusError;

    //temp=((3027.4 * gp2y0a21_adcRefVtg)/gp2y0a21_adcResolution) /vtg;
		temp = 147737 / (vtg * 10);//147822
    temp=10*(pow(temp,1.2134));

    *distance=(PFword)temp;

    return enStatusSuccess;
}

PFEnStatus pfGP2Y0A21GetRawValue(PFbyte id, PFword* value)
{
	#if (PF_GP2Y0A21_DEBUG == 1)
	if(gp2y0a21_InitFlag[id]==enBooleanFalse)
		return enStatusNotConfigured;
				
	if( id >= MAX_GP2Y0A21_SENSORS_SUPPORTED )
		return enStatusInvArgs;
  	#endif //#if (PF_GP2Y0A21_DEBUG == 1)
		
	pfAdcSingleConversion(gp2y0a21_adcChannel[id],value);
		
	return enStatusSuccess;	
}

PFEnStatus pfGP2Y0A21Close(PFbyte devId)
{
	#if (PF_GP2Y0A21_DEBUG == 1)
		/** check for initialization */
	if(gp2y0a21_InitFlag[devId]==enBooleanFalse)
		return enStatusNotConfigured;
	if( devId >= MAX_GP2Y0A21_SENSORS_SUPPORTED )
		return enStatusInvArgs;
	#endif	//#if (PF_GP2Y0A21_DEBUG == 1)
		
	gp2y0a21_Count--;
	gp2y0a21_InitFlag[devId] = enBooleanFalse;	
	return enStatusSuccess;
}



