/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework TCS230 Colour Sensor Driver
 *
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_TCS230_API PF TCS230 Color Sensor API 
 * @{
 */

/** Maxumum devices supported		*/ 
#define PF_MAX_TCS230_SUPPORTED 	2 
 
/** Period for pulse counting in milli-seconds */
#define TCS230ColourSensorMeasurePeriod 100                       

/** Enumeration for colour filter select */
typedef enum
{
    enTCS230filter_Red = 0,			/**< use red colour filter */
    enTCS230filter_Blue,			/**< use green colour filter */
    enTCS230filter_None,			/**< use blue colour filter */
    enTCS230filter_Green			/**< use no filter */
}PFEnTCS230filterSelect;

/** Enumeration for output frequency scaling selection */
typedef enum
{
    enTCS230scalingFrequency_2 = 0x01,		/**< output frequency is 2% of actual frequency */
    enTCS230scalingFrequency_20,			/**< output frequency is 20% of actual frequency */
    enTCS230scalingFrequency_100			/**< output frequency is 100% of actual frequency */
}PFEnTCS230scalingFrequencySelect;

/** TCS230 colour sensor configuration structure */
typedef struct{
    PFGpioPortPin s0_port;									/**< port select for S0 */
    PFGpioPortPin s1_port;									/**< port select for S1 */                                                          
    PFGpioPortPin s2_port;									/**< port select for S2 */																													
    PFGpioPortPin s3_port;									/**< port select for S3 */																													
    PFEnTCS230scalingFrequencySelect scalingFreq;           /**< frequency scaling selection */ 
	PFEnStatus (*timerReset)(void);							/**< pointer to function to reset timer count	*/
	PFEnStatus (*timerReadCount)(PFdword*);					/**< pointer to function to read timer count	*/
}PFCfgTCS230colourSensor;

/** Pointer to structure PFCfgTCS230colourSensor **/
typedef PFCfgTCS230colourSensor* PFpCfgTCS230colourSensor;

/**
 * Initialize TCS230 colour sensor with given config structure
 * 
 * \param sensorConfig TCS230 colour sensor configuration structure
 * 
 * \return TCS230 colour sensor initialization status
 */
PFEnStatus pfTCS230colourSensorOpen(PFbyte *id,PFpCfgTCS230colourSensor sensorConfig,PFbyte cnt);

/**
 * close TCS230 colour sensor
 * 
 * \return TCS230 colour sensor close operation status
 */
PFEnStatus pfTCS230colourSensorClose(PFbyte id);

/**
 * to update TCS230 colour sensor's black colour balance values for given material, 
 * whenever function called user should place black sheet before sensor
 * 
 * \return TCS230 colour sensor update black balance operation status
 */
PFEnStatus pfTCS230colourSensorBlackBalance(PFbyte id);

/**
 * to update TCS230 colour sensor's white colour balance values for given material, 
 * whenever function called user should place white sheet before sensor
 * 
 * \return TCS230 colour sensor update white balance operation status
 */

PFEnStatus pfTCS230colourSensorWhiteBalance(PFbyte id);

/**
 * to get colour of target sensed by TCS230 colour sensor 
 * 
 * \param colour pointer to copy TCS230 colour sensor configuration structure
 * 
 * \return status of read colour operation
 */
PFEnStatus pfTCS230colourSensorGetColor(PFbyte id ,PFdword* colour);

/** @} */

