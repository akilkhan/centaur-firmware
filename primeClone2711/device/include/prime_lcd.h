/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework ILI9320 Display Driver
 *
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_ILI9320_DRIVER_API ILI9320 Driver API
 * @{
 */

#define DATA_PORT_WIDTH		8	/** Macro for defining the width of the data port of LCD Display		*/
#define BLACK           0x0000	/**	Macro for BLACK Color	*/
#define BLUE            0x001F	/**	Macro for BLUE Color	*/
#define RED             0xF800	/**	Macro for RED Color	*/
#define GREEN           0x07E0	/**	Macro for GREEN Color	*/
#define CYAN            0x07FF	/**	Macro for CYAN Color	*/
#define MAGENTA         0xF81F	/**	Macro for MAGENTA Color	*/
#define YELLOW          0xFFE0	/**	Macro for YELLOW Color	*/
#define WHITE           0xFFFF	/**	Macro for WHITE Color	*/

/**		Enumeration for the orientation of the LCD Display Driver		*/
 typedef enum{
	enLcdOrientation_0 = 0,
	enLcdOrientation_90,
	enLcdOrientation_180,
	enLcdOrientation_270	
}PFEnLcdOrientation;

/** ILI9320 Configuration structure */
typedef struct{
	PFGpioPortPin gpioData[DATA_PORT_WIDTH];
	PFGpioPortPin gpioChipSelect; 				 /**< port select for CS */
	PFGpioPortPin gpioRegSelect;				 /**< port select for RS */
	PFGpioPortPin gpioWrite;				 /**< port select for WR */
	PFGpioPortPin gpioRead;				 /**< port select for RD */
	PFGpioPortPin gpioReset;			 /**< port select for RESET */
	PFEnLcdOrientation orientation;   			 /**< Screen Orientation	 */
	PFword height;           			 /**< DISPLAY X MAX */	
	PFword width;           			 /**< DISPLAy Y MAX */  
}PFCfgILI9320;

/** pointer to structure PFCfgVcnl */
typedef PFCfgILI9320* PFpCfgILI9320;

/**
 * To initialize ILI9320 Display module
 *
 * \param ILI9320Config, pointer to config structure to configure ILI9320 Module
 *
 * \return status of initialize
 * 
 */
PFEnStatus pfILI9320Open(PFpCfgILI9320 lcdConfig);
/**
 * To close ILI9320 display driver
 *
 * \return status of whether GRAM address is set or not
 * 
 */
PFEnStatus pfILI9320Close(void);
/**
 * To Setting GRAM Address of ILI9320 Display module
 *
 * \return status of whether GRAM address is set or not
 * 
 */
PFEnStatus pfILI9320Home(void);
/**
 * To Set Window for ILI9320 Display module
 *
 * \param  X and Y Co-ordinates for setting window
 *
 * \return status of whether is window is set or not
 * 
 */
PFEnStatus pfILI9320SetWindow(PFword x, PFword y, PFword x1, PFword y1);
/**
 * To Set Cursor for ILI9320 Display module
 *
 * \param  X and Y Co-ordinates for setting cursor
 *
 * \return status of whether is cursor is set or not
 * 
 */
PFEnStatus pfILI9320SetCursor(PFword x, PFword y);
/**
 * To Fill the color on Display
 *
 * \param  Color to be filled
 *
 * \return status of whether is display is filled with color or not
 * 
 */
PFEnStatus pfILI9320FillRGB(PFword data);
/**
 * To Draw Pixel on ILI9320 Display module
 *
 * \param  X and Y Co-ordinates for setting pixel on display and its color
 *
 * \return status of whether is pixel has been placed or not
 * 
 */
 
PFEnStatus pfILI9320DrawPixel(PFword x, PFword y, PFword color);
/**
 * To Fill color to the particular area on display
 *
 * \param  X and Y Co-ordinates for filling color on the display
 *
 * \param clr to be filled in particular area
 *
 * \return status of whether is window is set or not
 * 
 */
PFEnStatus pfILI9320FillArea(PFword xStart, PFword yStart, PFword xEnd, PFword yEnd, PFword clr);
/**
 * To write cmd to display ILI9320 controller
 *
 * \param  command : reg on which data has to be written
 *
 * \param data :  data to be written on the reg
 *
 * \return status of whether is window is set or not
 * 
 */
PFEnStatus pfILI9320Command(PFword command, PFword data);
/**
 * To write cmd to display ILI9320 controller
 *
 * \param  command : reg on which data has to be written
 *
 * \return status of whether is window is set or not
 * 
 */
PFEnStatus pfILI9320WriteCmd(PFword command);
/**
 * To write data to display ILI9320 controller
 *
 * \param data :  data to be written on the reg
 *
 * \return status of whether is window is set or not
 * 
 */
PFEnStatus pfILI9320WriteData(PFword data);
/**
 * To write data to display ILI9320 controller
 *
 * \return status of whether is window is set or not
 * 
 */
PFEnStatus pfILI9320SetAreaMax(void);

/**
 * To get the height of the LCD Display
 *
 * \param Variable to store the height of LCD Display
 *
 * \return status of whether is window is set or not
 * 
 */
PFEnStatus pfGetHeight(PFword *height);

/**
 * To get the width of the LCD Display
 *
 * \param Variable to store the width of the LCD Display
 *
 * \return status of whether is window is set or not
 * 
 */
PFEnStatus pfGetWidth(PFword *width);
/**
 * To get the width of the LCD Display
 *
 * \param value of old x axis cordinate
 *
 * \param value of old y axis cordinate
 *
 * \param pointer  of new y axis cordinate
 *
 * \param pointer of new y axis cordinate 
 *
 * \return status of whether is window is set or not
 * 
 */
 PFEnStatus pfMapPoints(PFword xOld,PFword yOld,PFword *xNew,PFword *yNew);
/**
 * To set the orientation of the LCD Display
 *
 * \param Variable to store the width of the LCD Display
 *
 * \return status of whether is window is set or not
 * 
 */
 
 PFEnStatus pfSetOrientation(PFEnLcdOrientation orient);
 /**
 * To get the orientation of the LCD Display
 *
 * \param Variable to store the width of the LCD Display
 *
 * \return status of whether is window is set or not
 * 
 */
PFEnStatus pfGetOrientation(PFEnLcdOrientation*  orient);
/**
 * To scroll the LCD Display
 *
 * \param Number of lines to scroll
 *
 * \return status of whether is lcd display is scrolled or not
 * 
 */
PFEnStatus pfILI9320Scroll(PFsdword data);
