/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief xLynx-CB firmware based on Prime Framework to drive L6470/A3977 Stepper & Servo Motor.
 * 
 *
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_XLYNX_CB_DRIVER_API XLYNX-CB DRIVER API
 * @{
 */

/** select channel for UART/CAN */
#define DEBUG_UART_CHANNEL                  0   /** to select UART channel for printing debug masseges */
#define COMM_UART_CHANNEL                   2   /** to select UART channel for communication between boards */
#define COMM_CAN_CHANNEL                    1   /** to select CAN channel for communication between boards */

#define _XLYNXCB_PRINT_ERROR_MSG_       // to print error msg on debug port
#define _XLYNXCB_PRINT_STATUS_MSG_      // to print status msg on debug port

#ifdef _XLYNXCB_PRINT_ERROR_MSG_
	#define _XLYNXCB_DEBUG_MSG_PRINTING_DELAY_  300000  //in micro-seconds
#else
	#ifdef _XLYNXCB_PRINT_STATUS_MSG_
		#define _XLYNXCB_DEBUG_MSG_PRINTING_DELAY_  300000  //in micro-seconds
	#else
		#define _XLYNXCB_DEBUG_MSG_PRINTING_DELAY_  0  //in micro-seconds
	#endif
#endif

#define __DEBUG_UART_WRITE(x)               pfUart##x##Write
#define __COMM_UART_READ(x)                 pfUart##x##Read
#define __COMM_UART_WRITE(x)                pfUart##x##Write
#define __COMM_UART_RX_BUFFER_FLUSH(x)      pfUart##x##RxBufferFlush
#define __COMM_UART_GET_RX_BUFFER_COUNT(x)  pfUart##x##GetRxBufferCount
#define __COMM_CAN_READ(x)                 	pfCan##x##Read
#define __COMM_CAN_WRITE(x)                	pfCan##x##Write
#define __COMM_CAN_RX_BUFFER_FLUSH(x)      	pfCan##x##RxBufferFlush
#define __COMM_CAN_GET_RX_BUFFER_COUNT(x)  	pfCan##x##GetRxBufferCount

#define _DEBUG_UART_WRITE(x)                __DEBUG_UART_WRITE(x)
#define _COMM_UART_READ(x)                  __COMM_UART_READ(x)
#define _COMM_UART_WRITE(x)                 __COMM_UART_WRITE(x)
#define _COMM_UART_RX_BUFFER_FLUSH(x)       __COMM_UART_RX_BUFFER_FLUSH(x)
#define _COMM_UART_GET_RX_BUFFER_COUNT(x)   __COMM_UART_GET_RX_BUFFER_COUNT(x)
#define _COMM_CAN_READ(x)                  	__COMM_CAN_READ(x)
#define _COMM_CAN_WRITE(x)                 	__COMM_CAN_WRITE(x)
#define _COMM_CAN_RX_BUFFER_FLUSH(x)       	__COMM_CAN_RX_BUFFER_FLUSH(x)
#define _COMM_CAN_GET_RX_BUFFER_COUNT(x)   	__COMM_CAN_GET_RX_BUFFER_COUNT(x)

#define DEBUG_UART_WRITE                    _DEBUG_UART_WRITE(DEBUG_UART_CHANNEL)             /** to write debug messages to debug uart */
#define COMM_UART_READ                      _COMM_UART_READ(COMM_UART_CHANNEL)                /** to read communication packets from comm uart */
#define COMM_UART_WRITE                     _COMM_UART_WRITE(COMM_UART_CHANNEL)               /** to write communication packets to comm uart */
#define COMM_UART_RX_BUFFER_FLUSH           _COMM_UART_RX_BUFFER_FLUSH(COMM_UART_CHANNEL)     /** to flush comm uart receive buffer */
#define COMM_UART_GET_RX_BUFFER_COUNT       _COMM_UART_GET_RX_BUFFER_COUNT(COMM_UART_CHANNEL) /** to get received data count from comm uart */
#define COMM_CAN_READ                       _COMM_CAN_READ(COMM_CAN_CHANNEL)                  /** to read communication packets from comm can */
#define COMM_CAN_WRITE                      _COMM_CAN_WRITE(COMM_CAN_CHANNEL)                 /** to write communication packets to comm can */
#define COMM_CAN_RX_BUFFER_FLUSH            _COMM_CAN_RX_BUFFER_FLUSH(COMM_CAN_CHANNEL)       /** to flush comm can receive buffer */
#define COMM_CAN_GET_RX_BUFFER_COUNT        _COMM_CAN_GET_RX_BUFFER_COUNT(COMM_CAN_CHANNEL)   /** to get received data count from comm can */
 
#define XLYNXCB_MAX_MOTOR_SUPPORTED             19  /** maximum motor supported by xlynx-CB board */
#define XLYNXCB_MAX_LYNX_SUPPORTED              3   /** maximum Lynx driver supported by xlynx-CB board */
#define XLYNXCB_MAX_SMARTLYNX_SUPPORTED 	    8   /** maximum SmartLynx driver supported by xlynx-CB board */
#define XLYNXCB_MAX_SERVO_SUPPORTED             8   /** maximum Servo motor supported by xlynx-CB board */

#define XLYNXCB_USE_CAN                         0   /** use CAN for communication between baseboard & xpansion board */
#define XLYNXCB_USE_SPI                         0   /** use SPI for communication between baseboard & xpansion board */
#define XLYNXCB_USE_UART                        1   /** use UART for communication between baseboard & xpansion board */
#define XLYNXCB_CAN_MSG_ID                      0x07/** can message id */
#define XLYNXCB_MAX_CMD_RETRY                   3   /** number of times driver tries to send command */
#define XLYNXCB_STX                             '$' /** start of packet */
#define XLYNXCB_ETX                             '@' /** end of packet */

/** command id to differentiate commands */
#define CMD_XLYNXCB_BOARD_RESET                 1   /** command id to reset xLynx-CB board */
#define CMD_XLYNXCB_LYNX_OPEN                   2   /** command id for lynx open */
#define CMD_XLYNXCB_SMARTLYNX_OPEN    	    		3   /** command id for smartlynx open */
#define CMD_XLYNXCB_SERVO_OPEN   				        4   /** command id for servo open */
#define CMD_XLYNXCB_MOTOR_CLOSE    				      5   /** command id for motor close */
#define CMD_XLYNXCB_MOTOR_RESET_DEVICE          6   /** command id for device reset */
#define CMD_XLYNXCB_MOTOR_RESET_POSITION        7   /** command id for position reset */
#define CMD_XLYNXCB_MOTOR_GOHOME    			      8   /** command id for Go home position */
#define CMD_XLYNXCB_MOTOR_SOFTSTOP    			    9   /** command id for soft stop */
#define CMD_XLYNXCB_MOTOR_HARDSTOP    			    10  /** command id for hard stop */
#define CMD_XLYNXCB_MOTOR_MOVE   				        11  /** command id for motor move */
#define CMD_XLYNXCB_MOTOR_SET_FULLSTEP_SPEED    12  /** command id for setting full step speed */
#define CMD_XLYNXCB_MOTOR_RUN    				        13  /** command id to run motor */
#define CMD_XLYNXCB_MOTOR_READY    				      14  /** command id to test motor ready */
#define CMD_XLYNXCB_MOTOR_SET_ACC_PROFILE     	15  /** command id to set acc profile */
#define CMD_XLYNXCB_MOTOR_GET_ACC_PROFILE      	16  /** command id to get acc profile */
#define CMD_XLYNXCB_MOTOR_GET_POSITION    		  17  /** command id to get position */
#define CMD_XLYNXCB_MOTOR_SLEEP   				      18  /** command id to put driver in sleep mode */
#define CMD_XLYNXCB_MOTOR_SET_STEPCLOCK_MODE    19  /** command id for setting motor in stepclock mode */
#define CMD_XLYNXCB_MOTOR_TAKE_SINGLE_STEP      20  /** command id to take single step movement */
#define CMD_XLYNXCB_MOTOR_GET_SPEED   			    21  /** command id for reading speed */
#define CMD_XLYNXCB_MOTOR_GOMARK    			      22  /** command id to go to mark position */
#define CMD_XLYNXCB_MOTOR_GOUNTIL    			      23  /** command id to go until switch press */
#define CMD_XLYNXCB_MOTOR_RELEASE_SW   			    24  /** command id to go until switch release */
#define CMD_XLYNXCB_MOTOR_SET_MIN_SPEED    		  25  /** command id for setting minimum speed */

/** enumeration for motor type selection */
typedef enum{
	enXlynxCbMotorSelectServo = 0,                    /**< to select servo motor */
	enXlynxCbMotorSelectLynx,                         /**< to select lynx driver */
	enXlynxCbMotorSelectSmartLynx                     /**< to select smartlynx driver */
}PFxlynxCbMotorSelect;

/** structure for motor type and channel selection */
typedef struct{
	PFxlynxCbMotorSelect motorType;                   /**< enum for motor selection */
	PFbyte channel;                                   /**< for channel selection of motor specified by motorType */
}PFxlynxCbMotor;

/** structure to configure servo motor */
typedef struct{
	PFbyte channel;                                   /**< for selection of servo motor */
	PFword refreshRate;                               /**< pwm frequency/total time period **/
	PFword pulseDurationForPosition0degree;           /**< pwm count (as per pwm timertick setting & stepper specification) to move servo to zero degree position **/
	PFword pulseDurationForPosition180degree;         /**< pwm count (as per pwm timertick setting & stepper specification) to move servo to 180 degree position **/
}PFCfgXlynxCbServo;
typedef PFCfgXlynxCbServo* PFpCfgXlynxCbServo;      /** pointer to structure PFCfgXlynxCbServo */

/** structure to configure lynx driver(A3977) */
typedef struct{
	PFbyte channel;                                   /**< for selection of motor with lynx driver */
	PFEnA3977StepResolution stepResolution;           /**< Step resolution for the stepper motor */
	PFbyte stepangle;                                 /**< Step angle in degrees. This value must be the actual angle multiplied by 10 */
	PFfloat acceleration;                             /**< acceleration profile in steps/s^2 (corresponds to full step) */
	PFfloat deceleration;                             /**< acceleration profile in steps/s^2 (corresponds to full step) */
}PFCfgXlynxCbStepperLynx;
typedef PFCfgXlynxCbStepperLynx* PFpCfgXlynxCbStepperLynx;  /** pointer to structure PFCfgXlynxCbStepperLynx */

/** structure to configure smartlynx driver(L6470) */
typedef struct{
	PFbyte channel;                                   /**< for selection of motor with smartlynx driver */
	PFEnL6470pwmMultiplier multiplier;                /**< multiplier for PWM frequency generation  **/
	PFEnL6470pwmDivisor divisor;                      /**< divisor for PWM frequency generation  **/
	PFEnL6470slewRate slewRate;                       /**< slew rate selection **/
	PFEnL6470oscSelect oscSelect;                     /**< clock selection **/
	PFEnL6470overCurrentThreshold  overCurrentThreshold;/**< threshold for overcurrent detection **/
	PFEnL6470AlarmEnable alarmEnable;                 /**< selection of alarm signals used to generate the FLAG output **/
	PFEnL6470stepSelect stepSize;                     /**< stepsize selection **/
	PFbyte stepAngle;                                 /**< Step resolution for the stepper motor */
	PFfloat acceleration;                             /**< acceleration profile in steps/sec^2 */
	PFfloat deceleration;                             /**< deceleration profile in steps/sec^2 */
}PFCfgXlynxCbStepperSmartlynx;
typedef PFCfgXlynxCbStepperSmartlynx* PFpCfgXlynxCbStepperSmartlynx;  /** pointer to structure PFCfgXlynxCbStepperSmartlynx */

/**
 * for communication between baseboard and xpansion board with SPI, xapansion board must be registed as slave
 *
 * \param chipSelect Pointer to PFGpioPortPin structure which describes the chip select pin used for the device.
 *
 * \return status of Board Open fuction 
 */
PFEnStatus pfXlynxCbBoardOpen(PFpGpioPortPin chipSelect);

/**
 * to reset Expansion board (Apply software reset). this wipe out all initialization/settings of Expansion board.
 *
 * \return status of board reset operation
 */
PFEnStatus pfXlynxCbBoardReset(void);

/**
 * To initialize lynx stepper drivers(A3977)
 *
 * \param deviceId pointer to copy id/id's assigned by driver to motor/motor's
 *
 * \param config pointer to structure to configure stepper driver A3977
 *
 * \param deviceCount number of stepper driver A3977 to be initialised 
 *
 * \return status of initialization
 */
PFEnStatus pfXlynxCbLynxOpen(PFbyte* deviceId, PFpCfgXlynxCbStepperLynx config, PFbyte deviceCount);

/**
 * To initialize smartlynx stepper drivers(L6470)
 *
 * \param deviceId pointer to copy Id's assigned to steppers
 *
 * \param config pointer to array of structures to configure steppers
 *
 * \param deviceCount number of devices to be initialised
 *
 * \return status of initialization
 */
PFEnStatus pfXlynxCbSmartlynxOpen(PFbyte* deviceId, PFpCfgXlynxCbStepperSmartlynx config, PFbyte deviceCount);

/**
 * to initialize servo with servoConfig
 *
 * \param deviceId pointer to copy Id's assigned to steppers
 *
 * \param config pointer to config structure to initialize servo
 *
 * \param deviceCount number of devices to be initialised
 *
 * \return status of initialization
 **/
PFEnStatus pfXlynxCbServoOpen(PFbyte* deviceId, PFpCfgXlynxCbServo config, PFbyte deviceCount);

/**
 * to close motor servo/stepper with specified deviceId
 *
 * \param deviceId pointer to Id, specifies the stepper/servo to be closed
 *
 * \return status of motor Close fuction 
 */
PFEnStatus pfXlynxCbMotorClose(PFbyte* deviceId);

/**
 * to reset lynx(A3977)/smartLynx(L6470) driver, specified by devId
 *
 * \param devId pointer to Id, specifies the stepper driver to be reset
 *
 * \return status of stepper reset operation
 */
PFEnStatus pfXlynxCbMotorResetDevice(PFbyte *devId);

/**
 * to reset position of stepper/servo, specified by devId
 *
 * \param devId pointer to Id, specifies motor stepper/servo
 *
 * \return status of stepper/servo position reset operation
 */
PFEnStatus pfXlynxCbMotorResetPosition(PFbyte *devId);

/**
 * to return stepper to its home(starting) position
 *
 * \param devId pointer to Id, specifies stepper motor
 *
 * \return status of move motor to its home postion
 */
PFEnStatus pfXlynxCbMotorGoHomePosition(PFbyte *devId);

/**
 * to perform soft stop(decelerate to zero speed for lynx/to minimum speed for smartlynx) to stepper specified by id
 *
 * \param devId pointer to id, specifies stepper motor
 *
 * \return status of soft Stop operation
 */
PFEnStatus pfXlynxCbMotorSoftStop(PFbyte *devId);

/**
 * to perform hard stop(immediate stop) to stepper specified by id
 *
 * \param devId pointer to id, specifies stepper motor
 *
 * \return status of hard Stop operation
 */
PFEnStatus pfXlynxCbMotorHardStop(PFbyte *devId);
 
/**
 * for stepper : to move motor (with lynx/smartlynx driver) by specified number of steps as per stepsize selection in specified direction
 * for servo   : to move servo by angle specified by steps
 *
 * \param devId pointer to id, specifies stepper/servo motor
 *
 * \param dir for stepper specify direction of rotation  0: for clockwise 1: for anticlockwise
 *            for servo dir ignored
 *
 * \param steps for stepper specify number of steps stepper shuold move, as per stepsize selection
 *              for servo steps specify angle by which servo should move
 *
 * \param speed for stepper speed is pointer to speed of rotation of stepper in full steps/sec for 
 *                          motor with Smartlynx driver speed ranging 15.25 to 15610 full steps/s.
 *              for servo : to move servo by angle specified by steps
 *
 * \return status of move operation
 * \note for stepper command keeps the BUSY flag low until the target number of steps is performed
 *       This command can only be performed when the motor is stopped.
 *       (for motor with Smartlynx driver speed set is multiple of 15.25 full step/s).
 */
PFEnStatus pfXlynxCbMotorMove(PFbyte *devId, PFbyte dir, PFdword steps, PFfloat* speed);

/**
 * to set full step speed limit above this speed motor with Smartlynx driver leaves selected step mode and enter into full step mode 
 *
 * \param devId pointer to id, specifies motor with smartlynx driver
 *
 * \param speed is full step speed limit in full steps/s
 *
 * \return status of set full step speed limit
 * \note speed setting range is from 7.63 to 15625 full steps/s which is multiple of 15.25 full steps/s.
 */
PFEnStatus pfXlynxCbMotorSetFullStepSpeed(PFbyte* devId, PFfloat* speed);

/**
 * The Run command produces a motion at speed speed.
 *
 * \param devId pointer to id, specifies stepper motor
 *
 * \param dir to select the direction of motion; dir bit: '1' anticlockwise or '0' clockwise.
 *
 * \param speed pointer to speed(in full steps/sec) at which stepper shuold run (for smartlynx speed 15.25 to 15610 full steps/s).
 *
 * \return status of run operation
 * \note Run keeps the BUSY flag low until the target speed is reached.
 *       This command can be given anytime and is immediately executed.
 *       (for motor with Smartlynx driver speed set is multiple of 15.25 full step/s).
 */
PFEnStatus pfXlynxCbMotorRun(PFbyte* devId, PFbyte dir, PFfloat* speed);

/**
 * to test wheather stepper driver is ready for next command
 *
 * \param devId pointer to id, specifies stepper motor
 *
 * \param status pointer to copy busy/ready status of driver. True if device is ready(not accelerating or deceleration).
 *
 * \return status of driver
 */
PFEnStatus pfXlynxCbMotorDeviceReady(PFbyte* devId, PFEnBoolean* status);

/**
 * to set new acceleration profile for stepper
 *
 * \param devId pointer to id, specifies stepper motor
 *
 * \param acceleration pointer to new acceleration value (in full steps/sec^2) to be set (for smartlynx ranging 14.55 to 59590 full steps/sec^2)
 *
 * \param deceleration pointer to new deceleration value (in full steps/sec^2) to be set (for smartlynx ranging 14.55 to 59590 full steps/sec^2)
 *
 * \return status of set Acceleration value
 * \note This command is performed only when motor is stopped
 *       (for motor with smartlynx driver Acceleration/deceleration values set are in multiple of 14.55 full steps/sec^2).
 */
PFEnStatus pfXlynxCbMotorSetAccelerationProfile(PFbyte* devId, PFfloat* acceleration, PFfloat* deceleration);

/**
 * to read current acceleration profile of stepper
 *
 * \param devId pointer to id, specifies stepper motor
 *
 * \param acceleration pointer to copy current acceleration value (in full steps/sec^2)
 *
 * \param deceleration pointer to copy current deceleration value (in full steps/sec^2)
 *
 * \return status of read Current Acceleration profile value
 */
PFEnStatus pfXlynxCbMotorGetAccelerationProfile(PFbyte* devId, PFfloat* acceleration, PFfloat* deceleration);

/**
 * to read current position of stepper motor in steps (corresponds to selected step size)
 *
 * \param devId pointer to id, specifies stepper motor
 *
 * \param position pointer to copy current position of stepper, +ve for anticlockwise while -ve for clockwise direction
 *        gives position in steps(count corresponds to selected step size)
 *
 * \return status of read Current Position
 */
PFEnStatus pfXlynxCbMotorGetPosition(PFbyte* devId, PFsdword* position);

/**
 * to put motor with lynx (A3977) driver in sleep mode
 *
 * \param devId pointer to id, specifies motor with lynx driver
 *
 * \param sleepState true: to put in sleep state.
 *                   false: to remove from sleep state
 *
 * \return status of sleep operation
 */
PFEnStatus pfXlynxCbMotorSleep(PFbyte* devId, PFEnBoolean sleepState);

/**
 * Puts the motor with smartlynx driver into Step-clock mode and imposes direction specified by dir
 *
 * \param devId pointer to id, specifies motor with smartlynx driver
 *
 * \param dir direction of rotation
 *
 * \return status of step clock mode setting
 * \note This command can only be given when the motor is stopped.
 */
PFEnStatus pfXlynxCbMotorSetStepClockMode(PFbyte* devId, PFbyte dir);

/**
 * to take single step (single step is in selected step-mode)
 *
 * \param devId pointer to id, specifies motor with smartlynx driver
 *
 * \return status of take single step
 * \note before calling this function pfL6470setStepClockMode function should be called
 */
PFEnStatus pfXlynxCbMotorTakeSingleStep(PFbyte* devId);

/**
 * to get current speed of stepper 
 *
 * \param devId pointer to id, specifies motor with lynx/smartlynx driver
 *
 * \param speed pointer to copy current speed of stepper in step/s
 *
 * \return status of read Current Speed
 */
PFEnStatus pfXlynxCbMotorGetSpeed(PFbyte* devId, PFfloat* speed);

/**
 * The GoMark command produces a motion to the MARK(position stored in MARK register) position performing the minimum path.
 *
 * \param devId pointer to id, specifies motor with smartlynx driver
 *
 * \return status of go to mark position operation
 * \note This command keeps the BUSY flag low until the MARK position is reached.
 *       This command can be given only when the previous motion command has been completed
 */
PFEnStatus pfXlynxCbMotorGoMark(PFbyte* devId);

/**
 * produces a motion at speed specified by speed imposing specified direction till external switch turn-on event occurs
 *
 * \param devId pointer to id, specifies motor with smartlynx driver
 *
 * \param act action on external switch turn-on event occurs 	enL6470resetABS_POS : current position is resetted,
 *                                                              enL6470copyABS_POStoMark : current position is copied into the MARK register
 *
 * \param dir direction of rotation  0: for clockwise 1: for anticlockwise
 *
 * \param speed pointer to speed(in steps/sec) at which stepper shuold run untill switch get pressed
 *
 * \return status of go until operation
 * \note This command keeps the BUSY flag low until the switch turn-on event occurs and the motor is stopped
 *			 This command can be given anytime and is immediately executed.
 */
PFEnStatus pfXlynxCbMotorGoUntil(PFbyte* devId, PFEnL6470actionForSwitchPress act, PFbyte dir, PFfloat* speed);

/**
 * The ReleaseSW command produces a motion at minimum speed until the switch input is released
 *
 * \param devId pointer to id, specifies motor with smartlynx driver
 *
 * \param act action on external switch turn-on event occurs 	
 *        enL6470resetABS_POS : current position is resetted,	
 *		    enL6470copyABS_POStoMark : current position is copied into the MARK register
 *
 * \param dir direction of rotation  0: for clockwise 1: for anticlockwise
 *
 * \return status of release Switch operation
 * \note This command keeps the BUSY flag low until the switch input is released and the motor is stopped
 *	     This command can be given anytime and is immediately executed.
 */
PFEnStatus pfXlynxCbMotorReleseSW(PFbyte* devId, PFEnL6470actionForSwitchPress act, PFbyte dir);

/**
 * to set minimum speed of rotation(motion start from this speed otherwise start from zero speed)
 *
 * \param devId pointer to id, specifies motor with smartlynx driver
 *
 * \param speed is minimum speed of rotation in step/s
 *
 * \return status of set minimum speed of rotation
 * \note This command is performed only when motor is stopped
 */
PFEnStatus pfXlynxCbMotorSetMinimumSpeed(PFbyte* devId, PFfloat* speed);

/** @} */
