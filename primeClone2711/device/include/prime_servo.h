#pragma once

/**
 * \defgroup PF_SERVO_API Servo Api's
 * @{
 */

/** Maximum number of servo can be connected */
#define MAX_SERVO_SUPPORTED 6


/** structure to configure servo motor */
typedef struct{
	PFword refreshRate;                              /**< pwm frequency/total time period **/
	PFword pulseDurationForPosition0degree;          /**< pwm count (as per pwm timertick setting & stepper specification) to move servo to zero degree position **/
	PFword pulseDurationForPosition180degree;        /**< pwm count (as per pwm timertick setting & stepper specification) to move servo to 180 degree position **/
	PFbyte channel;                                  /**< pwm channel to which servo is connected **/
	PFEnStatus (*pfPwmUpdateMatchRegister)(PFbyte regNum, PFdword regVal); /**< pwm register update function call **/
	PFEnStatus (*pfPwmGetTickFreq)(PFdword* tickClk);/**< pwm get frequency function call **/
	PFEnStatus (*pfPwmStop)(void);								   /**< pwm stop function call **/
}PFCfgServo;

/** pointer to structure PFCfgServo */
typedef PFCfgServo* PFpCfgServo;

/**
 * to initialize servo with servoConfig
 *
 * \param servoConfig pointer to config structure to initialize servo
 *
 * \return status of pfServoOpen fuction 
 **/
PFEnStatus pfServoOpen(PFbyte *devId, PFpCfgServo servoConfig, PFbyte device_count);

/**
 * to close servo
 *
 * \return status of pfServoClose fuction 
 */
PFEnStatus pfServoClose(PFbyte* devId);

/**
 * to move servo by specified angle 
 *
 * \param angle value is actual angle multiplied by 10
 *
 * \return status of pfServoMove fuction 
 */
PFEnStatus pfServoMove(PFbyte* devId, PFbyte angle);

/**
 * to reset servo to 0 degree position 
 *
 * \return status of pfServoResetPosition fuction 
 */
PFEnStatus pfServoResetPosition(PFbyte* devId);

/**
 * to get current servo position value which is angle multiplied by 10
 *
 * \param angle pointer holds current position, actual angle multiplied by 10
 *
 * \return status of pfServoMove fuction 
 */
PFEnStatus pfServoGetCurrentPosition(PFbyte* devId, PFbyte* angle);

/**
* to calculate the count value for pwm on time 
*
* \param pointer servoConfig with user defined time value in us
*
* \return status for servo_count_calculator function
*/

PFEnStatus servo_count_calculator ( PFpCfgServo servoConfig);

/** @} */
