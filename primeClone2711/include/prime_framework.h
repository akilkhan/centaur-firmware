/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework Base Header File.
 *
 * 
 * Review status: NO
 *
 */ 
#pragma once

#include "prime_compiler.h"
#include <stdio.h>
#include <stdlib.h>
#include "prime_types.h"
#include "prime_config.h"
#include "prime_debug.h"
#include "prime_utils.h"

#ifdef MCU_CHIP_lpc1768
#include "prime_lpc17xx_arch.h"
#elif MCU_CHIP_atmega2560
#include "prime_atmega2560_arch.h"
#elif MCU_CHIP_at90can128
#include "prime_at90can128_arch.h"
#endif









