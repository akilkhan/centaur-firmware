#include "prime_dcpcore.h"
#include "prime_dcpint.h"
#include "prime_dcpl2.h"

void pfDcpInEpHandle(PFpDcpServiceContext pServ, PFpDcpInEndpoint pEp)
{
    /* free ep if it is expired */
    if(pEp->uExpTime <= pfDcpTimGet())
    {
        PF_DCP_TRACE("remove expired ep %x:%d (seq = %d)!\n", pEp->uPeerAddr, pServ->uServId, pEp->uFirstNotAckedPackNum);
        pfDcpInEpFree(pServ, pEp);
    }
}

PFpDcpInEndpoint pfDcpInEpFind(PFpDcpServiceContext pServ, PFdcpDevAddr uAddr)
{
    PFpNode pNode;
    PFdcplock uFlags;

    uFlags = pfDcpLock();
    PF_LIST_FOREACH_NODE(pNode, (&pServ->inEps))
    {
        if(((PFpDcpInEndpoint)pNode)->uPeerAddr == uAddr)
        {
            /* if service entry exists, return pointer to it */
            pdDcpUnlock(uFlags);
            return (PFpDcpInEndpoint)pNode;
        }        
    }
    pdDcpUnlock(uFlags);

    return (PFpDcpInEndpoint)PF_NULL_PTR;
}

#define PF_DCP_EP_DATA_FLUSH(_ep_, _sz_) \
{ \
    (_ep_)->uSize -= (_sz_); \
}

PFEnStatus pfDcpDataPacketHandle(PFpDcpServiceContext pServ, PFpDcpInEndpoint pEp, PFpDcpL2DevDesc pL2Dev, 
                                PFpDcpIncomPacketBuf pIncPackBuf, PFpuint8 pBuf, PFdcpfxmemsize uBufSz)
{
    PFEnStatus eRes;
    PFuint16 uCrc;
    PFEnBoolean bLast;
    PFdcpSeqNum uSeqNum = PF_DCP_HDR_SEQ_GET(pIncPackBuf->headerBuf.data32[1]);
    PFdcpPacketSize uPs = PF_DCP_HDR_PS_GET(pIncPackBuf->headerBuf.data32[0]);

    PF_DCP_TRACE("data packet from %x:%d\n", pIncPackBuf->headerBuf.data32[4], pServ->uServId);

    /* check payload size */
    if((pEp->uSize + uBufSz) > pServ->uRxBufSz)
    {
        PF_DCP_ERR("packet buffer overflow by %u bytes!\n", (pEp->uSize + uBufSz) - pServ->uRxBufSz);
        eRes = pfDcpSpecPacketSend(pServ, pL2Dev, pIncPackBuf, PF_DCP_SPEC_PACKID_DGNACK);
        if(eRes != enStatusSuccess)
            PF_DCP_ERR("failed to send DGNACK packet (%d)!\n", eRes);
        pfDcpInEpFree(pServ, pEp);
        pServ->stats.uRxErrs++;
        return enStatusNoMem;
    }

    /* copy data chunk to user buffer */
    pfMemCopy(pEp->pData + pEp->uSize, pBuf, uBufSz);
    pEp->uSize += uBufSz;

    /* check if we received whole packet */
    if((pIncPackBuf->uSize - PF_DCP_HDR_SIZE) < uPs)
        return enStatusSuccess;

#if PF_DCP_TEST_FEATURES == 1
    if(pServ->uWinResetFragNum != (PFdcpSeqNum)-1  && pEp->uCurrFragsNum++ == pServ->uWinResetFragNum)
    {
        PF_DCP_TRACE("forced win reset!\n");
        eRes = pfDcpSpecPacketSend(pServ, pL2Dev, pIncPackBuf, PF_DCP_SPEC_PACKID_RESET);
        if(eRes != enStatusSuccess)
            PF_DCP_ERR("failed to send RESET packet (%d)!\n", eRes);
        pfDcpInEpFree(pServ, pEp);
        return enStatusSuccess;
    }
#endif
    if(uSeqNum != pEp->uFirstNotAckedPackNum)
    {
        /*resend ACK packet if packet is already received*/
        if(uSeqNum == (pEp->uFirstNotAckedPackNum - PF_DCP_RX_WIN_SIZE))
        {
            PF_DCP_ERR(" Resending ACK (%d) should be %d!!\n", uSeqNum,pEp->uFirstNotAckedPackNum);
            eRes = pfDcpSpecPacketSend(pServ, pL2Dev, pIncPackBuf, PF_DCP_SPEC_PACKID_ACK);
            if(eRes != enStatusSuccess)
            {
                PF_DCP_ERR("failed to send ACK (%d)!\n", eRes);
                /* ack was not send, flush packet data from user buffer 
                PHIDCP_EP_DATA_FLUSH(pEp, pIncPackBuf->uSize - PHIDCP_HDR_SIZE);
                phidcp_incomPacketFree(pServ->pCtx, pIncPackBuf);
                return eRes;*/
                return enStatusError;
            }
            /* ignore packet with invalid seq number, flush packet data from user buffer */
            PF_DCP_EP_DATA_FLUSH(pEp, pIncPackBuf->uSize - PF_DCP_HDR_SIZE);
            return enStatusSuccess;
        }
        else
        {	        
            PF_DCP_ERR("invalid seq num %d, should be %d!\n", uSeqNum, pEp->uFirstNotAckedPackNum);
            /* ignore packet with invalid seq number, flush packet data from user buffer */
            PF_DCP_EP_DATA_FLUSH(pEp, pIncPackBuf->uSize - PF_DCP_HDR_SIZE);
            pfDcpIncomPacketFree(pServ->pCtx, pIncPackBuf);
            pServ->stats.uRxErrs++;
            return enStatusError;
        }
    }

    /* update dgram expiration time */
    pEp->uExpTime = pfDcpTimGet() + PF_DCP_RX_DGRAM_TMO;

#if PF_DCP_TEST_FEATURES == 1
    if(pServ->uRetryCount++ < pServ->uAckMaxRetryNum)
    {
        PF_DCP_TRACE("ignore packet (seq = %d/%d)!\n", uSeqNum, pEp->uFirstNotAckedPackNum);
        /* ignore packet, flush packet data from user buffer */
        PF_DCP_EP_DATA_FLUSH(pEp, pIncPackBuf->uSize - PF_DCP_HDR_SIZE);
        pfDcpIncomPacketFree(pServ->pCtx, pIncPackBuf);
        return enStatusSuccess;
    }
    pServ->uRetryCount = 0;
#endif

    uCrc = PF_DCP_HDR_CRC_GET(pIncPackBuf->headerBuf.data32[1]);
    if(uCrc != pfDcpCrc16(pIncPackBuf->headerBuf.data8, enBooleanFalse, (PFpuint8)pEp->pData, pEp->uSize))
    {
        /* wrong CRC, flush packet data from user buffer */
        PF_DCP_EP_DATA_FLUSH(pEp, pIncPackBuf->uSize - PF_DCP_HDR_SIZE);
        eRes = pfDcpSpecPacketSend(pServ, pL2Dev, pIncPackBuf, PF_DCP_SPEC_PACKID_NACK);
        if(eRes != enStatusSuccess)
            PF_DCP_ERR("failed to send NACK packet (%d)!\n", eRes);
        pServ->stats.uRxErrs++;
        return enStatusCrcError;
    }

    bLast = (PF_DCP_HDR_FLG_GET(pIncPackBuf->headerBuf.data32[0]) & PF_DCP_HDR_FLG_LAST_FRAG) ? enBooleanTrue : enBooleanFalse;

    PF_DCP_TRACE("send ACK (seq = %d/%d)\n", uSeqNum, pEp->uFirstNotAckedPackNum);
    eRes = pfDcpSpecPacketSend(pServ, pL2Dev, pIncPackBuf, PF_DCP_SPEC_PACKID_ACK);
    if(eRes != enStatusSuccess)
    {
        PF_DCP_ERR("failed to send ACK (%d)!\n", eRes);
        /* ack was not send, flush packet data from user buffer 
        PF_DCP_EP_DATA_FLUSH(pEp, pIncPackBuf->uSize - PF_DCP_HDR_SIZE);
        pfDcpIncomPacketFree(pServ->pCtx, pIncPackBuf);
        return eRes;*/
    }

    if(!bLast)
        /* For properly added packet increment is always one. Another requirements will cause design changes */
        pEp->uFirstNotAckedPackNum += PF_DCP_RX_WIN_SIZE;
    else
    {
        if(pServ->pRxCb != PF_NULL_PTR)
            pServ->pRxCb(pEp->uPeerAddr, pServ->uServId, pEp->pData, pEp->uSize, pServ->pRxArg);

        pfDcpInEpFree(pServ, pEp);
        pServ->stats.uRxDgrams++;
    }

    return enStatusSuccess;
}
