#include "prime_dcpcore.h"
#include "prime_dcpint.h"
#include "prime_dcpl2.h"
#include "prime_dcppacket.h"
#include "prime_dcpxmit.h"

PFEnStatus pfDcpOutEpHandle(PFpDcpServiceContext pServ, PFpDcpOutEndpoint pEp)
{
    PFpNode pNode, pIter;
    PFdword i = 0;
    PFEnStatus eRes;
    PFpDcpPacketBuf pPacketBuf;
    PFtime uCurrTime;
    PFpuint32 pPacketData;

    uCurrTime = pfDcpTimGet();
    PF_FOREACH_NODE_SAFE(pNode, pIter, &(pEp->packetBufs))
    {
        if(i++ == pEp->uWinSz)
            break;

        pPacketBuf = (PFpDcpPacketBuf)pNode;
        pPacketData = (PFpuint32)PF_DCP_PACKET_DATA(pPacketBuf);
        if(pPacketBuf->uRetryNum == 0)
        {
            PF_DCP_ERR("packet to %x:%d TX retries exceeded (seq = %d)!\n", pf_ntohl(pPacketData[3]), 
                PF_DCP_HDR_SID_GET(pf_ntohl(pPacketData[2])), PF_DCP_HDR_SEQ_GET(pf_ntohl(pPacketData[1])));
            if(pServ->pTxCb != PF_NULL_PTR)
                pServ->pTxCb(enStatusTimeout, PF_DCP_HDR_ID_GET(pf_ntohl(pPacketData[1]), pf_ntohl(pPacketData[2])), pServ->pTxArg);

            pfDcpOutEpDgramFlush(pServ->pCtx, pEp, PF_DCP_HDR_ID_GET(pf_ntohl(pPacketData[1]), pf_ntohl(pPacketData[2])));
            pServ->stats.uTxDgramErrs++;
            pServ->stats.uTxTmoErrs++;
            
            if(PF_LIST_IS_EMPTY(&(pEp->packetBufs)))
            {
                /* no more packets in endpoint, so free it and return. it is handled */
                pfDcpOutEpFree(pServ->pCtx, pEp);
                return enStatusSuccess;
            }
            /* there are many packets can be flushed, so start again from the begining of the list */
            pNode = pIter = PF_LIST_HEAD(&(pEp->packetBufs));
            continue;
        }

        if(pPacketBuf->uRetryTime <= uCurrTime)
        {
            PF_DCP_TRACE("send packet to %x:%d (tries = %d, seq = %d)\n", pf_ntohl(pPacketData[3]), 
                PF_DCP_HDR_SID_GET(pf_ntohl(pPacketData[2])), pPacketBuf->uRetryNum, PF_DCP_HDR_SEQ_GET(pf_ntohl(pPacketData[1])));

            /* update retry time, if packet transmission will fail for any reason we will try to send it later */
            pPacketBuf->uRetryTime = uCurrTime + PF_DCP_TX_RETRY_TMO;
            pPacketBuf->uRetryNum--;
            eRes = ((PFpDcpL2Drv)(pEp->pRoute->pDev->pDrv))->startWrite(pEp->pRoute->pDev->uIfaceNum, pEp->pRoute->pHwAddr,
				pEp->pRoute->uPortId, (PFpdcpfxmemptr)pPacketData, pPacketBuf->uSize, PF_NULL_PTR);
			
            /* update statistics only for the first attempt to send */
            if(pPacketBuf->uRetryNum == (PF_DCP_TX_RETRIES_NUM - 1))
            {
                pServ->stats.uTxBytes += pPacketBuf->uSize - PF_DCP_HDR_SIZE;
                if(PF_DCP_HDR_FLG_GET(pf_ntohl(pPacketData[0])) & PF_DCP_HDR_FLG_LAST_FRAG)
                    pServ->stats.uTxDgrams++;
            }

            if(eRes != enStatusSuccess)
            {
                PF_DCP_ERR("failed to write packet to L2 device (%d)!\n", eRes);
                /* do not try to send other packets in window if L2 layer encounters problems */
                pServ->stats.uTxDevErrs++;
                return eRes;
            }
        }
    }

    return enStatusSuccess;
}

void pfDcpAckHandle(PFpDcpServiceContext pServ, PFdcpSeqNum uWinSz, PFdcpSeqNum uSeqNum, PFdcpDevAddr srcAddr)
{
    PFpDcpPacketBuf pPacketBuf;
    PFpDcpOutEndpoint pEp;
    PFpuint32 pData;

    PF_DCP_TRACE("ACK packet from %x:%d (seq = %d)\n", srcAddr, pServ->uServId, uSeqNum);

    pEp = pfDcpOutEpFind(pServ, srcAddr);
    if(pEp == PF_NULL_PTR)
    {
        PF_DCP_ERR("out ep %x:%d not found!\n", srcAddr, pServ->uServId);
        return;
    }

    pPacketBuf = (PFpDcpPacketBuf)PF_LIST_HEAD(&(pEp->packetBufs));
    pData = (PFpuint32)PF_DCP_PACKET_DATA(pPacketBuf);
    /* if ack for the oldest packet is received, remove it from outgoing queue */
    if(PF_DCP_HDR_SEQ_GET(pf_ntohl(pData[1])) == uSeqNum)
    {
        if((PF_DCP_HDR_FLG_GET(pf_ntohl(pData[0])) & PF_DCP_HDR_FLG_LAST_FRAG) && pServ->pTxCb != PF_NULL_PTR)
            pServ->pTxCb(enStatusSuccess, PF_DCP_HDR_ID_GET(pf_ntohl(pData[1]), pf_ntohl(pData[2])), pServ->pTxArg);

        pfDcpPacketFree(pServ->pCtx, pPacketBuf);

        if(PF_LIST_IS_EMPTY(&(pEp->packetBufs)))
            pfDcpOutEpFree(pServ->pCtx, pEp);
        else
            /* update window size */
            pEp->uWinSz = uWinSz;
    }
}

static void pfDcpCurrOutPacketFlush(PFpDcpServiceContext pServ, PFdcpDevAddr srcAddr, PFEnStatus eTxRes)
{
    PFpDcpOutEndpoint pEp;
    PFpuint32 pData;
    PFdcpPacketId uPackId;

    pEp = pfDcpOutEpFind(pServ, srcAddr);
    if(pEp == PF_NULL_PTR)
    {
        PF_DCP_ERR("out ep %x:%d not found!\n", srcAddr, pServ->uServId);
        return;
    }

    pData = (PFpuint32)PF_DCP_PACKET_DATA((PFpDcpPacketBuf)PF_LIST_HEAD(&(pEp->packetBufs)));
    uPackId = PF_DCP_HDR_ID_GET(pf_ntohl(pData[1]), pf_ntohl(pData[2]));
    pfDcpOutEpDgramFlush(pServ->pCtx, pEp, uPackId);
    
    if(pServ->pTxCb != PF_NULL_PTR)
        pServ->pTxCb(eTxRes, uPackId, pServ->pTxArg);
}

void pfDcpRejectHandle(PFpDcpServiceContext pServ, PFdcpDevAddr srcAddr)
{
    PF_DCP_TRACE("REJECT packet from %x:%d\n", srcAddr, pServ->uServId);
    pfDcpCurrOutPacketFlush(pServ, srcAddr, enStatusNetNoService);
    pServ->stats.uTxDgramErrs++;
}

void pfDcpResetHandle(PFpDcpServiceContext pServ, PFdcpDevAddr srcAddr)
{
    PF_DCP_TRACE("RESET packet from %x:%d\n", srcAddr, pServ->uServId);
    pfDcpCurrOutPacketFlush(pServ, srcAddr, enStatusNetNoService);
    pServ->stats.uTxDgramErrs++;
}

void pfDcpNackHandle(PFpDcpServiceContext pServ, PFdcpDevAddr srcAddr)
{
    PF_DCP_TRACE("NACK packet from %x:%d\n", srcAddr, pServ->uServId);
    pServ->stats.uTxNackErrs++;
}

void pfDcpDgnackHandle(PFpDcpServiceContext pServ, PFdcpDevAddr srcAddr)
{
    PF_DCP_TRACE("DGNACK packet from %x:%d\n", srcAddr, pServ->uServId);
    pfDcpCurrOutPacketFlush(pServ, srcAddr, enStatusNetDgramRejected);
    pServ->stats.uTxDgramErrs++;
}

PFpDcpOutEndpoint pfDcpOutEpFind(PFpDcpServiceContext pServ, PFdcpDevAddr uAddr)
{
    PFpNode pNode;
    PFdcplock uFlags;
    PFpList pList = &pServ->outEps;

    uFlags = pfDcpLock();
    PF_LIST_FOREACH_NODE(pNode, pList)
    {
        if(((PFpDcpOutEndpoint)pNode)->uPeerAddr == uAddr)
        {
            /* if service entry exists, return pointer to it */
            pdDcpUnlock(uFlags);
            return (PFpDcpOutEndpoint)pNode;
        }        
    }
    pdDcpUnlock(uFlags);

    return (PFpDcpOutEndpoint)PF_NULL_PTR;
}

void pfDcpOutEpDgramFlush(PFpDcpContext pCtx, PFpDcpOutEndpoint pEp, PFdcpPacketId uPackId)
{
    PFpNode pNode, pIter;
    PFpuint32 pPacketData;

    PF_FOREACH_NODE_SAFE(pNode, pIter, &(pEp->packetBufs))
    {
        pPacketData = (PFpuint32)PF_DCP_PACKET_DATA((PFpDcpPacketBuf)pNode);
        if(uPackId == PF_DCP_HDR_ID_GET(pf_ntohl(pPacketData[1]), pf_ntohl(pPacketData[2])))
        {
            pfDcpPacketFree(pCtx, (PFpDcpPacketBuf)pNode);
        }
    }
}
