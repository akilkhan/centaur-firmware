#ifndef PRIMEDCP_XMIT_H_
#define PRIMEDCP_XMIT_H_

#include "prime_dcpcore.h"

PF_EXTERN_C_BEGIN

PFpDcpOutEndpoint pfDcpOutEpFind(PFpDcpServiceContext pServ, PFdcpDevAddr uAddr);
PFEnStatus pfDcpOutEpHandle(PFpDcpServiceContext pServ, PFpDcpOutEndpoint pEp);
void pfDcpAckHandle(PFpDcpServiceContext pServ, PFdcpSeqNum uWinSz, PFdcpSeqNum uSeqNum, PFdcpDevAddr srcAddr);
void pfDcpRejectHandle(PFpDcpServiceContext pServ, PFdcpDevAddr srcAddr);
void pfDcpResetHandle(PFpDcpServiceContext pServ, PFdcpDevAddr srcAddr);
void pfDcpNackHandle(PFpDcpServiceContext pServ, PFdcpDevAddr srcAddr);
void pfDcpDgnackHandle(PFpDcpServiceContext pServ, PFdcpDevAddr srcAddr);
void pfDcpOutEpDgramFlush(PFpDcpContext pCtx, PFpDcpOutEndpoint pEp, PFdcpPacketId uPackId);

PF_EXTERN_C_END

#endif /*PRIMEDCP_XMIT_H_*/
