#include "prime_dcpcore.h"
#include "prime_dcpl2.h"

#include "prime_dcppacket.h"
#include "prime_dcpint.h"
#include "prime_dcpxmit.h"
#include "prime_dcprecv.h"

/* Id base Protocol  */
static PFbyte gs_dcpInitCount=0;
static PFEnBoolean gs_dcpInitFlag[PF_DCP_MAX_INSTANCE_SUPPORTED] = {enBooleanFalse};
static PFDcpContext gs_dcpProtoCtx[PF_DCP_MAX_INSTANCE_SUPPORTED]={0};

static PFbyte gs_protoMem[PF_DCP_PROTOMEMPOOL_SIZE]={0};
static PFDcpL2DevDesc gs_l2Devs[PF_DCP_MAX_DRIVER_SUPPORTED]={0};

/* Id base service  */
static PFbyte gs_servInitCount=0;
static PFEnBoolean gs_servInitFlag[PF_DCP_MAX_SERVICES_NUM] = {enBooleanFalse};
static PFDcpServiceContext gs_servCtx[PF_DCP_MAX_SERVICES_NUM]={0};

static PFbyte gs_servRxBufs[PF_DCP_MAX_SERVICES_NUM][PF_DCP_MAX_RXBUF_SIZE * PF_DCP_MAX_RXBUF_NUM]={0};
static PFDcpInEndpoint gs_servRxbufHdrs[PF_DCP_MAX_SERVICES_NUM][PF_DCP_MAX_RXBUF_NUM]={0};

extern PFdword g_primeL2Driver;

static PFpDcpRoute pfDcpGetRoute(PFpDcpContext pCtx, PFdcpRoutesNum* pPeerId ,PFdword* pDevAddr);
static PFpDcpIncomPacketBuf pfDcpIncomPacketAlloc(PFpDcpContext pCtx, PFpDcpL2DevDesc pL2Dev);
static PFEnStatus pfDcpPacketHandle(PFpDcpContext pCtx, PFpDcpL2DevDesc pL2Dev, PFpDcpIncomPacketBuf pIncPackBuf, 
    PFpuint8 pBuf, PFdcpfxmemsize uBufSz);
static PFpDcpServiceContext pfDcpServiceFind(PFpDcpContext pCtx, PFdcpServiceId uServId);
static void pfDcpServiceHandle(PFpDcpServiceContext pServ);

#ifndef MIN
#define MIN(_a_, _b_)   ((_a_) < (_b_) ? (_a_) : (_b_))
#endif


PFEnStatus pfDcpInitialize(PFbyte* pCtxId, PFdcpRoutesNum* pPeerId, PFpfxCfgDcp pCfg)
{
	PFpDcpContext	pCtx;
	PFdcpl2DevsNum  uL2DevsNums;
	PFCfgDcpL2      l2Cfg;
	PFpdcpmemptr    pFreeMem;
	PFdcpmemsize    uFreeMemSz;
    PFdcpmemsize    uMaxPacketSz;
    PFpDcpL2Drv     pDrv;
    PFEnStatus      eRes;
    PFdword         i;
		
	
	PF_ASSERT_RET(pCtxId != PF_NULL_PTR, enStatusInvArgs);
	PF_ASSERT_RET(gs_dcpInitCount < PF_DCP_MAX_INSTANCE_SUPPORTED,enStatusNotSupported);
	
	for(i=0;i<PF_DCP_MAX_INSTANCE_SUPPORTED;i++)
	{
		if(gs_dcpInitFlag[i] ==enBooleanFalse)
		{
			pCtx = &gs_dcpProtoCtx[i];
			gs_dcpInitFlag[i] = enBooleanTrue;
			*pCtxId = i;
			gs_dcpInitCount++;
			break;
		}
	}
	if(i==PF_DCP_MAX_INSTANCE_SUPPORTED)
	{
		return enStatusError;
	}
	
    PF_ASSERT_RET(pCtx != PF_NULL_PTR, enStatusInvArgs);
    PF_ASSERT_RET(pCfg != PF_NULL_PTR, enStatusInvArgs);

	l2Cfg.rxHandlerArg = pCtx;
	l2Cfg.wrCompArg = pCtx;
	
    gs_l2Devs[0].pDrv = (PFpdcpmemptr)&g_primeL2Driver;
	gs_l2Devs[0].uIfaceNum = *pCtxId;		//Protocol Context ID
	
	uL2DevsNums =  sizeof(gs_l2Devs)/sizeof(PFDcpL2DevDesc);
	
	
	PF_DCP_TRACE("initialize\n");
	
    pfMemSet(pCtx, 0, sizeof(PFDcpContext));
	
    pFreeMem = gs_protoMem;
    uFreeMemSz =  PF_DCP_PROTOMEMPOOL_SIZE;
	


#if PF_DCP_CONFIG_L2DEVDESC_COPY
    /* allocate memory for static routes table */
    if(uFreeMemSz < (uL2DevsNums*sizeof(PFDcpL2DevDesc)))
    {
        PF_DCP_ERR("no memory for L2 device desc table!\n");
		gs_dcpInitFlag[*pCtxId] = enBooleanFalse;
		gs_dcpInitCount--;
        return enStatusNoMem;
    }
    pCtx->pL2Devs = (PFpDcpL2DevDesc)pFreeMem;
    pFreeMem += uL2DevsNums * sizeof(PFDcpL2DevDesc);
    uFreeMemSz -= uL2DevsNums * sizeof(PFDcpL2DevDesc);

    pfMemCopy(pCtx->pL2Devs, gs_l2Devs, uL2DevsNums * sizeof(PFDcpL2DevDesc));
#else
    pCtx->pL2Devs = gs_l2Devs;
#endif
    pCtx->uL2DevsNum = uL2DevsNums;

    if(uFreeMemSz < (pCfg->uStaticRoutesNum*sizeof(PFDcpRoute)))
    {
        PF_DCP_ERR("no memory for static routes table!\n");
		gs_dcpInitFlag[*pCtxId] = enBooleanFalse;
		gs_dcpInitCount--;
        return enStatusNoMem;
    }
    pCtx->pStaticRoutes = (PFpDcpRoute)pFreeMem;
    pFreeMem += pCfg->uStaticRoutesNum * sizeof(PFDcpRoute);
    uFreeMemSz -= pCfg->uStaticRoutesNum * sizeof(PFDcpRoute);

   // pfMemCopy(pCtx->pStaticRoutes, pCfg->pStaticRoutes, pCfg->uStaticRoutesNum * sizeof(PFDcpRoute));
	for(i=0;i<pCfg->uStaticRoutesNum;i++)
	{
        *(pPeerId+i) = i;
		pCtx->pStaticRoutes[i].netAddr = pCfg->pStaticRoutes[i].netAddr;
		pCtx->pStaticRoutes[i].pHwAddr = pCfg->pStaticRoutes[i].pHwAddr;
		pCtx->pStaticRoutes[i].uPortId = pCfg->pStaticRoutes[i].uPortId;
        pCtx->pStaticRoutes[i].pDev = &gs_l2Devs[0];
	}
	
    pCtx->uStaticRoutesNum = pCfg->uStaticRoutesNum;

#if PF_DCP_CONFIG_DYNAMIC_ROUTES
    /* alocate memory for dynamic routes table */
    if(uFreeMemSz < (PF_DCP_MAX_DYNAMICROUTES_SUPPORTED * sizeof(PFDcpRoute)))
    {
        PF_DCP_ERR("no memory for dynamic routes table!\n");
		gs_dcpInitFlag[*pCtxId] = enBooleanFalse;
		gs_dcpInitCount--;
        return enStatusNoMem;
    }
    pCtx->pDynamicRoutes = (PFpDcpRoute)pFreeMem;
    pFreeMem += PF_DCP_MAX_DYNAMICROUTES_SUPPORTED * sizeof(PFDcpRoute);
    uFreeMemSz -= PF_DCP_MAX_DYNAMICROUTES_SUPPORTED * sizeof(PFDcpRoute);

    pfMemSet(pCtx->pDynamicRoutes, 0, PF_DCP_MAX_DYNAMICROUTES_SUPPORTED * sizeof(PFDcpRoute));
    pCtx->uDynamicRoutesNum = PF_DCP_MAX_DYNAMICROUTES_SUPPORTED;
    pCtx->uDynamicRoutesUsedNum = 0;
#endif

    pCtx->uOwnAddr = pCfg->uOwnAddr;
    pCtx->uOwnMask = PF_DCP_OWNMASK;
    
    uMaxPacketSz = PF_DCP_MAX_PACKET_SIZE;

    /* initialize L2 devices */
    for(i = 0; i < pCtx->uL2DevsNum; i++)
    {
        l2Cfg.pCfg = PF_NULL_PTR;
        pDrv = (PFpDcpL2Drv)(pCtx->pL2Devs[i].pDrv);
        eRes = pDrv->init(pCtx->pL2Devs[i].uIfaceNum, &l2Cfg);
        if(eRes != enStatusSuccess)
        {
            PF_DCP_ERR("failed to init L2 device (%d)!\n", eRes);
			gs_dcpInitFlag[*pCtxId] = enBooleanFalse;
			gs_dcpInitCount--;
            return eRes;
        }
        if(uMaxPacketSz < pDrv->uMtuSize)
            uMaxPacketSz = pDrv->uMtuSize;
    }

    /* init and service list */
    if(uFreeMemSz < (PF_DCP_MAX_SERVICES_NUM*sizeof(PFpDcpServiceContext)))
    {
        PF_DCP_ERR("no memory for services table!\n");
		gs_dcpInitFlag[*pCtxId] = enBooleanFalse;
		gs_dcpInitCount--;
        return enStatusNoMem;
    }
    pCtx->pServices = (PFppDcpServiceContext)pFreeMem;
    pCtx->uServMaxNum = PF_DCP_MAX_SERVICES_NUM;
    for(i = 0; i < pCtx->uServMaxNum; i++)
        pCtx->pServices[i] = PF_NULL_PTR;
    pFreeMem += pCtx->uServMaxNum*sizeof(PFpDcpServiceContext);
    uFreeMemSz -= pCtx->uServMaxNum*sizeof(PFpDcpServiceContext);
    PF_DCP_TRACE("alloc %d bytes for services table.\n", PF_DCP_MAX_SERVICES_NUM*sizeof(PFpDcpServiceContext));

    /* init and fill free outgoing endpoints list */
    if(uFreeMemSz < (PF_DCP_MAX_OUTEP_NUM*sizeof(PFDcpOutEndpoint)))
    {
        PF_DCP_ERR("no memory for outgoing endpoints table!\n");
		gs_dcpInitFlag[*pCtxId] = enBooleanFalse;
		gs_dcpInitCount--;
        return enStatusNoMem;
    }
    PF_LIST_INIT(&(pCtx->freeOutEps));
    for(i = 0; i < PF_DCP_MAX_OUTEP_NUM; i++)
    {        
        PF_LIST_ADDTAIL(&(pCtx->freeOutEps), (PFpNode)pFreeMem);
        pFreeMem += sizeof(PFDcpOutEndpoint);
        uFreeMemSz -= sizeof(PFDcpOutEndpoint);
    }
    PF_DCP_TRACE("alloc %d bytes for out endpoints.\n", PF_DCP_MAX_OUTEP_NUM*sizeof(PFDcpOutEndpoint));

    /* init and fill incoming live packets lists */
    if(uFreeMemSz < (PF_DCP_MAX_INCOMPACK_NUM*sizeof(PFDcpIncomPacketBuf)))
    {
        PF_DCP_ERR("no memory for incoming packets table!\n");
		gs_dcpInitFlag[*pCtxId] = enBooleanFalse;
		gs_dcpInitCount--;
        return enStatusNoMem;
    }
    PF_LIST_INIT(&pCtx->incomPacketBufs);
    PF_LIST_INIT(&pCtx->freeIncomPacketBufs);
    for(i = 0; i < PF_DCP_MAX_INCOMPACK_NUM; i++)
    {
        PF_LIST_ADDTAIL(&(pCtx->freeIncomPacketBufs), (PFpNode)pFreeMem);
        pFreeMem += sizeof(PFDcpIncomPacketBuf);
        uFreeMemSz -= sizeof(PFDcpIncomPacketBuf);
    }
    PF_DCP_TRACE("alloc %d bytes for incom packets.\n", PF_DCP_MAX_INCOMPACK_NUM*sizeof(PFDcpIncomPacketBuf));

    /* init and fill free packet buffers list, use the rest of memory for packet buffers */
    PF_DCP_TRACE("%d/%d packet buffers in pool.\n", uFreeMemSz, uFreeMemSz/(uMaxPacketSz + sizeof(PFDcpPacketBuf)));
    PF_LIST_INIT(&pCtx->freePacketBufs);
    while(uFreeMemSz >= (uMaxPacketSz + sizeof(PFDcpPacketBuf)))
    {
        PF_LIST_ADDTAIL(&(pCtx->freePacketBufs), (PFpNode)pFreeMem);

        uFreeMemSz -= uMaxPacketSz + sizeof(PFDcpPacketBuf);
        pFreeMem += uMaxPacketSz + sizeof(PFDcpPacketBuf);
    }

	return enStatusSuccess;
}

PFEnStatus pfDcpFinalize(PFbyte* pCtxId)
{
    PFEnStatus eRes = enStatusSuccess;
    PFdcpl2DevsNum i;
	PFpDcpContext pCtx;

	PF_ASSERT_RET(pCtxId != PF_NULL_PTR, enStatusInvArgs);
	PF_ASSERT_RET(gs_dcpInitFlag[*pCtxId] != enBooleanFalse, enStatusInvArgs);	
	
	pCtx = &gs_dcpProtoCtx[*pCtxId];
	
	PF_DCP_TRACE("finalize\n");

    /* finalize L2 devices */
    for(i = 0; i < pCtx->uL2DevsNum; i++)
    {
        PFpDcpL2Drv pDrv = (PFpDcpL2Drv)pCtx->pL2Devs[i].pDrv;
        PFEnStatus eRes = pDrv->finalize(pCtx->pL2Devs[i].uIfaceNum);
        if(eRes != enStatusSuccess)
        {
            PF_DCP_ERR("failed to finalize L2 device (%d)!\n", eRes);
            return eRes;
        }
    }
	
	gs_dcpInitFlag[*pCtxId] = enBooleanFalse;
	gs_dcpInitCount--;
	
    return eRes;
}

PFEnStatus pfDcpDatagramTx(PFbyte* pServId, PFpfxCfgDcpTx pCfg, PFpdcpPacketId pId)
{
    PFdcpmemsize uBufSz, uPayloadSz;
    PFdcpDevAddr uPeerAddr;
    PFdcpRoutesNum  uPeerId;
    PFpuint32 pPacketData;
    PFuint16 uFragNum;
    PFpDcpPacketBuf pPacketBuf;
    PFpDcpRoute pRoute;
    PFpDcpOutEndpoint pEp;
    PFdcplock uFlags;
	PFpDcpServiceContext pServ;

	PF_ASSERT_RET(pServId != PF_NULL_PTR, enStatusInvArgs);
	PF_ASSERT_RET(gs_servInitFlag[*pServId] != enBooleanFalse, enStatusInvArgs);	
	
	pServ = &gs_servCtx[*pServId];

    PF_ASSERT_RET(pServ != PF_NULL_PTR, enStatusInvArgs);
    PF_ASSERT_RET(pCfg != PF_NULL_PTR, enStatusInvArgs);
    PF_ASSERT_RET(pCfg->pBuf != PF_NULL_PTR, enStatusInvArgs);
    PF_ASSERT_RET(pCfg->uBufSz != 0, enStatusInvArgs);

    uPeerId = pCfg->uPeerId;
    pRoute = pfDcpGetRoute(pServ->pCtx, &uPeerId, &uPeerAddr);
    if(pRoute == PF_NULL_PTR)
    {
        PF_DCP_ERR("no route for peer Id %d!\n", pCfg->uPeerId);
        return enStatusNetNoRouteFound;
    }
    
    PF_DCP_TRACE("tx %d bytes to 0x%x:%d\n", pCfg->uBufSz, uPeerAddr, pServ->uServId);
    
    pEp = pfDcpOutEpFind(pServ, uPeerAddr);
    if(pEp == PF_NULL_PTR)
    {
        /* if endpoint does not exist, initialize new entry for it */
        pEp = PFDcpOutEpAlloc(pServ->pCtx);
        if(pEp == PF_NULL_PTR)
        {
            PF_DCP_ERR("no memory for outgoing service 0x%x:%d!\n", uPeerAddr, pServ->uServId);
            return enStatusNoMem;
        }
        pEp->pRoute = pRoute;
        pEp->uPeerAddr = uPeerAddr;
        uFlags = pfDcpLock();
        PF_LIST_ADDTAIL(&(pServ->outEps), (PFpNode)pEp);
        pdDcpUnlock(uFlags);
    }

    uFragNum = 0;
    uBufSz = pCfg->uBufSz;
     uFlags = pfDcpLock();
    while(uBufSz > 0)
    {
        uPayloadSz = MIN(((PFpDcpL2Drv)pRoute->pDev->pDrv)->uMtuSize - PF_DCP_HDR_SIZE, uBufSz);
        pPacketBuf = pfDcpPacketAlloc(pServ->pCtx, uPayloadSz);
        if(pPacketBuf == PF_NULL_PTR)
        {
            PF_DCP_ERR("failed to alloc packet for %d fragment!\n", uFragNum);
            pfDcpOutEpDgramFlush(pServ->pCtx, pEp, pServ->uTxPackId);
            if(PF_LIST_IS_EMPTY(&(pEp->packetBufs)))
            {
                /* no more packets in endpoint, so free it and return. it is handled */
                pfDcpOutEpFree(pServ->pCtx, pEp);
            }
            pdDcpUnlock(uFlags);
            return enStatusNoMem;
        }
        pPacketData = (PFpuint32)PF_DCP_PACKET_DATA(pPacketBuf);

        /* copy user data */
        pfMemCopy((PFpuint8)pPacketData + PF_DCP_HDR_SIZE, (PFpuint8)pCfg->pBuf + (pCfg->uBufSz - uBufSz), uPayloadSz);
        uBufSz -= uPayloadSz;
        /* prepare header */
        pPacketData[0] = PF_DCP_HDR_SB(PF_DCP_HDR_SB_MARK) | PF_DCP_HDR_VER(PF_DCP_VER) | PF_DCP_HDR_PS(uPayloadSz);
        if(uBufSz == 0)
            pPacketData[0] |= PF_DCP_HDR_FLG(PF_DCP_HDR_FLG_LAST_FRAG);
        if(uFragNum == 0)
            pPacketData[0] |= PF_DCP_HDR_FLG(PF_DCP_HDR_FLG_SYN_FRAG);
        pPacketData[0] = pf_htonl(pPacketData[0]);
#if PF_DCP_TEST_FEATURES == 1
        if(uFragNum > 1)
            pPacketData[1] = pf_htonl(PF_DCP_HDR_SEQ(pServ->uSeqNum+pServ->uTxFragSeqNumShift+uFragNum) | PF_DCP_HDR_ID1(pServ->uTxPackId));
        else
#endif
            pPacketData[1] = pf_htonl(PF_DCP_HDR_SEQ(pServ->uSeqNum+uFragNum) | PF_DCP_HDR_ID1(pServ->uTxPackId));
        pPacketData[2] = pf_htonl(PF_DCP_HDR_ID2(pServ->uTxPackId) | PF_DCP_HDR_FN(uFragNum) | PF_DCP_HDR_SID(pServ->uServId));
        pPacketData[3] = pf_htonl(uPeerAddr);
        pPacketData[4] = pf_htonl(pServ->pCtx->uOwnAddr);
  
        /* calc CRC */
        pPacketData[1] = pf_htonl(pf_ntohl(pPacketData[1]) | 
            PF_DCP_HDR_CRC(pfDcpCrc16((PFpuint8)pPacketData, enBooleanTrue, (PFpuint8)pPacketData + PF_DCP_HDR_SIZE, uPayloadSz)));

        uFragNum++;

        PF_LIST_ADDTAIL(&(pEp->packetBufs), (PFpNode)pPacketBuf);
    }

    pServ->uSeqNum += uFragNum;
    pServ->uTxPackId++;
    pdDcpUnlock(uFlags);

    if(pId != PF_NULL_PTR)
        *pId = (PFdcpPacketId)pServ->uTxPackId;

	return enStatusSuccess;
}

PFEnStatus pfDcpDatagramTxResultAccept(PFbyte* pServId, PFdcpPacketId uId)
{
	PF_DCP_TRACE("pfDcpDatagramTxResultAccept\n");

	return enStatusSuccess;
}

PFEnStatus pfDcpServiceStart(PFbyte* pCtxId,PFbyte* pServId , PFpfxCfgDcpService pCfg)
{
    PFdcpServNum i, uFreeIdx;
    PFpDcpInEndpoint pEp;
	PFpDcpContext pCtx;
	PFpDcpServiceContext pServ;

	PF_ASSERT_RET(pCtxId != PF_NULL_PTR, enStatusInvArgs);
	PF_ASSERT_RET(gs_dcpInitFlag[*pCtxId] != enBooleanFalse, enStatusInvArgs);	
	
	pCtx = &gs_dcpProtoCtx[*pCtxId];
    
	PF_ASSERT_RET(pServId != PF_NULL_PTR, enStatusInvArgs);
	PF_ASSERT_RET(gs_servInitCount < PF_DCP_MAX_SERVICES_NUM,enStatusNotSupported);
	
	for(i=0;i<PF_DCP_MAX_SERVICES_NUM;i++)
	{
		if(gs_servInitFlag[i] == enBooleanFalse)
		{			
			pServ = &gs_servCtx[i];
			gs_servInitFlag[i] = enBooleanTrue;
			*pServId = i;
			gs_servInitCount++;
			break;
		}
	}
	if(i==PF_DCP_MAX_SERVICES_NUM)
	{
		return enStatusError;
	}
    
    PF_ASSERT_RET(pCtx != PF_NULL_PTR, enStatusInvArgs);
    PF_ASSERT_RET(pServ != PF_NULL_PTR, enStatusInvArgs);
    PF_ASSERT_RET(pCfg != PF_NULL_PTR, enStatusInvArgs);

    PF_DCP_TRACE("init service %d\n", pCfg->uServId);

    for(i = 0, uFreeIdx = pCtx->uServMaxNum; i < pCtx->uServMaxNum; i++)
    {
        if(pCtx->pServices[i] == PF_NULL_PTR)
            uFreeIdx = i;
        else if(pCtx->pServices[i]->uServId == pCfg->uServId)
        {
            PF_DCP_ERR("service with ID %d is already running!\n", pCfg->uServId);
            return enStatusConfigConflict;
        }
    }
    if(uFreeIdx == pCtx->uServMaxNum)
    {
        PF_DCP_ERR("no mem for new service!\n");
        return enStatusNoMem;
    }
    pCtx->pServices[uFreeIdx] = pServ;

    pServ->pCtx = pCtx;
    pServ->uServId = pCfg->uServId;
    pServ->pTxCb = pCfg->pTxCb;
    pServ->pTxArg = pCfg->pTxArg;
    pServ->pRxCb = pCfg->pRxCb;
    pServ->pRxArg = pCfg->pRxArg;
    pServ->uSeqNum = 0;
    pServ->uTxPackId = 0;
    pfMemSet(&pServ->stats, 0, sizeof(PFDcpStats));
#if PF_DCP_TEST_FEATURES == 1
    pServ->uAckMaxRetryNum = 0;
    pServ->uRetryCount = 0;
    pServ->uAckSeqNumShift = 0;
    pServ->uForcedAckServId = pServ->uServId;
    pServ->uWinResetFragNum = (PFdcpSeqNum)-1;
    pServ->uTxFragSeqNumShift = 0;
#endif

    pServ->uRxBufSz = PF_DCP_MAX_RXBUF_SIZE;
    PF_LIST_INIT(&(pServ->outEps));
    PF_LIST_INIT(&(pServ->inEps));
    PF_LIST_INIT(&(pServ->freeInEps));

	pEp = &gs_servRxbufHdrs[*pServId][0];
    for(i = 0; i < PF_DCP_MAX_RXBUF_NUM; i++)
    {
		pEp->pData = (PFpdcpmemptr)&gs_servRxBufs[*pServId][0]  + i*PF_DCP_MAX_RXBUF_SIZE;
        PF_LIST_ADDTAIL(&(pServ->freeInEps), (PFpNode)pEp);
        pEp++;
    }
	return enStatusSuccess;
}

PFEnStatus pfDcpServiceStop(PFbyte* pServId)
{
    PFdcplock uFlags;
    PFpNode pEpNode, pPackNode, pEpIter, pPackIter;
    PFpDcpOutEndpoint pEp;
    PFdcpServNum i;
	PFpDcpServiceContext pServ;

	PF_ASSERT_RET(pServId != PF_NULL_PTR, enStatusInvArgs);
	PF_ASSERT_RET(gs_servInitFlag[*pServId] != enBooleanFalse, enStatusInvArgs);	
	
	pServ = &gs_servCtx[*pServId];

    PF_ASSERT_RET(pServ != PF_NULL_PTR, enStatusInvArgs);

    PF_DCP_TRACE("stop service %d\n", pServ->uServId);

    uFlags = pfDcpLock();

    /* release all packet buffers */
    PF_FOREACH_NODE_SAFE(pEpNode, pEpIter, &(pServ->outEps))
    {
        pEp = (PFpDcpOutEndpoint)pEpNode;
        PF_FOREACH_NODE_SAFE(pPackNode, pPackIter, &(pEp->packetBufs))
        {
            pfDcpPacketFree(pServ->pCtx, (PFpDcpPacketBuf)pPackNode);
        }
        pfDcpOutEpFree(pServ->pCtx, pEp);
    }

    for(i = 0; i < pServ->pCtx->uServMaxNum; i++)
    {
        if(pServ->pCtx->pServices[i] == pServ)
        {
            pServ->pCtx->pServices[i] = PF_NULL_PTR;
            pdDcpUnlock(uFlags);
			gs_servInitFlag[*pServId] = enBooleanFalse;
			gs_servInitCount--;
            return enStatusSuccess;
        }
    }

    pdDcpUnlock(uFlags);

    PF_DCP_ERR("no such service!\n");

    return enStatusInvArgs;
}
PFEnStatus dcpServiceReset(PFpDcpServiceContext pServ)
{
    PFdcplock uFlags;
    PFpNode pEpNode, pPackNode, pEpIter, pPackIter;
    PFpDcpOutEndpoint pEp;

    PF_ASSERT_RET(pServ != PF_NULL_PTR, enStatusInvArgs);

    PF_DCP_TRACE("reset service %d\n", pServ->uServId);

    uFlags = pfDcpLock();

    /* release all packet buffers */
    PF_FOREACH_NODE_SAFE(pEpNode, pEpIter, &(pServ->outEps))
    {
        pEp = (PFpDcpOutEndpoint)pEpNode;
        PF_FOREACH_NODE_SAFE(pPackNode, pPackIter, &(pEp->packetBufs))
        {
            pfDcpPacketFree(pServ->pCtx, (PFpDcpPacketBuf)pPackNode);
        }
        pfDcpOutEpFree(pServ->pCtx, pEp);
    }

    pfMemSet(&pServ->stats, 0, sizeof(PFDcpStats));

    pdDcpUnlock(uFlags);

    return enStatusSuccess;
}
PFEnStatus pfDcpReset(PFbyte* pCtxId)
{
    PFdcpServNum    i;
    PFEnStatus           eRes;
	PFpDcpContext	pCtx;

	PF_ASSERT_RET(pCtxId != PF_NULL_PTR, enStatusInvArgs);
	PF_ASSERT_RET(gs_dcpInitFlag[*pCtxId] != enBooleanFalse, enStatusInvArgs);	
	
	pCtx = &gs_dcpProtoCtx[*pCtxId];

    PF_ASSERT_RET(pCtx != PF_NULL_PTR, enStatusInvArgs);

    for(i = 0; i < pCtx->uServMaxNum; i++)
    {
        if(pCtx->pServices[i] != PF_NULL_PTR)
        {
            eRes = dcpServiceReset(pCtx->pServices[i]);
            if(eRes != enStatusSuccess)
            {
                PF_DCP_ERR("failed to reset service (%d)!\n", eRes);
                return eRes;
            }
        }
    }

    return enStatusSuccess;
}

PFEnStatus pfDcpStatusGet(PFbyte* pCtxId, PFpENDcpState peState)
{
	
    PF_ASSERT_RET(pCtxId != PF_NULL_PTR, enStatusInvArgs);
    PF_ASSERT_RET(peState != PF_NULL_PTR, enStatusInvArgs);

    if(gs_dcpInitFlag[*pCtxId] == enBooleanTrue)
    {
        *peState = enDcpStateInitialized;
    }
    else
    {
        *peState = enDcpStateUnknown ;
    }

    return enStatusSuccess;
}

PFEnStatus pfDcpStatistisGet(PFbyte* pCtxId, PFpDcpStats pStats)
{
    PFdcpServNum   i;
	PFpDcpContext pCtx;

	PF_ASSERT_RET(pCtxId != PF_NULL_PTR, enStatusInvArgs);
	PF_ASSERT_RET(gs_dcpInitFlag[*pCtxId] != enBooleanFalse, enStatusInvArgs);	
	
	pCtx = &gs_dcpProtoCtx[*pCtxId];

    PF_ASSERT_RET(pCtx != PF_NULL_PTR, enStatusInvArgs);
    PF_ASSERT_RET(pStats != PF_NULL_PTR, enStatusInvArgs);

    pfMemSet(pStats, 0, sizeof(PFDcpStats));

    for(i = 0; i < pCtx->uServMaxNum; i++)
    {
        if(pCtx->pServices[i] != PF_NULL_PTR)
        {
            pStats->uTxBytes        += pCtx->pServices[i]->stats.uTxBytes;
            pStats->uTxDgramErrs    += pCtx->pServices[i]->stats.uTxDgramErrs;
            pStats->uTxDgrams       += pCtx->pServices[i]->stats.uTxDgrams;
            pStats->uTxDevErrs      += pCtx->pServices[i]->stats.uTxDevErrs;
            pStats->uTxTmoErrs      += pCtx->pServices[i]->stats.uTxTmoErrs;
            pStats->uTxNackErrs     += pCtx->pServices[i]->stats.uTxNackErrs;
            pStats->uRxBytes        += pCtx->pServices[i]->stats.uRxBytes;
            pStats->uRxDgrams       += pCtx->pServices[i]->stats.uRxDgrams;
            pStats->uRxErrs         += pCtx->pServices[i]->stats.uRxErrs;
        }
    }

    return enStatusSuccess;
}

PFEnStatus pfDcpServiceReset(PFbyte* pServId)
{
	PFpDcpServiceContext pServ;

	PF_ASSERT_RET(pServId != PF_NULL_PTR, enStatusInvArgs);
	PF_ASSERT_RET(gs_servInitFlag[*pServId] != enBooleanFalse, enStatusInvArgs);	
	pServ = &gs_servCtx[*pServId];	
	
	dcpServiceReset(pServ);
    
    return enStatusSuccess;
}

PFEnStatus pfDcpServiceStatusGet(PFbyte* pServId, PFpENDcpServiceState peState)
{
    PF_ASSERT_RET(pServId != PF_NULL_PTR, enStatusInvArgs);
    PF_ASSERT_RET(peState != PF_NULL_PTR, enStatusInvArgs);

    *peState = enDcpServiceStateInitialized;

    return enStatusSuccess;
}

PFEnStatus pfDcpServiceStatisticsGet(PFbyte* pServId, PFpDcpStats pStats)
{
	PFpDcpServiceContext pServ;

	PF_ASSERT_RET(pServId != PF_NULL_PTR, enStatusInvArgs);
	PF_ASSERT_RET(gs_servInitFlag[*pServId] != enBooleanFalse, enStatusInvArgs);	
	
	pServ = &gs_servCtx[*pServId];

    PF_ASSERT_RET(pServ != PF_NULL_PTR, enStatusInvArgs);
    PF_ASSERT_RET(pStats != PF_NULL_PTR, enStatusInvArgs);

    pfMemCopy(pStats, &pServ->stats, sizeof(PFDcpStats));

    return enStatusSuccess;
}

#if PF_DCP_TEST_FEATURES == 1

PFEnStatus pfDcpServiceOptionsSet(PFbyte* pServId, PFfxENDcpServiceFeature eOpt, PFpfxgeneric pVal)
{
    PFdcplock uFlags;
	PFpDcpServiceContext pServ;

	PF_ASSERT_RET(pServId != PF_NULL_PTR, enStatusInvArgs);
	PF_ASSERT_RET(gs_servInitFlag[*pServId] != enBooleanFalse, enStatusInvArgs);	
	
	pServ = &gs_servCtx[*pServId];

    PF_ASSERT_RET(pServ != PF_NULL_PTR, enStatusInvArgs);

    switch(eOpt)
    {
    case enDcpServiceFeatureAckRetryNum:
        uFlags = pfDcpLock();
        pServ->uAckMaxRetryNum = *((PFpdcpSeqNum)pVal);
        pdDcpUnlock(uFlags);
        break;
    case enDcpServiceFeatureAckNumShift:
        uFlags = pfDcpLock();
        pServ->uAckSeqNumShift = *((PFpdcpSeqNum)pVal);
        pdDcpUnlock(uFlags);
        break;
    case enDcpServiceFeatureForceAckServId:
        uFlags = pfDcpLock();
        pServ->uForcedAckServId = *((PFpdcpServiceId)pVal);
        pdDcpUnlock(uFlags);
        break;
    case enDcpServiceFeatureWinResetFragNum:
        uFlags = pfDcpLock();
        pServ->uWinResetFragNum = *((PFpdcpSeqNum)pVal);
        pdDcpUnlock(uFlags);
        break;
    case enDcpServiceFeatureTxFragSeqNumShift:
        uFlags = pfDcpLock();
        pServ->uTxFragSeqNumShift = *((PFpdcpSeqNum)pVal);
        pdDcpUnlock(uFlags);
        break;
    default:
        return enStatusInvArgs;
    }

    return enStatusSuccess;
}

#endif

PFEnStatus pfDcpTask(PFbyte* pCtxId)
{
    PFEnStatus eRes;
    PFdcplock uFlags;
    PFpNode pNode, pIter;
    PFdcpServNum i;
    PFtime uCurrTime;
    PFpDcpIncomPacketBuf pIncPackBuf;
    PFpDcpInEndpoint pEp;
    PFpDcpServiceContext pServ;
	PFpDcpL2Drv pDrv;
	PFpDcpContext pCtx;

	PF_ASSERT_RET(pCtxId != PF_NULL_PTR, enStatusInvArgs);
	PF_ASSERT_RET(gs_dcpInitFlag[*pCtxId] != enBooleanFalse, enStatusInvArgs);	
	
	pCtx = &gs_dcpProtoCtx[*pCtxId];
	
    /* process services */
    for(i = 0; i < pCtx->uServMaxNum; i++)
    {
        if(pCtx->pServices[i] != PF_NULL_PTR)
            pfDcpServiceHandle(pCtx->pServices[i]);
    }

    uCurrTime = pfDcpTimGet();

    uFlags = pfDcpLock();

    /* process incomplete packets: remove dead ones, send spec packets */
    PF_FOREACH_NODE_SAFE(pNode, pIter, &(pCtx->incomPacketBufs))
    {
        pIncPackBuf = (PFpDcpIncomPacketBuf)pNode;
        if(pIncPackBuf->uExpTime <= uCurrTime)
        {
            if(pIncPackBuf->uRetryNum > 1)
            {
                /* handle spec packet re-transmission */
                eRes = ((PFpDcpL2Drv)(pIncPackBuf->l2Dev.pDrv))->startWrite(pIncPackBuf->l2Dev.uIfaceNum, 
                    (PFpdcpHwAddr)&pIncPackBuf->hwAddr, pIncPackBuf->l2Dev.uPortId, pIncPackBuf->headerBuf.data8, PF_DCP_HDR_SIZE, pIncPackBuf);
                /* update expiration time for spec packet to be xmited */
                pIncPackBuf->uExpTime = uCurrTime + PF_DCP_SPECPACK_TX_RETRY_TMO;
                if(eRes == enStatusBusy)
                {
                    PF_DCP_ERR("failed to write spec packet to L2 device (BUSY)!\n");
                    /* if L2 dev is busy we will retry to send the packet later */
                    if(--pIncPackBuf->uRetryNum == 1)
                    {
                        PF_DCP_ERR("spec packet max retries num exceeded!\n");
                        pfDcpIncomPacketFree(pCtx, pIncPackBuf);
                    }
                }
                else if(eRes != enStatusSuccess)
                {
                    PF_DCP_ERR("failed to write spec packet to L2 device (%d)!\n", eRes);
                    pfDcpIncomPacketFree(pCtx, pIncPackBuf);
                }
            }
            else
            {
                if(pIncPackBuf->uRetryNum == 1)
                    PF_DCP_ERR("spec packet max retries num exceeded!\n");
                else
                    PF_DCP_ERR("incomplete packet timeout exceeded!\n");
                if(pIncPackBuf->uSize > PF_DCP_HDR_SIZE)
                {   
                    pServ = pfDcpServiceFind(pCtx, PF_DCP_HDR_SID_GET(pIncPackBuf->headerBuf.data32[2]));
                    if(pServ != PF_NULL_PTR)
                    {
                        pEp = pfDcpInEpFind(pServ, pIncPackBuf->headerBuf.data32[4]);
                        if(pEp != PF_NULL_PTR)
                            pfDcpInEpFree(pServ, pEp);
                    }
                }
                pfDcpIncomPacketFree(pCtx, pIncPackBuf);
            }
        }
    }
		
    pdDcpUnlock(uFlags);
		for(i = 0; i < pCtx->uL2DevsNum; i++)
    {
        pDrv = (PFpDcpL2Drv)(pCtx->pL2Devs[i].pDrv);
				pDrv->rxCheck();
		}
	return enStatusSuccess;	
}

PFEnStatus pfDcpWriteCompleted(PFpgeneric pPriv, PFpgeneric pPackDesc)
{
    PFpDcpContext pCtx = (PFpDcpContext)pPriv;
    PFpDcpIncomPacketBuf pIncPackBuf = (PFpDcpIncomPacketBuf)pPackDesc;

    if(pIncPackBuf != PF_NULL_PTR)
        pfDcpIncomPacketFree(pCtx, pIncPackBuf);

	return enStatusSuccess;
}

PFEnStatus pfDcpRxHandler(PFpgeneric pArg, PFpDcpL2DevDesc pL2Dev, PFpdcpHwAddr pHwAddr, 
                          PFpdcpfxmemptr pHwBuf, PFdcpfxmemsize uHwBufSz)
{
    PFpDcpContext pCtx = (PFpDcpContext)pArg;
    PFpNode pNode;
    PFpuint32 pBuf = (PFpuint32)pHwBuf;
    PFdcpmemsize uBufSz = uHwBufSz, uHdrChunkSz;
    PFpDcpIncomPacketBuf pInPackBuf = PF_NULL_PTR, pTmpPack;

    PF_LIST_FOREACH_NODE(pNode, &(pCtx->incomPacketBufs))
    {
        pTmpPack = (PFpDcpIncomPacketBuf)pNode;
        /* skip outgoing spec packets */
        if(pTmpPack->uRetryNum > 0)
            continue;

        if(pTmpPack->l2Dev.pDrv == pL2Dev->pDrv && pTmpPack->l2Dev.uIfaceNum == pL2Dev->uIfaceNum && 
            (!(pfMemCompare(&pTmpPack->hwAddr, pHwAddr, ((PFpDcpL2Drv)(pL2Dev->pDrv))->uHwAddrSize))) == 0)
        {
            pInPackBuf = pTmpPack;
            break;
        }
    }
    /* if no active packet, look for start byte seq */
    if(pInPackBuf == PF_NULL_PTR)
    {
        /* look for start byte sequence */
        do
        {
            if(PF_DCP_HDR_SB_GET(pf_ntohl(*pBuf)) == PF_DCP_HDR_SB_MARK)
                break;
            pBuf++;
        } while((PFpdcpmemptr)pBuf < (pHwBuf + uHwBufSz));

        if((PFpdcpmemptr)pBuf >= (pHwBuf + uHwBufSz))
            return enStatusSuccess; /* no start byte for new sender = nothing happend */

        uBufSz = (pHwBuf + uHwBufSz) - (PFpdcpmemptr)pBuf;

        pInPackBuf = pfDcpIncomPacketAlloc(pCtx, pL2Dev);
        if(pInPackBuf == PF_NULL_PTR)
        {
            PF_DCP_ERR("failed to alloc incoming packet entry!\n");
            return enStatusNoMem;
        }
        pInPackBuf->uExpTime = pfDcpTimGet() + PF_DCP_RX_PACKET_TMO;
        pfMemCopy(&pInPackBuf->hwAddr, pHwAddr, ((PFpDcpL2Drv)(pL2Dev->pDrv))->uHwAddrSize);
        PF_LIST_ADDTAIL(&(pCtx->incomPacketBufs), (PFpNode)pInPackBuf);
    }

    if(pInPackBuf->uSize < PF_DCP_HDR_SIZE)
    {
        /* add data to header */
        uHdrChunkSz = (pInPackBuf->uSize + uBufSz) > PF_DCP_HDR_SIZE ? (PF_DCP_HDR_SIZE - pInPackBuf->uSize) : uBufSz;
        pfMemCopy(&(pInPackBuf->headerBuf.data8[pInPackBuf->uSize]), pBuf, uHdrChunkSz);
        uBufSz -= uHdrChunkSz;
        pBuf = (PFpuint32)((PFpuint8)pBuf +  uHdrChunkSz);  //BUG #1: typecasting 
        pInPackBuf->uSize += uHdrChunkSz;
    
        /* if header is incomplete yet */
        if(pInPackBuf->uSize < PF_DCP_HDR_SIZE)
            return enStatusSuccess;

        /* convert header */
        pInPackBuf->headerBuf.data32[0] = pf_ntohl(pInPackBuf->headerBuf.data32[0]);
        pInPackBuf->headerBuf.data32[1] = pf_ntohl(pInPackBuf->headerBuf.data32[1]);
        pInPackBuf->headerBuf.data32[2] = pf_ntohl(pInPackBuf->headerBuf.data32[2]);
        pInPackBuf->headerBuf.data32[3] = pf_ntohl(pInPackBuf->headerBuf.data32[3]);
        pInPackBuf->headerBuf.data32[4] = pf_ntohl(pInPackBuf->headerBuf.data32[4]);
    }
    
	return pfDcpPacketHandle(pCtx, pL2Dev, pInPackBuf, (PFpuint8)pBuf, uBufSz);
}

static PFpDcpServiceContext pfDcpServiceFind(PFpDcpContext pCtx, PFdcpServiceId uServId)
{
    PFdcpServNum   i;

    /* process services */
    for(i = 0; i < pCtx->uServMaxNum; i++)
    {
        if(pCtx->pServices[i] != PF_NULL_PTR && pCtx->pServices[i]->uServId == uServId)
            return pCtx->pServices[i];
    }

    return (PFpDcpServiceContext)PF_NULL_PTR;
}

static void pfDcpServiceHandle(PFpDcpServiceContext pServ)
{
    PFpNode pNode, pIter;
    PFpDcpOutEndpoint pOutEp;
    PFdcplock uFlags;
    PFEnStatus eRes;

    uFlags = pfDcpLock();

    /* handle in eps */
    PF_FOREACH_NODE_SAFE(pNode, pIter, &(pServ->inEps))
    {
        pfDcpInEpHandle(pServ, (PFpDcpInEndpoint)pNode);
    }

    /* handle out eps */
    PF_FOREACH_NODE_SAFE(pNode, pIter, &(pServ->outEps))
    {
        pOutEp = (PFpDcpOutEndpoint)pNode;
        eRes = pfDcpOutEpHandle(pServ, pOutEp);
        if(eRes == enStatusBusy)
        {
            /* if ep handling failed due to device is BUSY, the most probable is that 
               device is overloaded with packets from previous eps, so move failed ep 
               to the begining of the list to handle it first the next time user calls pfDcpTask */
            PF_LIST_REMOVE(pNode);
            PF_LIST_ADDHEAD(&(pServ->outEps), pNode);
            break;
        }
    }

    pdDcpUnlock(uFlags);
}

static PFpDcpRoute pfDcpGetRoute(PFpDcpContext pCtx, PFdcpRoutesNum* pPeerId ,PFdword* pDevAddr)
{
    PFdcpRoutesNum uRouteNum;

#if PF_DCP_CONFIG_DYNAMIC_ROUTES
    /* Look for dynamic route first */
    if(*pPeerId >= pCtx->uStaticRoutesNum)
    {
        uRouteNum = (*pPeerId) - pCtx->uStaticRoutesNum;
        if(pCtx->uDynamicRoutesInitFlag[uRouteNum] == enBooleanTrue)
        {
            *pDevAddr = pCtx->pDynamicRoutes[uRouteNum].netAddr;
            return &(pCtx->pDynamicRoutes[uRouteNum]);
        }
    }
#endif

    if(*pPeerId < pCtx->uStaticRoutesNum)
    {
        *pDevAddr = pCtx->pStaticRoutes[*pPeerId].netAddr;
        return &(pCtx->pStaticRoutes[*pPeerId]);
    }

    return (PFpDcpRoute)PF_NULL_PTR;
}

#if (PF_DCP_CONFIG_DYNAMIC_ROUTES == 1)
PFEnStatus pfDcpRouteAdd(PFbyte* pCtxId, PFdcpRoutesNum* pPeerId, PFpDcpPeerRoute pRoute)
{
    PFdcpRoutesNum i,uIndex;
    PFdcplock uFlags;
    PFEnStatus eRes = enStatusSuccess;
	PFpDcpContext pCtx;

	PF_ASSERT_RET(pCtxId != PF_NULL_PTR, enStatusInvArgs);
	PF_ASSERT_RET(gs_dcpInitFlag[*pCtxId] != enBooleanFalse, enStatusInvArgs);	
	
	pCtx = &gs_dcpProtoCtx[*pCtxId];

    PF_ASSERT_RET(pCtx != PF_NULL_PTR, enStatusInvArgs);
    PF_ASSERT_RET(pRoute != PF_NULL_PTR, enStatusInvArgs);
    //PF_ASSERT_RET(uIndex >= pCtx->uDynamicRoutesNum, enStatusInvArgs);

    for(i = 0;i<PF_DCP_MAX_DYNAMICROUTES_SUPPORTED;i++)
    {
        if(pCtx->uDynamicRoutesInitFlag[i] == enBooleanFalse)
        {
            uIndex = i;
            pCtx->uDynamicRoutesInitFlag[i] = enBooleanTrue;
            break;
        }
    }
    if(i == PF_DCP_MAX_DYNAMICROUTES_SUPPORTED)
    {
        PF_DCP_ERR("no space for new route!\n");
        return enStatusNoMem;
    }
        
    *pPeerId = uIndex+ pCtx->uStaticRoutesNum;
    PF_DCP_TRACE("route add to position %d\n", *pPeerId);

    uFlags = pfDcpLock();

    pCtx->pDynamicRoutes[uIndex].netAddr = pRoute->netAddr;
    pCtx->pDynamicRoutes[uIndex].pDev   = &gs_l2Devs[0];
    pCtx->pDynamicRoutes[uIndex].pHwAddr = pRoute->pHwAddr;
    pCtx->pDynamicRoutes[uIndex].uPortId = pRoute->uPortId;

    pCtx->uDynamicRoutesUsedNum++;

    pdDcpUnlock(uFlags);

    return eRes;
}

PFEnStatus pfDcpRouteRemove(PFbyte* pCtxId, PFdcpRoutesNum* pPeerId)
{
    PFdcpRoutesNum uIndex;
    PFdcplock uFlags;
    PFEnStatus eRes = enStatusSuccess;
	PFpDcpContext pCtx;

	PF_ASSERT_RET(pCtxId != PF_NULL_PTR, enStatusInvArgs);
    PF_ASSERT_RET(pPeerId != PF_NULL_PTR, enStatusInvArgs);
	PF_ASSERT_RET(gs_dcpInitFlag[*pCtxId] != enBooleanFalse, enStatusInvArgs);	
	PF_ASSERT_RET(*pPeerId >= pCtx->uStaticRoutesNum , enStatusInvArgs); 
    
	pCtx = &gs_dcpProtoCtx[*pCtxId];
    uIndex = *pPeerId - pCtx->uStaticRoutesNum;

    PF_ASSERT_RET(pCtx != PF_NULL_PTR, enStatusInvArgs);
    PF_ASSERT_RET(uIndex < PF_DCP_MAX_DYNAMICROUTES_SUPPORTED , enStatusInvArgs);  
    PF_ASSERT_RET(pCtx->uDynamicRoutesInitFlag[*pPeerId] != enBooleanFalse, enStatusInvArgs);	
    
    
    PF_DCP_TRACE("route remove from position %d\n", *pPeerId);

    uFlags = pfDcpLock();

    pCtx->uDynamicRoutesInitFlag[uIndex] = enBooleanFalse;
    pCtx->uDynamicRoutesUsedNum--;

    pdDcpUnlock(uFlags);

    return eRes;
}
#endif

PFpDcpPacketBuf pfDcpPacketAlloc(PFpDcpContext pCtx, PFdcpfxmemsize uPayloadSz)
{
    PFpDcpPacketBuf pPacketBuf;
    PFdcplock uFlags;
	
    /* we do not need to check payload size, because it is guaranteed that datagrams 
    are split into fragments that fit to the packet buffers */

    uFlags = pfDcpLock();
    pPacketBuf = (PFpDcpPacketBuf)pfListHeadRemove(&pCtx->freePacketBufs);
    pdDcpUnlock(uFlags);

    if(pPacketBuf == PF_NULL_PTR)
        return (PFpDcpPacketBuf)PF_NULL_PTR;

    pPacketBuf->uSize = PF_DCP_HDR_SIZE + uPayloadSz;
    pPacketBuf->uRetryTime = 0;
    pPacketBuf->uRetryNum = PF_DCP_TX_RETRIES_NUM;

    return pPacketBuf;
}

void pfDcpPacketFree(PFpDcpContext pCtx, PFpDcpPacketBuf pPacketBuf)
{
    PFdcplock uFlags;

    uFlags = pfDcpLock();
    PF_LIST_REMOVE((PFpNode)pPacketBuf);
    PF_LIST_ADDTAIL(&pCtx->freePacketBufs, (PFpNode)pPacketBuf);
    pdDcpUnlock(uFlags);
}

static PFpDcpIncomPacketBuf pfDcpIncomPacketAlloc(PFpDcpContext pCtx, PFpDcpL2DevDesc pL2Dev)
{
    PFpDcpIncomPacketBuf pIncPacketBuf;
    PFdcplock uFlags;

    uFlags = pfDcpLock();
    pIncPacketBuf = (PFpDcpIncomPacketBuf)pfListHeadRemove(&pCtx->freeIncomPacketBufs);
    pdDcpUnlock(uFlags);

    if(pIncPacketBuf == PF_NULL_PTR)
        return (PFpDcpIncomPacketBuf)PF_NULL_PTR;

    pIncPacketBuf->uSize = 0;
    pIncPacketBuf->l2Dev.uIfaceNum = pL2Dev->uIfaceNum;
    pIncPacketBuf->l2Dev.pDrv = pL2Dev->pDrv;
    pIncPacketBuf->uRetryNum = 0;

    return pIncPacketBuf;
}

void pfDcpIncomPacketFree(PFpDcpContext pCtx, PFpDcpIncomPacketBuf pIncPacketBuf)
{
    PF_LIST_REMOVE((PFpNode)pIncPacketBuf);
    PF_LIST_ADDTAIL(&pCtx->freeIncomPacketBufs, (PFpNode)pIncPacketBuf);
}

PFpDcpOutEndpoint PFDcpOutEpAlloc(PFpDcpContext pCtx)
{
    PFpDcpOutEndpoint pEp;
    PFdcplock uFlags;

    uFlags = pfDcpLock();
    pEp = (PFpDcpOutEndpoint)pfListHeadRemove(&pCtx->freeOutEps);
    pdDcpUnlock(uFlags);

    if(pEp != PF_NULL_PTR)
    {
        pEp->uAckedPackNum = 0;
        pEp->uWinSz = 1;
        PF_LIST_INIT(&(pEp->packetBufs));
    }

    return pEp;
}

void pfDcpOutEpFree(PFpDcpContext pCtx, PFpDcpOutEndpoint pEp)
{
    PFdcplock uFlags;

    uFlags = pfDcpLock();
    PF_LIST_REMOVE((PFpNode)pEp);
    PF_LIST_ADDTAIL(&pCtx->freeOutEps, (PFpNode)pEp);
    pdDcpUnlock(uFlags);
}

PFpDcpInEndpoint pfDcpInEpAlloc(PFpDcpServiceContext pServ, PFdcpSeqNum uStartSeqNum)
{
    PFpDcpInEndpoint pEp;
    PFdcplock uFlags;

    uFlags = pfDcpLock();
    pEp = (PFpDcpInEndpoint)pfListHeadRemove(&pServ->freeInEps);
    pdDcpUnlock(uFlags);

    if(pEp != PF_NULL_PTR)
    {
        pEp->uSize = 0;
        pEp->uFirstNotAckedPackNum = uStartSeqNum;
        pEp->uExpTime = pfDcpTimGet() + PF_DCP_RX_DGRAM_TMO;
#if PF_DCP_TEST_FEATURES == 1
        pEp->uCurrFragsNum = 0;
#endif
    }

    return pEp;
}

void pfDcpInEpFree(PFpDcpServiceContext pServ, PFpDcpInEndpoint pEp)
{
    PFdcplock uFlags;

    uFlags = pfDcpLock();
    PF_LIST_REMOVE((PFpNode)pEp);
    PF_LIST_ADDTAIL(&pServ->freeInEps, (PFpNode)pEp);
    pdDcpUnlock(uFlags);
}

PFuint16 pfDcpCrc16(PFpuint8 pHdr, PFEnBoolean bHdrNetOrder, PFpuint8 pBuf, PFdcpfxmemsize uLen)
{
    return 0xFFFF;
}

static PFEnStatus pfDcpPacketHandle(PFpDcpContext pCtx, PFpDcpL2DevDesc pL2Dev, PFpDcpIncomPacketBuf pIncPackBuf, 
                                PFpuint8 pBuf, PFdcpfxmemsize uBufSz)
{
    PFEnStatus eRes;
    PFpDcpServiceContext pServ;
    PFpDcpInEndpoint pEp;
    PFuint16 uCrc;
    PFdcplock uFlags;

    pIncPackBuf->uSize += uBufSz;

    if(PF_DCP_HDR_VER_GET(pIncPackBuf->headerBuf.data32[0]) != PF_DCP_VER)
    {
        PF_DCP_ERR("unsupported proto ver %x in packet!\n", PF_DCP_HDR_VER_GET(pIncPackBuf->headerBuf.data32[0]));
        /* ignore packet with unsupported protocol version */
        pfDcpIncomPacketFree(pCtx, pIncPackBuf);
        return enStatusNotSupported;
    }

    if((pIncPackBuf->headerBuf.data32[3] & pCtx->uOwnMask) != pCtx->uOwnAddr)
    {
        PF_DCP_ERR("invalid dest addr %x!\n", pIncPackBuf->headerBuf.data32[3]);
        /* ignore packet with missmatched address */
        pfDcpIncomPacketFree(pCtx, pIncPackBuf);
        return enStatusNotExist;
    }

    pServ = pfDcpServiceFind(pCtx, PF_DCP_HDR_SID_GET(pIncPackBuf->headerBuf.data32[2]));
    if(pServ == PF_NULL_PTR)
    {
        PF_DCP_ERR("unknown service ID %d!\n", PF_DCP_HDR_SID_GET(pIncPackBuf->headerBuf.data32[2]));
        if((PF_DCP_HDR_FLG_GET(pIncPackBuf->headerBuf.data32[0]) & PF_DCP_HDR_FLG_SPEC_PACK) == 0)
        {
            eRes = pfDcpRejectSend(pCtx, pL2Dev, pIncPackBuf, PF_DCP_HDR_SID_GET(pIncPackBuf->headerBuf.data32[2]));
            if(eRes != enStatusSuccess)
                PF_DCP_ERR("failed to send REJECT packet (%d)!\n", eRes);
        }
        return enStatusNotExist;
    }

    pServ->stats.uRxBytes += uBufSz;

    /* handle special packet */
    if(PF_DCP_HDR_FLG_GET(pIncPackBuf->headerBuf.data32[0]) & PF_DCP_HDR_FLG_SPEC_PACK)
    {
        uCrc = PF_DCP_HDR_CRC_GET(pIncPackBuf->headerBuf.data32[1]);
        if(uCrc != pfDcpCrc16(pIncPackBuf->headerBuf.data8, enBooleanFalse, PF_NULL_PTR, 0))
        {
            PF_DCP_ERR("spec packet with wrong CRC!\n");
            pfDcpIncomPacketFree(pCtx, pIncPackBuf);
            pServ->stats.uRxErrs++;
            return enStatusCrcError;
        }
        switch(PF_DCP_HDR_ID_GET(pIncPackBuf->headerBuf.data32[1], pIncPackBuf->headerBuf.data32[2]))
        {
        case PF_DCP_SPEC_PACKID_ACK:
            pfDcpAckHandle(pServ, PF_DCP_ACK_WS_GET(pIncPackBuf->headerBuf.data32[0]), 
                PF_DCP_HDR_SEQ_GET(pIncPackBuf->headerBuf.data32[1]), pIncPackBuf->headerBuf.data32[4]);
            break;
        case PF_DCP_SPEC_PACKID_NACK:
            pfDcpNackHandle(pServ, pIncPackBuf->headerBuf.data32[4]);
            break;
        case PF_DCP_SPEC_PACKID_DGNACK:
            pfDcpDgnackHandle(pServ, pIncPackBuf->headerBuf.data32[4]);
            break;
        case PF_DCP_SPEC_PACKID_RESET:
            pfDcpResetHandle(pServ, pIncPackBuf->headerBuf.data32[4]);
            break;
        case PF_DCP_SPEC_PACKID_REJECT:
            pfDcpRejectHandle(pServ, pIncPackBuf->headerBuf.data32[4]);
            break;
        default:
            PF_DCP_ERR("unknown special packet ID %x!\n", 
                PF_DCP_HDR_ID_GET(pIncPackBuf->headerBuf.data32[1], pIncPackBuf->headerBuf.data32[2]));
            pfDcpIncomPacketFree(pCtx, pIncPackBuf);
            return enStatusNotSupported;
        }
        pfDcpIncomPacketFree(pCtx, pIncPackBuf);
        return enStatusSuccess;
    }

    if(pIncPackBuf->uSize == PF_DCP_HDR_SIZE)
        return enStatusSuccess;

    pEp = pfDcpInEpFind(pServ, pIncPackBuf->headerBuf.data32[4]);
    if(pEp == PF_NULL_PTR)
    {
        /* for new datagram first fragment should be sync */
        if(!(PF_DCP_HDR_FLG_GET(pIncPackBuf->headerBuf.data32[0]) & PF_DCP_HDR_FLG_SYN_FRAG))
        {
            PF_DCP_ERR("non-SYNC first packet from %x:%d!\n", pIncPackBuf->headerBuf.data32[4], pServ->uServId);
            pfDcpIncomPacketFree(pCtx, pIncPackBuf);
            return enStatusError;
        }
        pEp = pfDcpInEpAlloc(pServ, PF_DCP_HDR_SEQ_GET(pIncPackBuf->headerBuf.data32[1]));
        if(pEp == PF_NULL_PTR)
        {
            PF_DCP_ERR("no mem for in ep %x:%d!\n", pIncPackBuf->headerBuf.data32[4], pServ->uServId);
            eRes = pfDcpSpecPacketSend(pServ, pL2Dev, pIncPackBuf, PF_DCP_SPEC_PACKID_RESET);
            if(eRes != enStatusSuccess)
                PF_DCP_ERR("failed to send RESET packet (%d)!\n", eRes);
            pServ->stats.uRxErrs++;
            return enStatusNoMem;
        }
        pEp->uPeerAddr = pIncPackBuf->headerBuf.data32[4];
        uFlags = pfDcpLock();
        PF_LIST_ADDTAIL(&(pServ->inEps), (PFpNode)pEp);
        pdDcpUnlock(uFlags);
    }

    pfDcpDataPacketHandle(pServ, pEp, pL2Dev, pIncPackBuf, pBuf, uBufSz);

    return enStatusSuccess;
}

PFEnStatus pfDcpSpecPacketSend(PFpDcpServiceContext pServ, PFpDcpL2DevDesc pL2Dev,
                                PFpDcpIncomPacketBuf pIncPackBuf, PFbyte uId)
{
    PFEnStatus eRes;
    PFdcpSeqNum uSeqNum = PF_DCP_HDR_SEQ_GET(pIncPackBuf->headerBuf.data32[1]);

#if PF_DCP_TEST_FEATURES == 1
    if(uId == PF_DCP_SPEC_PACKID_ACK)
        uSeqNum += pServ->uAckSeqNumShift;
#endif
    pIncPackBuf->headerBuf.data32[0] = pf_htonl(PF_DCP_HDR_SB(PF_DCP_HDR_SB_MARK) | PF_DCP_HDR_VER(PF_DCP_VER) | 
        PF_DCP_HDR_FLG(PF_DCP_HDR_FLG_SPEC_PACK) | PF_DCP_ACK_WS(PF_DCP_RX_WIN_SIZE));
    pIncPackBuf->headerBuf.data32[1] = pf_htonl(PF_DCP_HDR_SEQ(uSeqNum) | PF_DCP_HDR_ID1(uId));
#if PF_DCP_TEST_FEATURES == 1
    if(uId == PF_DCP_SPEC_PACKID_ACK)
        pIncPackBuf->headerBuf.data32[2] = pf_htonl(PF_DCP_HDR_ID2(uId) | PF_DCP_HDR_SID(pServ->uForcedAckServId));
    else
#endif
        pIncPackBuf->headerBuf.data32[2] = pf_htonl(PF_DCP_HDR_ID2(uId) | PF_DCP_HDR_SID(pServ->uServId));
    pIncPackBuf->headerBuf.data32[3] = pf_htonl(pIncPackBuf->headerBuf.data32[4]);
    pIncPackBuf->headerBuf.data32[4] = pf_htonl(pServ->pCtx->uOwnAddr);

    /* calc CRC */
    pIncPackBuf->headerBuf.data32[1] = pf_htonl(pf_ntohl(pIncPackBuf->headerBuf.data32[1]) | 
        PF_DCP_HDR_CRC(pfDcpCrc16(pIncPackBuf->headerBuf.data8, enBooleanTrue, PF_NULL_PTR, 0)));
	
    eRes = ((PFpDcpL2Drv)(pL2Dev->pDrv))->startWrite(pL2Dev->uIfaceNum, (PFpdcpHwAddr)&pIncPackBuf->hwAddr,
		pL2Dev->uPortId	, pIncPackBuf->headerBuf.data8, PF_DCP_HDR_SIZE, pIncPackBuf);

    /* update expiration time for spec packet to be xmited */
    pIncPackBuf->uExpTime = pfDcpTimGet() + PF_DCP_SPECPACK_TX_RETRY_TMO;

    if(eRes == enStatusSuccess)
    {
        /* to distinguish between incoming packets and outstanding spec packets */
        pIncPackBuf->uRetryNum = 1;
    }
    else if(eRes == enStatusBusy)
    {
        PF_DCP_ERR("failed to write spec packet to L2 device (BUSY)!\n");
        /* if L2 dev is busy we will retry to send the packet later when user will call pfDcpTask */
        pIncPackBuf->uRetryNum = PF_DCP_SPECPACK_RETRIES_NUM + 1;
    }
    else if(eRes != enStatusSuccess)
    {
        PF_DCP_ERR("failed to write spec packet to L2 device (%d)!\n", eRes);
        pfDcpIncomPacketFree(pServ->pCtx, pIncPackBuf);
    }

    return eRes;
}

PFEnStatus pfDcpRejectSend(PFpDcpContext pCtx, PFpDcpL2DevDesc pL2Dev, PFpDcpIncomPacketBuf pIncPackBuf, PFdcpServiceId uServId)
{
    PFEnStatus eRes;
    PFdcpSeqNum uSeqNum = PF_DCP_HDR_SEQ_GET(pIncPackBuf->headerBuf.data32[1]);

    pIncPackBuf->headerBuf.data32[0] = pf_htonl(PF_DCP_HDR_SB(PF_DCP_HDR_SB_MARK) | PF_DCP_HDR_VER(PF_DCP_VER) | 
        PF_DCP_HDR_FLG(PF_DCP_HDR_FLG_SPEC_PACK) | PF_DCP_ACK_WS(PF_DCP_RX_WIN_SIZE));
    pIncPackBuf->headerBuf.data32[1] = pf_htonl(PF_DCP_HDR_SEQ(uSeqNum) | PF_DCP_HDR_ID1(PF_DCP_SPEC_PACKID_REJECT));
    pIncPackBuf->headerBuf.data32[2] = pf_htonl(PF_DCP_HDR_ID2(PF_DCP_SPEC_PACKID_REJECT) | PF_DCP_HDR_SID(uServId));
    pIncPackBuf->headerBuf.data32[3] = pf_htonl(pIncPackBuf->headerBuf.data32[4]);
    pIncPackBuf->headerBuf.data32[4] = pf_htonl(pCtx->uOwnAddr);

    /* calc CRC */
    pIncPackBuf->headerBuf.data32[1] = pf_htonl(pf_ntohl(pIncPackBuf->headerBuf.data32[1]) | 
        PF_DCP_HDR_CRC(pfDcpCrc16(pIncPackBuf->headerBuf.data8, enBooleanTrue, PF_NULL_PTR, 0)));

    eRes = ((PFpDcpL2Drv)(pL2Dev->pDrv))->startWrite(pL2Dev->uIfaceNum, (PFpdcpHwAddr)&pIncPackBuf->hwAddr,
      pL2Dev->uPortId, pIncPackBuf->headerBuf.data8, PF_DCP_HDR_SIZE, pIncPackBuf);

    /* update expiration time for spec packet to be xmited */
    pIncPackBuf->uExpTime = pfDcpTimGet() + PF_DCP_SPECPACK_TX_RETRY_TMO;

    if(eRes == enStatusSuccess)
    {
        /* to distinguish between incoming packets and outstanding spec packets */
        pIncPackBuf->uRetryNum = 1;
    }
    else if(eRes == enStatusBusy)
    {
        PF_DCP_ERR("failed to write REJECT packet to L2 device (BUSY)!\n");
        /* if L2 dev is busy we will retry to send the packet later when user will call pfDcpTask */
        pIncPackBuf->uRetryNum = PF_DCP_SPECPACK_RETRIES_NUM;
    }
    else if(eRes != enStatusSuccess)
    {
        PF_DCP_ERR("failed to write REJECT packet to L2 device (%d)!\n", eRes);
        pfDcpIncomPacketFree(pCtx, pIncPackBuf);
        return eRes;
    }

    return enStatusSuccess;
}
