/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework Data Communication Protocol(DCP) library.
 *
 * 
 * Review status: NO
 *
 */  
#pragma once

/**
 * \defgroup PF_DCP_API DCP API
 * @{
 */ 


#include "prime_dcpconfig.h"
#include "prime_framework.h"
#include "prime_gpio.h"
#include "prime_sysClk.h"
#include "prime_canCommon.h"
#include "prime_can2.h"
#include "prime_delay.h"

PF_EXTERN_C_BEGIN

/** Type representing Own DCP address value */
typedef PFdword   PFdcpDevAddr;
/** Type representing pointer to Own DCP address value  */
typedef PFpuint8  PFpdcpHwAddr;
/** Type representing packet Id value */
typedef PFuint16   PFdcpPacketId;
/** Type representing pointer to packet Id value  */
typedef PFuint16 * PFpdcpPacketId;
/** Type representing service Id value */
typedef PFuint16   PFdcpServiceId;
/** Type representing lock value */
typedef PFdword    PFdcplock;
/** Type representing pointer to generic buffer*/
typedef PFpuint8    PFpdcpmemptr;
/** Type representing pointer to generic constant buffer*/
typedef PFpfxuint8  PFpdcpfxmemptr;
/** Type representing generic buffer size */
typedef PFdword    PFdcpmemsize;
/** Type representing generic buffer size */
typedef PFfxuint32  PFdcpfxmemsize;
typedef PFdword    PFdcpRoutesNum;
/** CAN Configuration structure for DCP */
typedef struct
{
	PFEnStatus (*canWrite)(PFpCanMsgHeader msgHeader,PFbyte* data, PFdword size); /**< Pointer to CAN Write Function */
	PFEnStatus (*canGetTxBufferCount)(PFdword* count);                            /**< Pointer to function to get transmit buffer count of CAN */ 
	PFEnStatus (*canRead)(PFpCanMessage message);                                 /**< Pointer to CAN Read message function */
	PFEnStatus (*canGetRxBufferCount)(PFdword* count);                            /**< Pointer to function to get recive buffer count of CAN */
}PFCfgDcpL2Can;

/** pointer to structure PFCfgDcpL2Can */
typedef PFCfgDcpL2Can * PFpCfgDcpL2Can;


/** UART Configuration structure for DCP */
typedef struct
{
	PFEnStatus (*uartWrite)(PFbyte* data, PFdword size);                    /**< Pointer to UART write function */
	PFEnStatus (*uartGetTxBufferCount)(PFdword* count);                     /**< Pointer to function to get transmit buffer count of UART */ 
	PFEnStatus (*uartRead)(PFbyte* data, PFdword size, PFdword* readBytes); /**< Pointer to UART Read function */
	PFEnStatus (*uartGetRxBufferCount)(PFdword* count);                     /**< Pointer to function to get recive buffer count of UART */
}PFCfgDcpL2Uart;

/** pointer to structure PFCfgDcpL2Uart */
typedef PFCfgDcpL2Uart * PFpCfgDcpL2Uart;

/** SPI Configuration structure for DCP */
typedef struct
{
    PFGpioPortPin spiChipSel;                                   /**< Pointer to PFGpioPortPin structure which describes the chip select pin used for the SPI interface */
    PFGpioPortPin spiDcpSync;                                   /**< Pointer to PFGpioPortPin structure which describes the synchronize pin used for synchronization between SPI master and slave.*/
    PFEnStatus (*spiChipSelect)(PFbyte* id, PFbyte pinStatus);  /**< Pointer to SPI chip select function*/
    PFEnStatus (*spiRegisterDevice)(PFbyte* id, PFpGpioPortPin chipSelect); /**< Pointer to SPI register device function */
    PFEnStatus (*spiUnregisterDevice)(PFbyte* id);                          /**< Pointer to SPI device unregister function */
#if(PF_DCP_L2SPI_ENABLE_MASTER == 1)
    PFEnStatus (*spiExchangeByte)(PFbyte *id, PFbyte data, PFbyte* rxData); /**< Pointer to SPI byte exchange function */
#else
    PFEnStatus (*spiWrite)(PFbyte* id, PFbyte* data, PFdword size);         /**< Pointer to SPI write function */
    PFEnStatus (*spiGetTxBufferCount)(PFdword* count);                      /**< Pointer to function to get transmit buffer count of SPI */ 
    PFEnStatus (*spiRead)(PFbyte* id, PFbyte* data, PFdword size, PFdword* readBytes);  /**< Pointer to SPI Read function */
    PFEnStatus (*spiGetRxBufferCount)(PFdword* count);                      /**< Pointer to function to get recive buffer count of SPI */
#endif  //(PF_DCP_L2SPI_ENABLE_MASTER == 1)
}PFCfgDcpL2Spi;

/** pointer to structure PFCfgDcpL2Spi */
typedef PFCfgDcpL2Spi * PFpCfgDcpL2Spi;



/** Structure for Peer Route */
typedef struct
{
    PFdcpDevAddr    netAddr;    /**< Peer's logical Address of DCP */
	PFdword			uPortId;    /**< Interface-port ID assigned by interface register functions */
    PFpdcpHwAddr    pHwAddr;    /**< Peer's Hardware Address of interface e.g. CAN Address*/
} PFDcpPeerRoute;

/** pointer to structure PFDcpPeerRoute */
typedef PFDcpPeerRoute * PFpDcpPeerRoute;

typedef void (*PFDcpTxResultHandler)(PFEnStatus eRes, PFdcpPacketId uId, PFpfxgeneric pPriv);
typedef void (*PFDcpRxHandler)(PFdcpDevAddr uDevAddr, PFdcpServiceId uServId, PFpdcpfxmemptr pBuf, PFdcpfxmemsize uBufSz, PFpfxgeneric pPriv);

/** DCP Configuration structure */
typedef struct
{
    PFpDcpPeerRoute        pStaticRoutes;       /**< Pointer to array of Peer Routes structer */
    PFdcpRoutesNum         uStaticRoutesNum;    /**< Pointer to specify number of peer route structure */
    PFdcpDevAddr           uOwnAddr;            /**< Own DCP address. */
} PFCfgDcp;

/** DCP Configuration structure of constant type */
typedef const PFCfgDcp PFfxCfgDcp;

/** pointer to structure PFCfgDcp */
typedef PFCfgDcp * PFpCfgDcp;

/** constant pointer to structure PFCfgDcp */
typedef const PFCfgDcp * PFpfxCfgDcp;

/** Service Configuration structure for DCP */
typedef struct
{
    PFdcpServiceId          uServId;        /**< service ID to be initialized */
    PFDcpTxResultHandler    pTxCb;          /**< Pointer to the callback function to notify application about datagram transmission status */
    PFpfxgeneric            pTxArg;         /**< Argument for the callback function pTxCb */
    PFDcpRxHandler          pRxCb;          /**< Pointer to the application receive callback function */
    PFpfxgeneric            pRxArg;         /**< Argument for the callback function pRxCb */
} PFCfgDcpService;

/** Service Configuration structure of constant type */
typedef const PFCfgDcpService PFfxCfgDcpService;

/** pointer to structure PFCfgDcpService */
typedef PFCfgDcpService * PFpCfgDcpService;

/** constant pointer to structure PFCfgDcpService */
typedef const PFCfgDcpService * PFpfxCfgDcpService;

/** Configuration structure for Datagram  transmission */
typedef struct
{
    PFdcpRoutesNum          uPeerId;        /**< Peer Id to be used for transmission */
	PFpdcpmemptr             pBuf;          /**< Pointer to the data to be sent.*/
	PFdcpmemsize            uBufSz;         /**< Total number of bytes to send. */
} PFCfgDcpTx;

/** Configuration structure of constant type for Datagram  transmission */
typedef const PFCfgDcpTx PFfxCfgDcpTx;

/** pointer to structure PFCfgDcpTx */
typedef PFCfgDcpTx * PFpCfgDcpTx;

/** constant pointer to structure PFCfgDcpTx */
typedef const PFCfgDcpTx * PFpfxCfgDcpTx;


/** Structure for DCP status Information */
typedef struct
{
    PFdword                    uTxDgrams;       /**< The total number of datagrams where successfully sent via service. */
    PFdword                    uTxDgramErrs;    /**< The total number of datagrams where not completely sent due to timeout,rejected or not received completely by remote side or when reset occurred on remote side and special packet
received with such an indication */
    PFdword                    uTxNackErrs;     /**< The total number of packets where not correctly sent due to NACK received in reply. */
    PFdword                    uTxDevErrs;      /**< The total number of packets where not sent due to packet write to L2 device(CAN,SPI or UART) failed */
    PFdword                    uTxTmoErrs;      /**< The total number of datagrams where not completely sent due to timeout. */
    PFdword                    uTxBytes;        /**< The total number of payload bytes where successfully sent via the service. */
    PFdword                    uRxDgrams;       /**< the total number of datagrams where successfully received by the service. */
    PFdword                    uRxBytes;        /**< the total number of payload bytes where successfully received by the service. */
    PFdword                    uRxErrs;         /**< the total number of RX errors (due to incorrect CRC, no available input endpoint or its payload buffer overflow, incorrect order of the packets). */
} PFDcpStats;

/** pointer to structure PFDcpStats */
typedef PFDcpStats * PFpDcpStats;

/** enumeration to represents possible DCP protocol states */
typedef enum
{
	enDcpStateInitialized = 0,  /**< DCP protocol Initialized*/
	enDcpStateUnknown = 1       /**< DCP protocol is not initialized*/
} PFENDcpState;

/** enumeration of constant type for DCP state */
typedef const PFENDcpState PFfxENDcpState;

/** pointer to  PFENDcpState */
typedef PFENDcpState * PFpENDcpState;

/** enumeration to represents possible service states */
typedef enum
{
	enDcpServiceStateInitialized = 0,   /**< Service Initialized*/
	enDcpServiceStateUnknown = 1        /**< Service is not initialized*/
} PFENDcpServiceState;

/** enumeration of constant type for Service state */
typedef const PFENDcpServiceState PFfxENDcpServiceState;

/** pointer to  PFENDcpServiceState */
typedef PFENDcpServiceState * PFpENDcpServiceState;


/**
 * This function registers CAN interface to operate it as L2 Device in DCP. 
 *
 * \param pPortId Pointer to copy Port Id generated for the interface being registered. This Id is required while registering Peer Route statically or dynamically.
 * \param pIfaceCfg Pointer to PFCfgDcpL2Can structure to configure CAN interface of DCP.
 *
 * \return CAN interface registration status.
 */
PFEnStatus pfDcpL2CanInterfaceRegister(PFdword *pPortId, PFpCfgDcpL2Can pIfaceCfg);

/**
 * This function registers UART interface to operate it as L2 Device in DCP. 
 *
 * \param pPortId Pointer to copy Port Id generated for the interface being registered. This Id is required while registering Peer Route statically or dynamically.
 * \param pIfaceCfg Pointer to PFCfgDcpL2Uart structure to configure UART interface of DCP.
 *
 * \return UART interface registration status.
 */
PFEnStatus pfDcpL2UartInterfaceRegister(PFdword *pPortId, PFpCfgDcpL2Uart pIfaceCfg);

/**
 * This function registers SPI interface to operate it as L2 Device in DCP. 
 *
 * \param pPortId Pointer to copy Port Id generated for the interface being registered. This Id is required while registering Peer Route statically or dynamically.
 * \param pIfaceCfg Pointer to PFCfgDcpL2Spi structure to configure SPI interface of DCP.
 *
 * \return SPI interface registration status.
 */
PFEnStatus pfDcpL2SpiInterfaceRegister(PFdword *pPortId, PFpCfgDcpL2Spi pIfaceCfg);

/**
 * Single function to Remove any type of interface.
 * No operation on that interface is possible once it is removed.
 *
 * \param pPortId Pointer to Port Id of the interface to be removed
 *
 * \return Interface remove status.
 */
PFEnStatus pfDcpL2InterfaceRemove(PFdword *pPortId);


/**
 * This function initializes Protocol core, interfaces and prepares global protocol context for future operations. 
 *
 * \param pCtxId Pointer to copy Protocol Context Id . This Id is required while using other DCP functions.
 * \param pPeerId Pointer to copy PeerId/PeerIds to represent static Peer Route given in config structure . This Id is required while sending datagrams.
 * \param pCfg Pointer to config structure (PFfxCfgDcp) of DCP.
 *
 * \return DCP initialization status.
 */
PFEnStatus pfDcpInitialize(PFbyte* pCtxId, PFdcpRoutesNum* pPeerId, PFpfxCfgDcp pCfg);

/**
 * This function finalize Protocol with Protocol Context Id.
 *
 * \param pCtxId Pointer to specify Protocol Context Id.
 *
 * \return DCP finalize status.
 */
PFEnStatus pfDcpFinalize(PFbyte* pCtxId);

/**
 * This function returns Protocol state as it would just started without destruction of existing services created by the user. This function shall flush all buffers, reset statuses and statistics.
 *
 * \param pCtxId Pointer to specify Protocol Context Id.
 *
 * \return DCP reset status.
 */
PFEnStatus pfDcpReset(PFbyte* pCtxId);

/**
 * This function gives Protocol state. 
 *
 * \param pCtxId Pointer to specify Protocol Context Id.
 * \param peState Pointer to copy DCP state.
 *
 * \return DCP Protocol state get status.
 */
PFEnStatus pfDcpStatusGet(PFbyte* pCtxId, PFpENDcpState peState);

/**
 * This function gives Protocol Statistics. 
 *
 * \param pCtxId Pointer to specify Protocol Context Id.
 * \param pStats Pointer to copy DCP Statistics.
 *
 * \return DCP Protocol Statistics get status.
 */
PFEnStatus pfDcpStatistisGet(PFbyte* pCtxId, PFpDcpStats pStats);

/**
 * This function adds new Peer route into the table at specific position. 
 *
 * \param pCtxId Pointer to specify Protocol Context Id.
 * \param pPeerId Pointer to copy PeerId to represent given dynamic Peer Route . This Id is required while sending datagrams.
 * \param pRoute Pointer to PFDcpPeerRoute.
 *
 * \return Route add status.
 */
PFEnStatus pfDcpRouteAdd(PFbyte* pCtxId, PFdcpRoutesNum* pPeerId, PFpDcpPeerRoute pRoute);

/**
 * This function removes Peer route from the table. 
 *
 * \param pCtxId Pointer to specify Protocol Context Id.
 * \param pPeerId Pointer to specify PeerId to be removed.
 * \param pRoute Pointer to PFDcpPeerRoute.
 *
 * \return Route Remove status.
 */
PFEnStatus pfDcpRouteRemove(PFbyte* pCtxId, PFdcpRoutesNum* pPeerId);

/**
 * This function is called each time the datagram shall be sent.
 * The function itself doesn't send the data, its purpose is to allocate packet buffers, perform datagram fragmentation (if needed), fill the packet buffers with appropriate headers & data and enqueue buffers to the appropriate output endpoint. 
 *
 * \param pServId Pointer to specify Service Context Id.
 * \param pCfg Pointer to PFfxCfgDcpTx structure which has all required information: Peer Id, pointer to the buffer and its size.
 * \param pId Pointer to copy packet Id.
 *
 * \return Datagram transmission status.
 */
PFEnStatus pfDcpDatagramTx(PFbyte* pServId, PFpfxCfgDcpTx pCfg, PFpdcpPacketId pId);

/**
 * This function initializes service and starts listening to service number/port number specified in service config structure.
 * \note Same set of service number/port number must be initialized at transmit and recive node.
 *
 * \param pCtxId Pointer to specify Protocol Context Id.
 * \param pServId Pointer to copy Service Id. This Id is required while sending datagrams and using service specific functions.
 * \param pCfg Pointer to service config structure(PFfxCfgDcpService).
 *
 * \return Service Start status.
 */
PFEnStatus pfDcpServiceStart(PFbyte* pCtxId,PFbyte* pServId, PFpfxCfgDcpService pCfg);

/**
 * This function stops listening to service number/port number and transmission process, releases all used in transmission packet buffers.
 *
 * \param pServId Pointer to specify Service Context Id.
 *
 * \return Service stop status.
 */
PFEnStatus pfDcpServiceStop(PFbyte* pServId);

/**
 * This function gives Service state. 
 *
 * \param pServId Pointer to specify Service Context Id.
 * \param peState Pointer to copy Service state.
 *
 * \return Service state get status.
 */
PFEnStatus pfDcpServiceStatusGet(PFbyte* pServId, PFpENDcpServiceState peState);

/**
 * This function gives Service Statistics. 
 *
 * \param pServId Pointer to specify Service Context Id.
 * \param pStats Pointer to copy Service Statistics.
 *
 * \return Service Statistics get status.
 */
PFEnStatus pfDcpServiceStatisticsGet(PFbyte* pServId, PFpDcpStats pStats);

/**
 * This function returns Service state as it would just started.
 * This function releases all used in transmission packet buffers.
 *
 * \param pServId Pointer to specify Service Context Id.
 *
 * \return Service reset status.
 */
PFEnStatus pfDcpServiceReset(PFbyte* pServId);

/**
 * This function should be called by the application's main loop as often as possible.
 * This function perform all periodic protocol operations (such as retransmissions, special packets handling, analysis and so on).
 * Actual datagram transmission also done by this function.
 *
 * \param pCtxId Pointer to specify Protocol Context Id.
 * \return Task status.
 */
PFEnStatus pfDcpTask(PFbyte* pCtxId);

PF_EXTERN_C_END


