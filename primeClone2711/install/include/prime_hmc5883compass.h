/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework HMC5883L Magnetometer Driver.
 * 
 *
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup HMC5883_Compass_Control_Function HMC5883 Compass Control Functions
 * @{
 */
 
#define PF_MAX_HMC5883_SUPPORTED 2

/** HMC5883 module address */
#define HMC5883_COMPASS_SLAVE_ADDR      0x3C

/** HMC5883 Register addresses */
#define HMC5883_COMPASS_CONFIG_A        0x00
#define HMC5883_COMPASS_CONFIG_B        0x01
#define HMC5883_COMPASS_MODE            0x02
#define HMC5883_COMPASS_DATAX_H         0x03
#define HMC5883_COMPASS_DATAX_L         0x04
#define HMC5883_COMPASS_DATAZ_H         0x05
#define HMC5883_COMPASS_DATAZ_L         0x06
#define HMC5883_COMPASS_DATAY_H         0x07
#define HMC5883_COMPASS_DATAY_L         0x08
#define HMC5883_COMPASS_STATUS          0x09
#define HMC5883_COMPASS_ID_A            0x0A
#define HMC5883_COMPASS_ID_B            0x0B
#define HMC5883_COMPASS_ID_C            0x0C

/** enumeration for Measurement mode of HMC5883 compass */
typedef enum{
    enHMC5883compassNormalBias=0,              /**< (default) Normal Measurement configuration mode */
    enHMC5883compassPositiveBias,              /**< force Positive Bias current through load Resistor */
    enHMC5883compassNegetiveBias               /**< force Negative Bias current through load Resistor */
}PFEnHMC5883compassBias;

/** enumeration for Data Output Rate of HMC5883 compass */
typedef enum{
    enHMC5883compassDataRate_0_75Hz=0,
    enHMC5883compassDataRate_1_5Hz,
    enHMC5883compassDataRate_3_0Hz,
    enHMC5883compassDataRate_7_5Hz,
    enHMC5883compassDataRate_15_0Hz,             /**< default Data Output Rate */
    enHMC5883compassDataRate_30_0Hz,
    enHMC5883compassDataRate_75_0Hz
}PFEnHMC5883compassDataRate;

/** enumeration for Averaging samples per Measurement */
typedef enum{
    enHMC5883compassAvgSample_1=0,               /**< default Averaging samples */
    enHMC5883compassAvgSample_2,
    enHMC5883compassAvgSample_4,
    enHMC5883compassAvgSample_8
}PFEnHMC5883compassAvgSample;

/** enumeration for Gain selection of Device */
typedef enum{
    enHMC5883compassGain_1370=0,
    enHMC5883compassGain_1090,                   /**< default Gain selection */
    enHMC5883compassGain_820,
    enHMC5883compassGain_660,
    enHMC5883compassGain_440,
    enHMC5883compassGain_390,
    enHMC5883compassGain_330,
    enHMC5883compassGain_230
}PFEnHMC5883compassGain;

/** enumeration for Device measurement Mode selection */
typedef enum{
    enHMC5883compassModeCont=0,  
    enHMC5883compassModeSingle,                   /**< default mode of measurement */
    enHMC5883compassModeIdle
}PFEnHMC5883compassMode;

/** HMC5883compass configuration structure */
typedef struct{
    PFEnHMC5883compassBias bias;                  /**< bias current polarity through load resistance */
    PFEnHMC5883compassDataRate dataRate;          /**< output data rate selection */
    PFEnHMC5883compassAvgSample avgSample;        /**< averaging samples for each measurement */
    PFEnHMC5883compassGain gain;                  /**< gain selection */
    PFEnHMC5883compassMode mode;                  /**< mode of hmc5883 operation */
		PFEnStatus (*pfIicWrite)(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFword size);
		PFEnStatus (*pfIicRead)(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFword size, PFword* readBytes);
}PFCfgHMC5883compass;

/** pointer to PFCfgHMC5883compass structure */
typedef PFCfgHMC5883compass* PPFCfgHMC5883compass;

/**
 * To initialize HMC5883L Compass module
 *
 * \param devId pointer to copy id/id's assigned by driver
 *
 * \param sensorConfig structure to config HMC5883L Compass
 *
 * \return status of initialization
 *
 * \note Function needs TWI/I2C to be initialized before calling this function
 */
PFEnStatus pfHMC5883compassOpen(PFbyte* devId, PPFCfgHMC5883compass sensorConfig,PFbyte deviceCount );

/**
 * To get heading(raw data) of hmc5883 with respect to three axes namely X, Y & Z
 *
 * \param devId specify id allocated to driver
 *
 * \param headingX pointer to copy heading with Respect to X-axis
 *
 * \param headingY pointer to copy heading with Respect to Y-axis
 *
 * \param headingZ pointer to copy heading with Respect to z-axis
 *
 * \return status of read heading with respect to three axes namely X, Y & Z
 */
PFEnStatus pfHMC5883compassGetHeading(PFbyte devId, PFword* headingX, PFword* headingY, PFword* headingZ);

/**
 * To get heading Angle in horizontal plane
 *
 * \param devId specify id allocated to driver
 *
 * \param headingAngle to copy heading Angle
 *
 * \return status of get headingAngle
 */
PFEnStatus pfHMC5883compassGetHeadingAngle(PFbyte devId, PFword* headingAngle);

/**
 * To get magnetism (magnitude of Earth's magnetic fields) in milliGauss
 *
 * \param devId specify id allocated to driver
 *
 * \param magnetismX pointer to copy magnetism with Respect to X-axis
 *
 * \param magnetismY pointer to copy magnetism with Respect to Y-axis
 *
 * \param magnetismZ pointer to copy magnetism with Respect to z-axis
 *
 * \return status of read magnetism
 */
PFEnStatus pfHMC5883compassGetMagnetism(PFbyte devId, PFword* magnetismX, PFword* magnetismY, PFword* magnetismZ);

/**
 * To read Identification Registers A, B & C
 *
 * \param devId specify id allocated to driver
 *
 * \param ida to copy Identification Register A
 *
 * \param idb to copy Identification Register B
 *
 * \param idc to copy Identification Register C
 *
 * \return status of ID read operation
 */
PFEnStatus pfHMC5883compassGetID(PFbyte devId, PFbyte* ida, PFbyte* idb, PFbyte* idc);

/** 
 * Function closes HMC5883 Compass module with devId
 *
 * \param devId specify id allocated to driver
 *
 * \return HMC5883 disable status
 */
PFEnStatus pfHmc5883close(PFbyte devId);

/** @} */ 
