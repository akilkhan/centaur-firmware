#ifndef ATMEGA2560_H_
#define ATMEGA2560_H_


//#pragma anon_unions

#define _RW volatile
#define _R volatile const

// #define PERIPH(x)	_PERIPH(x)
// #define _PERIPH(x)	PERIPH_##x
// 


typedef struct
{
	_RW PFbyte PIN;
	_RW PFbyte DDR;
	_RW PFbyte PORT;
}GPIO_typedef;

typedef struct
{
	_RW PFbyte UCSRA;
	_RW PFbyte UCSRB;
	_RW PFbyte UCSRC;
	_R PFbyte reserved;
	union
	{
		_RW PFword UBRR;
		struct
		{
			_RW PFbyte UBRRL;
			_RW PFbyte UBRRH;	
		};
	};
	_RW PFbyte UDR;
}UART_typedef;

typedef struct
{
	_RW PFbyte SPCR;
	_RW PFbyte SPSR;
	_RW PFbyte SPDR;
}SPI_typedef;


typedef struct
{
	_RW PFbyte TCCRA;
	_RW PFbyte TCCRB;
	_RW PFbyte TCNT;
	_RW PFbyte OCRA;
	_RW PFbyte OCRB;
}TIMER_8bit_typedef;


typedef struct
{
	_RW PFbyte TIMER0;
	_RW PFbyte TIMER1;
	_RW PFbyte TIMER2;
	_RW PFbyte TIMER3;
	_RW PFbyte TIMER4;
	_RW PFbyte TIMER5;
}TIMER_interrupt_typedef;

typedef struct
{
	_RW PFbyte TCCRA;
	_RW PFbyte TCCRB;
	_RW PFbyte TCCRC;
	_R  PFbyte reserved;
	union
	{
		_RW PFword TCNT;
		struct
		{
			_RW PFbyte TCNTL;
			_RW PFbyte TCNTH;
		};
	};
	union
	{
		_RW PFword ICR;
		struct
		{
			_RW PFbyte ICRL;
			_RW PFbyte ICRH;
		};
	};
	union
	{
		_RW PFword OCRA;
		struct
		{
			_RW PFbyte OCRAL;
			_RW PFbyte OCRAH;
		};
	};
	union
	{
		_RW PFword OCRB;
		struct
		{
			_RW PFbyte OCRBL;
			_RW PFbyte OCRBH;
		};
	};
	union
	{
		_RW PFword OCRC;
		struct
		{
			_RW PFbyte OCRCL;
			_RW PFbyte OCRCH;
		};
	};						
	
}TIMER_16bit_typedef;


typedef struct
{
	_RW PFbyte TWBR;
	_RW PFbyte TWSR;
	_RW PFbyte TWAR;
	_RW PFbyte TWDR;
	_RW PFbyte TWCR;
	_RW PFbyte TWAMR;
}TWI_typedef;



typedef struct
{
	_RW PFbyte ADCL;
	_RW PFbyte ADCH;
	_RW PFbyte ADCSRA;
	_RW PFbyte ADCSRB;
	_RW PFbyte ADMUX;
}ADC_typedef;

typedef struct
{
	_RW PFbyte EECR;
	_RW PFbyte EEDR;
	_RW PFbyte EEARL;
	_RW PFbyte EEARH;	
}EEPROM_typedef;

typedef struct
{
	_RW PFbyte EICRB;
	_RW PFbyte EICRA;	
}EXTINT_typedef;

typedef struct
{
	_RW PFbyte EIMSK;
}EIMSK_typedef;

typedef struct
{
	_RW PFbyte PCICR;
	_R PFbyte reserved[2];
	_RW PFbyte PCMSK0;
	_RW PFbyte PCMSK1;
	_RW PFbyte PCMSK2;
}PCINT_typedef;

typedef struct
{
	_RW PFbyte PFMCUSR;
	_R 	PFbyte reserved[11];
	_RW PFbyte PFWDTCSR;
}WDT_typedef;

typedef struct
{
	_RW PFbyte TIMER0;
	_RW PFbyte TIMER1;
	_RW PFbyte TIMER2;
	_RW PFbyte TIMER3;
	_RW PFbyte TIMER4;
	_RW PFbyte TIMER5;
	_RW PFbyte PCINT;
	_RW PFbyte EXINT;
}IntrFlag_typedef;


#define BIT_0		0x01
#define BIT_1		0x02
#define BIT_2		0x04
#define BIT_3		0x08
#define BIT_4		0x10
#define BIT_5		0x20
#define BIT_6		0x40
#define BIT_7		0x80

/** All vectors */

#define		RESET			0
#define		EINT0			1
#define		EINT1			2
#define		EINT2			3
#define		EINT3			4
#define		EINT4			5
#define		EINT5			6
#define		EINT6			7
#define		EINT7			8
#define		PCINTR0			9
#define		PCINTR1			10
#define		PCINTR2			11
#define		WDT_VECT		12
#define		TIMER2_COMPA	13
#define		TIMER2_COMPB	14
#define		TIMER2_OVF		15
#define		TIMER1_CAPT		16
#define		TIMER1_COMPA	17
#define		TIMER1_COMPB	18
#define		TIMER1_COMPC	19
#define		TIMER1_OVF		20
#define		TIMER0_COMPA	21
#define		TIMER0_COMPB	22
#define		TIMER0_OVF		23
#define		SPI_VECT		24
#define		USART0_RX		25
#define		USART0_UDRE		26
#define		USART0_TX		27
#define		ANALOG_COMP		28
#define		ADC_VECT		29
#define		EEPROM_VECT		30
#define		TIMER3_CAPT		31
#define		TIMER3_COMPA	32
#define		TIMER3_COMPB	33
#define		TIMER3_COMPC	34
#define		TIMER3_OVF		35
#define		USART1_RX		36
#define		USART1_UDRE		37
#define		USART1_TX		38
#define		I2C_VECT		39
#define		SPROGMEM		40
#define		TIMER4_CAPT		41
#define		TIMER4_COMPA	42
#define		TIMER4_COMPB	43
#define		TIMER4_COMPC	44
#define		TIMER4_OVF		45
#define		TIMER5_CAPT		46
#define		TIMER5_COMPA	47
#define		TIMER5_COMPB	48
#define		TIMER5_COMPC	49
#define		TIMER5_OVF		50
#define		USART2_RX		51
#define		USART2_UDRE		52
#define		USART2_TX		53
#define		USART3_RX		54
#define		USART3_UDRE		55
#define		USART3_TX 		56

/** ALL gpio port*/

#define PORTA_BASE  		0x20
#define PORTB_BASE  		0x23
#define PORTC_BASE  		0x26
#define PORTD_BASE 			0x29
#define PORTE_BASE  		0x2c
#define PORTF_BASE  		0x2f
#define PORTG_BASE  		0x32
#define PORTH_BASE  		0x120
#define PORTJ_BASE  		0x123
#define PORTK_BASE  		0x126
#define PORTL_BASE  		0x129

/** UART */

#define UART0_BASE  		0xC0
#define UART1_BASE  		0xC8
#define UART2_BASE  		0xD0
#define UART3_BASE  		0x130

/** SPI*/
#define SPI_BASE			0x4C

/** TIMER 8bit*/
#define TIMER0_BASE			0x44
#define TIMER2_BASE			0xB0

/** TIMER 16bit */
#define TIMER1_BASE			0x80
#define TIMER3_BASE			0x90
#define TIMER4_BASE			0xA0
#define TIMER5_BASE			0x120

#define TIMER_INT_BASE		0x6E

/** INTERRUPT FLAG */
#define INT_FLAG_BASE		0x35

/** TWI (I2c)*/
#define TWI_BASE			0xB8

/** EXTINT */
#define EXTINT_BASE			0x69

/** EXTINT */
#define EIMSK_BASE			0x3D

/** PCINT */
#define PCINT_BASE			0x68

/** ADC */
#define ADC_BASE			0x78

/** EEPROM */
#define EEPROM_BASE			0x3F

/** WDT */
#define WDT_BASE			0x54


#define GPIO_PORTA			((GPIO_typedef *) PORTA_BASE )
#define GPIO_PORTB			((GPIO_typedef *) PORTB_BASE )
#define GPIO_PORTC			((GPIO_typedef *) PORTC_BASE )
#define GPIO_PORTD			((GPIO_typedef *) PORTD_BASE )
#define GPIO_PORTE			((GPIO_typedef *) PORTE_BASE )
#define GPIO_PORTF			((GPIO_typedef *) PORTF_BASE )
#define GPIO_PORTG			((GPIO_typedef *) PORTG_BASE )
#define GPIO_PORTH			((GPIO_typedef *) PORTH_BASE )
#define GPIO_PORTJ			((GPIO_typedef *) PORTJ_BASE )
#define GPIO_PORTK			((GPIO_typedef *) PORTK_BASE )
#define GPIO_PORTL			((GPIO_typedef *) PORTL_BASE )

#define PERIPH_UART0		((UART_typedef *) UART0_BASE )
#define PERIPH_UART1		((UART_typedef *) UART1_BASE )
#define PERIPH_UART2		((UART_typedef *) UART2_BASE )
#define PERIPH_UART3		((UART_typedef *) UART3_BASE )

#define PERIPH_SPI			((SPI_typedef *) SPI_BASE )

#define PERIPH_TIMER0		((TIMER_8bit_typedef *) TIMER0_BASE )
#define PERIPH_TIMER1		((TIMER_16bit_typedef *) TIMER1_BASE )
#define PERIPH_TIMER2		((TIMER_8bit_typedef *) TIMER2_BASE )
#define PERIPH_TIMER3		((TIMER_16bit_typedef *) TIMER3_BASE )
#define PERIPH_TIMER4		((TIMER_16bit_typedef *) TIMER4_BASE )
#define PERIPH_TIMER5		((TIMER_16bit_typedef *) TIMER5_BASE )
#define TIMSK_INT			((TIMER_interrupt_typedef *) TIMER_INT_BASE )

#define PERIPH_TWI			((TWI_typedef *) TWI_BASE )
#define PERIPH_ADC			((ADC_typedef *) ADC_BASE )
#define PERIPH_EEPROM		((EEPROM_typedef *) EEPROM_BASE )
#define PERIPH_WDT			((WDT_typedef *) WDT_BASE )


#define EXTINT				((EXTINT_typedef *) EXTINT_BASE )
#define PERIPH_EIMSK		((EIMSK_typedef *) EIMSK_BASE )
#define PCINT				((PCINT_typedef *) PCINT_BASE )

#define INT_FLAG			((IntrFlag_typedef *) INT_FLAG_BASE )

#endif /* ATMEGA2560_H_ */