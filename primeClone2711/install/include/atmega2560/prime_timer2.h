/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework 8-bit Timer2 driver for ATmega2560.
 *
 * 
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_TIMER2_API Timer2 API
 * @{
 */ 
 
 #define TIMER2_CH				TIMER2
 #define TIMER2_CHANNEL			PERIPH(TIMER2_CH)
 
 
/** Enumeration for timer2 modes			*/
typedef enum
{
	enTimer2Noclock = 0,				/**No clock source is selected and the timer2 is disabled	*/
	enTimer2NoprescaledClk,			/**< clock source is selected and the timer2 is not pre-scaled	*/
	enTimer2ClkDivideby8,			/**< Clock source frequency is divided by 8	*/
	enTimer2ClkDivideby64,			/**< Clock source frequency is divided by 8	*/
	enTimer2ClkDivideby256,			/**< Clock source frequency is divided by 8	*/
	enTimer2ClkDivideby1024,			/**< Clock source frequency is divided by 8	*/
	enTimer2ExtClkFallingEdge,		/**<  timer2 with external source on falling edge 	*/
	enTimer2ExtClkRisingEdge			/**<  timer2 with external source on falling edge 	*/
}PFEnTimer2Clocksource;


/** Enumeration for external match control		*/
typedef enum
{
	enTimer2ExtMatchCtrlNone = 0,				/**< Do nothing on count match						*/
	enTimer2ExtMatchCtrlTogglePin,			/**< Toggle match pin								*/
	enTimer2ExtMatchCtrlClearPin,				/**< Clear match pin								*/
	enTimer2ExtMatchCtrlSetPin				/**< Set match pin									*/
}PFEnTimer2ExtMatchCtrl;

typedef enum
{
	enTimer2NormalMode,	/**<  timer2 in Normal Mode 	*/
	enTimer2CtcMode=0x02	/**<  timer2 in clear timer2 on compare  Mode 	*/
	
}PFEnTimer2Mode;

/**	No interrupt	*/
#define TIMER2_INT_NONE			0x00
/**	interrupt on overflow	*/
#define TIMER2_INT_OVERFLOW		0x01
/**	interrupt on matching with the value in OCR2A 	*/
#define TIMER2_INT_MATCHREG_A	0x02
/**	interrupt on matching with the value in OCR2B 	*/
#define TIMER2_INT_MATCHREG_B	0x04
/**	interrupt on matching with the value in OCR2A ,OCR2B and overflow 	*/
#define TIMER2_INT_ALL			0x07

/**		Timer2 configure structure		*/
typedef struct
{
	PFEnTimer2Clocksource	clockSource;		/**< Select clock source								*/
	PFEnTimer2Mode			timer2Mode;			/**< timer2 mode										*/
	PFbyte					matchValueA;		/**< Match register A compare value						*/
	PFbyte					matchValueB;		/**< Match register B compare value						*/
	PFEnTimer2ExtMatchCtrl 	exMatchActionA;		/**< match pin control on count match 					*/ 
	PFEnTimer2ExtMatchCtrl 	exMatchActionB;		/**< match pin control on count match 					*/
	PFbyte					interrupt;			/**< To enable or disable timer2 interrupt				*/
	PFcallback				cmpMatchACallback;	/**< Callback function for timer2 match reg A interrupt	*/
	PFcallback				cmpMatchBCallback;	/**< Callback function for timer2 match reg B interrupt	*/
	PFcallback				overFlowCallback;	/**< Callback function for timer2 overflow interrupt	*/
}PFCfgTimer2;

/** Pointer to PFCfgTiemr structure		*/
typedef PFCfgTimer2* PFpCfgTimer2;

/**
 * Initialized timer2 with given parameters
 * 
 * \param config timer2 configuration structure
 * 
 * \return timer2 initialization status
 */
PFEnStatus pfTimer2Open(PFpCfgTimer2 config);

/**
 * Stops timer2 operation and turn offs the timer2 module
 * 
 * \return timer2 turn off operation status
 */
PFEnStatus pfTimer2Close(void);

/**
 * Starts timer2 operation
 * 
 * \return timer2 start status
 */
PFEnStatus pfTimer2Start(void);

/**
 * Stops timer2 operation
 * 
 * \return timer2 stop status
 */
PFEnStatus pfTimer2Stop(void);

/**
 * Resets the timer2 operation. Timer2 will start counting from zero again.
 * 
 * \return timer2 reset status
 */
PFEnStatus pfTimer2Reset(void);

/**
 * Enables timer interrupt.
 * 
 * \param interrupt value to enable specific timer interrupt.
 * 
 * \return timer interrupt enable status.
 */
PFEnStatus pfTimer2IntEnable(PFbyte interrupt);

/**
 * Disables timer interrupt.
 * 
 * \param interrupt value to disable specific timer interrupt.
 * 
 * \return timer interrupt disable status.
 */
PFEnStatus pfTimer2IntDisable(PFbyte interrupt);

/**
 * Returns the timer2 count
 * 
 * \return timer2 count
 */
PFEnStatus pfTimer2ReadCount(PFbyte* data);

/**
* Writes new value to the match register and enables latch for the match register
*
* \param regNum index of match register to be updated
* \param regValue new value for match register
*
* \return match register update status
*/
PFEnStatus pfTimer2UpdateMatchRegister(PFbyte regNum, PFdword regVal); 



/** @} */


