/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework 16-bit Timer driver for ATmega2560.
 *
 * 
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_TIMER_API Timer API
 * @{
 */ 
 #define Timer3InputCaptureNoiseCanceler__  	0
 #define TIMER3_CH								TIMER3
 #define TIMER3_CHANNEL						PERIPH(TIMER3_CH)
 
/** Enumeration for timer modes			*/
typedef enum
{
	enTimer3Noclock = 0,			/**< No clock source is selected and the timer is disabled				*/
	enTimer3NoprescaledClk,			/**< clock source is selected and the timer is not pre-scaled			*/
	enTimer3ClkDivideby8,			/**< Clock source frequency is divided by 8								*/
	enTimer3ClkDivideby64,			/**< Clock source frequency is divided by 64							*/
	enTimer3ClkDivideby256,			/**< Clock source frequency is divided by 256							*/
	enTimer3ClkDivideby1024,		/**< Clock source frequency is divided by 1024							*/
	enTimer3ExtClkFallingEdge,		/**<  timer with external source on falling edge 						*/
	enTimer3ExtClkRisingEdge		/**<  timer with external source on falling edge 						*/
}PFEnTimer3Clocksource;


/** Enumeration for external match control		*/
typedef enum
{
	enTimer3ExtMatchCtrlNone = 0,				/**< Do nothing on count match								*/
	enTimer3ExtMatchCtrlTogglePin,				/**< Toggle match pin											*/
	enTimer3ExtMatchCtrlClearPin,				/**< Clear match pin										*/
	enTimer3ExtMatchCtrlSetPin					/**< Set match pin												*/
}PFEnTimer3ExtMatchCtrl;

typedef enum
{
	enTimer3NormalMode=0x00,	    /**<  timer in Normal Mode 												*/
	enTimer3CtcOcrMode=0x08,		/**<  timer in clear timer on compare  Mode,  compare with OCR			*/
	enTimer3CtcIcrMode=0x18			/**<  timer in clear timer on compare  Mode, compare with ICR			*/
}PFEnTimer3Mode;

/**	No interrupt	*/
#define TIMER3_INT_NONE			0x00
/**	interrupt on overflow	*/
#define TIMER3_INT_OVERFLOW		0x01
/**	interrupt on matching with the value in OCR3A 	*/
#define TIMER3_INT_MATCHREG_A	0x02
/**	interrupt on matching with the value in OCR3B 	*/
#define TIMER3_INT_MATCHREG_B	0x04
/**	interrupt on matching with the value in OCR3C 		*/
#define TIMER3_INT_MATCHREG_C	0x08
/**	interrupt on matching with the value in ICR3 		*/
#define TIMER3_INT_INPUTCAPTURE	0x20
/**	interrupt on matching with the value in OCR3A ,OCR3B,ICR3 and overflow 	*/
#define TIMER3_INT_ALL			0x2F

/**		Timer configure structure		*/
typedef struct
{
	PFEnTimer3Clocksource		clockSource;			/**< Select clock source							*/
	PFEnTimer3Mode		        timer3Mode;				/** timer mode										*/
	PFword				        matchValueA;			/**< Match register A compare value					*/
	PFword				        matchValueB;			/**< Match register B compare value					*/
	PFword				        matchValueC;			/**< Match register C compare value					*/
	PFEnTimer3ExtMatchCtrl 		exMatchActionA;			/**< match pin control on count match 				*/
	PFEnTimer3ExtMatchCtrl 		exMatchActionB;			/**< match pin control on count match 				*/ 
	PFEnTimer3ExtMatchCtrl 		exMatchActionC;			/**< match pin control on count match 				*/
	PFbyte			        	interrupt;				/**< To enable or disable timer interrupt			*/
	PFEnBoolean                 inputCaptureRisingEdge; /**< to set input capture signal edge selection 	*/
	PFcallback			        cmpMatchACallback;		/**< Callback function for timer ISR				*/
	PFcallback			        cmpMatchBCallback;
	PFcallback			        cmpMatchCCallback;
	PFcallback			        inputCaptureCallback;
	PFcallback			        overFlowCallback;
}PFCfgTimer3;

/** Pinter to PFCfgTiemr structure		*/
typedef PFCfgTimer3* PFpCfgTimer3;

/**
 * Initialized timer with given parameters
 * 
 * \param config timer configuration structure
 * 
 * \return timer initialization status
 */
PFEnStatus pfTimer3Open(PFpCfgTimer3 config);

/**
 * Stops timer operation and turn offs the timer module
 * 
 * \return timer turn off operation status
 */
PFEnStatus pfTimer3Close(void);

/**
 * Starts timer operation
 * 
 * \return timer start status
 */
PFEnStatus pfTimer3Start(void);

/**
 * Stops timer operation
 * 
 * \return timer stop status
 */
PFEnStatus pfTimer3Stop(void);

/**
 * Resets the timer operation. Timer will start counting from zero again.
 * 
 * \return timer reset status
 */
PFEnStatus pfTimer3Reset(void);

/**
 * Enables timer interrupt.
 * 
 * \param interrupt value to enable specific timer interrupt.
 * 
 * \return timer interrupt enable status.
 */
PFEnStatus pfTimer3IntEnable(PFbyte interrupt);

/**
 * Disables timer interrupt.
 * 
 * \param interrupt value to disable specific timer interrupt.
 * 
 * \return timer interrupt disable status.
 */
PFEnStatus pfTimer3IntDisable(PFbyte interrupt);

/**
 * Returns the timer count
 * 
 * \return timer count
 */
PFEnStatus pfTimer3ReadCount(PFdword* count);

/**
 * Returns Time stamp at which event occurred at ICP pin
 * 
 * \return Input capture count
 */
PFEnStatus pfTimer3ReadCaptureCount(PFdword* count);

/**
* Writes new value to the match register and enables latch for the match register
*
* \param regNum index of match register to be updated
* \param regValue new value for match register
*
* \return match register update status
*/
PFEnStatus pfTimer3UpdateMatchRegister(PFbyte regNum, PFword regVal); 

/** @} */
