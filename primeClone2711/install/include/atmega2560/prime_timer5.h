/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework 16-bit Timer driver for ATmega2560.
 *
 * 
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_TIMER_API Timer API
 * @{
 */ 
 #define Timer5InputCaptureNoiseCanceler__  	0
 #define TIMER5_CH								TIMER5
 #define TIMER5_CHANNEL						PERIPH(TIMER5_CH)
 
/** Enumeration for timer modes			*/
typedef enum
{
	enTimer5Noclock = 0,			/**< No clock source is selected and the timer is disabled				*/
	enTimer5NoprescaledClk,		/**< clock source is selected and the timer is not pre-scaled			*/
	enTimer5ClkDivideby8,			/**< Clock source frequency is divided by 8								*/
	enTimer5ClkDivideby64,			/**< Clock source frequency is divided by 64							*/
	enTimer5ClkDivideby256,		/**< Clock source frequency is divided by 256							*/
	enTimer5ClkDivideby1024,		/**< Clock source frequency is divided by 1024							*/
	enTimer5ExtClkFallingEdge,		/**<  timer with external source on falling edge 						*/
	enTimer5ExtClkRisingEdge		/**<  timer with external source on falling edge 						*/
}PFEnTimer5Clocksource;


/** Enumeration for external match control		*/
typedef enum
{
	enTimer5ExtMatchCtrlNone = 0,				/**< Do nothing on count match								*/
	enTimer5ExtMatchCtrlTogglePin,			/**< Toggle match pin											*/
	enTimer5ExtMatchCtrlClearPin,				/**< Clear match pin										*/
	enTimer5ExtMatchCtrlSetPin				/**< Set match pin												*/
}PFEnTimer5ExtMatchCtrl;

typedef enum
{
	enTimer5NormalMode=0x00,	    /**<  timer in Normal Mode 												*/
	enTimer5CtcOcrMode=0x08,		/**<  timer in clear timer on compare  Mode,  compare with OCR			*/
	enTimer5CtcIcrMode=0x18		/**<  timer in clear timer on compare  Mode, compare with ICR			*/
}PFEnTimer5Mode;

/**	No interrupt	*/
#define TIMER5_INT_NONE			0x00
/**	interrupt on overflow	*/
#define TIMER5_INT_OVERFLOW		0x01
/**	interrupt on matching with the value in OCR5A 	*/
#define TIMER5_INT_MATCHREG_A	0x02
/**	interrupt on matching with the value in OCR5B 	*/
#define TIMER5_INT_MATCHREG_B	0x04
/**	interrupt on matching with the value in OCR5C 		*/
#define TIMER5_INT_MATCHREG_C	0x08
/**	interrupt on matching with the value in ICR5 		*/
#define TIMER5_INT_INPUTCAPTURE	0x20
/**	interrupt on matching with the value in OCR5A ,OCR5B,ICR5 and overflow 	*/
#define TIMER5_INT_ALL			0x2F

/**		Timer configure structure		*/
typedef struct
{
	PFEnTimer5Clocksource		clockSource;			/**< Select clock source							*/
	PFEnTimer5Mode		        timer5Mode;				/** timer mode										*/
	PFword				        matchValueA;			/**< Match register A compare value					*/
	PFword				        matchValueB;			/**< Match register B compare value					*/
	PFword				        matchValueC;			/**< Match register C compare value					*/
	PFEnTimer5ExtMatchCtrl 		exMatchActionA;			/**< match pin control on count match 				*/
	PFEnTimer5ExtMatchCtrl 		exMatchActionB;			/**< match pin control on count match 				*/ 
	PFEnTimer5ExtMatchCtrl 		exMatchActionC;			/**< match pin control on count match 				*/
	PFbyte			        	interrupt;				/**< To enable or disable timer interrupt			*/
	PFEnBoolean                 inputCaptureRisingEdge; /**< to set input capture signal edge selection 	*/
	PFcallback			        cmpMatchACallback;		/**< Callback function for timer ISR				*/
	PFcallback			        cmpMatchBCallback;
	PFcallback			        cmpMatchCCallback;
	PFcallback			        inputCaptureCallback;
	PFcallback			        overFlowCallback;
}PFCfgTimer5;

/** Pinter to PFCfgTiemr structure		*/
typedef PFCfgTimer5* PFpCfgTimer5;

/**
 * Initialized timer with given parameters
 * 
 * \param config timer configuration structure
 * 
 * \return timer initialization status
 */
PFEnStatus pfTimer5Open(PFpCfgTimer5 config);

/**
 * Stops timer operation and turn offs the timer module
 * 
 * \return timer turn off operation status
 */
PFEnStatus pfTimer5Close(void);

/**
 * Starts timer operation
 * 
 * \return timer start status
 */
PFEnStatus pfTimer5Start(void);

/**
 * Stops timer operation
 * 
 * \return timer stop status
 */
PFEnStatus pfTimer5Stop(void);

/**
 * Resets the timer operation. Timer will start counting from zero again.
 * 
 * \return timer reset status
 */
PFEnStatus pfTimer5Reset(void);

/**
 * Enables timer interrupt.
 * 
 * \param interrupt value to enable specific timer interrupt.
 * 
 * \return timer interrupt enable status.
 */
PFEnStatus pfTimer5IntEnable(PFbyte interrupt);

/**
 * Disables timer interrupt.
 * 
 * \param interrupt value to disable specific timer interrupt.
 * 
 * \return timer interrupt disable status.
 */
PFEnStatus pfTimer5IntDisable(PFbyte interrupt);

/**
 * Returns the timer count
 * 
 * \return timer count
 */
PFEnStatus pfTimer5ReadCount(PFdword* count);

/**
 * Returns Time stamp at which event occurred at ICP pin
 * 
 * \return Input capture count
 */
PFEnStatus pfTimer5ReadCaptureCount(PFdword* count);

/**
* Writes new value to the match register and enables latch for the match register
*
* \param regNum index of match register to be updated
* \param regValue new value for match register
*
* \return match register update status
*/
PFEnStatus pfTimer5UpdateMatchRegister(PFbyte regNum, PFword regVal); 

/** @} */
