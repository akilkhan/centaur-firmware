/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework PWM2 driver for ATmega2560.
 *
 * 
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_TIMER_API Timer API
 * @{
 */ 

#define PWM2_CH				TIMER2
#define PWM2_CHANNEL			PERIPH(PWM2_CH)
#if(( PWM2_TIMER !=TIMER0) ||(PWM2_TIMER !=TIMER2))
  #error "PWM2 is available only on TIMER0 and TIMER2 for this controller "
#endif
  
#define PWM2_OVF_VECT 		__PWM2_OVF_VECT(PWM2_CHANNEL)
#define PWM2_COMPA_VECT 		__PWM2_COMPA_VECT(PWM2_CHANNEL)
#define PWM2_COMPB_VECT 		__PWM2_COMPB_VECT(PWM2_CHANNEL)
  
	
/** Enumeration for pwm modes			*/
typedef enum
{
	enPwm2Noclock = 0,				/**< No clock source is selected and the timer is disabled	*/
	enPwm2NoprescaledClk,			/**< clock source is selected and the timer is not pre-scaled	*/
	enPwm2ClkDivideby8,				/**< Clock source frequency is divided by 8	*/
	enPwm2ClkDivideby64,			/**< Clock source frequency is divided by 8	*/
	enPwm2ClkDivideby256,			/**< Clock source frequency is divided by 8	*/
	enPwm2ClkDivideby1024,			/**< Clock source frequency is divided by 8	*/
	enPwm2ExtClkFallingEdge,		/**< pwm with external source on falling edge 	*/
	enPwm2ExtClkRisingEdge			/**< pwm with external source on falling edge 	*/
}PFEnPwm2Clocksource;


/** Enumeration for external match control		*/
typedef enum
{
	enPwm2ExtMatchCtrlNone = 0,				/**< Do nothing on count match						*/
	enPwm2ExtMatchCtrlTogglePin,			/**< Toggle match pin								*/
	enPwm2ExtMatchCtrlClearPin,				/**< Clear match pin								*/
	enPwm2ExtMatchCtrlSetPin				/**< Set match pin									*/
}PFEnPwm2ExtMatchCtrl;

/**	No interrupt	*/
#define PWM2_INT_NONE			0x00
/**	interrupt on overflow	*/
#define PWM2_INT_OVERFLOW		0x01
/**	interrupt on matching with the value in OCR0A 	*/
#define PWM2_INT_MATCHREG_A	0x02
/**	interrupt on matching with the value in OCR0B 	*/
#define PWM2_INT_MATCHREG_B	0x04
/**	interrupt on matching with the value in OCR0A ,OCR0B and overflow 	*/
#define PWM2_INT_ALL			0x07

typedef enum
{
	enPwm2IntNone,				/**	No interrupt	*/
	enPwm2OverflowIntr=0x01,	/**	interrupt on overflow	*/
	enPwm2MatchRegAIntr=0x02,	/**	interrupt on matching with the value in OCR0A 	*/
	enPwm2MatchRegBIntr=0x04,	/**	interrupt on matching with the value in OCR0B 	*/
	enPwm2AllBIntr=0x07,		/**	interrupt on matching with the value in OCR0A ,OCR0B and overflow 	*/
}PFEnPwm2Intrrupt;

/**		Timer configure structure		*/
typedef struct
{
	PFEnPwm2Clocksource	    clockSource;		/**< Select clock source				*/
	PFEnPwm2Mode 			pwm2Mode;			/** PWM2 mode*/
	PFbyte				    matchValueA;		/**< Match register A compare value							*/
	PFbyte				    matchValueB;		/**< Match register B compare value							*/
	PFEnPwm2ExtMatchCtrl 	exMatchActionA;	/**< match pin control on count match 				*/ 
	PFEnPwm2ExtMatchCtrl 	exMatchActionB;	/**< match pin control on count match 				*/
	PFEnPwm2Intrrupt     	interrupt;			/**< To enable or disable timer interrupt			*/
	PFcallback			    cmpMatchACallback;	/**< Callback function for timer ISR				*/
	PFcallback			    cmpMatchBCallback;
	PFcallback			    overFlowCallback;
}PFCfgPwm2;

/** Pinter to PFCfgTiemr structure		*/
typedef PFCfgPwm2* PFpCfgPwm2;

/**
 * Initialized timer with given parameters
 * 
 * \param config timer configuration structure
 * 
 * \return timer initialization status
 */
PFEnStatus pfPwm2Open(PFpCfgPwm2 config);

/**
 * Stops timer operation and turn offs the timer module
 * 
 * \return timer turn off operation status
 */
PFEnStatus pfPwm2Close(void);

/**
 * Starts timer operation
 * 
 * \return timer start status
 */
PFEnStatus pfPwm2Start(void);

/**
 * Stops timer operation
 * 
 * \return timer stop status
 */
PFEnStatus pfPwm2Stop(void);

/**
 * Resets the timer operation. Timer will start counting from zero again.
 * 
 * \return timer reset status
 */
PFEnStatus pfPwm2Reset(void);

/**
 * Enables timer interrupt.
 * 
 * \param interrupt value to enable specific timer interrupt.
 * 
 * \return timer interrupt enable status.
 */
PFEnStatus pfPwm2IntEnable(PFbyte interrupt);

/**
 * Disables timer interrupt.
 * 
 * \param interrupt value to disable specific timer interrupt.
 * 
 * \return timer interrupt disable status.
 */
PFEnStatus pfPwm2IntDisable(PFbyte interrupt);


/**
 * Returns the timer count
 * 
 * \param count Pointer to variable where timer count will be loaded.
 *
 * \return Status.
 */
PFEnStatus pfPwm2ReadCount(PFdword* count);



/** @} */

