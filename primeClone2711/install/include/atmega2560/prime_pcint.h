/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework Pin Change Interrupt driver for ATmega2560.
 *
 * 
 * Review status: NO
 *
 */

#pragma once
/**
 * \defgroup PF_PC_INT_API PC INT API
 * @{
 */ 

/**		Enumeration for External Interrupt mode	*/
#define PCIINT_MAX_NO		24


/**		External interrupt	configure Structure	*/
typedef struct
{
	PFbyte 			pcintChannel;						/**< PCIINT channel number		*/
	PFcallback 		pcintCallback;						/**< Pointer to array of callbacks to attach to interrupt		*/
}PFCfgPcint;

typedef PFCfgPcint* PFpCfgPcint;

/**
 * The function configures and enables External Interrupt with given settings.
 *
 * \param id  Array of variables where id for all initialized interrupts will be loaded.
 *
 * \param config configuration structure which contains the settings for the external interrupt to be used.
 *
 * \param count count is the number of PCIINT interrupt to be initialized
 *
 * \return External Interrupt status.
 */
PFEnStatus pfPcintOpen(PFbyte *id,PFpCfgPcint config,PFbyte count);

/**
 * The function enables External Interrupt 
 *
 *\param id id assigned to the PCIINT interrupt.
 * \return External Interrupt status.
 */
PFEnStatus pfPcintEnable(PFbyte id);

/**
 * The function disables External Interrupt 
 *
 *\param id id assigned to the PCIINT interrupt.
 * \return External Interrupt status.
 */
PFEnStatus pfPcintDisable(PFbyte id);

/**
 * The function adds a callback to callback list if tailchaining is enabled.
 *
 * \param callback callback function to add in the callback list.
 *
 * \return add callback status
 */
//PFEnStatus pfPcintAddCallback(PFcallback callback);

/**
 * The function removes the specified callback from callback list.
 *
 * \param callback callback function to add in the callback list.
 *
 * \return remove callback status
 */
//PFEnStatus pfPcintRemoveCallback(PFcallback callback);

/**
 * The function disables External Interrupt .
 *
 *\param id id assigned to the PCIINT interrupt.
 * \return External Interrupt status.
 */
PFEnStatus pfPcintClose(PFbyte id);


/** @} */