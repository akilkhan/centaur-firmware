/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework Watchdog Timer driver for Atmega2560.
 *
 * Review status: NO
 *
 */
#pragma once
 /**
 * \defgroup PF_WDT_API Watchdog API
 * @{
 */
#define WDT_CH				WDT
#define WDT_CHANNEL			PERIPH(WDT_CH)
#define WDT_INT_HANDLER		INT_HANDLER(WDT_CH)

 /** Enumeration for timeout selection	*/
 typedef enum
 {
	enWatchdogTimeout16mS = 0,			/**< Timeout selection for 16m seconds	 */
	enWatchdogTimeout32mS = 1,			/**< Timeout selection for 32m seconds	 */
	enWatchdogTimeout64mS = 2,			/**< Timeout selection for 64m seconds	 */
	enWatchdogTimeout125mS = 3,			/**< Timeout selection for 125m seconds	 */
	enWatchdogTimeout250mS = 4,			/**< Timeout selection for 250m seconds	 */
	enWatchdogTimeout500mS = 5,			/**< Timeout selection for 500m seconds	 */
	enWatchdogTimeout1000mS = 6,		/**< Timeout selection for 1000m seconds	 */
	enWatchdogTimeout2000mS = 7,		/**< Timeout selection for 20000m seconds */
	enWatchdogTimeout4000mS =  32,		/**< Timeout selection for 40000m seconds */
	enWatchdogTimeout8000mS =  33		/**< Timeout selection for 8000m seconds	 */
  }PFEnWatchdogTimeout;
  
 /** Enumeration for mode selection of watchdog timer */
 typedef enum
 {
	enWDTReset = 2,		/**< Watchdog timer reset mode selection		*/
	enWDTResetWithInt	/**< Watchdog timer interrupt and reset mode */							
 }PFEnWatchdogMode;
  
  
 /** Watchdog configuration Structure 		*/
 typedef struct
 {
	PFEnWatchdogMode mode;			/**< Watchdog timer mode selection		*/
	PFEnWatchdogTimeout timeout;	/**< Watchdog timer timeout selection in milli sec	*/
	PFcallback 	callback			/**< Callback function for watchdog timer interrupt	*/
 }PFCfgWatchdog;
  
  
 /** Pointer to PFCfgWatchdog structure		*/
typedef PFCfgWatchdog* PFpCfgWatchdog;

 /**
 * Initialized watchdog with given parameters
 * 
 * \param config pointer to watchdog configuration structure
 * 
 * \return watchdog initialization status 	
 */
 PFEnStatus pfWatchdogOpen(PFpCfgWatchdog config);
 
 /**
 * Stop the watchdog operation and close module 	
 *
 * \return watchdog close status
 */
 PFEnStatus pfWatchdogClose(void);
 
 /**
 * To start the watchdog operation 
 *
 * \return watchdog start operation status		
 */
 PFEnStatus pfWatchdogStart(void);
 
 /**
 *To check whether interrupt flag is set or not
 * 
 * \param to store the status of interrupt flag
 *
 * \return watchdog interrupt flag status
 */
 PFEnStatus pfWatchdogChkIntFlag(PFEnBoolean *flagStatus);
 
 /**
 *
 * To update the timeout value of watchdog timer
 *
 * \param to update the timeout value of watchdog timer
 *
 * \return status whether value is updated is successful or not
 */
 PFEnStatus pfWatchdogUpdateTimeout(PFbyte timeout);
 
 /**
 * To Stop the Watchdog operation will put it in halt condition 
 *
 * \return watchdog stop operation Status		
 */
 PFEnStatus pfWatchdogStop(void);
  
  
 
 