/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework GPS driver.
 *
 * 
 * Review status: NO
 *
 */  
#pragma once


/**
 * \defgroup PF_GPS_API GPS API
 * @{
 */ 

 /**(used only in GPS debug mode) defines time out value after which driver stops processing if no proper data recived form GPS device */
#define PF_GPS_TIMEOUT 3000  /*msec*/

 /**		Enumeration for cardinal directions for GPS device */
typedef enum
{
	enGpsDirNorth = 'N',		/**< North */
	enGpsDirSouth = 'S',		/**< South */
	enGpsDirEast  = 'E',		/**< East  */
	enGpsDirWest  = 'W',		/**< West  */
}PFEnGpsDirection;


 /** 	Structure for GGA(Global Positioning System Fixed Data).		*/
typedef struct
{
	PFfloat			utcTime;				/**< UTC time in hhmmss.sss	format	*/
	PFfloat			latitude;				/**< Latitude in ddmm.mmmm format.	*/
	PFEnGpsDirection	NSIndicator;			/**< N=north(enGpsDirNorth) or S=south(enGpsDirSouth) */
	PFfloat			longitude;				/**< Longitude in dddmm.mmmm format	*/
	PFEnGpsDirection	EWIndicator;			/**< E=east(enGpsDirEast) or W=west(enGpsDirWest) */
	PFfloat			HDOP;					/**< Horizontal Dilution of Precision */
	PFfloat			mslAltitude;			/**< Antenna Altitude above/below mean-saelevel Unit:Meters	*/
	PFfloat			geoidalSeparation;		/**< Geoidal Separation	Unit:Meters	 */
	PFfloat			ageGiffCorr;			/**< Age of Diff. Corr. 0 when DGPS is not used	*/
	PFbyte				validData;				/**< 0=Invalid Data,1=GPS fix(valid Data),2=Differential GPS fix(valid Data) */
	PFbyte				usedSatellites;			/**< Satellites Used (range: 0 to 14) */
	PFbyte				checksum;				/**< Checksum value in Hex(from GGA data) */
	PFEnBoolean			validChecksum;			/**< True:Data is valid, False:Data is Invalid.(Received checksum is compared with calculated value) */
}PFGpsDataGGA;

/** 		Pointer to PFGpsDataGGA structure		*/
typedef PFGpsDataGGA* PFpGpsDataGGA;

 /** 	Structure for RMC(Recommended Minimum Navigation Information).		*/
typedef struct
{
	PFfloat			utcTime;				/**< UTC time in hhmmss.sss	format	*/
	PFfloat			latitude;				/**< Latitude in ddmm.mmmm format.	*/
	PFEnGpsDirection	NSIndicator;			/**< N=north(enGpsDirNorth) or S=south(enGpsDirSouth) */
	PFfloat			longitude;				/**< Longitude in dddmm.mmmm format		*/
	PFEnGpsDirection	EWIndicator;			/**< E=east or W=west		*/
	PFfloat			speedOverGnd;			/**< Speed over Ground Unit:Knots */
	PFfloat			courseOverGnd;			/**< Course over Ground Unit:degrees */
	PFdword				date;					/**< Date in ddmmyy */
	PFbyte				mode;					/**< A= Autonomous mode D= Differential mode E= Estimated mode */
	PFbyte				validData;				/**< A=data valid or V=data not valid. */
	PFbyte				checksum;				/**< Checksum value in Hex(from GGA data)		*/
	PFEnBoolean			validChecksum;			/**< True:Data is valid, False:Data is Invalid.(Received checksum is compared with calculated value)*/
}PFGpsDataRMC;

/** Pointer to PFGpsDataRMC structure	*/
typedef PFGpsDataRMC* PFpGpsDataRMC;

 /** Structure for VTG(Course and speed information relative to the ground).	*/
typedef struct
{
	PFfloat		course1;					/**< Measured heading in degrees	*/
	PFfloat		course2;					/**< Measured heading in degrees	*/
	PFfloat		speedKnot;					/**< Measured horizontal speed in Knots */
	PFfloat		speedKmhr;					/**< Measured horizontal speed in km/hr */
	PFbyte			reference1;					/**< Reference T:True */
	PFbyte			reference2;					/**< Reference M:Magnetic */
	PFbyte			mode;						/**< A= Autonomous mode D= Differential mode E= Estimated mode */
	PFbyte			checksum;					/**< Checksum value in Hex(from GGA data)		*/
	PFEnBoolean		validChecksum;				/**< True:Data is valid, False:Data is Invalid.(Received checksum is compared with calculated value)*/
}PFGpsDataVTG;

/** 		Pointer to PFGpsDataVTG structure		*/
typedef PFGpsDataVTG* PFpGpsDataVTG; 

 /** Structure for providing coordinates to pfGpsConvertDD function for Data conversion.	*/
typedef struct
{
	PFfloat			latitude;			/**< Latitude in ddmm.mmmm format.	*/
	PFfloat			longitude;			/**< Longitude in dddmm.mmmm format		*/
	PFEnGpsDirection	NSIndicator;		/**< N=north(enGpsDirNorth) or S=south(enGpsDirSouth) */
	PFEnGpsDirection	EWIndicator;		/**< E=east(enGpsDirEast) or W=west(enGpsDirWest) */
}PFGpsDataConvert;

typedef PFGpsDataConvert* PFpGpsDataConvert;

/**		Enumeration for baudrate to set for GPS device */
typedef enum
{
	enGpsBaudrate_4800 = 0,			/**< baudrate 4800			*/
	enGpsBaudrate_19200,			/**< baudrate 19200			*/
	enGpsBaudrate_38400,			/**< baudrate 34800			*/
	enGpsBaudrate_57600,			/**< baudrate 57600			*/
	enGpsBaudrate_115200,			/**< baudrate 115200		*/
}PFEnGpsBaudrate;

/**		Enumeration for NMEA sentence Selection for GPS device */
typedef enum
{
	enGpsSeqRMC = 0,			/**< RMC-Recommended Minimum Specific GNSS Sentence*/
	enGpsSeqVTG,				/**< VTG-Course over Ground and Ground Speed*/
	enGpsSeqGGA,				/**< GGA-GPS Fix Data*/
	enGpsSeqDefault,			/**< Restore the system default setting*/
}PFEnGpsSequence;


/**		Enumeration for NMEA port update rate Selection for GPS device */
typedef enum
{
	enGpsUpdate_1Hz = 0,		/**< Update Rate: 1Hz  (1000 milisecond)	*/
	enGpsUpdate_2Hz,			/**< Update Rate: 2Hz  (500  milisecond)	*/
	enGpsUpdate_4Hz,			/**< Update Rate: 4Hz  (250  milisecond)	*/
	enGpsUpdate_5Hz,			/**< Update Rate: 5Hz  (200  milisecond)	*/
	enGpsUpdate_8Hz,			/**< Update Rate: 8Hz  (125  milisecond)	*/
	enGpsUpdate_10Hz,			/**< Update Rate: 10Hz (100  milisecond)	*/
}PFEnGpsUpdateRate;

 /** Structure for configuration of GPS Device	*/
typedef struct
{
	PFEnGpsUpdateRate	rateHz;			/**< NMEA port update rate Selection for GPS device. 	*/
	PFEnGpsSequence		gpsSeq;			/**< NMEA sentence Selection for GPS device */
	PFEnGpsBaudrate		gpsBaudrate;	/**< Baudrate to set for GPS device		*/
	PFEnStatus (*comRead)(PFbyte* data, PFdword size, PFdword* readBytes);		/**< Function pointer of Uart Read function for GPS device */
	PFEnStatus (*comWrite)(PFbyte* data, PFdword size);		/**< Function pointer of Uart Write function for GPS device */
}PFCfgGps;

/** Pointer to PFCfgGps	*/
typedef PFCfgGps* PFpCfgGps;

/**
 * Function registers Uart Read and write function to which GPS is connected and sets Upadate rate and Baudrate to GPS.
 * It also selscts specific sequence for the output of GPS device.
 *
 * \param config pointer to PFCfgGps structure
 *
 * \return status of initialize
 * 
 * \note Function needs UART(to which GPS is connected) to be initialized before calling this function
 */
PFEnStatus pfGpsOpen(PFpCfgGps config);

/**
 * Function extracts GGA(Global Positioning System Fixed Data) from GPS and stores all values in one structure.
 *
 * \param dataGGA pointer to PFGpsDataGGA Structure to copy processed Data(GGA)
 *
 * \return status of Process
 */
PFEnStatus pfGpsProcessGGA(PFpGpsDataGGA dataGGA);

/**
 * Function extracts VTG(Course and speed information relative to the ground) from GPS and stores all values in one structure.
 *
 * \param dataVTG pointer to PFGpsDataVTG Structure to copy processed Data(GGA)
 *
 * \return status of Process
 */
PFEnStatus pfGpsProcessVTG(PFpGpsDataVTG dataVTG);

/**
 * Function extracts RMC(Recommended Minimum Navigation Information) from GPS and stores all values in one structure.
 *
 * \param dataRMC pointer to PFGpsDataRMC Structure to copy processed Data(GGA)
 *
 * \return status of Process
 */
PFEnStatus pfGpsProcessRMC(PFpGpsDataRMC dataRMC);


/**
 * Function converts GPS cordinates(latitude, longitude) from degrees and minutes (DD�MM.mmm� notation) to degrees (DD.dddddd� notation)
 *
 * \param data pointer to PFGpsDataConvert structure
 *
 * \param latitude pointer to copy converted latitude(DD�MM.mmm�) value
 *
 * \param longitude pointer to copy converted latitude(DD�MM.mmm�) value
 *
 * \return status of Conversion
 */
PFEnStatus pfGpsConvertDD(PFpGpsDataConvert data, PFfloat* latitude,PFfloat* longitude);

/**
 * Function sets Buadrate of GPS device.
 *
 * \param baud value of GPS baudrate of PFEnGpsBaudrate type.
 *
 * \return status of Baudrate set process
 */
PFEnStatus pfGpsSetBaudrate(PFEnGpsBaudrate baud);

/**
 * Function sets specific sequence form the output of GPS device.
 *
 * \param seq Sequence of GPS of PFEnGpsSequence type.
 *
 * \return status of Sequence selection process
 */
PFEnStatus pfGpsSetSequence(PFEnGpsSequence seq);

/**
 * Function sets NMEA port update rate output of GPS device.
 *
 * \param rate Update rate of PFEnGpsUpdateRate type.
 *
 * \return status of Uapdate Rate setting 
 *
 * \note 10Mz Update rate shoud be selected for fast GPS Processing
 */
PFEnStatus pfGpsSetUpdateRate(PFEnGpsUpdateRate rate);
