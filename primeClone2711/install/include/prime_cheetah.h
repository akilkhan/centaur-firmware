/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework Cheetah driver
 *
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_Cheetah_API Cheetah API
 * @{
 */
#define PF_MAX_CHEETAH_SUPPORTED 		4
/** Configuration structure for Cheetah Board		*/
typedef struct
{
	PFCfgQeiLS7366 qeiConfig;			/**< Configuration structure for LS7366 QEi chip	*/
	PFGpioPortPin motorCsGpio;			/**< Chip select pin for motor driver				*/
	PFGpioPortPin motorDirGpio;			/**< Direction pin for motor driver					*/
	PFGpioPortPin motorDisableGpio;		/**< Disable pin for motor driver					*/
	PFGpioPortPin motorEnableGpio;		/**< Enable pin for motor driver					*/
	PFbyte battMonAdcCh;				/**< ADC channel for battery voltage monitoring		*/
	PFbyte motorPwmCh;					/**< PWM channel for motor driver					*/
	PFEnStatus (*ppfPwmGetRepititionRate)(PFdword* repRateVal);
	PFEnStatus (*ppfPwmUpdateMatchRegister)(PFbyte regNum, PFdword regVal);
	PFEnStatus (*ppfAdcGetVoltageSingleConversion)(PFbyte channel, PFdword* milliVolt);
}PFCfgCheetah;
 
/** Pointer to PFCfgCheetah structure			*/
typedef PFCfgCheetah* PPFCfgCheetah;

/**
 * Initializes Cheetah board with given configuration structure.
 *
 * \param config Configuration structure for Cheetah board.
 * 
 * \return Initialization status for Cheetah board.
 */
PFEnStatus pfCheetahOpen(PFbyte* id, PPFCfgCheetah config,PFbyte count);

/**
 * Deinitialized Cheetah board
 */
PFEnStatus pfCheetahClose(PFbyte id);

/**
 * Enable or disable Cheetah board
 *
 * \param status \a enBooleanTrue to enable the board, \a enBooleanFalse to disable the board.
 *
 * \return Status
 */
PFEnStatus pfCheetahEnable(PFbyte id,PFEnBoolean status);

/**
 * Sets the direction for motor rotation.
 *
 * \param dir Direction motor rotation. Set 0 for forward and 1 for reverse direction.
 *
 * \return Status for set direction operation.
 */
PFEnStatus pfCheetahSetDirection(PFbyte id,PFbyte dir);

/**
 * Sets rotation speed for motor.
 *
 * \param speed Speed for motor must be mentioned in percentage of full speed.
 *
 * \return Status for set speed operation.
 */
PFEnStatus pfCheetahSetSpeed(PFbyte id,PFbyte speed);

/**
 * Sets rotation speed and direction for motor.
 *
 * \param dir Direction motor rotation. Set 0 for forward and 1 for reverse direction.
 * \param speed Speed for motor must be mentioned in percentage of full speed.
 *
 * \return Status for set speed operation.
 */
PFEnStatus pfCheetahMove(PFbyte id,PFbyte dir, PFbyte speed);

/**
 * Reads the quadrature encoder count.
 * 
 * \param count Pointer to load the encoder count.
 *
 * \return Status for get count operation.
 */
PFEnStatus pfCheetahGetEncoderCount(PFbyte id,PFdword* count);

/**
 * Resets the quadrature encoder count to zero.
 * 
 * \return Status for clear count operation.
 */
PFEnStatus pfCheetahClearEncoderCount(PFbyte id);

/**
 * Reads the motor battery voltage in milli volts.
 * 
 * \param battVtg Pointer to load the battery voltage in milli volts value.
 *
 * \return Status for battery voltage reading.
 */
PFEnStatus pfCheetahGetBatteryVoltage(PFbyte id,PFdword* battVtg);

/** @} */


