/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework TSL1401CS Linear Camera Driver
 *
 * Review status: NO
 *
 */

#pragma once
/**
 * \defgroup PF_TSL1401CS_API PF TSL1401CS Linear Camera API
 * @{
 */
 
/** Maximum devices supported		*/ 
#define PF_MAX_TSL1401CS_SUPPORTED 		2

/** TSL1401CS Linear Camera configuration structure */
typedef struct
{
    PFGpioPortPin siPort ;          /**< GPIO pin connected to camera SI pin */
    PFGpioPortPin clkPort;			/**< GPIO pin connected to camera CLK pin */
	PFbyte adcChannel;				/**< ADC channel connected to camera analog output */
}PFCfgTsl1401LinearCamera;

/** pointer to PFCfgTsl1401LinearCamera structure */
typedef PFCfgTsl1401LinearCamera* PFpCfgTsl1401LinearCamera;

/**
 * Initialize TSL1401CS Linear Camera
 *
 * \param id to subcribed to the TSL1401CS Linear Camera	
 * \param sensorConfig structure to initialize TSL1401CS Linear Camera
 *
 * \return status of initialization
 *
 * \note Function needs GPIO to be initialized before calling this function
 */
PFEnStatus pfTsl1401LinearCameraOpen(PFbyte *id , PFpCfgTsl1401LinearCamera sensorConfig , PFbyte cnt);

/**
 * Close TSL1401CS Linear Camera device
 *
 * \param id of the TSL1401CS Linear Camera
 *
 * \return status of close operation
 */
PFEnStatus pfTsl1401LinearCameraClose(PFbyte id);

/**
 * Read frame data from TSL1401CS Linear Camera
 *
 * \param id of the TSL1401CD Linear Camera
 * \param frame array to copy 128 pixels data
 * \param frameIntTime frame integration time in ms should be less equal to than 110ms
 *
 * \return status of get frame
 */
PFEnStatus pfTsl1401LinearCameraGetFrame(PFbyte id, PFdword frame[], PFword frameIntTime);

/**
 * Flush frame data from TSL1401CS Linear Camera to initiate fresh frame integration
 *
 * \param id of the TSL1401CS  Linear Camera
 *
 * \return status of flush frame
 */
PFEnStatus pfTsl1401LinearCameraFlush(PFbyte id);

/** @} */
