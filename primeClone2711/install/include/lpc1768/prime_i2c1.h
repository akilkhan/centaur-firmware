/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework I2C1 driver for LPC17xx.
 * 
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_I2C1_API I2C1 API
 * @{
 */

/** Configuration macros for I2C1 module		*/

#define I2C1_CH					I2C1
#define I2C1_CHANNEL			PERIPH(I2C1_CH)
#define I2C1_INT_HANDLER		INT_HANDLER(I2C1_CH)

/** 
 * Mark I2C1_USE_FIFO as 1 if internal software buffer is to be used in interrupt based communication.
 * If it is marked as 0, user should provide callbacks to handle transmit and receive interrupts.
 */
#define I2C1_USE_FIFO				0
	
#if(I2C1_USE_FIFO != 0)
/** 
 * Define size in bytes for internal software buffer.
 * The buffer size should be a non-zero and power of 2 number.
 */
	#define I2C1_BUFFER_SIZE		256
#endif	// #if(I2C1_USE_FIFO != 0)

/**	Configuration structure for I2C1		*/
typedef struct
{
	PFdword baudrate;			/**< Baudrate for I2C1 bus clock. This parameter is valid for master mode*/
	PFEnPclkDivider	clkDiv;		/**< Peripheral clock divider for I2C1 module		*/
	PFbyte dutyCycle;			/**< Duty cycle for bus clock in percentage. This parameter is valid for master mode	*/
	PFEnBoolean enableAck;		/**< Enable or disable sending acknowledgement		*/
	PFEnBoolean enableGenCall;	/**< Enable or disable acknowledgement to general call. This parameter is valid for slave mode*/
	PFbyte ownAddress[4];		/**< Set own I2C1 addresses. The addreses should be left aligned. This parameter is valid for slave mode*/
	PFbyte addrMask[4];			/**< Set mask for own I2C1 addresses. Bits set to 1 will be ignored in address comparision. The addreses should be left aligned. This parameter is valid for slave mode*/
	PFEnBoolean intEnable;
	#if(I2C1_USE_FIFO == 0)
	PFcallback callback;		/**< callback to handle I2C1 interrupt				*/
#endif	// #if(I2C1_USE_FIFO != 0)	
}PFCfgI2c1;

/** Pointer to PFCfgI2c1 structure		*/
typedef PFCfgI2c1* PFpCfgI2c1;

/**
 * @brief State diagram for I2C1 master transmitter
 *
 * @dot
 *		digraph I2C1_Master_Transmitter{
 *			idle [label = "IDLE"];
 *			start [label = "START"];
 *			send_addr [label = "SEND ADDRESS"];
 *			send_data [label = "SEND DATA"];
*			start -> send_addr [label = "START"]; 
*			send_addr -> send_data [label = "ACK"];
 *			send_addr -> idle [label = "NACK"];
 *			send_data -> send_data [label = "ACK"];
 *			send_data -> idle [label = "NACK"];
 *		}
 * @enddot
 */
 
/**
 * Initializes the I2C1 channel with provided settings
 *
 * \param config configuration structure which contains the settings for the communication channel to be used.
 * \return I2C1 initialization status.
 */
PFEnStatus pfI2c1Open(PFpCfgI2c1 config);

/**
 * Turn offs the I2C1 channel
 */
PFEnStatus pfI2c1Close(void);

/**
 * Function enables I2C1 interrupt
 * 
 * \return Interrupt enable status
 */
PFEnStatus pfI2c1IntEnable(void);

/**
 * Function disables I2C1 interrupt
 * 
 * \return Interrupt disable status
 */
PFEnStatus pfI2c1IntDisable(void);

/**
 * Function sets the given flag in I2C1 control register
 *
 * \param flag Control register flag to set
 *
 * \return status for setting control flag
 */
PFEnStatus pfI2c1SetControlFlag(PFbyte flag);

/**
 * Function clears the given flag in I2C1 control register
 *
 * \param flag Control register flag to clear
 *
 * \return status for clearing control flag
 */
PFEnStatus pfI2c1ClearControlFlag(PFbyte flag);

/**
 * Function gives current I2C1 communication state
 *
 * \param state pointer to PFbyte to load current I2C1 state
 *
 * \return status for reading current state
 */
PFEnStatus pfI2c1GetState(PFbyte* state);

/**
 * The function sends multiple bytes on I2C1 channel.  
 * If transmit interrupt is enabled, the function will enqueue the data in transmit FIFO.
 * Otherwise it will wait in the function and send each byte by polling the line status.
 *
 * \param data pointer to the data to be sent.
 * \param master I2C1 mode. Set \a enBooleanTrue if working in master mode, \a enBooleanFlase for slave mode.
 * \param slaveAddr left aligned address of slave device to write data.
 * \param size total number of bytes to send.
 *
 * \return I2C1 write status.
 */
PFEnStatus pfI2c1Write(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFdword size);

/**
 * The function reads one byte from I2C1 channel.
 * If receive interrupt is enabled, the function will read the byte from receive FIFO.
 * Otherwise it will wait in the function for one byte to receive by polling the line status.
 *
 * \param data Unsigned char pointer to the buffer where the read data should be loaded.
 * \param master I2C1 mode. Set \a enBooleanTrue if working in master mode, \a enBooleanFlase for slave mode.
 * \param slaveAddr left aligned address of slave device to read data from.
 * \param size Total number of bytes to read.
 * \param readBytes Pointer to double word, in which function will fill number bytes actually read. 
 *
 * \return I2C1 read status.
 */
PFEnStatus pfI2c1Read(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFdword size, PFdword* readBytes);

#if(I2C1_USE_FIFO != 0)
/**
 * Returns the number of bytes received in I2C1 buffer.
 *
 * \param count pointer to load number of bytes received in I2C1 buffer.
 *
 * \return status for getting received byte count
 */
PFEnStatus pfI2c1GetRxBufferCount(PFdword* count);

/**
 * This function empties the transmit buffer.
 * 
 * \return Buffer flush status.
 */
PFEnStatus pfI2c1TxBufferFlush(void);

/**
 * This function empties the receive buffer.
 * 
 * \return Buffer flush status.
 */
PFEnStatus pfI2c1RxBufferFlush(void);
#endif	// #if(I2C1_USE_FIFO != 0)

/**
 * This function transmits START condition on I2C1 channel.
 * \return I2C start status.
 */
PFEnStatus pfI2c1Start( void );

/**
 * This function transmits STOP condition on I2C1 channel.
 *
 * \return I2C stop status.
 */
 PFEnStatus pfI2c1Stop( void );

/** @} */ 


