/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework SysTick driver for LPC17xx.
 *
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_SYSTICK_API SysTick API
 * @{
 */ 
 
typedef enum
{
	enSystickClkExt = 0,					/**< Select external clock to pin STCLK as clock source 				*/
	enSystickClkCpu							/**< Select internal CPU clock as clock source							*/
}PFEnSystickClkSrc;		
 
/** Configuration structure for SysTick		*/ 
typedef struct
{
	PFdword				compareValue;		/**< Compare value for SysTick to generate interrupt, 0x00 - 0xFFFFFF	*/
	PFcallback			callback;			/**< Callback function for SysTick ISR									*/
	PFEnSystickClkSrc 	clkSource;			/**< Clock source for SysTick timer										*/
}PFCfgSystick;

/** Pointer to PFCfgSystick structure		*/
typedef PFCfgSystick* PFpCfgSystick;

/**
 * Initializes SysTick module with given parameters.
 *
 * \param config Pointer to SysTick configuration structure.
 * 
 * \return SysTick initialization status
 */
PFEnStatus pfSystickOpen(PFpCfgSystick config);

/**
 * Stops SysTick operation and turns off the SysTick module
 */
void pfSystickClose(void); 

/**
 * Return systick count which is incremented on every SysTick interrupt.
 *
 * \param count Pointer to variable where systick count will be loaded.
 * 
 * \return SysTick get count status
 */
 PFEnStatus pfSystickGetCount(PFdword* count);


/** @} */

