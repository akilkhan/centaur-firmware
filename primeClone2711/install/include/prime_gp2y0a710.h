/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework Sharp GP2Y0A710 IR sensor Driver.
 * 
 *
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup GP2Y0A710_SHARP_IR_control_Function GP2Y0A710 SHARP IR control Function
 * @{
 */
 
 #define MAX_GP2Y0A710_SENSORS_SUPPORTED	5
 
 /**
 * to initialize and open GP2Y0A710 sensor
 *
 * \param id specify id allocated to driver
 *
 * \param adc channel corresponding to the sensor
 *
 * \param Number of devices/sensors to initialize
 *
 * \return status of initialization
 *
 * \note Function needs GPIO & Timer to be initialized before calling this function
 */
 
  
PFEnStatus pfGP2Y0A710Open(PFbyte* devId,PFbyte* adcChannel,PFbyte deviceCount);

/**
 * to get distance between object and GP2Y0A710 scaled in mm (milli-meter)
 *
 * \param distance is pointer to copy distance
 *
 * \return status of get distance
 *
 * \note Function needs GPIO and ADC to be initialized before calling this function
 */
 
PFEnStatus pfGP2Y0A710GetDistance(PFbyte id, PFdword* distance);

/**
 * to get distance Raw Digital value according to the processed analog input signal
 *
 * \param id specify id allocated to driver
 * 
 * \param value is pointer to raw output value
 *
 * \return status of get distance
 *
 * \note Function needs GPIO and ADC to be initialized before calling this function
 */
 
PFEnStatus pfGP2Y0A710GetRawValue(PFbyte id, PFword* value);

/**
 * to close the GP2Y0A710 sensor with id number devId.
 *
 * \param devId specify id assigned to driver
 *
 * \return status of initialization
 */

PFEnStatus pfGP2Y0A710Close(PFbyte devId);


/** @} */
