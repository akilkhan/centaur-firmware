#pragma once
#ifdef MCU_CHIP_lpc1768
/** Convention: Jumper_Pin number_Periph_Name of the pin_port/pin */

/**							 LED configuration macros 					*/

#define		HYDRA_LED_UL1_PORT	                GPIO_PORT_4
#define 	HYDRA_LED_UL1_PIN		            GPIO_PIN_29

#define		HYDRA_LED_UL2_PORT	                GPIO_PORT_0
#define 	HYDRA_LED_UL2_PIN		            GPIO_PIN_26

#define 	HYDRA_LED_UL3_PORT	                GPIO_PORT_0
#define		HYDRA_LED_UL3_PIN		            GPIO_PIN_25

/** 					Pushbutton configuration macros 			*/

#define		HYDRA_PUSHBUTTON_PB1_PORT 	        GPIO_PORT_0
#define		HYDRA_PUSHBUTTON_PB1_PIN		    GPIO_PIN_24

#define 	HYDRA_PUSHBUTTON_PB2_PORT 	        GPIO_PORT_0
#define		HYDRA_PUSHBUTTON_PB2_PIN		    GPIO_PIN_23

#define 	HYDRA_PUSHBUTTON_PB3_PORT 	        GPIO_PORT_1
#define		HYDRA_PUSHBUTTON_PB3_PIN		    GPIO_PIN_31

/** 						UART configuration macros 						*/

/** UART 0 */
#define 	HYDRA_UART_0_TX_PORT		        GPIO_PORT_0
#define 	HYDRA_UART_0_TX_PIN			        GPIO_PIN_2

#define 	HYDRA_UART_0_RX_PORT		        GPIO_PORT_0
#define		HYDRA_UART_0_RX_PIN			        GPIO_PIN_3

/** UART 2 */
#define		HYDRA_EXP_16_UART_2_TX_PORT	        GPIO_PORT_0
#define 	HYDRA_EXP_16_UART_2_TX_PIN		    GPIO_PIN_10

#define		HYDRA_EXP_15_UART_2_RX_PORT	        GPIO_PORT_0
#define 	HYDRA_EXP_15_UART_2_RX_PIN		    GPIO_PIN_11

/** UART 3 */
#define 	HYDRA_J6_23_UART_3_TX_PORT	        GPIO_PORT_0
#define 	HYDRA_J6_23_UART_3_TX_PIN		    GPIO_PIN_25

#define 	HYDRA_J6_22_UART_3_RX_PORT	        GPIO_PORT_0
#define 	HYDRA_J6_22_UART_3_RX_PIN		    GPIO_PIN_26

/** 					External Interrupt configuration macros 					*/

/** EINT 0 */
#define 	HYDRA_J6_7_EINT_0_PORT		        GPIO_PORT_2
#define 	HYDRA_J6_7_EINT_0_PIN			    GPIO_PIN_10

/** EINT 1 */
#define		HYDRA_EXP_17_EINT_1_PORT	        GPIO_PORT_2
#define 	HYDRA_EXP_17_EINT_1_PIN		        GPIO_PIN_11

/** EINT 2 */
#define		HYDRA_EXP_18_EINT_2_PORT	        GPIO_PORT_2
#define 	HYDRA_EXP_18_EINT_2_PIN		        GPIO_PIN_12

/** EINT 3 */
#define		HYDRA_J6_6_EINT_3_PORT		        GPIO_PORT_2
#define		HYDRA_J6_6_EINT_3_PIN			    GPIO_PIN_13

/** 							ADC Channel configuration macros 								*/
	
/** ADC 0 */
#define		HYDRA_J6_25_ADC_0_PORT		        GPIO_PORT_0
#define		HYDRA_J6_25_ADC_0_PIN 		        GPIO_PIN_23

/** ADC 1 */
#define		HYDRA_J6_24_ADC_1_PORT		        GPIO_PORT_0
#define		HYDRA_J6_24_ADC_1_PIN			    GPIO_PIN_24

/** ADC 2 */
#define		HYDRA_J6_23_ADC_2_PORT		        GPIO_PORT_0
#define		HYDRA_J6_23_ADC_2_PIN			    GPIO_PIN_25

/** ADC 3 */
#define		HYDRA_J6_22_ADC_3_PORT		        GPIO_PORT_0
#define		HYDRA_J6_22_ADC_3_PIN			    GPIO_PIN_26

/** ADC 4 pin connected to Vbus */

/** ADC 5 */
#define		HYDRA_J6_26_ADC_5_PORT		        GPIO_PORT_1
#define		HYDRA_J6_26_ADC_5_PIN			    GPIO_PIN_31

/** ADC 6 and ADC 7 pins connected to USB UART's TX and RX */

/** 								I2C configuration macros 								*/

/** I2C 0 used for USB_SDA (P0.27) and USB_SCL (P0.28) */

/** I2C 1 */
#define 	HYDRA_J6_10_I2C_1_SDA_PORT		    GPIO_PORT_0
#define 	HYDRA_J6_10_I2C_1_SDA_PIN			GPIO_PIN_19

#define 	HYDRA_J6_9_I2C_1_SCL_PORT			GPIO_PORT_0
#define 	HYDRA_J6_9_I2C_1_SCL_PIN			GPIO_PIN_20

/** I2C 2 */
#define 	HYDRA_EXP_16_I2C_2_SDA_PORT		    GPIO_PORT_0
#define 	HYDRA_EXP_16_I2C_2_SDA_PIN			GPIO_PIN_10

#define 	HYDRA_EXP_15_I2C_2_SCL_PORT		    GPIO_PORT_0
#define 	HYDRA_EXP_15_I2C_2_SCL_PIN		    GPIO_PIN_11

/**							 SPI configuration macros 				*/

/** SSP 0 */
#define 	HYDRA_EXP_9_SSP_0_MOSI_PORT		    GPIO_PORT_1
#define 	HYDRA_EXP_9_SSP_0_MOSI_PIN		    GPIO_PIN_24

#define 	HYDRA_EXP_8_SSP_0_MISO_PORT		    GPIO_PORT_1
#define 	HYDRA_EXP_8_SSP_0_MISO_PIN		    GPIO_PIN_23

#define 	HYDRA_EXP_7_SSP_0_SSEL_PORT		    GPIO_PORT_1
#define 	HYDRA_EXP_7_SSP_0_SSEL_PIN		    GPIO_PIN_21

#define 	HYDRA_EXP_6_SSP_0_SCK_PORT		    GPIO_PORT_1
#define 	HYDRA_EXP_6_SSP_0_SCK_PIN			GPIO_PIN_20

/** 						CAN configuration macros 							*/

/** CAN 1 */
#define 	HYDRA_EXP_13_CAN_1_RD_1_PORT	    GPIO_PORT_0
#define		HYDRA_EXP_13_CAN_1_RD_1_PIN		    GPIO_PIN_21

#define 	HYDRA_EXP_14_CAN_1_TD_1_PORT	    GPIO_PORT_0
#define		HYDRA_EXP_14_CAN_1_TD_1_PIN		    GPIO_PIN_22

/** CAN 2 */

#define 	HYDRA_CAN_2_RD_2_PORT		        GPIO_PORT_2
#define		HYDRA_CAN_2_RD_2_PIN		        GPIO_PIN_7

#define 	HYDRA_CAN_2_TD_2_PORT		        GPIO_PORT_2
#define		HYDRA_CAN_2_TD_2_PIN		        GPIO_PIN_8

/** 							DAC configuration macros 							*/

#define 	HYDRA_J6_22_DAC_AOUT_PORT			GPIO_PORT_0
#define		HYDRA_J6_22_DAC_AOUT_PIN			GPIO_PIN_26

/** 							PWM configuration macros 							*/

/** Output from PWM channels (1-6) */
#define		HYDRA_J6_18_PWM_CH_1_PORT		    GPIO_PORT_2
#define		HYDRA_J6_18_PWM_CH_1_PIN		    GPIO_PIN_0

#define		HYDRA_J6_17_PWM_CH_2_PORT		    GPIO_PORT_2
#define		HYDRA_J6_17_PWM_CH_2_PIN		    GPIO_PIN_1

#define 	HYDRA_J6_16_PWM_CH_3_PORT		    GPIO_PORT_2
#define		HYDRA_J6_16_PWM_CH_3_PIN		    GPIO_PIN_2

#define		HYDRA_J6_15_PWM_CH_4_PORT		    GPIO_PORT_2
#define		HYDRA_J6_15_PWM_CH_4_PIN		    GPIO_PIN_3

#define		HYDRA_J6_14_PWM_CH_5_PORT		    GPIO_PORT_2
#define		HYDRA_J6_14_PWM_CH_5_PIN		    GPIO_PIN_4

#define		HYDRA_J6_13_PWM_CH_6_PORT		    GPIO_PORT_2
#define		HYDRA_J6_13_PWM_CH_6_PIN		    GPIO_PIN_5

/** Capture input for PWM */
#define		HYDRA_EXP_5_PWM_PCAP_0_PORT	        GPIO_PORT_1
#define		HYDRA_EXP_5_PWM_PCAP_0_PIN	        GPIO_PIN_28

#define		HYDRA_J6_5_PWM_PCAP_1_PORT	        GPIO_PORT_1
#define		HYDRA_J6_5_PWM_PCAP_1_PIN		    GPIO_PIN_29


/**				 Motor Control PWM (MCPWM) configuration macros 				*/


/** Output A for channels 0, 1, 2 */
/** Output Channel 1(MCOA0) used for USB interface */
#define		HYDRA_EXP_3_MCPWM_MCOA_1_PORT	    GPIO_PORT_1
#define		HYDRA_EXP_3_MCPWM_MCOA_1_PIN	    GPIO_PIN_25

#define		HYDRA_EXP_5_MCPWM_MCOA_2_PORT	    GPIO_PORT_1
#define		HYDRA_EXP_5_MCPWM_MCOA_2_PIN	    GPIO_PIN_28

/** Output B for channels 0, 1, 2 */
/** Output Channel 1(MCOA0) used for USB interface */
#define		HYDRA_EXP_4_MCPWM_MCOB_1_PORT	    GPIO_PORT_1
#define		HYDRA_EXP_4_MCPWM_MCOB_1_PIN	    GPIO_PIN_26

#define		HYDRA_J6_5_MCPWM_MCOB_2_PORT	    GPIO_PORT_1
#define		HYDRA_J6_5_MCPWM_MCOB_2_PIN		    GPIO_PIN_29

/** Low-active Fast Abort */
#define		HYDRA_EXP_7_MCPWM_MCABORT_PORT	    GPIO_PORT_1
#define		HYDRA_EXP_7_MCPWM_MCABORT_PIN		GPIO_PIN_21

/** Input for channels 0, 1, 2 */
#define	 	HYDRA_EXP_6_MCPWM_MCI_0_PORT	    GPIO_PORT_1
#define	 	HYDRA_EXP_6_MCPWM_MCI_0_PIN		    GPIO_PIN_20

#define		HYDRA_EXP_8_MCPWM_MCI_1_PORT	    GPIO_PORT_1
#define		HYDRA_EXP_8_MCPWM_MCI_1_PIN		    GPIO_PIN_23

#define		HYDRA_EXP_9_MCPWM_MCI_2_PORT	    GPIO_PORT_1
#define		HYDRA_EXP_9_MCPWM_MCI_2_PIN		    GPIO_PIN_24

/** 			Timer 0/1/2/3 configuration macros 			*/

/** Input Capture Signals */

/** Timer 0 */
#define		HYDRA_EXP_4_TIMER_0_CAP_0_PORT	    GPIO_PORT_1
#define		HYDRA_EXP_4_TIMER_0_CAP_0_PIN		GPIO_PIN_26

#define		HYDRA_EXP_10_TIMER_0_CAP_1_PORT	    GPIO_PORT_1
#define		HYDRA_EXP_10_TIMER_0_CAP_1_PIN	    GPIO_PIN_27

/** Timer 1 Capture pins connected to Vbus(P1.18) and USB_PPWR(P1.19) */

/** Timer 2 Capture pins used for CAN Interfacing */

/** Timer 3 */
#define 	HYDRA_J6_25_TIMER_3_CAP_0_PORT	    GPIO_PORT_0
#define 	HYDRA_J6_25_TIMER_3_CAP_0_PIN		GPIO_PIN_23

#define		HYDRA_J6_24_TIMER_3_CAP_1_PORT	    GPIO_PORT_0
#define 	HYDRA_J6_24_TIMER_3_CAP_1_PIN		GPIO_PIN_24

/** External Match Output pins */

/** Timer 0 */
#define		HYDRA_EXP_5_TIMER_0_MAT_0_PORT	    GPIO_PORT_1
#define		HYDRA_EXP_5_TIMER_0_MAT_0_PIN		GPIO_PIN_28

#define 	HYDRA_J6_5_TIMER_0_MAT_1_PORT		GPIO_PORT_1
#define		HYDRA_J6_5_TIMER_0_MAT_1_PIN		GPIO_PIN_29

/** Timer 1 */
/** Timer 1, Match Output for Channel 0 (MAT1[0] - (P1.22)) pin is connected to USB_PWRD */

#define 	HYDRA_EXP_3_TIMER_1_MAT_1_PORT	    GPIO_PORT_1
#define 	HYDRA_EXP_3_TIMER_1_MAT_1_PIN		GPIO_PIN_25

/** Timer 2 */
/** Timer 2, Match Output for Channel 0 (MAT2[0] - P0.6 / P4.28) is used for SD card Interface */

#define		HYDRA_J6_19_TIMER_2_MAT_1_PORT	    GPIO_PORT_4
#define		HYDRA_J6_19_TIMER_2_MAT_1_PIN		GPIO_PIN_29

/** Timer 2, Match Output for Channels 2 and 3 (MAT2[2] - P0.8, MAT2[3] - P0.9) are used for SD card Interface */

/** Timer 3 */
#define		HYDRA_EXP_16_TIMER_3_MAT_0_PORT	    GPIO_PORT_0
#define		HYDRA_EXP_16_TIMER_3_MAT_0_PIN	    GPIO_PIN_10

#define		HYDRA_EXP_15_TIMER_3_MAT_1_PORT	    GPIO_PORT_0
#define		HYDRA_EXP_15_TIMER_3_MAT_1_PIN	    GPIO_PIN_11


/** 								I2S Configuration macros 													*/

/** I2SRX_CLK (P0.4) and I2SRX_WS (P0.5) are used for CAN Interface */

/** I2SRX_SDA (P0.6), I2STX_CLK (P0.7), I2STX_SDA (P0.9) and RX_MCLK (P4.28) are used for SD Card Interface */

#define		HYDRA_J6_19_I2S_TX_MCLK_PORT		GPIO_PORT_4
#define		HYDRA_J6_19_I2S_TX_MCLK_PIN			GPIO_PIN_29

/** LED gpio pin configuration */
extern PFCfgGpio pfHydraLEDGpioCfg[3];

/** Pushbutton gpio pin configuration */
extern PFCfgGpio pfHydraPUSHBUTTONGpioCfg[3];

extern PFCfgGpio pfHydraUART0GpioCfg[2];		 /** UART0 gpio pin configuration */
extern PFCfgGpio pfHydraUART2GpioCfg[2];         /** UART2 gpio pin configuration */
extern PFCfgGpio pfHydraUART3GpioCfg[2];         /** UART3 gpio pin configuration */

extern PFCfgGpio pfHydraEINT0GpioCfg;            /** EINT0 gpio pin configuration */
extern PFCfgGpio pfHydraEINT1GpioCfg;            /** EINT1 gpio pin configuration */
extern PFCfgGpio pfHydraEINT2GpioCfg;            /** EINT2 gpio pin configuration */
extern PFCfgGpio pfHydraEINT3GpioCfg;            /** EINT3 gpio pin configuration */

extern PFCfgGpio pfHydraADC0GpioCfg;             /** ADC0 gpio pin configuration */
extern PFCfgGpio pfHydraADC1GpioCfg;             /** ADC1 gpio pin configuration */
extern PFCfgGpio pfHydraADC2GpioCfg;             /** ADC2 gpio pin configuration */
extern PFCfgGpio pfHydraADC3GpioCfg;             /** ADC3 gpio pin configuration */
extern PFCfgGpio pfHydraADC5GpioCfg;             /** ADC5 gpio pin configuration */

extern PFCfgGpio pfHydraI2C1GpioCfg[2];          /** I2C1 gpio pin configuration */
extern PFCfgGpio pfHydraI2C2GpioCfg[2];          /** I2C2 gpio pin configuration */

extern PFCfgGpio pfHydraSSP0GpioCfg[4];          /** SSP0 gpio pin configuration */

extern PFCfgGpio pfHydraCAN1GpioCfg[2];          /** CAN1 gpio pin configuration */
extern PFCfgGpio pfHydraCAN2GpioCfg[2];          /** CAN2 gpio pin configuration */

extern PFCfgGpio pfHydraDACGpioCfg;              /** DAC  gpio pin configuration */

extern PFCfgGpio pfHydraPWM1GpioCfg;             /** PWM1 gpio pin configuration */
extern PFCfgGpio pfHydraPWM2GpioCfg;             /** PWM2 gpio pin configuration */
extern PFCfgGpio pfHydraPWM3GpioCfg;             /** PWM3 gpio pin configuration */
extern PFCfgGpio pfHydraPWM4GpioCfg;             /** PWM4 gpio pin configuration */
extern PFCfgGpio pfHydraPWM5GpioCfg;             /** PWM5 gpio pin configuration */
extern PFCfgGpio pfHydraPWM6GpioCfg;             /** PWM6 gpio pin configuration */

extern PFCfgGpio pfHydraPWMCap0GpioCfg;          /** PWMCap0 gpio pin configuration */
extern PFCfgGpio pfHydraPWMCap1GpioCfg;          /** PWMCap1 gpio pin configuration */

extern PFCfgGpio pfHydraI2SGpioCfg;              /** I2S  gpio pin configuration */

extern PFCfgGpio pfHydraMCPWMAGpioCfg[2];        /** MCPWMA gpio pin configuration */
extern PFCfgGpio pfHydraMCPWMBGpioCfg[2];        /** MCPWMB gpio pin configuration */
extern PFCfgGpio pfHydraMCPWMAbortGpioCfg;       /** MCPWMAbort gpio pin configuration */
extern PFCfgGpio pfHydraMCPWMMCIGpioCfg[3];      /** MCPWMMCI gpio pin configuration */
extern PFCfgGpio pfHydraTIMER0CAPGpioCfg[2];     /** TIMER0CAP gpio pin configuration */
extern PFCfgGpio pfHydraTIMER3CAPGpioCfg[2];     /** TIMER3CAP gpio pin configuration */
extern PFCfgGpio pfHydraTIMER0MATGpioCfg[2];     /** TIMER0MAT gpio pin configuration */
extern PFCfgGpio pfHydraTIMER1MATGpioCfg;        /** TIMER1MAT gpio pin configuration */
extern PFCfgGpio pfHydraTIMER2MATGpioCfg;        /** TIMER2MAT gpio pin configuration */
extern PFCfgGpio pfHydraTIMER3MATGpioCfg[2];     /** TIMER3MAT gpio pin configuration */


#endif	// #ifdef MCU_CHIP_lpc1768
