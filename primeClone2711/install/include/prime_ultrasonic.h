/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework Ultrasonic Sensor Driver.
 * 
 *
 * Review status: NO
 *
 */

#pragma once
/**
 * \defgroup PF_ULTRASONIC_API	PF ULTRASONIC API
 * @{
 */


/** Number of ultrasonic devices supported */
#define MAX_NO_OF_ULTRASONIC_SUPPORTED 		5     

/** Ultrasonic sensor configuration structure */
typedef struct{
    PFGpioPortPin trigger;         /**< GPIO pin for connected to Trigger pin	*/
    PFGpioPortPin echo;            /**< GPIO pin for connected to Echo pin	 	*/
}PFCfgUltrasonic;

/** Pointer to PFCfgUltrasonic structure */
typedef PFCfgUltrasonic* PFpCfgUltrasonic;

/**
 * Initialize ultrasonic sensor
 *
 * \param devId Pointer to specify id allocated to driver
 * \param configs Structure to initialize the ultrasonic sensor
 *
 * \return Status
 *
 * \note Function needs GPIO & Timer to be initialized before calling this function
 */
PFEnStatus pfUltrasonicOpen(PFbyte* devId,PFpCfgUltrasonic configs, PFbyte deviceCount);

/**
 * Close ultrasonic sensor
 *
 * \param devId Pointer to specify id assigned to driver
 *
 * \return Status
 */
PFEnStatus pfUltrasonicClose(PFbyte* devId);

/**
 * Get distance of obstacle
 *
 * \param devId Pointer to specify id allocated to driver
 * \param distance Pointer to copy distance in mm
 *
 * \return Status
 */
PFEnStatus pfUltrasonicGetDistance(PFbyte* devId, PFdword* distance);


/** @} */

