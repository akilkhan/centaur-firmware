/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework MPU6050 driver.
 *
 * 
 * Review status: NO
 *
 */
#pragma once
/** 
 * \defgroup MPU6050_Register_Addresses MPU6050 Register Addresses 
 * @{
 */
#define PF_MAX_MPU6050_SUPPORTED 2
#define MPU6050_MAX_DEVICE_CONNECTED 2


#define AUX_VDDIO						0x01
#define MPU6050_RA_SMPLRT_DIV			0x19
#define MPU6050_RA_CONFIG				0x1A
#define MPU6050_RA_GYRO_CONFIG			0x1B
#define MPU6050_RA_ACCEL_CONFIG			0x1C
#define MPU6050_RA_FF_THR				0x1D 
#define MPU6050_RA_FF_DUR				0x1E 
#define MPU6050_RA_MOT_THR				0x1F
#define MPU6050_RA_MOT_DUR				0x20 
#define MPU6050_RA_ZRMOT_THR			0x21 
#define MPU6050_RA_ZRMOT_DUR			0x22 
#define MPU6050_RA_FIFO_EN				0x23
#define MPU6050_RA_I2C_MST_CTRL			0x24
#define MPU6050_RA_I2C_SLV0_ADDR		0x25
#define MPU6050_RA_I2C_SLV0_REG			0x26
#define MPU6050_RA_I2C_SLV0_CTRL		0x27
#define MPU6050_RA_I2C_SLV1_ADDR		0x28
#define MPU6050_RA_I2C_SLV1_REG			0x29
#define MPU6050_RA_I2C_SLV1_CTRL		0x2A
#define MPU6050_RA_I2C_SLV2_ADDR		0x2B
#define MPU6050_RA_I2C_SLV2_REG			0x2C
#define MPU6050_RA_I2C_SLV2_CTRL		0x2D
#define MPU6050_RA_I2C_SLV3_ADDR		0x2E
#define MPU6050_RA_I2C_SLV3_REG			0x2F
#define MPU6050_RA_I2C_SLV3_CTRL		0x30
#define MPU6050_RA_I2C_SLV4_ADDR		0x31
#define MPU6050_RA_I2C_SLV4_REG			0x32
#define MPU6050_RA_I2C_SLV4_DO			0x33
#define MPU6050_RA_I2C_SLV4_CTRL		0x34
#define MPU6050_RA_I2C_SLV4_DI			0x35
#define MPU6050_RA_I2C_MST_STATUS		0x36
#define MPU6050_RA_INT_PIN_CFG			0x37
#define MPU6050_RA_INT_ENABLE			0x38
#define MPU6050_RA_DMP_INT_STATUS		0x39//
#define MPU6050_RA_INT_STATUS			0x3A
#define MPU6050_RA_ACCEL_XOUT_H			0x3B
#define MPU6050_RA_ACCEL_XOUT_L			0x3C
#define MPU6050_RA_ACCEL_YOUT_H			0x3D
#define MPU6050_RA_ACCEL_YOUT_L			0x3E
#define MPU6050_RA_ACCEL_ZOUT_H			0x3F
#define MPU6050_RA_ACCEL_ZOUT_L			0x40
#define MPU6050_RA_TEMP_OUT_H			0x41
#define MPU6050_RA_TEMP_OUT_L			0x42
#define MPU6050_RA_GYRO_XOUT_H			0x43
#define MPU6050_RA_GYRO_XOUT_L			0x44
#define MPU6050_RA_GYRO_YOUT_H			0x45
#define MPU6050_RA_GYRO_YOUT_L			0x46
#define MPU6050_RA_GYRO_ZOUT_H			0x47
#define MPU6050_RA_GYRO_ZOUT_L			0x48
#define MPU6050_RA_EXT_SENS_DATA_00		0x49
#define MPU6050_RA_EXT_SENS_DATA_01		0x4A
#define MPU6050_RA_EXT_SENS_DATA_02		0x4B
#define MPU6050_RA_EXT_SENS_DATA_03		0x4C
#define MPU6050_RA_EXT_SENS_DATA_04		0x4D
#define MPU6050_RA_EXT_SENS_DATA_05		0x4E
#define MPU6050_RA_EXT_SENS_DATA_06		0x4F
#define MPU6050_RA_EXT_SENS_DATA_07		0x50
#define MPU6050_RA_EXT_SENS_DATA_08		0x51
#define MPU6050_RA_EXT_SENS_DATA_09		0x52
#define MPU6050_RA_EXT_SENS_DATA_10		0x53
#define MPU6050_RA_EXT_SENS_DATA_11		0x54
#define MPU6050_RA_EXT_SENS_DATA_12		0x55
#define MPU6050_RA_EXT_SENS_DATA_13		0x56
#define MPU6050_RA_EXT_SENS_DATA_14		0x57
#define MPU6050_RA_EXT_SENS_DATA_15		0x58
#define MPU6050_RA_EXT_SENS_DATA_16		0x59
#define MPU6050_RA_EXT_SENS_DATA_17		0x5A
#define MPU6050_RA_EXT_SENS_DATA_18		0x5B
#define MPU6050_RA_EXT_SENS_DATA_19		0x5C
#define MPU6050_RA_EXT_SENS_DATA_20		0x5D
#define MPU6050_RA_EXT_SENS_DATA_21		0x5E
#define MPU6050_RA_EXT_SENS_DATA_22		0x5F
#define MPU6050_RA_EXT_SENS_DATA_23		0x60
#define MPU6050_RA_MOT_DETECT_STATUS	0x61//
#define MPU6050_RA_I2C_SLV0_DO			0x63
#define MPU6050_RA_I2C_SLV1_DO			0x64
#define MPU6050_RA_I2C_SLV2_DO			0x65
#define MPU6050_RA_I2C_SLV3_DO			0x66
#define MPU6050_RA_I2C_MST_DELAY_CTRL	0x67
#define MPU6050_RA_SIGNAL_PATH_RESET	0x68
#define MPU6050_RA_MOT_DETECT_CTRL		0x69
#define MPU6050_RA_USER_CTRL			0x6A
#define MPU6050_RA_PWR_MGMT_1			0x6B
#define MPU6050_RA_PWR_MGMT_2			0x6C
#define MPU6050_RA_BANK_SEL				0x6D//
#define MPU6050_RA_MEM_START_ADDR		0x6E//
#define MPU6050_RA_MEM_R_W				0x6F//
#define MPU6050_RA_DMP_CFG_1			0x70//
#define MPU6050_RA_DMP_CFG_2			0x71//
#define MPU6050_RA_FIFO_COUNTH			0x72
#define MPU6050_RA_FIFO_COUNTL			0x73
#define MPU6050_RA_FIFO_R_W				0x74
#define MPU6050_RA_WHO_AM_I				0x75


/** 
 * \defgroup MPU6050_Control_Funtions MPU6050 Control Functions 
 * @{
 */


#define MPU6050_CONFIGINTACTIVELOW      	    0        /** Interrupt connection configuration */
#define MPU6050_INTREADCLEAR            		  1         /** Interrupt status bit clear by any read operation */ 
#define MPU6050_INTERRUPTDRIVE           			0         /** Interrupt drive open drain */
#define MPU6050_USEFIFO                   		1         /** Use MPU6050's hardware FIFO */
#define MPU6050_CONFIGFSYNCINTACTIVEHIGH   		1         /** select logic level for FSYNC pin active low */
#define MPU6050_FSYNCINTENABLE            		0         /** Enable Interrupt from FSYNC pin */
#define MPU6050_I2CBYPASSENABLE           		1         /** to allow host processor direct access to Auxiliary I2C bus */
#define MPU6050_I2CMASTERMODEENABLE      			0         /** Enable I2C master mode */





/** enumeration for External Frame Synchronization selection */
typedef enum{
    enMPU6050_extSyncNone=0,
    enMPU6050_extSyncTempOutL,
    enMPU6050_extSyncGyroXout,
    enMPU6050_extSyncGyroYout,
    enMPU6050_extSyncGyroZout,
    enMPU6050_extSyncAccelXout,
    enMPU6050_extSyncAccelYout,
    enMPU6050_extSyncAccelZout
}PFEnMPU6050_extSync;

/** enumeration for Digital Low Pass Filter Bandwidth selectio */
typedef enum{
    enMPU6050_DlpfBandWidth_256=0,
    enMPU6050_DlpfBandWidth_188,
    enMPU6050_DlpfBandWidth_98,
    enMPU6050_DlpfBandWidth_42,
    enMPU6050_DlpfBandWidth_20,
    enMPU6050_DlpfBandWidth_10,
    enMPU6050_DlpfBandWidth_5
}PFEnMPU6050_DlpfBandWidth;

/** enumeration for Gyroscope range selection */
typedef enum{
    enMPU6050_gyroMeasurementRange_250=0,
    enMPU6050_gyroMeasurementRange_500,
    enMPU6050_gyroMeasurementRange_1000,
    enMPU6050_gyroMeasurementRange_2000
}PFEnMPU6050_gyroMeasurementRange;

/** enumeration for Acceleration range selection */
typedef enum{
    enMPU6050_accelMeasuementRange_2g=0,
    enMPU6050_accelMeasuementRange_4g,
    enMPU6050_accelMeasuementRange_8g,
    enMPU6050_accelMeasuementRange_16g
}PFEnMPU6050_accelMeasuementRange;

/** enumeration for allow to use Fifo enable */
typedef enum{
    enMPU6050_fifoDisable=0,
    enMPU6050_fifoEnableForTemp = 0x80,
    enMPU6050_fifoEnableForGyroX = 0x40,
    enMPU6050_fifoEnableForGyroY = 0x20,
    enMPU6050_fifoEnableForGyroZ = 0x10,
    enMPU6050_fifoEnableForAccel = 0x08,
    enMPU6050_fifoEnableForSlave2 = 0x04,
    enMPU6050_fifoEnableForSlave1 = 0x02,
    enMPU6050_fifoEnableForSlave0 = 0x01
}PFEnMPU6050_fifoEnable;

/** enumeration for clock source for Master I2C mode */
typedef enum{
    enMPU6050_masterI2Cclock_348KHz=0,
    enMPU6050_masterI2Cclock_333KHz,
    enMPU6050_masterI2Cclock_320KHz,
    enMPU6050_masterI2Cclock_308KHz,
    enMPU6050_masterI2Cclock_296KHz,
    enMPU6050_masterI2Cclock_286KHz,
    enMPU6050_masterI2Cclock_276KHz,
    enMPU6050_masterI2Cclock_267KHz,
    enMPU6050_masterI2Cclock_258KHz,
    enMPU6050_masterI2Cclock_500KHz,
    enMPU6050_masterI2Cclock_471KHz,
    enMPU6050_masterI2Cclock_444KHz,
    enMPU6050_masterI2Cclock_421KHz,
    enMPU6050_masterI2Cclock_400KHz,
    enMPU6050_masterI2Cclock_381KHz,
    enMPU6050_masterI2Cclock_364KHz
}PFEnMPU6050_masterI2Cclock;

/** enumeration for interrupt selection */
typedef enum{
    enMPU6050_intDisable=0,
    enMPU6050_intEnableOnDataReady = 0x01,
    enMPU6050_intEnableOnI2cMasterSrc = 0x08,
    enMPU6050_intEnableOnFifoOverFlow = 0x10,
    enMPU6050_intEnableOnMotionDetection = 0x40
}PFEnMPU6050_intEnable;

/** enumeration for Accelerometer power on time selection */
typedef enum{
    enMPU6050_accelOnDelay_1ms=0,
    enMPU6050_accelOnDelay_2ms,
    enMPU6050_accelOnDelay_3ms,
    enMPU6050_accelOnDelay_4ms
}PFEnMPU6050_AccelOnDelay;

/** enumeration for MPU6050 device clock selection */
typedef enum{
    enMPU6050_devClockInternal_8MHz=0,
    enMPU6050_devClockPllXgyro,
    enMPU6050_devClockPllYgyro,
    enMPU6050_devClockPllZgyro,
    enMPU6050_devClockExt_32_768KHz,
    enMPU6050_devClockExt_19_2MHz
}PFEnMPU6050_devClock;

/** MPU6050 configuration structure */
typedef struct{
    PFbyte                                sampleRateDivider;   //comments
    PFEnMPU6050_extSync                   extSync;
    PFEnMPU6050_DlpfBandWidth             dlpfBandwidth;
    PFEnMPU6050_gyroMeasurementRange      gyroRange;
    PFEnMPU6050_accelMeasuementRange      accelRange;
    PFbyte                                freeFallDetectionThreshold;
    PFbyte                                freeFallDuration;
    PFEnMPU6050_fifoEnable                fifoEnablefor;
    PFEnMPU6050_intEnable                 intEnable;
    PFEnMPU6050_AccelOnDelay              accelOnDelay;
    PFEnMPU6050_devClock                  devClock;
		PFbyte                                slaveaddr;
		PFEnStatus (*pfI2cWrite)(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFword size);
		PFEnStatus (*pfI2cRead)(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFword size, PFword* readBytes);

}PFCfgMPU6050;

/** pointer to PFCfgMPU6050 structure */
typedef PFCfgMPU6050* PPFCfgMPU6050;

/**
 * To initialize MPU6050's Accelerometer, Gyroscope and Temperature sensor
 *
 *\param devId, pointer to specifyd allocated to driver
 * 
 *\param sensorConfig, structure to config LSM303DLH Accelerometer with Magnetometer
 *
 *\param deviceCount, number of MPU6050 to be initialized
 *
 * \return status of initialization
 *
 * \note Function needs TWI/I2C to be initialized before calling this function
 */
PFEnStatus pfMPU6050_open(PFbyte* devId,PPFCfgMPU6050 sensorConfig,PFbyte deviceCount);

/**
 * to close MPU6050 with id devId
 *
 * \param devId, pointer to specifyd allocated to driver
 *
 * \return status, of MPU6050 close operation
 */
PFEnStatus pfMPU6050_close(PFbyte* devId);

/**
 * To read raw values from MPU6050's Accelerometer in Three Axis
 * 
 * \param devId, pointer to specifyd assigned to driver
 *
 * \param Xcomp, pointer to copy acceleration in X-axis 
 *
 * \param Ycomp, pointer to copy acceleration in Y-axis
 *
 * \param Zcomp, pointer to copy acceleration in Z-axis
 *
 * \return status of Read Accelerometer
 */
PFEnStatus pfMPU6050_readAccelerometer(PFbyte* devId,PFsword* Xcomp, PFsword* Ycomp, PFsword* Zcomp);

/**
 * To read Acceleration from MPU6050's Accelerometer in Three Axis scaled in mg (milliG)
 * 
 * \param devId, pointer to specifyd assigned to driver
 *
 * \param Xcomp, pointer to copy acceleration in X-axis 
 *
 * \param Ycomp, pointer to copy acceleration in Y-axis
 *
 * \param Zcomp, pointer to copy acceleration in Z-axis
 *
 * \return status of Read Accelerometer
 */
PFEnStatus pfMPU6050_getAcceleration(PFbyte* devId,PFsword* Xcomp, PFsword* Ycomp, PFsword* Zcomp);

/**
 * To read raw values from MPU6050's Gyroscope in Three Axis 
 * 
 * \param devId, pointer to specifyd assigned to driver
 *
 * \param Xcomp, pointer to copy Rotation in X-axis 
 *
 * \param Ycomp, pointer to copy Rotation in Y-axis
 *
 * \param Zcomp, pointer to copy Rotation in Z-axis
 *
 * \return status of Read Gyroscope
 */
PFEnStatus pfMPU6050_readGyroscope(PFbyte* devId,PFsword* Xcomp, PFsword* Ycomp, PFsword* Zcomp);

/**
 * To read Angular Rate from MPU6050's Gyroscope in Three Axis scaled in �/sec
 * 
 * \param devId, pointer to specifyd assigned to driver
 *
 * \param Xcomp, pointer to copy Rotation in X-axis 
 *
 * \param Ycomp, pointer to copy Rotation in Y-axis
 *
 * \param Zcomp, pointer to copy Rotation in Z-axis
 *
 * \return status of Read Rotation
 */
PFEnStatus pfMPU6050_getRotation(PFbyte* devId,PFsword* Xcomp, PFsword* Ycomp, PFsword* Zcomp);

/**
 * To read Temperature from MPU6050's Digital Temperature sensor in �C
 * 
 * \param devId, pointer to specifyd assigned to driver
 *
 * \param temp, pointer to copy digital Temperature value
 *
 * \return status of Read Temperature
 */
PFEnStatus pfMPU6050_readTemperature(PFbyte* devId,PFsword* temp);

/**
 * To read device Id from Who_Am_I register
 * 
 * \param devId, pointer to specifyd assigned to driver
 *
 * \param ida, to copy device Id
 *
 * \return status of ID read operation
 */
PFEnStatus pfMPU6050_getDeviceID(PFbyte* devId,PFbyte* ida);

/** @}  */
