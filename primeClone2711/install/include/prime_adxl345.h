/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework ADXL345 Accelerometer Driver.
 * 
 *
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_ADXL345_API PF ADXL345 Accelerometer API
 * @{
 */
 
/** Maximum devices supported		*/ 
#define ADXL345_MAX_DEVICE_SUPPORTED 2

/** Addresses of Registers of ADXL345 Accelerometer */
#define ADXL345_REG_DEVID                  0x00
#define ADXL345_REG_RESERVED1              0x01
#define ADXL345_REG_THRESH_TAP             0x1d

#define ADXL345_REG_OFSX                   0x1e
#define ADXL345_REG_OFSY                   0x1f
#define ADXL345_REG_OFSZ                   0x20

#define ADXL345_REG_DUR                    0x21
#define ADXL345_REG_LATENT                 0x22
#define ADXL345_REG_WINDOW                 0x23
#define ADXL345_REG_THRESH_ACT             0x24
#define ADXL345_REG_THRESH_INACT           0x25
#define ADXL345_REG_TIME_INACT             0x26
#define ADXL345_REG_ACT_INACT_CTL          0x27

#define ADXL345_REG_THRESH_FF              0x28
#define ADXL345_REG_TIME_FF                0x29

#define ADXL345_REG_TAP_AXES               0x2a
#define ADXL345_REG_ACT_TAP_STATUS         0x2b
#define ADXL345_REG_BW_RATE                0x2c
#define ADXL345_REG_POWER_CTL              0x2D

#define ADXL345_REG_INT_ENABLE             0x2e
#define ADXL345_REG_INT_MAP                0x2f
#define ADXL345_REG_INT_SOURCE             0x30

#define ADXL345_REG_DATA_FORMAT            0x31

#define ADXL345_REG_DATAX_0                0x32
#define ADXL345_REG_DATAX_1                0x33
#define ADXL345_REG_DATAY_0                0x34
#define ADXL345_REG_DATAY_1                0x35
#define ADXL345_REG_DATAZ_0                0x36
#define ADXL345_REG_DATAZ_1                0x37

#define ADXL345_REG_FIFO_CTL               0x38
#define ADXL345_REG_FIFO_STATUS            0x39
 
#define ADXL345_ACC_USETAPDETECTION                       1       /** To use Tap detection functionality */
#define ADXL345_ACC_USEFREEFALLDETECTION                  1       /** to use FreeFall detection functionality */
#define ADXL345_ACC_USEACTIVITYDETECTION                  1       /** to use Activity detection functionality */
#define ADXL345_ACC_USEINACTIVITYDETECTION                1       /** to use Inactivity detection Functionality */

#define ADXL345_ACC_INTERRUPTACTIVELOW                    1       /** Interrupt configuration Active Low / Active High */
#define ADXL345_ACC_USEFULLRESOLUTION                     1       /** resolution of Data 13/10 bit */
#define ADXL345_ACC_USELOWPOWEROPERATION                  0       /** Low/Normal power Operation */
#define ADXL345_ACC_LINKACTIVITYWITHINACTIVITY            0       /** link Activity detection with Inactivity Detection */

#define ADXL345_ACC_OFFSETFORXAXIS                        0x02    /** offset for X-axis component 15.6mg/LSb */
#define ADXL345_ACC_OFFSETFORYAXIS                        0x02    /** offset for Y-axis component 15.6mg/LSB */
#define ADXL345_ACC_OFFSETFORZAXIS                        0x02    /** offset for Z-axis component 15.6mg/LSB */

#define ADXL345_ACC_TAPTHRESHOLD                          0x30    /** tap threshold 62.5mg/LSB */
#define ADXL345_ACC_TAPDURATION                           0x10    /** duration for valid tap 625us/LSB */
#define ADXL345_ACC_DOUBLETAPLATENCY                      0x10    /** latency to start scanning for second tap 1.25ms/LSB*/
#define ADXL345_ACC_DOUBLETAPWINDOW                       0x40    /** window size to scan for second tap 1.25ms/LSB */

#define ADXL345_ACC_ACTIVITYTHRESHOLD                     0x40    /** activity threshold 62.5mg/LSB */
#define ADXL345_ACC_INACTIVITYTHRESHOLD                   0x20    /** inactivity threshold 62.5mg/LSB */
#define ADXL345_ACC_INACTIVITYDURATION                    0x03    /** duration to validate inactivity 1s/LSB */
#define ADXL345_ACC_FREEFALLTHRESHOLD                     0x05    /** FreeFall threshold 62.5mg/LSB */
#define ADXL345_ACC_FREEFALLDURATION                      0x14    /** duration to validate freefall 5ms/LSB */

/** enumeration for participant axes in measurement */
typedef enum{
    enAdxl345ComponentNone=0,		/**< Participant axis: None		*/
    enAdxl345Component_Z,			/**< Participant axis: Z		*/
    enAdxl345Component_Y,			/**< Participant axis: Y		*/
    enAdxl345Component_ZY,			/**< Participant axis: Z, Y		*/
    enAdxl345Component_X,			/**< Participant axis: X		*/
    enAdxl345Component_ZX,			/**< Participant axis: Z, X		*/
    enAdxl345Component_XY,			/**< Participant axis: X, Y		*/
    enAdxl345Component_XYZ			/**< Participant axis: X, Y, Z	*/
}PFenAdxl345Component;

/** enumeration for Output Data Rate */
typedef enum{
    enAdxl345DataRate_3200=0,		/**< Output data rate = 3200 Hz	*/	
    enAdxl345DataRate_1600,			/**< Output data rate = 1600 Hz	*/	
    enAdxl345DataRate_800,			/**< Output data rate = 800 Hz	*/	
    enAdxl345DataRate_400,			/**< Output data rate = 400 Hz	*/	
    enAdxl345DataRate_200,			/**< Output data rate = 200 Hz	*/	
    enAdxl345DataRate_100,			/**< Output data rate = 100 Hz	*/	
    enAdxl345DataRate_50,			/**< Output data rate = 50 Hz	*/	
    enAdxl345DataRate_25,			/**< Output data rate = 25 Hz	*/	
    enAdxl345DataRate_12_5,			/**< Output data rate = 12.5 Hz	*/	
    enAdxl345DataRate_6_25,			/**< Output data rate = 6.25 Hz	*/	
    enAdxl345DataRate_3_13,			/**< Output data rate = 3.13 Hz	*/	
    enAdxl345DataRate_1_56,			/**< Output data rate = 1.56 Hz	*/	
    enAdxl345DataRate_0_78,			/**< Output data rate = 0.78 Hz	*/	
    enAdxl345DataRate_0_39,			/**< Output data rate = 0.39 Hz	*/	
    enAdxl345DataRate_0_20,			/**< Output data rate = 0.2 Hz	*/	
    enAdxl345DataRate_0_10			/**< Output data rate = 0.1 Hz	*/	
}PFEnAdxl345DataRate;

/** enumeration for interrupt source */
typedef enum{
    enAdxl345Int_None=0,			/**< No interrupt		*/
    enAdxl345Int_DataReady = 0x01,	/**< Enable data ready interrupt	*/
    enAdxl345Int_SingleTap = 0x02,	/**< Enable single tap interrupt	*/
    enAdxl345Int_DoubleTap = 0x04,	/**< Enable double tap interrupt	*/
    enAdxl345Int_Activity  = 0x08,	/**< Enable activity interrupt	*/
    enAdxl345Int_Inactivity= 0x10,	/**< Enable inactivity interrupt	*/
    enAdxl345Int_FreeFall  = 0x20,	/**< Enable free fall interrupt	*/
    enAdxl345Int_Watermark = 0x40,	/**< Enable watermark interrupt	*/
    enAdxl345Int_Overrun   = 0x80	/**< Enable overrun interrupt	*/
}PFEnAdxl345Int;

/** enumeration for measurement range selection */
typedef enum{
    enAdxl345Range_2g=0,		/**< Full scale range = +/- 2g	*/
    enAdxl345Range_4g,			/**< Full scale range = +/- 4g	*/
    enAdxl345Range_8g,			/**< Full scale range = +/- 8g	*/
    enAdxl345Range_16g			/**< Full scale range = +/- 16g	*/
}PFEnAdxl345Range;

/** enumeration for ADXL345 hardware fifo mode selection */
typedef enum{
    enAdxl345FifoMode_Bypass=0,		/**< Bypass FIFO 	*/
    enAdxl345FifoMode_Fifo,			/**< Enable FIFO mode	*/
    enAdxl345FifoMode_Stream,		/**< FIFO streaming mode	*/
    enAdxl345FifoMode_Trigger		/**< FIFO trigger mode		*/
}PFEnAdxl345FifoMode;

/** ADXL345 Accelerometer configuration structure */
typedef struct
{
	PFbyte						adxl345_slaveAddr;			/**< Slave Address of ADXL345 */
    PFEnBoolean                	act_ac_dc;                  /**< activity measurement coupling */
    PFEnBoolean                	inact_ac_dc;                /**< inactivity measurement coupling */
    PFenAdxl345Component       	actAxisComponent;           /**< participating axis for Activity measurement */
    PFenAdxl345Component       	inactAxisComponent;         /**< participation axis for Inactivity measurement */
    PFenAdxl345Component       	tapAxisComponent;           /**< participation axis for Tap detection */
    PFEnBoolean                	suppressDoubleTap;          /**< suppress double tap detection */
    PFEnAdxl345DataRate        	dataRate;                   /**< output data rate */
    PFEnAdxl345Range           	operatingRange;             /**< operating range for measurement */
    PFEnAdxl345FifoMode        	fifoMode;                   /**< ADXL345's hardware fifo mode selection */
    PFbyte 						fifoSamples;                /**< number of samples ADXL345's hardware fifo */
    PFEnBoolean                	fifoTriggerINT2;            /**< link trigger event to INT2/INT1 */
    PFEnAdxl345Int             	intEnable;                  /**< interrupt enable */
    PFEnAdxl345Int             	intMap_on_INT2;             /**< enabled interrupts mapping on INT2/INT1 */
	PFEnStatus (*i2cWrite)(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFword size);	/**< Pointer to I2C write function to which ADXL345 is connected */
	PFEnStatus (*i2cRead)(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFword size, PFword* readBytes); /**< Pointer to I2C Read function to which ADXL345 is connected */
}PFCfgAdxl345;

/** pointer to PFCfgAdxl345 structure */
typedef PFCfgAdxl345* PFpCfgAdxl345;

/**
 * To initialize ADXL345 Accelerometer module
 *
 * \param devId pointer to copy id/id's assigned by driver
 * \param sensorConfig structure to config ADXL345 Accelerometer
 * \param deviceCount number of adxl345 to be initialized
 *
 * \return status of initialization
 *
 * \note Function needs TWI/I2C to be initialized before calling this function
 */
PFEnStatus pfAdxl345Open(PFbyte* devId, PFpCfgAdxl345 sensorConfig,PFbyte deviceCount );

/** 
 * to close ADXL345 Accelerometer module with id devId
 *
 * \param devId specify id allocated to driver
 *
 * \return ADXL345 disable status
 */
PFEnStatus pfAdxl345Close(PFbyte *devId);

/**
 * To read Device ID of ADXL345 accelerometer
 *
 * \param devId specify id allocated to driver
 * \param deviceId pointer to copy Device ID
 *
 * \return status of ID read operation
 */
PFEnStatus pfAdxl345GetID(PFbyte* devId, PFbyte* deviceId);

/**
 * To get Acceleration of ADXL345 in Three Axis in mg scale
 *
 * \param devId specify id assigned to driver
 * \param Xcomp pointer to copy acceleration in X-axis 
 * \param Ycomp pointer to copy acceleration in Y-axis
 * \param Zcomp pointer to copy acceleration in Z-axis
 *
 * \return status of get Acceleration
 */
PFEnStatus pfAdxl345GetAcceleration(PFbyte* devId, PFword* Xcomp, PFword* Ycomp, PFword* Zcomp);

/**
 * To get interrupt source/sources of ADXL345 
 *
 * \param devId specify id assigned to driver
 * \param intSurce array to copy interrupt source/sources
 *
 * \return status of get interrupt source
 */
PFEnStatus pfAdxl345GetIntSource(PFbyte* devId, PFEnAdxl345Int intSource[]);

/**
 * To get number of samples retained in ADXL345's hardware fifo after occurance of Trigger event
 *
 * \param devId specify id assigned to driver
 * \param samples pointer to copy number of samples retained in ADXL345's hardware fifo
 *
 * \return status of get number of samples retained in FIFO
 */
PFEnStatus pfAdxl345GetNoOfSamplesAfterTrigger(PFbyte *devId, PFbyte* samples);

/** @}  */

