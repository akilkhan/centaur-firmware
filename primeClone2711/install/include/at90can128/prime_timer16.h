/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework 16-bit Timer driver for AT90CAN128.
 *
 * 
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_TIMER_API Timer API
 * @{
 */ 
 #define Timer16InputCaptureNoiseCanceler__  	0
 #define TIMER16_CH								TIMER16
 #define TIMER16_CHANNEL						PERIPH(TIMER16_CH)
 
/** Enumeration for timer modes			*/
typedef enum
{
	enTimer16Noclock = 0,			/**No clock source is selected and the timer is disabled	*/
	enTimer16NoprescaledClk,		/**< clock source is selected and the timer is not pre-scaled	*/
	enTimer16ClkDivideby8,			/**< Clock source freqency is divided by 8	*/
	enTimer16ClkDivideby64,		/**< Clock source freqency is divided by 64	*/
	enTimer16ClkDivideby256,		/**< Clock source freqency is divided by 256	*/
	enTimer16ClkDivideby1024,		/**< Clock source freqency is divided by 1024	*/
	enTimer16ExtClkFallingEdge,	/**<  timer with external source on falling edge 	*/
	enTimer16ExtClkRisingEdge		/**<  timer with external source on falling edge 	*/
}PFEnTimer16Clocksource;


/** Enumeration for external match control		*/
typedef enum
{
	enTimer16ExtMatchCtrlNone = 0,				/**< Do nothing on count match						*/
	enTimer16ExtMatchCtrlTogglePin,			/**< Toggle match pin								*/
	enTimer16ExtMatchCtrlClearPin,				/**< Clear match pin								*/
	enTimer16ExtMatchCtrlSetPin				/**< Set match pin									*/
}PFEnTimer16ExtMatchCtrl;

typedef enum
{
	enTimer16NormalMode=0x00,	    /**<  timer in Nornal Mode 	*/
	enTimer16CtcOcrMode=0x08,	/**<  timer in clear timer on compare  Mode,  compare with OCR	*/
	enTimer16CtcIcrMode=0x18	/**<  timer in clear timer on compare  Mode, compare with ICR	*/
}PFEnTimer16Mode;

/**	No interrupt	*/
#define TIMER16_INT_NONE			0x00
/**	interrupt on overflow	*/
#define TIMER16_INT_OVERFLOW		0x01
	/**	interrupt on matching with the value in OCR1A 	*/
#define TIMER16_INT_MATCHREG_A	0x02
/**	interrupt on matching with the value in OCR1B 	*/
#define TIMER16_INT_MATCHREG_B	0x04
/**	interrupt on matching with the value in OCR1C 		*/
#define TIMER16_INT_MATCHREG_C	0x08
/**	interrupt on matching with the value in ICR1 		*/
#define TIMER16_INT_INPUTCAPTURE	0x20
/**	interrupt on matching with the value in OCR1A ,OCR1B,ICR1 and overflow 	*/
#define TIMER16_INT_ALL			0x2F

/**		Timer configure structure		*/
typedef struct
{
	PFEnTimer16Clocksource		clockSource;		/**< Select clock source				*/
	PFEnTimer16Mode		        timer16Mode;			/** timer mode*/
	PFword				        matchValueA;		/**< Match register A compare value							*/
	PFword				        matchValueB;		/**< Match register B compare value							*/
	PFword				        matchValueC;		/**< Match register C compare value							*/
	PFEnTimer16ExtMatchCtrl 	exMatchActionA;	/**< match pin control on count match 				*/
	PFEnTimer16ExtMatchCtrl 	exMatchActionB;	/**< match pin control on count match 				*/ 
	PFEnTimer16ExtMatchCtrl 	exMatchActionC;	/**< match pin control on count match 				*/
	PFbyte				        interrupt;			/**< To enable or disable timer interrupt			*/
	PFEnBoolean                 inputCaptureRisingEdge;  /**< to set input capture signal edge selection */
	PFcallback			        cmpMatchACallback;	/**< Callback function for timer ISR				*/
	PFcallback			        cmpMatchBCallback;
	PFcallback			        cmpMatchCCallback;
	PFcallback			        inputCaptureCallback;
	PFcallback			        overFlowCallback;
}PFCfgTimer16;

/** Pinter to PFCfgTiemr structure		*/
typedef PFCfgTimer16* PPFCfgTimer16;

/**
 * Initialized timer with given parameters
 * 
 * \param config timer configuration structure
 * 
 * \return timer initialization status
 */
PFEnStatus pfTimer16Open(PPFCfgTimer16 config);

/**
 * Stops timer operation and turn offs the timer module
 * 
 * \return timer turn off operation status
 */
PFEnStatus pfTimer16Close(void);

/**
 * Starts timer operation
 * 
 * \return timer start status
 */
PFEnStatus pfTimer16Start(void);

/**
 * Stops timer operation
 * 
 * \return timer stop status
 */
PFEnStatus pfTimer16Stop(void);

/**
 * Resets the timer operation. Timer will start counting from zero again.
 * 
 * \return timer reset status
 */
PFEnStatus pfTimer16Reset(void);

/**
 * Returns the timer count
 * 
 * \return timer count
 */
PFEnStatus pfTimer16ReadCount(PFword* count);

/**
 * Returns Timestamp at which event occurred at ICP pin
 * 
 * \return Input capture count
 */
PFEnStatus pfTimer16ReadCaptureCount(PFword* CaptureCount);

/**
* Writes new value to the match register and enables latch for the match register
*
* \param regNum index of match register to be updated
* \param regValue new value for match register
*
* \return match register update status
*/
PFEnStatus pfTimer16UpdateMatchRegister(PFbyte regNum, PFword regVal); 

/** @} */
