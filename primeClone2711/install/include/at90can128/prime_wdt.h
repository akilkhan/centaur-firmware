/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework  WatchDog Timer driver for AT90CAN128.
 *
 * 
 * Review status: NO
 *
 */
 #pragma once
 /**
 * \defgroup PF_WDT_API Watchdog API
 * @{
 */
#define WDT_CH				WDT
#define WDT_CHANNEL			PERIPH(WDT_CH)
#define WDT_INT_HANDLER		INT_HANDLER(WDT_CH)

 /** Enumeration for timeout Selection			*/
 typedef enum
 {
	 enWatchdogTimeout16_3mS = 0,		/**< Timeout selection for 16.3m seconds	 */
	 enWatchdogTimeout32_5mS = 1,		/**< Timeout selection for 32.5m seconds	 */
	 enWatchdogTimeout65mS = 2,			/**< Timeout selection for 65m seconds	 */
	 enWatchdogTimeout130mS = 3,		/**< Timeout selection for 130m seconds	 */
	 enWatchdogTimeout260mS = 4,		/**< Timeout selection for 260m seconds	 */
	 enWatchdogTimeout520mS = 5,		/**< Timeout selection for 520m seconds	 */
	 enWatchdogTimeout1000mS = 6,		/**< Timeout selection for 1000m seconds	 */
	 enWatchdogTimeout2100mS= 7			/**< Timeout selection for 2100m seconds	 */
 }PFEnWatchdogTimeout;
 
 /** Watchdog configuration structure 	*/
 typedef struct  
 {
	PFEnWatchdogTimeout timeout;   /**< timeout value for watchdog timer in milli sec*/
 }PFCfgWatchdog ;
  
 /** Pointer to PFCfgWatchdog  structure		*/
typedef PFCfgWatchdog* PFpCfgWatchdog;

 /**
 * Initialized watchdog  with given parameters
 * 
 * \param config pointer to watchdog configuration structure
 * 
 * \return watchdog initialization status 	
 */
 PFEnStatus pfWatchdogOpen(PFpCfgWatchdog config);
 
 /**
 * Stop the watchdog operation and close module 	
 *
 * \return watchdog close status
 */
 PFEnStatus pfWatchdogClose(void);
 
 /**
 * To start the watchdog operation 
 *
 * \return watchdog start operation Status		
 */
 PFEnStatus pfWatchdogStart(void);
  
 /**
 * To check whether interrupt flag is set or not
 * 
 * \param to store the status of interrupt flag
 *
 * \return watchdog interrupt flag status
 */
 PFEnStatus pfWatchdogChkIntFlag(PFEnBoolean *flagStatus);
  
 /**
 * To update the timeout value of watchdog timer
 *
 * \param To update the timeout value of watchdog timer
 *
 * \return Status whether value is updated is successful or not
 */
 PFEnStatus pfWatchdogUpdateTimeout(PFbyte timeout);
  
 /**
 * To stop the watchdog operation will put in halt condition 
 *
 * \return watchdog stop operation status		
 */
 PFEnStatus pfWatchdogStop(void); 