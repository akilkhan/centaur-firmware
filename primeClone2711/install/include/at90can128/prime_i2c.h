/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework I2C driver for AT90CAN128.
 *
 * 
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_I2C_API I2C API
 * @{
 */
#define I2C_CH				TWI
#define I2C_CHANNEL			PERIPH(I2C_CH)
/** 
 * Mark PF_I2C_USE_FIFO as 1 if internal software buffer is to be used in interrupt based communication.
 * If it is marked as 0, user should provide callbacks to handle transmit and receive interrupts.
 */
#define PF_I2C_USE_FIFO				1
	
#if(PF_I2C_USE_FIFO != 0)
/** 
 * Define size in bytes for internal software buffer.
 * The buffer size should be a non-zero and power of 2 number.
 */
	#define I2C_BUFFER_SIZE		128
#endif	// #if(PF_I2C_USE_FIFO != 0)


typedef enum
{
	enI2cPrescale_1,			/**< Selecting Pre-scale value = 1*/
	enI2cPrescale_4,			/**< Selecting Pre-scale value = 4*/
	enI2cPrescale_16,			/**< Selecting Pre-scale value = 16*/
	enI2cPrescale_64			/**< Selecting Pre-scale value = 64*/
}PFEnI2cPrescale;


/**	Configuration structure for I2C		*/
typedef struct
{
	PFEnI2cPrescale		preScale;		/**< prescale for I2C bus clock. This parameter is valid for master mode*/
	PFword				baudrate;		/**< Baudrate for I2C bus clock. This parameter is valid for master mode*/
	PFEnBoolean			enableAck;		/**< Enable or disable sending acknowledgement		*/
	PFEnBoolean			enableGenCall;	/**< Enable or disable acknowledgement to general call. This parameter is valid for slave mode*/
	PFbyte				ownAddress;		/**< Set own I2C addresses. The addreses should be left aligned. This parameter is valid for slave mode*/
	PFEnBoolean			i2cIntEnable;	/**< Enable or disable interrupt					*/
#if(PF_I2C_USE_FIFO == 0)
	PFcallback			callback;		/**< callback to handle I2C interrupt				*/
#endif	// #if(PF_I2C_USE_FIFO != 0)	
}PFCfgI2c;

/** Pointer to PFCfgI2c structure		*/
typedef PFCfgI2c* PFpCfgI2c;


/**
 * Initializes the I2C channel with provided settings
 *
 * \param config configuration structure which contains the settings for the communication channel to be used.
 * \return I2C initialization status.
 */
PFEnStatus pfI2cOpen(PFpCfgI2c config);

/**
 * Turn offs the I2C channel
 */
PFEnStatus pfI2cClose(void);

/**
 * Function enables I2C interupt
 * 
 * \return Interrupt enable status
 */
PFEnStatus pfI2cIntEnable(void);

/**
 * Function disables I2C interupt
 * 
 * \return Interrupt disable status
 */
PFEnStatus pfI2cIntDisable(void);

/**
 * The function sends multiple bytes on I2C channel.  
 * If transmit interrupt is enabled, the function will enqueue the data in transmit FIFO.
 * Otherwise it will wait in the function and send each byte by polling the line status.
 *
 * \param data pointer to the data to be sent.
 * \param slaveAddr left aligned address of slave device to write data.
 * \param size total number of bytes to send.
 *
 * \return I2C write status.
 */
PFEnStatus pfI2cWrite(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFword size);


/**
 * The function reads one byte from I2C channel.
 * If receive interrupt is enabled, the function will read the byte from receive FIFO.
 * Otherwise it will wait in the function for one byte to receive by polling the line status.
 *
 * \param data Unsigned char pointer to the buffer where the read data should be loaded.
 * \param slaveAddr left aligned address of slave device to read data from.
 * \param size Total number of bytes to read.
 * \param readBytes pointer to double word, in which fucntion will fill number bytes actually read. 
 *
 * \return I2c read status.
 */
PFEnStatus pfI2cRead(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFword size, PFword* readBytes);

/**
 * Function gives current I2C communication state
 *
 * \param state pointer to PFbyte to load current I2C0 state
 *
 * \return status for reading current state
 */
PFEnStatus pfI2cGetState(PFdword* state);

#if(PF_I2C_USE_FIFO != 0)
/**
 * Returns the number of bytes received in I2C buffer.
 *
 * \return number of bytes received in I2C buffer.
 */
PFEnStatus pfI2cGetRxBufferCount(PFdword* count);

/**
 * This function empties the transmit buffer.
 */
PFEnStatus pfI2cTxBufferFlush(void);

/**
 * This function empties the receive buffer.
 */
PFEnStatus pfI2cRxBufferFlush(void);


/**
 * This function transmits START condition over I2C channel.
 */
PFEnStatus pfI2cStart(void);


/**
 * This function transmits STOP condition over I2C channel.
 */
PFEnStatus pfI2cStop( void);


#endif	// #if(PF_I2C_USE_FIFO != 0)



/** @} */ 




