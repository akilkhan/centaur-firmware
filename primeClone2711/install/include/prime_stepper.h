/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework L6470/A3977 Stepper Motor Driver.
 * 
 *
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_XLYNX_CB_STEPPER_DRIVER_API XLYNX-CB STEPPER DRIVER API
 * @{
 */

/** maximum number of stepper supported */
#define MAX_STEPPER_SUPPORTED 		7

/**
 * To initialize stepper driver A3977
 *
 * \param devId pointer to copy id/id's assigned by driver to motor/motor's
 *
 * \param config pointer to structure to configure stepper driver A3977
 *
 * \param deviceCount number of stepper driver A3977 to be initialised 
 *
 * \return status of initialization
 *
 * \note timer should be initialized with isr stepperControlFunction before calling the function
 */
PFEnStatus pfLynxOpen(PFbyte* id, PFpCfgStepperA3977 config, PFbyte deviceCount);

/**
 * To initialize stepper drivers L6470
 *
 * \param deviceId pointer to copy Id's assigned to steppers
 *
 * \param sensorConfig pointer to array of structures to configure steppers
 *
 * \param deviceCount number of devices to be initialised
 *
 * \return status of initialization
 *
 * \note Function needs SPI to be initialized before calling this function
 *       this function should be called only once for all devices
 */
PFEnStatus pfSmartlynxOpen(PFbyte* deviceId, PFpCfgStepperL6470 config, PFbyte deviceCount);

/**
 * To initialize stepper driver A3977 connected to Xlynx-CB Xpansion board
 *
 * \param devId pointer to copy id/id's assigned by driver to motor/motor's
 *
 * \param config pointer to structure to configure stepper driver A3977
 *
 * \param deviceCount number of stepper driver A3977 to be initialised 
 *
 * \return status of initialization
 *
 * \note timer should be initialized with isr stepperControlFunction before calling the function
 */
PFEnStatus pfStepperLynxOpen(PFbyte* deviceId, PFpCfgXlynxCbStepperLynx config, PFbyte deviceCount);

/**
 * To initialize stepper drivers L6470 connected to Xlynx-CB Xpansion board
 *
 * \param deviceId pointer to copy Id's assigned to steppers
 *
 * \param sensorConfig pointer to array of structures to configure steppers
 *
 * \param deviceCount number of devices to be initialised
 *
 * \return status of initialization
 *
 * \note Function needs SPI to be initialized before calling this function
 *       this function should be called only once for all devices
 */
PFEnStatus pfStepperSmartlynxOpen(PFbyte* deviceId, PFpCfgXlynxCbStepperSmartlynx config, PFbyte deviceCount);

/**
 * for SPI communication between Baseboard and Xpansion board, register xapansion board as spi device
 *
 * \param chipSelect Pointer to PFGpioPortPin structure which describes the chip select pin used for the device.
 */
PFEnStatus pfXlynxCbBoardOpen(PFpGpioPortPin chipSelect);

/**
 * to close stepper motor with specified deviceId
 *
 * \param devId pointer to Id, specifies the stepper to be closed
 *
 * \return status of motor Close fuction 
 */
PFEnStatus pfStepperClose(PFbyte* devId);

/**
 * to reset lynx(A3977)/smartLynx(L6470) driver, specified by devId
 *
 * \param devId pointer to Id, specifies the stepper driver to be reset
 *
 * \return status of stepper reset operation
 */
PFEnStatus pfStepperResetDevice(PFbyte *devId);

/**
 * to reset position of stepper, specified by devId
 *
 * \param devId pointer to Id, specifies motor stepper
 *
 * \return status of stepper position reset operation
 */
PFEnStatus pfStepperResetPosition(PFbyte *devId);

/**
 * to return stepper to its home(starting) position
 *
 * \param devId pointer to Id, specifies stepper motor
 *
 * \return status of move motor to its home postion
 */
PFEnStatus pfStepperGoHomePosition(PFbyte *devId);

/**
 * to perform soft stop(decelerate to zero speed for lynx/to minimum speed for smartlynx) to stepper specified by id
 *
 * \param devId pointer to id, specifies stepper motor
 *
 * \return status of soft Stop operation
 */
PFEnStatus pfStepperSoftStop(PFbyte *devId);

/**
 * to perform hard stop(immediate stop) to stepper specified by id
 *
 * \param devId pointer to id, specifies stepper motor
 *
 * \return status of hard Stop operation
 */
PFEnStatus pfStepperHardStop(PFbyte *devId);
 
/**
 * to move stepper(lynx/smartlynx) by specified number of steps as per stepsize selection in specified direction
 *
 * \param devId pointer to id, specifies stepper motor
 *
 * \param dir direction of rotation  0: for clockwise 1: for anticlockwise
 *
 * \param steps number of steps stepper shuold move, as per stepsize selection
 *
 * \param speed pointer to speed value specify speed of rotation of stepper in full steps/sec for 
 *        motor with Smartlynx driver speed ranging 15.25 to 15610 full steps/s.
 *
 * \return status of move operation
 * \note command keeps the BUSY flag low until the target number of steps is performed
 *       This command can only be performed when the motor is stopped.
 *       (for motor with Smartlynx driver speed set is multiple of 15.25 full step/s).
 */
PFEnStatus pfStepperMove(PFbyte *devId, PFbyte dir, PFdword steps, PFfloat* speed);

/**
 * to set full step speed limit above this speed motor with Smartlynx driver leaves selected step mode and enter into full step mode 
 *
 * \param devId pointer to id, specifies motor with smartlynx driver
 *
 * \param speed is full step speed limit in full steps/s
 *
 * \return status of set full step speed limit
 * \note speed setting range is from 7.63 to 15625 full steps/s which is multiple of 15.25 full steps/s.
 */
PFEnStatus pfStepperSetFullStepSpeed(PFbyte* devId, PFfloat* speed);

/**
 * The Run command produces a motion at speed speed.
 *
 * \param devId pointer to id, specifies stepper motor
 *
 * \param dir to select the direction of motion; dir bit: '1' anticlockwise or '0' clockwise.
 *
 * \param speed pointer to speed(in full steps/sec) at which stepper shuold run (for smartlynx speed 15.25 to 15610 full steps/s).
 *
 * \return status of run operation
 * \note Run keeps the BUSY flag low until the target speed is reached.
 *       This command can be given anytime and is immediately executed.
 *       (for motor with Smartlynx driver speed set is multiple of 15.25 full step/s).
 */
PFEnStatus pfStepperRun(PFbyte* devId, PFbyte dir, PFfloat* speed);

/**
 * to test wheather stepper driver is ready for next command
 *
 * \param devId pointer to id, specifies stepper motor
 *
 * \param status pointer to copy busy/ready status of driver. True if device is ready(not accelerating or deceleration).
 *
 * \return status of driver
 */
PFEnStatus pfStepperDeviceReady(PFbyte* devId, PFEnBoolean* status);

/**
 * to set new acceleration profile for stepper
 *
 * \param devId pointer to id, specifies stepper motor
 *
 * \param acceleration pointer to new acceleration value (in full steps/sec^2) to be set (for smartlynx ranging 14.55 to 59590 full steps/sec^2)
 *
 * \param deceleration pointer to new deceleration value (in full steps/sec^2) to be set (for smartlynx ranging 14.55 to 59590 full steps/sec^2)
 *
 * \return status of set Acceleration value
 * \note This command is performed only when motor is stopped
 *       (for motor with smartlynx driver Acceleration/deceleration values set are in multiple of 14.55 full steps/sec^2).
 */
PFEnStatus pfStepperSetAccelerationProfile(PFbyte* devId, PFfloat* acceleration, PFfloat* deceleration);

/**
 * to read current acceleration profile of stepper
 *
 * \param devId pointer to id, specifies stepper motor
 *
 * \param acceleration pointer to copy current acceleration value (in full steps/sec^2)
 *
 * \param deceleration pointer to copy current deceleration value (in full steps/sec^2)
 *
 * \return status of read Current Acceleration profile value
 */
PFEnStatus pfStepperGetAccelerationProfile(PFbyte* devId, PFfloat* acceleration, PFfloat* deceleration);

/**
 * to read current position of stepper motor in steps (corresponds to selected step size)
 *
 * \param devId pointer to id, specifies stepper motor
 *
 * \param position pointer to copy current position of stepper, +ve for anticlockwise while -ve for clockwise direction
 *        gives position in steps(count corresponds to selected step size)
 *
 * \return status of read Current Position
 */
PFEnStatus pfStepperGetPosition(PFbyte* devId, PFsdword* position);

/**
 * to put motor with lynx (A3977) driver in sleep mode
 *
 * \param devId pointer to id, specifies motor with lynx driver
 *
 * \param sleepState true: to put in sleep state.
 *                   false: to remove from sleep state
 *
 * \return status of sleep operation
 */
PFEnStatus pfStepperSleep(PFbyte* devId, PFEnBoolean sleepState);

/**
 * Puts the motor with smartlynx driver into Step-clock mode and imposes direction specified by dir
 *
 * \param devId pointer to id, specifies motor with smartlynx driver
 *
 * \param dir direction of rotation
 *
 * \return status of step clock mode setting
 * \note This command can only be given when the motor is stopped.
 */
PFEnStatus pfStepperSetStepClockMode(PFbyte* devId, PFbyte dir);

/**
 * to take single step (single step is in selected step-mode)
 *
 * \param devId pointer to id, specifies motor with smartlynx driver
 *
 * \return status of take single step
 * \note before calling this function pfL6470setStepClockMode function should be called
 */
PFEnStatus pfStepperTakeSingleStep(PFbyte* devId);

/**
 * to get current speed of stepper 
 *
 * \param devId pointer to id, specifies motor with lynx/smartlynx driver
 *
 * \param speed pointer to copy current speed of stepper in step/s
 *
 * \return status of read Current Speed
 */
PFEnStatus pfStepperGetSpeed(PFbyte* devId, PFfloat* speed);

/**
 * The GoMark command produces a motion to the MARK(position stored in MARK register) position performing the minimum path.
 *
 * \param devId pointer to id, specifies motor with smartlynx driver
 *
 * \return status of go to mark position operation
 * \note This command keeps the BUSY flag low until the MARK position is reached.
 *       This command can be given only when the previous motion command has been completed
 */
PFEnStatus pfStepperGoMark(PFbyte* devId);

/**
 * produces a motion at speed specified by speed imposing specified direction till external switch turn-on event occurs
 *
 * \param devId pointer to id, specifies motor with smartlynx driver
 *
 * \param act action on external switch turn-on event occurs 	enL6470resetABS_POS : current position is resetted,
 *                                                              enL6470copyABS_POStoMark : current position is copied into the MARK register
 *
 * \param dir direction of rotation  0: for clockwise 1: for anticlockwise
 *
 * \param speed pointer to speed(in steps/sec) at which stepper shuold run untill switch get pressed
 *
 * \return status of go until operation
 * \note This command keeps the BUSY flag low until the switch turn-on event occurs and the motor is stopped
 *			 This command can be given anytime and is immediately executed.
 */
PFEnStatus pfStepperGoUntil(PFbyte* devId, PFEnL6470actionForSwitchPress act, PFbyte dir, PFfloat* speed);

/**
 * The ReleaseSW command produces a motion at minimum speed until the switch input is released
 *
 * \param devId pointer to id, specifies motor with smartlynx driver
 *
 * \param act action on external switch turn-on event occurs 	
 *        enL6470resetABS_POS : current position is resetted,	
 *		  enL6470copyABS_POStoMark : current position is copied into the MARK register
 *
 * \param dir direction of rotation  0: for clockwise 1: for anticlockwise
 *
 * \return status of release Switch operation
 * \note This command keeps the BUSY flag low until the switch input is released and the motor is stopped
 *	     This command can be given anytime and is immediately executed.
 */
PFEnStatus pfStepperReleseSW(PFbyte* devId, PFEnL6470actionForSwitchPress act, PFbyte dir);

/**
 * to set minimum speed of rotation(motion start from this speed otherwise start from zero speed)
 *
 * \param devId pointer to id, specifies motor with smartlynx driver
 *
 * \param speed is minimum speed of rotation in step/s
 *
 * \return status of set minimum speed of rotation
 * \note This command is performed only when motor is stopped
 */
PFEnStatus pfStepperSetMinimumSpeed(PFbyte* devId, PFfloat* speed);

/** @} */
