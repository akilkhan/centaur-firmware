/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework Bluetooth Driver
 *
 * Review status: NO
 *
 */
 
#pragma once
/**
 * \defgroup PF_BLUETOOTH_API PF BLUETOOTH API
 * @{
 */

 /**< Maximum bluetooth devices supported		*/
#define PF_MAX_BLUETOOTH_SUPPORTED				3

/** Bluetooth Configurartion structure **/
typedef struct
{

	PFbyte  name[20];    							 		/**< Bluetooth module name to assign	*/
	PFEnBoolean makeDiscoverable;  							/**< Set bluetooth discoverable mode	*/
	PFEnStatus (*comWrite)(PFbyte* data, PFdword size);		/**< Pointer to UART write function		*/
	PFEnStatus (*comWriteByte)(PFbyte data);				/**< Pointer to UART write byte function		*/
	PFEnStatus (*comRead)(PFbyte* data, PFdword size, PFdword* readBytes);	/**< Pointer to UART read function		*/
	PFEnStatus (*comGetRxBufferCount)(PFdword*);			/**< Pointer to UART read buffer count function		*/			
}PFCfgBluetooth;

/** Pointer to PFCfgBluetooth structure			*/
typedef PFCfgBluetooth* PFpCfgBluetooth;

/** Bluetooth name structure for name and adress of enquired device		*/
 typedef struct 
{
	 PFbyte name[20];		/**< Bluetooth device name		*/
	 PFbyte address[12];	/**< Bluetooth device address	*/
}PFBluetoothInquiryName;

/**
 * Initializes Bluetooth Module  with provided settings
 *
 * \param id of subcribed bluetooth module
 * \param config configuration structure which contains the settings for the bluetooth module to be used.
 * \param Number of device to be configured
 *
 * \return Bluetooth Open status.
 */
PFEnStatus pfBluetoothOpen(PFbyte *idArray,PFpCfgBluetooth config, PFbyte devicecnt);

/**
 * This function close the bluetooth module 
 *
 * \param id of subcribed bluetooth module
 *
 * \return Bluetooth Name Status 
 */
PFEnStatus pfBluetoothClose(PFbyte id);
/**
 * This function give the name of the bluetooth module 
 *
 * \param buffer for storing the data returned from bluetooth module .
 * \param id of subcribed bluetooth module
 *
 * \return Bluetooth Name Status 
 */
PFEnStatus pfBluetoothGetName(PFbyte id, PFbyte *data);

/**
 * This function set the name of the bluetooth module 
 *
 * \param  id of the subcribed bluetooth module.
 * \param  passing the name of bluetooth module to be set.
 *
 * \return Bluetooth set name status.
 */
PFEnStatus pfBluetoothSetName(PFbyte id,PFbyte *data,PFbyte len);

/**
 * This function give the address of the bluetooth module 
 *
 * \param id of the subcribed bluetooth module.
 * \param buffer for storing the address returned from bluetooth module.
 * \param length of the Bluetooth name to be set
 *  
 * \return Bluetooth Address Status.
 */
PFEnStatus pfBluetoothGetAddress(PFbyte id,PFbyte *data);

/**
 * This function information(Name and address) about near by bluetooth devices 
 * This function can enquire about maximum 3 bluetooth device.
 *
 * \param id of the subcribed bluetooth module.
 * \param structure for storing address and name of the near by bluetooth devices.
 * \param number of device found.
 *
 * \return Bluetooth Inquiry Status.
 */
PFEnStatus pfBluetoothInquiry( PFbyte id,PFBluetoothInquiryName data[], PFbyte *devcnt);
/**
 * This function connect the bluetooth module to the given address of bluetooth 
 *
 * \param id of the subcribed bluetooth module.
 * \param passing the address of the bluetooth module to be connected.
 *
 * \return Bluetooth Status  .
 */
PFEnStatus pfBluetoothConnect(PFbyte id,PFbyte *address);

/**
 * This function send the data over the bluetooth module 
 *
 * \param id of the subcribed bluetooth module.
 * \param passing the data to be send over the bluetooth module.
 * \param size of the data to be send over the bluetooth module.
 * 
 *
 * \return Bluetooth Status.
 */
PFEnStatus pfBluetoothSendData(PFbyte id,PFbyte* data,PFdword size);

/**
 * This function receive the data over the bluetooth module 
 *
 * \param  buffer for storing the returned data from bluetooth module.
 * \param  id of the subcribed bluetooth module.
 *
 * \return Bluetooth  Status.
 */
PFEnStatus pfBluetoothReceiveData(PFbyte id,PFbyte *data,PFdword *ncount);

/**
 * This function gives the buffer count of the received data over the bluetooth module 
 *
 * \param  id of the subcribed bluetooth module.
 * \param Bluetooth received count.
 *
 * \return Bluetooth Status.
 */
PFEnStatus pfBluetoothRxBufferCount(PFbyte id,PFdword cnt);

/** @} */ 
