
#pragma once
#ifdef MCU_CHIP_lpc1768
/** Convention: Jumper_Pin number_Periph_Name of the pin_port/pin 
 */

/**	LED configuration macros 
 */
#define		XLYNX_LED_LD1_PORT					GPIO_PORT_1
#define 	XLYNX_LED_LD1_PIN					GPIO_PIN_15
			
#define		XLYNX_LED_LD2_PORT					GPIO_PORT_1
#define 	XLYNX_LED_LD2_PIN				    GPIO_PIN_29
			
#define		XLYNX_LED_LD3_PORT					GPIO_PORT_3
#define 	XLYNX_LED_LD3_PIN					GPIO_PIN_26
			
#define		XLYNX_LED_LD4_PORT					GPIO_PORT_1
#define 	XLYNX_LED_LD4_PIN					GPIO_PIN_16
			
#define		XLYNX_LED_LD5_PORT					GPIO_PORT_0
#define 	XLYNX_LED_LD5_PIN					GPIO_PIN_19
			
#define		XLYNX_LED_LD6_PORT					GPIO_PORT_0
#define 	XLYNX_LED_LD6_PIN					GPIO_PIN_20


/** Pushbutton configuration macros 			
 */
#define		XLYNX_PUSHBUTTON_PB1_PORT 	        GPIO_PORT_2
#define		XLYNX_PUSHBUTTON_PB1_PIN		    GPIO_PIN_11

/**	Buzzer configuration macros
 */
#define		XLYNX_BUZZER_PB1_PORT 			    GPIO_PORT_1
#define		XLYNX_BUZZER_PB1_PIN				GPIO_PIN_26


/**	UART configuration macros 
 */
/** UART 0 */
#define 	XLYNX_UART_0_TX_PORT				GPIO_PORT_0
#define 	XLYNX_UART_0_TX_PIN					GPIO_PIN_2

#define 	XLYNX_UART_0_RX_PORT				GPIO_PORT_0
#define		XLYNX_UART_0_RX_PIN					GPIO_PIN_3


/** UART 2 */
#define		XLYNX_J2_4_UART_2_TX_PORT			GPIO_PORT_0
#define 	XLYNX_J2_4_UART_2_TX_PIN			GPIO_PIN_10

#define		XLYNX_J2_6_UART_2_RX_PORT			GPIO_PORT_0
#define 	XLYNX_J2_6_UART_2_RX_PIN			GPIO_PIN_11

/** UART 3 */
#define 	XLYNX_J9_8_UART_3_TX_PORT			GPIO_PORT_0
#define 	XLYNX_J9_8_UART_3_TX_PIN			GPIO_PIN_25

#define 	XLYNX_J9_9_UART_3_RX_PORT			GPIO_PORT_0
#define 	XLYNX_J9_9_UART_3_RX_PIN			GPIO_PIN_26

/** External Interrupt configuration macros 			
 */

/** EINT 1 */
#define		XLYNX_PUSHBUTTON_EINT_1_PORT		GPIO_PORT_2
#define 	XLYNX_PUSHBUTTON_EINT_1_PIN			GPIO_PIN_11

/** EINT 2 */
#define		XLYNX_J5_7_EINT_2_PORT				GPIO_PORT_2
#define 	XLYNX_J5_7_EINT_2_PIN				GPIO_PIN_12


/**	ADC Channel configuration macros 
 */
	
/** ADC 0 */
#define		XLYNX_J9_7_ADC_0_PORT				GPIO_PORT_0
#define		XLYNX_J9_7_ADC_0_PIN 				GPIO_PIN_23

/** ADC 1 */
#define		XLYNX_BAT_IN_ADC_1_PORT				GPIO_PORT_0
#define		XLYNX_BAT_IN_ADC_1_PIN				GPIO_PIN_24

/** ADC 2 */
#define		XLYNX_J9_8_ADC_2_PORT				GPIO_PORT_0
#define		XLYNX_J9_8_ADC_2_PIN				GPIO_PIN_25

/** ADC 3 */
#define		XLYNX_J9_9_ADC_3_PORT				GPIO_PORT_0
#define		XLYNX_J9_9_ADC_3_PIN				GPIO_PIN_26

/** ADC 4 pin connected to Vbus */

/** ADC 5 */
#define		XLYNX_J9_6_ADC_5_PORT				GPIO_PORT_1
#define		XLYNX_J9_6_ADC_5_PIN				GPIO_PIN_31

/** ADC 6 and ADC 7 pins connected to USB UART's TX and RX
 */


/**	I2C configuration macros 		
 */

/** I2C 0 used for USB_SDA (P0.27) and USB_SCL (P0.28) 
 */

/** I2C 1 */
#define 	XLYNX_J2_3_I2C_1_SDA_PORT			GPIO_PORT_0
#define 	XLYNX_J2_3_I2C_1_SDA_PIN			GPIO_PIN_19

#define 	XLYNX_J2_5_I2C_1_SCL_PORT			GPIO_PORT_0
#define 	XLYNX_J2_5_I2C_1_SCL_PIN			GPIO_PIN_20

/** I2C 2 */
#define 	XLYNX_J2_4_I2C_2_SDA_PORT			GPIO_PORT_0
#define 	XLYNX_J2_4_I2C_2_SDA_PIN			GPIO_PIN_10

#define 	XLYNX_J2_6_I2C_2_SCL_PORT			GPIO_PORT_0
#define 	XLYNX_J2_6_I2C_2_SCL_PIN		  	GPIO_PIN_11

/** 								SSP configuration macros 					
 */

/** SSP 0 */
#define XLYNX_EXP_J1_8_SPI0_MISO_PORT			GPIO_PORT_1
#define XLYNX_EXP_J1_8_SPI0_MISO_PIN			GPIO_PIN_23

#define XLYNX_EXP_J1_9_SPI0_MOSI_PORT			GPIO_PORT_1
#define XLYNX_EXP_J1_9_SPI0_MOSI_PIN			GPIO_PIN_24

/**
  \note SSEL will be connected to that pin on which switch JP1 is closed
				We assume that JP1-4 is closed.
 */
#define XLYNX_EXP_J1_7_SPI0_SSEL_PORT			GPIO_PORT_1
#define XLYNX_EXP_J1_7_SPI0_SSEL_PIN			GPIO_PIN_21

#define XLYNX_EXP_J1_6_SPI0_SCK_PORT			GPIO_PORT_1
#define XLYNX_EXP_J1_6_SPI0_SCK_PIN				GPIO_PIN_20

/** SSP 1 is used for SD card interface */

/** CAN configuration macros 				
 */

/** CAN 1 */
#define XLYNX_EXP_J1_13_CAN_1_RD_1_PORT			GPIO_PORT_0
#define XLYNX_EXP_J1_13_CAN_1_RD_1_PIN			GPIO_PIN_21

#define XLYNX_EXP_J1_14_CAN_1_TD_1_PORT			GPIO_PORT_0
#define XLYNX_EXP_J1_14_CAN_1_TD_1_PIN			GPIO_PIN_22

/** CAN 2 */
#define	 	XLYNX_J4_22_CAN_2_RD_2_PORT		    GPIO_PORT_2
#define		XLYNX_J4_22_CAN_2_RD_2_PIN		    GPIO_PIN_7

#define 	XLYNX_J4_19_CAN_2_TD_2_PORT			GPIO_PORT_2
#define		XLYNX_J4_19_CAN_2_TD_2_PIN			GPIO_PIN_8


/** 							DAC configuration macros 		
 */

#define 	XLYNX_J9_9_DAC_AOUT_PORT			GPIO_PORT_0
#define		XLYNX_J9_9_DAC_AOUT_PIN				GPIO_PIN_26


/** 							PWM configuration macros 			
 */

/** Output from PWM channels (1-6) */
#define		XLYNX_J4_1_PWM_CH_1_PORT			GPIO_PORT_2
#define		XLYNX_J4_1_PWM_CH_1_PIN				GPIO_PIN_0

#define		XLYNX_J4_4_PWM_CH_2_PORT			GPIO_PORT_2
#define		XLYNX_J4_4_PWM_CH_2_PIN				GPIO_PIN_1

#define 	XLYNX_J4_7_PWM_CH_3_PORT			GPIO_PORT_2
#define		XLYNX_J4_7_PWM_CH_3_PIN				GPIO_PIN_2

#define		XLYNX_J4_10_PWM_CH_4_PORT			GPIO_PORT_2
#define		XLYNX_J4_10_PWM_CH_4_PIN			GPIO_PIN_3

#define		XLYNX_J4_13_PWM_CH_5_PORT			GPIO_PORT_2
#define		XLYNX_J4_13_PWM_CH_5_PIN			GPIO_PIN_4

#define		XLYNX_J4_16_PWM_CH_6_PORT			GPIO_PORT_2
#define		XLYNX_J4_16_PWM_CH_6_PIN			GPIO_PIN_5


/** channels(0-7) to connect servo 
 */
/** pins to connect servo driven using Timer */
#define		XLYNX_J4_1_SERVO_CH_0_PORT			GPIO_PORT_2
#define		XLYNX_J4_1_SERVO_CH_0_PIN			GPIO_PIN_0

#define		XLYNX_J4_4_SERVO_CH_1_PORT			GPIO_PORT_2
#define		XLYNX_J4_4_SERVO_CH_1_PIN			GPIO_PIN_1

#define 	XLYNX_J4_7_SERVO_CH_2_PORT			GPIO_PORT_2
#define		XLYNX_J4_7_SERVO_CH_2_PIN			GPIO_PIN_2

#define		XLYNX_J4_10_SERVO_CH_3_PORT			GPIO_PORT_2
#define		XLYNX_J4_10_SERVO_CH_3_PIN			GPIO_PIN_3

#define		XLYNX_J4_13_SERVO_CH_4_PORT			GPIO_PORT_2
#define		XLYNX_J4_13_SERVO_CH_4_PIN			GPIO_PIN_4

#define		XLYNX_J4_16_SERVO_CH_5_PORT			GPIO_PORT_2
#define		XLYNX_J4_16_SERVO_CH_5_PIN			GPIO_PIN_5

/** pins to connect servo driven using Timer */
#define		XLYNX_J4_19_SERVO_CH_6_PORT			GPIO_PORT_2
#define		XLYNX_J4_19_SERVO_CH_6_PIN			GPIO_PIN_6

#define		XLYNX_J4_22_SERVO_CH_7_PORT			GPIO_PORT_2
#define		XLYNX_J4_22_SERVO_CH_7_PIN			GPIO_PIN_7


/** Capture input for PWM */
#define		XLYNX_J6_3_PWM_PCAP_0_PORT			GPIO_PORT_1
#define		XLYNX_J6_3_PWM_PCAP_0_PIN			GPIO_PIN_28


/** Motor Control PWM (MCPWM) configuration macros 	
 */


/** Output A for channels 0, 1, 2 */
/** Output Channel 1(MCOA0) used for USB interface */
#define		XLYNX_J6_5_MCPWM_MCOA_1_PORT		GPIO_PORT_1
#define		XLYNX_J6_5_MCPWM_MCOA_1_PIN			GPIO_PIN_25

#define		XLYNX_J6_3_MCPWM_MCOA_2_PORT		GPIO_PORT_1
#define		XLYNX_J6_3_MCPWM_MCOA_2_PIN			GPIO_PIN_28

/** Low-active Fast Abort */
#define		XLYNX_EXP_J1_7_MCPWM_MCABORT_PORT	GPIO_PORT_1
#define		XLYNX_EXP_J1_7_MCPWM_MCABORT_PIN	GPIO_PIN_21

/** Input for channels 0, 1, 2 */
#define	 	XLYNX_EXP_J1_6_MCPWM_MCI_0_PORT		GPIO_PORT_1
#define	 	XLYNX_EXP_J1_6_MCPWM_MCI_0_PIN		GPIO_PIN_20

#define		XLYNX_EXP_J1_8_MCPWM_MCI_1_PORT		GPIO_PORT_1
#define		XLYNX_EXP_J1_8_MCPWM_MCI_1_PIN		GPIO_PIN_23

#define		XLYNX_EXP_J1_9_MCPWM_MCI_2_PORT		GPIO_PORT_1
#define		XLYNX_EXP_J1_9_MCPWM_MCI_2_PIN		GPIO_PIN_24

/** 			Timer 0/1/2/3 configuration macros 		
 */

/** Input Capture Signals */

#define		XLYNX_J6_4_TIMER_0_CAP_1_PORT		GPIO_PORT_1
#define		XLYNX_J6_4_TIMER_0_CAP_1_PIN		GPIO_PIN_27

/** Timer 1 Capture pins connected to Vbus(P1.18) and USB_PPWR(P1.19)  
 */

/** Timer 2 Capture pins used for CAN Interfacing */

/** Timer 3 */
#define 	XLYNX_J9_7_TIMER_3_CAP_0_PORT		GPIO_PORT_0
#define 	XLYNX_J9_7_TIMER_3_CAP_0_PIN		GPIO_PIN_23

/** External Match Output pins */

/** Timer 0 */
#define		XLYNX_J6_3_TIMER_0_MAT_0_PORT		GPIO_PORT_1
#define		XLYNX_J6_3_TIMER_0_MAT_0_PIN		GPIO_PIN_28

/** Timer 1 */

#define 	XLYNX_J6_6_TIMER_1_MAT_0_PORT		GPIO_PORT_1
#define 	XLYNX_J6_6_TIMER_1_MAT_0_PIN		GPIO_PIN_22

#define 	XLYNX_J6_5_TIMER_1_MAT_1_PORT		GPIO_PORT_1
#define 	XLYNX_J6_5_TIMER_1_MAT_1_PIN		GPIO_PIN_25


/** Timer 2 */

#define		XLYNX_J5_5_TIMER_2_MAT_0_PORT		GPIO_PORT_4
#define		XLYNX_J5_5_TIMER_2_MAT_0_PIN		GPIO_PIN_28

#define		XLYNX_J5_8_TIMER_2_MAT_1_PORT		GPIO_PORT_4
#define		XLYNX_J5_8_TIMER_2_MAT_1_PIN		GPIO_PIN_29


/** Timer 3 */
#define		XLYNX_J2_4_TIMER_3_MAT_0_PORT		GPIO_PORT_0
#define		XLYNX_J2_4_TIMER_3_MAT_0_PIN		GPIO_PIN_10

#define		XLYNX_J2_6_TIMER_3_MAT_1_PORT		GPIO_PORT_0
#define		XLYNX_J2_6_TIMER_3_MAT_1_PIN		GPIO_PIN_11


/** L6470(smartlynx) integrated microstepping motor driver gpio configuration macros */ 
 #define	XLYNX_J5_2_L6470_MISO_PORT			GPIO_PORT_0		
 #define	XLYNX_J5_2_L6470_MISO_PIN			GPIO_PIN_8
 
 #define    XLYNX_J5_3_L6470_SCK_PORT			GPIO_PORT_0
 #define	XLYNX_J5_3_L6470_SCK_PIN       		GPIO_PIN_7
 
 #define	XLYNX_J5_4_L6470_MOSI_PORT			GPIO_PORT_0
 #define	XLYNX_J5_4_L6470_MOSI_PIN      		GPIO_PIN_9
 
 #define	XLYNX_J5_5_L6470_SYNC_PORT			GPIO_PORT_4
 #define	XLYNX_J5_5_L6470_SYNC_PIN         	GPIO_PIN_28
 
 #define	XLYNX_J5_6_L6470_SS_PORT			GPIO_PORT_0
 #define	XLYNX_J5_6_L6470_SS_PIN        		GPIO_PIN_6
 
 #define	XLYNX_J5_7_L6470_FLAG_PORT			GPIO_PORT_2
 #define	XLYNX_J5_7_L6470_FLAG_PIN         	GPIO_PIN_12
 
 #define	XLYNX_J5_8_L6470_STEP_PORT			GPIO_PORT_4
 #define	XLYNX_J5_8_L6470_STEP_PIN      		GPIO_PIN_29
 
 #define	XLYNX_J5_9_L6470_RST_PORT			GPIO_PORT_1
 #define	XLYNX_J5_9_L6470_RST_PIN       		GPIO_PIN_17
 

 /** A3977(Lynx) Microstepping DMOS Driver gpio configuration macros for channel 0
 */
  #define	XLYNX_J9_3_A3977_0_HOME_PORT 		GPIO_PORT_0
 #define	XLYNX_J9_3_A3977_0_HOME_PIN       	GPIO_PIN_27
 
 #define	XLYNX_J9_4_A3977_0_DIR_PORT			GPIO_PORT_0
 #define	XLYNX_J9_4_A3977_0_DIR_PIN      	GPIO_PIN_28
 
 #define	XLYNX_J9_5_A3977_0_MS2_PORT			GPIO_PORT_1
 #define	XLYNX_J9_5_A3977_0_MS2_PIN         	GPIO_PIN_30
 
 #define	XLYNX_J9_6_A3977_0_MS1_PORT			GPIO_PORT_1	
 #define	XLYNX_J9_6_A3977_0_MS1_PIN        	GPIO_PIN_31
	
 #define	XLYNX_J9_7_A3977_0_RST_PORT			GPIO_PORT_0		
 #define	XLYNX_J9_7_A3977_0_RST_PIN        	GPIO_PIN_23
 
 #define	XLYNX_J9_8_A3977_0_STEP_PORT		GPIO_PORT_0		
 #define	XLYNX_J9_8_A3977_0_STEP_PIN       	GPIO_PIN_25
 
 #define	XLYNX_J9_9_A3977_0_SLEEP_PORT		GPIO_PORT_0								
 #define	XLYNX_J9_9_A3977_0_SLEEP_PIN      	GPIO_PIN_26

 
 /** A3977(Lynx) Microstepping DMOS Driver gpio configuration macros for channel 1
 */
 #define	XLYNX_J7_3_A3977_1_HOME_PORT		GPIO_PORT_1
 #define	XLYNX_J7_3_A3977_1_HOME_PIN      	GPIO_PIN_0
 
 #define	XLYNX_J7_4_A3977_1_DIR_PORT			GPIO_PORT_1
 #define	XLYNX_J7_4_A3977_1_DIR_PIN      	GPIO_PIN_1
 
 #define	XLYNX_J7_5_A3977_1_MS2_PORT			GPIO_PORT_1
 #define	XLYNX_J7_5_A3977_1_MS2_PIN        	GPIO_PIN_4
 
 #define	XLYNX_J7_6_A3977_1_MS1_PORT			GPIO_PORT_1	
 #define	XLYNX_J7_6_A3977_1_MS1_PIN        	GPIO_PIN_8
	
 #define	XLYNX_J7_7_A3977_1_RST_PORT			GPIO_PORT_1		
 #define	XLYNX_J7_7_A3977_1_RST_PIN       	GPIO_PIN_9
 
 #define	XLYNX_J7_8_A3977_1_STEP_PORT		GPIO_PORT_1		
 #define	XLYNX_J7_8_A3977_1_STEP_PIN      	GPIO_PIN_10
 
 #define	XLYNX_J7_9_A3977_1_SLEEP_PORT		GPIO_PORT_1								
 #define	XLYNX_J7_9_A3977_1_SLEEP_PIN      	GPIO_PIN_14
 
 
 /** A3977(Lynx) Microstepping DMOS Driver gpio configuration macros for channel 2
 */
  #define	XLYNX_J6_3_A3977_2_HOME_PORT 		GPIO_PORT_1
 #define	XLYNX_J6_3_A3977_2_HOME_PIN       	GPIO_PIN_28
 
 #define	XLYNX_J6_4_A3977_2_DIR_PORT			GPIO_PORT_1
 #define	XLYNX_J6_4_A3977_2_DIR_PIN        	GPIO_PIN_27
 
 #define	XLYNX_J6_5_A3977_2_MS2_PORT			GPIO_PORT_1
 #define	XLYNX_J6_5_A3977_2_MS2_PIN        	GPIO_PIN_25
 
 #define	XLYNX_J6_6_A3977_2_MS1_PORT			GPIO_PORT_1	
 #define	XLYNX_J6_6_A3977_2_MS1_PIN        	GPIO_PIN_22
	
 #define	XLYNX_J6_7_A3977_2_RST_PORT			GPIO_PORT_1		
 #define	XLYNX_J6_7_A3977_2_RST_PIN       	GPIO_PIN_19
 
 #define	XLYNX_J6_8_A3977_2_STEP_PORT		GPIO_PORT_1		
 #define	XLYNX_J6_8_A3977_2_STEP_PIN         GPIO_PIN_18
 
 #define	XLYNX_J6_9_A3977_2_SLEEP_PORT		GPIO_PORT_3								
 #define	XLYNX_J6_9_A3977_2_SLEEP_PIN        GPIO_PIN_25

 
/** LED gpio pin configuration */
extern PFCfgGpio pfXlynxLEDGpioCfg[6];

/** Pushbutton gpio pin configuration */
extern PFCfgGpio pfXlynxPUSHBUTTONGpioCfg;

extern PFCfgGpio pfXlynxUART0GpioCfg[2];		 /** UART0 gpio pin configuration */
extern PFCfgGpio pfXlynxUART2GpioCfg[2];		 /** UART2 gpio pin configuration */
extern PFCfgGpio pfXlynxUART3GpioCfg[2];		 /** UART3 gpio pin configuration */

extern PFCfgGpio pfXlynxEINT1GpioCfg;            /** EINT1 gpio pin configuration */
extern PFCfgGpio pfXlynxEINT2GpioCfg;            /** EINT2 gpio pin configuration */

extern PFCfgGpio pfXlynxADC0GpioCfg;             /** ADC0 gpio pin configuration */
extern PFCfgGpio pfXlynxADC1GpioCfg;             /** ADC1 gpio pin configuration */
extern PFCfgGpio pfXlynxADC2GpioCfg;             /** ADC2 gpio pin configuration */
extern PFCfgGpio pfXlynxADC3GpioCfg;             /** ADC3 gpio pin configuration */
extern PFCfgGpio pfXlynxADC5GpioCfg;             /** ADC5 gpio pin configuration */

extern PFCfgGpio pfXlynxI2C1GpioCfg[2];          /** I2C1 gpio pin configuration */
extern PFCfgGpio pfXlynxI2C2GpioCfg[2];          /** I2C2 gpio pin configuration */

extern PFCfgGpio pfXlynxSSP0GpioCfg[4];          /** SSP0 gpio pin configuration */

extern PFCfgGpio pfXlynxCAN1GpioCfg[2];          /** CAN1 gpio pin configuration */
extern PFCfgGpio pfXlynxCAN2GpioCfg[2];          /** CAN2 gpio pin configuration */

extern PFCfgGpio pfXlynxDACGpioCfg;              /** DAC  gpio pin configuration */

extern PFCfgGpio pfXlynxPWM1GpioCfg;             /** PWM1 gpio pin configuration */
extern PFCfgGpio pfXlynxPWM2GpioCfg;             /** PWM2 gpio pin configuration */
extern PFCfgGpio pfXlynxPWM3GpioCfg;             /** PWM3 gpio pin configuration */
extern PFCfgGpio pfXlynxPWM4GpioCfg;             /** PWM4 gpio pin configuration */
extern PFCfgGpio pfXlynxPWM5GpioCfg;             /** PWM5 gpio pin configuration */
extern PFCfgGpio pfXlynxPWM6GpioCfg;             /** PWM6 gpio pin configuration */

extern PFCfgGpio pfXlynxPWMCap0GpioCfg;          /** PWMCap0 gpio pin configuration */

extern PFCfgGpio pfXlynxMCPWMAGpioCfg[2];        /** MCPWMA gpio pin configuration */
extern PFCfgGpio pfXlynxMCPWMAbortGpioCfg;       /** MCPWMAbort gpio pin configuration */
extern PFCfgGpio pfXlynxMCPWMMCIGpioCfg[3];      /** MCPWMMCI gpio pin configuration */

extern PFCfgGpio pfXlynxTIMER0CAPGpioCfg;        /** TIMER0CAP gpio pin configuration */
extern PFCfgGpio pfXlynxTIMER3CAPGpioCfg;        /** TIMER3CAP gpio pin configuration */

extern PFCfgGpio pfXlynxTIMER0MATGpioCfg;        /** TIMER0MAT gpio pin configuration */
extern PFCfgGpio pfXlynxTIMER1MATGpioCfg[2];     /** TIMER0MAT gpio pin configuration */
extern PFCfgGpio pfXlynxTIMER2MATGpioCfg[2];     /** TIMER0MAT gpio pin configuration */
extern PFCfgGpio pfXlynxTIMER3MATGpioCfg[2];     /** TIMER0MAT gpio pin configuration */

extern PFCfgGpio pfXlynxSmartLynxGpioCfg[8];     /** SmartLynx gpio pin configuration */
extern PFCfgGpio pfXlynxLynxGpioCfg[3][7];       /** Lynx gpio pin configurations for 3 channel */
extern PFCfgGpio pfXlynxServoGpioCfg[8];         /** Servo gpio pin configurations for 6 channel*/
#endif	// #ifdef MCU_CHIP_lpc1768

