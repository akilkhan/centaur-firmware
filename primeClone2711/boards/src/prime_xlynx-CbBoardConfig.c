#include "prime_framework.h"
#ifdef MCU_CHIP_lpc1768
#include "prime_gpio.h"
#include "prime_xlynx-CbBoardConfig.h"

PFCfgGpio pfXlynxLEDGpioCfg[6] = 
{
	// LED LD1
	{XLYNX_LED_LD1_PORT, XLYNX_LED_LD1_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// LED LD2
	{XLYNX_LED_LD2_PORT, XLYNX_LED_LD2_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// LED LD3
	{XLYNX_LED_LD3_PORT, XLYNX_LED_LD3_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// LED LD4
	{XLYNX_LED_LD4_PORT, XLYNX_LED_LD4_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// LED LD5
	{XLYNX_LED_LD5_PORT, XLYNX_LED_LD5_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// LED LD6
	{XLYNX_LED_LD6_PORT, XLYNX_LED_LD6_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio} 
};


PFCfgGpio pfXlynxPUSHBUTTONGpioCfg = { XLYNX_PUSHBUTTON_PB1_PORT, XLYNX_PUSHBUTTON_PB1_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio};

PFCfgGpio pfXlynxUART0GpioCfg[2] = 
{
	//TX0
	{XLYNX_UART_0_TX_PORT,XLYNX_UART_0_TX_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_2_TXD0},
	//RX0
	{XLYNX_UART_0_RX_PORT,XLYNX_UART_0_RX_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_3_RXD0} 
};


PFCfgGpio pfXlynxUART2GpioCfg[2] =
{
	//TX2
	{XLYNX_J2_4_UART_2_TX_PORT,XLYNX_J2_4_UART_2_TX_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_10_TXD2},
	//RX2
	{XLYNX_J2_6_UART_2_RX_PORT,XLYNX_J2_6_UART_2_RX_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_11_RXD2}
};



PFCfgGpio pfXlynxUART3GpioCfg[2] =
{
	//TX3
	{XLYNX_J9_8_UART_3_TX_PORT,XLYNX_J9_8_UART_3_TX_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_25_TXD3},
	//RX3
	{XLYNX_J9_9_UART_3_RX_PORT,XLYNX_J9_9_UART_3_RX_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_26_RXD3}
};


PFCfgGpio pfXlynxEINT1GpioCfg = {XLYNX_PUSHBUTTON_EINT_1_PORT,XLYNX_PUSHBUTTON_EINT_1_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_11_EINT1};
PFCfgGpio pfXlynxEINT2GpioCfg = {XLYNX_J5_7_EINT_2_PORT,XLYNX_J5_7_EINT_2_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_12_EINT2};


PFCfgGpio pfXlynxADC0GpioCfg = {XLYNX_J9_7_ADC_0_PORT,XLYNX_J9_7_ADC_0_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_23_ADC0};
PFCfgGpio pfXlynxADC1GpioCfg = {XLYNX_BAT_IN_ADC_1_PORT,XLYNX_BAT_IN_ADC_1_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_24_ADC1};
PFCfgGpio pfXlynxADC2GpioCfg = {XLYNX_J9_8_ADC_2_PORT,XLYNX_J9_8_ADC_2_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_25_ADC2};
PFCfgGpio pfXlynxADC3GpioCfg = {XLYNX_J9_9_ADC_3_PORT,XLYNX_J9_9_ADC_3_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_26_ADC3};
PFCfgGpio pfXlynxADC5GpioCfg = {XLYNX_J9_6_ADC_5_PORT,XLYNX_J9_6_ADC_5_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_31_ADC5};


PFCfgGpio pfXlynxI2C1GpioCfg[2] = 
{	
		//SDA
	{XLYNX_J2_3_I2C_1_SDA_PORT,XLYNX_J2_3_I2C_1_SDA_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_19_SDA1},
		//SCL
	{XLYNX_J2_5_I2C_1_SCL_PORT,XLYNX_J2_5_I2C_1_SCL_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_20_SCL1}
};

PFCfgGpio pfXlynxI2C2GpioCfg[2] =
{
		//SDA
	{XLYNX_J2_4_I2C_2_SDA_PORT,XLYNX_J2_4_I2C_2_SDA_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_10_SDA2},
		//SCL
	{XLYNX_J2_6_I2C_2_SCL_PORT,XLYNX_J2_6_I2C_2_SCL_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_11_SCL2}
};


PFCfgGpio pfXlynxSSP0GpioCfg[4] =
{
	//MISO0
	{XLYNX_EXP_J1_8_SPI0_MISO_PORT, XLYNX_EXP_J1_8_SPI0_MISO_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_23_MISO0},
	//MOSI0
	{XLYNX_EXP_J1_9_SPI0_MOSI_PORT,XLYNX_EXP_J1_9_SPI0_MOSI_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_24_MOSI0},
	//SCK0
	{XLYNX_EXP_J1_6_SPI0_SCK_PORT,XLYNX_EXP_J1_6_SPI0_SCK_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_20_SCK0},
	//SSEL0
	{XLYNX_EXP_J1_7_SPI0_SSEL_PORT,XLYNX_EXP_J1_7_SPI0_SSEL_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_21_SSEL0}
};
	

	
PFCfgGpio pfXlynxCAN1GpioCfg[2] =	
{
	//RD1
	{XLYNX_EXP_J1_13_CAN_1_RD_1_PORT, XLYNX_EXP_J1_13_CAN_1_RD_1_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_21_RD1},
	//TD1
	{XLYNX_EXP_J1_14_CAN_1_TD_1_PORT,XLYNX_EXP_J1_14_CAN_1_TD_1_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_22_TD1}
};


PFCfgGpio pfXlynxCAN2GpioCfg[2] =	
{
	//RD2
	{XLYNX_J4_22_CAN_2_RD_2_PORT,XLYNX_J4_22_CAN_2_RD_2_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_7_RD2},
	//TD1
	{XLYNX_J4_19_CAN_2_TD_2_PORT,XLYNX_J4_19_CAN_2_TD_2_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_8_TD2}
};


PFCfgGpio pfXlynxDACGpioCfg = {XLYNX_J9_9_DAC_AOUT_PORT,XLYNX_J9_9_DAC_AOUT_PORT,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_26_AOUT};

PFCfgGpio pfXlynxPWM1GpioCfg = {XLYNX_J4_1_PWM_CH_1_PORT,XLYNX_J4_1_PWM_CH_1_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_0_PWM1_1};
PFCfgGpio pfXlynxPWM2GpioCfg = {XLYNX_J4_4_PWM_CH_2_PORT,XLYNX_J4_4_PWM_CH_2_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_1_PWM1_2};
PFCfgGpio pfXlynxPWM3GpioCfg = {XLYNX_J4_7_PWM_CH_3_PORT,XLYNX_J4_7_PWM_CH_3_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_2_PWM1_3};
PFCfgGpio pfXlynxPWM4GpioCfg = {XLYNX_J4_10_PWM_CH_4_PORT,XLYNX_J4_10_PWM_CH_4_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_3_PWM1_4};
PFCfgGpio pfXlynxPWM5GpioCfg = {XLYNX_J4_13_PWM_CH_5_PORT,XLYNX_J4_13_PWM_CH_5_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_4_PWM1_5};
PFCfgGpio pfXlynxPWM6GpioCfg = {XLYNX_J4_16_PWM_CH_6_PORT,XLYNX_J4_16_PWM_CH_6_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_5_PWM1_6};

PFCfgGpio pfXlynxPWMCap0GpioCfg = {XLYNX_J6_3_PWM_PCAP_0_PORT,XLYNX_J6_3_PWM_PCAP_0_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_28_PCAP1_0};


PFCfgGpio pfXlynxMCPWMAGpioCfg[2] =
{
	{XLYNX_J6_5_MCPWM_MCOA_1_PORT,XLYNX_J6_5_MCPWM_MCOA_1_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_25_MCOA1},
	{XLYNX_J6_3_MCPWM_MCOA_2_PORT,XLYNX_J6_3_MCPWM_MCOA_2_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_28_MCOA2}
};


PFCfgGpio pfXlynxMCPWMAbortGpioCfg = {XLYNX_EXP_J1_7_MCPWM_MCABORT_PORT,XLYNX_EXP_J1_7_MCPWM_MCABORT_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_21_MCABORT};


PFCfgGpio pfXlynxMCPWMMCIGpioCfg[3] = 
{
	{XLYNX_EXP_J1_6_MCPWM_MCI_0_PORT,XLYNX_EXP_J1_6_MCPWM_MCI_0_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_20_MCI0},
	{XLYNX_EXP_J1_8_MCPWM_MCI_1_PORT,XLYNX_EXP_J1_8_MCPWM_MCI_1_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_23_MCI1},
	{XLYNX_EXP_J1_9_MCPWM_MCI_2_PORT,XLYNX_EXP_J1_9_MCPWM_MCI_2_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_24_MCI2}
};


PFCfgGpio pfXlynxTIMER0CAPGpioCfg = {XLYNX_J6_4_TIMER_0_CAP_1_PORT,XLYNX_J6_4_TIMER_0_CAP_1_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_27_CAP0_1};
PFCfgGpio pfXlynxTIMER3CAPGpioCfg = {XLYNX_J9_7_TIMER_3_CAP_0_PORT,XLYNX_J9_7_TIMER_3_CAP_0_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_23_CAP3_0};
PFCfgGpio pfXlynxTIMER0MATGpioCfg = {XLYNX_J6_3_TIMER_0_MAT_0_PORT,XLYNX_J6_3_TIMER_0_MAT_0_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_28_MAT0_0};

PFCfgGpio pfXlynxTIMER1MATGpioCfg[2] = 
{
	{XLYNX_J6_6_TIMER_1_MAT_0_PORT,XLYNX_J6_6_TIMER_1_MAT_0_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_22_MAT1_0 },
	{XLYNX_J6_5_TIMER_1_MAT_1_PORT,XLYNX_J6_5_TIMER_1_MAT_1_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_25_MAT1_1 }
};

PFCfgGpio pfXlynxTIMER2MATGpioCfg[2] = 
{
	{XLYNX_J5_5_TIMER_2_MAT_0_PORT,XLYNX_J5_5_TIMER_2_MAT_0_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_4_28_MAT2_0 },
	{XLYNX_J5_8_TIMER_2_MAT_1_PORT,XLYNX_J5_8_TIMER_2_MAT_1_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_4_29_MAT2_1 }
};


PFCfgGpio pfXlynxTIMER3MATGpioCfg[2] =	
{
	{XLYNX_J2_4_TIMER_3_MAT_0_PORT,XLYNX_J2_4_TIMER_3_MAT_0_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_10_MAT3_0},
	{XLYNX_J2_6_TIMER_3_MAT_1_PORT,XLYNX_J2_6_TIMER_3_MAT_1_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_11_MAT3_1}
};

PFCfgGpio pfXlynxSmartLynxGpioCfg[8] = 
{
   //SPI 1 MISO
  {XLYNX_J5_2_L6470_MISO_PORT, XLYNX_J5_2_L6470_MISO_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable,GPIO_0_8_MISO1 },
   //SPI 1 MOSI
  {XLYNX_J5_4_L6470_MOSI_PORT, XLYNX_J5_4_L6470_MOSI_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable,GPIO_0_9_MOSI1},
   // SPI 1 SCK
  {XLYNX_J5_3_L6470_SCK_PORT, XLYNX_J5_3_L6470_SCK_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable,GPIO_0_7_SCK1 },
   //SPI 1 SS
  {XLYNX_J5_6_L6470_SS_PORT,XLYNX_J5_6_L6470_SS_PIN,enGpioDirOutput,enGpioPinModePullUp,enGpioOpenDrainDisable,enGpioFunctionGpio},
   //L6470_SYNC 
  {XLYNX_J5_5_L6470_SYNC_PORT,XLYNX_J5_5_L6470_SYNC_PIN,enGpioDirInput,enGpioPinModePullUp,enGpioOpenDrainDisable,enGpioFunctionGpio},
  // L6470_FLAG
  {XLYNX_J5_7_L6470_FLAG_PORT,XLYNX_J5_7_L6470_FLAG_PIN,enGpioDirInput,enGpioPinModePullUp,enGpioOpenDrainDisable,enGpioFunctionGpio},
  // L6470_STEP
  {XLYNX_J5_8_L6470_STEP_PORT,XLYNX_J5_8_L6470_STEP_PIN,enGpioDirOutput,enGpioPinModePullUp,enGpioOpenDrainDisable,enGpioFunctionGpio},
  // L6470_RST
  {XLYNX_J5_9_L6470_RST_PORT,XLYNX_J5_9_L6470_RST_PIN,enGpioDirOutput,enGpioPinModePullUp,enGpioOpenDrainDisable,enGpioFunctionGpio}
};

PFCfgGpio pfXlynxLynxGpioCfg[3][7] = 
{
	{
		{XLYNX_J9_3_A3977_0_HOME_PORT, XLYNX_J9_3_A3977_0_HOME_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
		{XLYNX_J9_4_A3977_0_DIR_PORT, XLYNX_J9_4_A3977_0_DIR_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
		{XLYNX_J9_6_A3977_0_MS1_PORT, XLYNX_J9_6_A3977_0_MS1_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
		{XLYNX_J9_5_A3977_0_MS2_PORT, XLYNX_J9_5_A3977_0_MS2_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
		{XLYNX_J9_7_A3977_0_RST_PORT, XLYNX_J9_7_A3977_0_RST_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
		{XLYNX_J9_9_A3977_0_SLEEP_PORT, XLYNX_J9_9_A3977_0_SLEEP_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
		{XLYNX_J9_8_A3977_0_STEP_PORT, XLYNX_J9_8_A3977_0_STEP_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio}
	},
	
	{	
		{XLYNX_J7_3_A3977_1_HOME_PORT, XLYNX_J7_3_A3977_1_HOME_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
		{XLYNX_J7_4_A3977_1_DIR_PORT, XLYNX_J7_4_A3977_1_DIR_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
		{XLYNX_J7_6_A3977_1_MS1_PORT, XLYNX_J7_6_A3977_1_MS1_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
		{XLYNX_J7_5_A3977_1_MS2_PORT, XLYNX_J7_5_A3977_1_MS2_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
		{XLYNX_J7_7_A3977_1_RST_PORT, XLYNX_J7_7_A3977_1_RST_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
		{XLYNX_J7_9_A3977_1_SLEEP_PORT, XLYNX_J7_9_A3977_1_SLEEP_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
		{XLYNX_J7_8_A3977_1_STEP_PORT, XLYNX_J7_8_A3977_1_STEP_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio}
	},

	{
		{XLYNX_J6_3_A3977_2_HOME_PORT, XLYNX_J6_3_A3977_2_HOME_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
		{XLYNX_J6_4_A3977_2_DIR_PORT, XLYNX_J6_4_A3977_2_DIR_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
		{XLYNX_J6_6_A3977_2_MS1_PORT, XLYNX_J6_6_A3977_2_MS1_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
		{XLYNX_J6_5_A3977_2_MS2_PORT, XLYNX_J6_5_A3977_2_MS2_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
		{XLYNX_J6_7_A3977_2_RST_PORT, XLYNX_J6_7_A3977_2_RST_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
		{XLYNX_J6_9_A3977_2_SLEEP_PORT, XLYNX_J6_9_A3977_2_SLEEP_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
		{XLYNX_J6_8_A3977_2_STEP_PORT, XLYNX_J6_8_A3977_2_STEP_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio}
	}
};

PFCfgGpio pfXlynxServoGpioCfg[8] = 
{
	{XLYNX_J4_1_SERVO_CH_0_PORT,XLYNX_J4_1_SERVO_CH_0_PIN,enGpioDirOutput,enGpioPinModeNoPullUpDown,enGpioOpenDrainDisable, GPIO_2_0_PWM1_1},
	{XLYNX_J4_4_SERVO_CH_1_PORT,XLYNX_J4_4_SERVO_CH_1_PIN,enGpioDirOutput, enGpioPinModeNoPullUpDown, enGpioOpenDrainDisable, GPIO_2_1_PWM1_2},
	{XLYNX_J4_7_SERVO_CH_2_PORT,XLYNX_J4_7_SERVO_CH_2_PIN,enGpioDirOutput, enGpioPinModeNoPullUpDown, enGpioOpenDrainDisable, GPIO_2_2_PWM1_3},
	{XLYNX_J4_10_SERVO_CH_3_PORT,XLYNX_J4_10_SERVO_CH_3_PIN,enGpioDirOutput, enGpioPinModeNoPullUpDown, enGpioOpenDrainDisable, GPIO_2_3_PWM1_4},
	{XLYNX_J4_13_SERVO_CH_4_PORT,XLYNX_J4_13_SERVO_CH_4_PIN,enGpioDirOutput, enGpioPinModeNoPullUpDown, enGpioOpenDrainDisable, GPIO_2_4_PWM1_5},
	{XLYNX_J4_16_SERVO_CH_5_PORT,XLYNX_J4_16_SERVO_CH_5_PIN,enGpioDirOutput, enGpioPinModeNoPullUpDown, enGpioOpenDrainDisable, GPIO_2_5_PWM1_6},
	{XLYNX_J4_19_SERVO_CH_6_PORT,XLYNX_J4_19_SERVO_CH_6_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	{XLYNX_J4_22_SERVO_CH_7_PORT,XLYNX_J4_22_SERVO_CH_7_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio}
};


#endif	// #ifdef MCU_CHIP_lpc1768
