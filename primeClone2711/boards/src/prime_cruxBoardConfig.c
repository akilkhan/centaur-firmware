#include "prime_framework.h"
#ifdef MCU_CHIP_at90can128
#include "prime_gpio.h"
#include "prime_cruxBoardConfig.h"


PFCfgGpio pfCruxLEDGpioCfg[3] =	
{
	//	LED UL1
	{CRUX_LED_UL1_PORT, CRUX_LED_UL1_PIN, enGpioDirOutput, enGpioPinModePullUp},	
	
	//	LED UL2
	{CRUX_LED_UL2_PORT, CRUX_LED_UL2_PIN, enGpioDirOutput, enGpioPinModePullUp},	
		
	//	LED UL3
	{CRUX_LED_UL3_PORT, CRUX_LED_UL3_PIN, enGpioDirOutput, enGpioPinModePullUp}
};		
	

PFCfgGpio pfCruxPushButtonGpioCfg[3] =
{
	//	Push Button PB1
	{CRUX_PUSHBUTTON_PB1_PORT, CRUX_PUSHBUTTON_PB1_PIN, enGpioDirOutput, enGpioPinModePullUp},
	
	//	Push Button PB2
	{CRUX_PUSHBUTTON_PB2_PORT, CRUX_PUSHBUTTON_PB2_PIN, enGpioDirOutput, enGpioPinModePullUp},	

	//	Push Button PB3
	{CRUX_PUSHBUTTON_PB3_PORT, CRUX_PUSHBUTTON_PB3_PIN, enGpioDirOutput, enGpioPinModePullUp}

};

//	For Synchronous Master Mode of USART0
PFCfgGpio pfCruxUsart0MasterGpioCfg[3] =
{
	//	USART0 Rx
	{CRUX_J3_15_USART0_RXD0_PORT, CRUX_J3_15_USART0_RXD0_PIN, enGpioDirInput, enGpioPinModePullUp},
	
	//	USART0 Tx
	{CRUX_J3_16_USART0_TXD0_PORT, CRUX_J3_16_USART0_TXD0_PIN, enGpioDirOutput, enGpioPinModePullUp},
	
	//	USART0 XCK
	{CRUX_J4_9_USART0_XCK0_PORT, CRUX_J4_9_USART0_XCK0_PIN, enGpioDirOutput, enGpioPinModePullUp}
};

//	For Synchronous Slave Mode of USART0
PFCfgGpio pfCruxUsart0SlaveGpioCfg[3]  =
{
	//	USART0 Rx
	{CRUX_J3_15_USART0_RXD0_PORT, CRUX_J3_15_USART0_RXD0_PIN, enGpioDirInput, enGpioPinModePullUp},
	
	//	USART0 Tx
	{CRUX_J3_16_USART0_TXD0_PORT, CRUX_J3_16_USART0_TXD0_PIN, enGpioDirOutput, enGpioPinModePullUp},
	
	//	USART0 XCK
	{CRUX_J4_9_USART0_XCK0_PORT, CRUX_J4_9_USART0_XCK0_PIN, enGpioDirInput, enGpioPinModePullUp}
	
};

//	For Asynchronous Mode of USART0
PFCfgGpio pfCruxUart0MGpioCfg[2]  =
{
	//	USART0 Rx
	{CRUX_J3_15_USART0_RXD0_PORT, CRUX_J3_15_USART0_RXD0_PIN, enGpioDirInput, enGpioPinModePullUp},
	
	//	USART0 Tx
	{CRUX_J3_16_USART0_TXD0_PORT, CRUX_J3_16_USART0_TXD0_PIN, enGpioDirOutput, enGpioPinModePullUp}
};

//	For Asynchronous Mode of USART1
PFCfgGpio pfCruxUart1MGpioCfg[2] =
{
	//	USART1 Rx
	{CRUX_J4_5_USART1_RXD1_PORT, CRUX_J4_5_USART1_RXD1_PIN, enGpioDirInput, enGpioPinModePullUp},
	
	//	USART1 Tx
	{CRUX_J4_4_USART1_TXD1_PORT, CRUX_J4_4_USART1_TXD1_PIN, enGpioDirOutput, enGpioPinModePullUp}
};

PFCfgGpio pfCruxInt0GpioCfg = {CRUX_J3_11_INT0_PORT, CRUX_J3_11_INT0_PIN, enGpioDirInput, enGpioPinModePullUp};	
PFCfgGpio pfCruxInt1GpioCfg = {CRUX_J3_12_INT1_PORT,CRUX_J3_12_INT1_PIN, enGpioDirInput, enGpioPinModePullUp};
PFCfgGpio pfCruxInt2GpioCfg = {CRUX_J4_5_INT2_PORT, CRUX_J4_5_INT2_PIN, enGpioDirInput, enGpioPinModePullUp};
PFCfgGpio pfCruxInt3GpioCfg = {CRUX_J4_4_INT3_PORT, CRUX_J4_4_INT3_PIN, enGpioDirInput, enGpioPinModePullUp};	
PFCfgGpio pfCruxInt4GpioCfg = {CRUX_J3_17_INT4_PORT, CRUX_J3_17_INT4_PIN, enGpioDirInput, enGpioPinModePullUp};	
PFCfgGpio pfCruxInt5GpioCfg = {CRUX_J3_18_INT5_PORT, CRUX_J3_18_INT5_PIN, enGpioDirInput, enGpioPinModePullUp};	
	
PFCfgGpio pfCruxADC0GpioCfg = {CRUX_J4_19_ADC0_PORT, CRUX_J4_19_ADC0_PIN, enGpioDirInput, enGpioPinModePullUp};
PFCfgGpio pfCruxADC1GpioCfg = {CRUX_J4_18_ADC1_PORT, CRUX_J4_18_ADC1_PIN, enGpioDirInput, enGpioPinModePullUp};
PFCfgGpio pfCruxADC2GpioCfg = {CRUX_J4_17_ADC2_PORT, CRUX_J4_17_ADC2_PIN, enGpioDirInput, enGpioPinModePullUp};
PFCfgGpio pfCruxADC3GpioCfg = {CRUX_J4_16_ADC3_PORT, CRUX_J4_16_ADC3_PIN, enGpioDirInput, enGpioPinModePullUp};
PFCfgGpio pfCruxADC4GpioCfg = {CRUX_J4_15_ADC4_PORT, CRUX_J4_15_ADC4_PIN, enGpioDirInput, enGpioPinModePullUp};
PFCfgGpio pfCruxADC5GpioCfg = {CRUX_J4_14_ADC5_PORT, CRUX_J4_14_ADC5_PIN, enGpioDirInput, enGpioPinModePullUp};
PFCfgGpio pfCruxADC6GpioCfg = {CRUX_J4_13_ADC6_PORT, CRUX_J4_13_ADC6_PIN, enGpioDirInput, enGpioPinModePullUp};
PFCfgGpio pfCruxADC7GpioCfg = {CRUX_J4_12_ADC7_PORT, CRUX_J4_12_ADC7_PIN, enGpioDirInput, enGpioPinModePullUp};
	
PFCfgGpio pfCruxSPIMasterGpioCfg[4] = {
								{CRUX_J3_9_SPI_MOSI_PORT, CRUX_J3_9_SPI_MOSI_PIN, enGpioDirOutput, enGpioPinModePullUp},
								{CRUX_J3_8_SPI_MISO_PORT, CRUX_J3_8_SPI_MISO_PIN, enGpioDirInput, enGpioPinModePullUp},
								{CRUX_J3_6_SPI_SCK_PORT, CRUX_J3_6_SPI_SCK_PIN, enGpioDirOutput, enGpioPinModePullUp},
								{CRUX_SPI_SS_PORT, CRUX_SPI_SS_PIN, enGpioDirOutput, enGpioPinModePullUp}								
							} ;
PFCfgGpio pfCruxSPISlaveGpioCfg[4] = {
								{CRUX_J3_9_SPI_MOSI_PORT, CRUX_J3_9_SPI_MOSI_PIN, enGpioDirInput, enGpioPinModePullUp},
								{CRUX_J3_8_SPI_MISO_PORT, CRUX_J3_8_SPI_MISO_PIN, enGpioDirOutput, enGpioPinModePullUp},
								{CRUX_J3_6_SPI_SCK_PORT, CRUX_J3_6_SPI_SCK_PIN, enGpioDirInput, enGpioPinModePullUp},
								{CRUX_SPI_SS_PORT, CRUX_SPI_SS_PIN, enGpioDirInput, enGpioPinModePullUp}								
							} ;
	
	
PFCfgGpio pfCruxI2CGpioCfg[2] = {
								{CRUX_J3_12_I2C_SDA_PORT, CRUX_J3_12_I2C_SDA_PIN, enGpioDirInput, enGpioPinModePullUp},
								{CRUX_J3_11_I2C_SCL_PORT, CRUX_J3_11_I2C_SCL_PIN, enGpioDirOutput, enGpioPinModePullUp}
							}	;						

PFCfgGpio pfCruxCANCGpioCfg[2] = {
								{CRUX_J3_13_CAN_RXCAN_PORT, CRUX_J3_13_CAN_RXCAN_PIN, enGpioDirInput, enGpioPinModePullUp},
								{CRUX_J3_14_CAN_TXCAN_PORT, CRUX_J3_14_CAN_TXCAN_PIN, enGpioDirOutput, enGpioPinModePullUp}
							}	;
								
PFCfgGpio pfCruxAnlgCompGpioCfg[2] = {
								{CRUX_J4_9_ACOMP_AIN0_PORT, CRUX_J4_9_ACOMP_AIN0_PIN, enGpioDirInput, enGpioPinModePullUp},
								{CRUX_J4_8_ACOMP_AIN1_PORT, CRUX_J4_8_ACOMP_AIN1_PIN, enGpioDirInput, enGpioPinModePullUp}
							};							
					
#endif	// #ifdef MCU_CHIP_at90can128

	
