#include "prime_framework.h"
#ifdef MCU_CHIP_lpc1768
#include "prime_gpio.h"
#include "prime_hydraBoardConfig.h"

PFCfgGpio pfHydraLEDGpioCfg[3] = 
{
	// LED UL1
	{HYDRA_LED_UL1_PORT, HYDRA_LED_UL1_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// LED UL2
	{HYDRA_LED_UL2_PORT, HYDRA_LED_UL2_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// LED UL3
	{HYDRA_LED_UL3_PORT, HYDRA_LED_UL3_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio}
};

PFCfgGpio pfHydraPUSHBUTTONGpioCfg[3] =
{	
	// PB1
	{HYDRA_PUSHBUTTON_PB1_PORT, HYDRA_PUSHBUTTON_PB1_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// PB2
	{HYDRA_PUSHBUTTON_PB2_PORT, HYDRA_PUSHBUTTON_PB2_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// PB3
	{HYDRA_PUSHBUTTON_PB3_PORT, HYDRA_PUSHBUTTON_PB3_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio}
};

PFCfgGpio pfHydraUART0GpioCfg[2] = 
{
	//TX0
	{HYDRA_UART_0_TX_PORT,HYDRA_UART_0_TX_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_2_TXD0},
	//RX0
	{HYDRA_UART_0_RX_PORT,HYDRA_UART_0_RX_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_3_RXD0}
};

PFCfgGpio pfHydraUART2GpioCfg[2] =
{
	//TX2
	{HYDRA_EXP_16_UART_2_TX_PORT,HYDRA_EXP_16_UART_2_TX_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_10_TXD2},
	//RX2
	{HYDRA_EXP_15_UART_2_RX_PORT,HYDRA_EXP_15_UART_2_RX_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_11_RXD2}
};

PFCfgGpio pfHydraUART3GpioCfg[2] =
{
	//TX3
	{HYDRA_J6_23_UART_3_TX_PORT,HYDRA_J6_23_UART_3_TX_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_25_TXD3},
	//RX3
	{HYDRA_J6_22_UART_3_RX_PORT,HYDRA_J6_22_UART_3_RX_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_26_RXD3}
};

PFCfgGpio pfHydraEINT0GpioCfg = {HYDRA_J6_7_EINT_0_PORT,HYDRA_J6_7_EINT_0_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_10_EINT0};
PFCfgGpio pfHydraEINT1GpioCfg = {HYDRA_EXP_17_EINT_1_PORT,HYDRA_EXP_17_EINT_1_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_11_EINT1};
PFCfgGpio pfHydraEINT2GpioCfg = {HYDRA_EXP_18_EINT_2_PORT,HYDRA_EXP_18_EINT_2_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_12_EINT2};
PFCfgGpio pfHydraEINT3GpioCfg = {HYDRA_J6_6_EINT_3_PORT,HYDRA_J6_6_EINT_3_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_13_EINT3};

PFCfgGpio pfHydraADC0GpioCfg = {HYDRA_J6_25_ADC_0_PORT,HYDRA_J6_25_ADC_0_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_23_ADC0};
PFCfgGpio pfHydraADC1GpioCfg = {HYDRA_J6_24_ADC_1_PORT,HYDRA_J6_24_ADC_1_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_24_ADC1};
PFCfgGpio pfHydraADC2GpioCfg = {HYDRA_J6_23_ADC_2_PORT,HYDRA_J6_23_ADC_2_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_25_ADC2};
PFCfgGpio pfHydraADC3GpioCfg = {HYDRA_J6_22_ADC_3_PORT,HYDRA_J6_22_ADC_3_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_26_ADC3};
PFCfgGpio pfHydraADC5GpioCfg = {HYDRA_J6_26_ADC_5_PORT,HYDRA_J6_26_ADC_5_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_31_ADC5};

PFCfgGpio pfHydraI2C1GpioCfg[2] = 
{	
		//SDA
	{HYDRA_J6_10_I2C_1_SDA_PORT,HYDRA_J6_10_I2C_1_SDA_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_19_SDA1},
		//SCL
	{HYDRA_J6_9_I2C_1_SCL_PORT,HYDRA_J6_9_I2C_1_SCL_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_20_SCL1}
};

PFCfgGpio pfHydraI2C2GpioCfg[2] =
{
		//SDA
	{HYDRA_EXP_16_I2C_2_SDA_PORT,HYDRA_EXP_16_I2C_2_SDA_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_10_SDA2},
		//SCL
	{HYDRA_EXP_15_I2C_2_SCL_PORT,HYDRA_EXP_15_I2C_2_SCL_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_11_SCL2}
};

PFCfgGpio pfHydraSSP0GpioCfg[4] =
{
	//MISO0
	{HYDRA_EXP_8_SSP_0_MISO_PORT,HYDRA_EXP_8_SSP_0_MISO_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_23_MISO0},
	//MOSI0
	{HYDRA_EXP_9_SSP_0_MOSI_PORT,HYDRA_EXP_9_SSP_0_MOSI_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_24_MOSI0},
	//SCK0
	{HYDRA_EXP_6_SSP_0_SCK_PORT,HYDRA_EXP_6_SSP_0_SCK_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_20_SCK0},
	//SSEL0
	{HYDRA_EXP_7_SSP_0_SSEL_PORT,HYDRA_EXP_7_SSP_0_SSEL_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_21_SSEL0}
};
	
PFCfgGpio pfHydraCAN1GpioCfg[2] =	
{
	//RD1
	{HYDRA_EXP_13_CAN_1_RD_1_PORT,HYDRA_EXP_13_CAN_1_RD_1_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_21_RD1},
	//TD1
	{HYDRA_EXP_14_CAN_1_TD_1_PORT,HYDRA_EXP_14_CAN_1_TD_1_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_22_TD1}
};

PFCfgGpio pfHydraCAN2GpioCfg[2] =	
{
	//RD2
	{HYDRA_CAN_2_RD_2_PORT,HYDRA_CAN_2_RD_2_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_7_RD2},
	//TD1
	{HYDRA_CAN_2_TD_2_PORT,HYDRA_CAN_2_TD_2_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_8_TD2}
};

PFCfgGpio pfHydraDACGpioCfg = {HYDRA_J6_22_DAC_AOUT_PORT,HYDRA_J6_22_DAC_AOUT_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_26_AOUT};

PFCfgGpio pfHydraPWM1GpioCfg = {HYDRA_J6_18_PWM_CH_1_PORT,HYDRA_J6_18_PWM_CH_1_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_0_PWM1_1};

PFCfgGpio pfHydraPWM2GpioCfg = {HYDRA_J6_17_PWM_CH_2_PORT,HYDRA_J6_17_PWM_CH_2_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_1_PWM1_2};
PFCfgGpio pfHydraPWM3GpioCfg = {HYDRA_J6_16_PWM_CH_3_PORT,HYDRA_J6_16_PWM_CH_3_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_2_PWM1_3};
PFCfgGpio pfHydraPWM4GpioCfg = {HYDRA_J6_15_PWM_CH_4_PORT,HYDRA_J6_15_PWM_CH_4_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_3_PWM1_4};
PFCfgGpio pfHydraPWM5GpioCfg = {HYDRA_J6_14_PWM_CH_5_PORT,HYDRA_J6_14_PWM_CH_5_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_4_PWM1_5};
PFCfgGpio pfHydraPWM6GpioCfg = {HYDRA_J6_13_PWM_CH_6_PORT,HYDRA_J6_13_PWM_CH_6_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_5_PWM1_6};

PFCfgGpio pfHydraPWMCap0GpioCfg = {HYDRA_EXP_5_PWM_PCAP_0_PORT,HYDRA_EXP_5_PWM_PCAP_0_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_28_PCAP1_0};
PFCfgGpio pfHydraPWMCap1GpioCfg = {HYDRA_J6_5_PWM_PCAP_1_PORT,HYDRA_J6_5_PWM_PCAP_1_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_29_PCAP1_1};

PFCfgGpio pfHydraI2SGpioCfg = {HYDRA_J6_19_I2S_TX_MCLK_PORT,HYDRA_J6_19_I2S_TX_MCLK_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_4_29_TX_MCLK};

PFCfgGpio pfHydraMCPWMAGpioCfg[2] =
{
	{HYDRA_EXP_3_MCPWM_MCOA_1_PORT,HYDRA_EXP_3_MCPWM_MCOA_1_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_25_MCOA1},
	{HYDRA_EXP_5_MCPWM_MCOA_2_PORT,HYDRA_EXP_5_MCPWM_MCOA_2_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_28_MCOA2}
};
 
PFCfgGpio pfHydraMCPWMBGpioCfg[2] =
{
	{HYDRA_EXP_4_MCPWM_MCOB_1_PORT,HYDRA_EXP_4_MCPWM_MCOB_1_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_26_MCOB1},
	{HYDRA_J6_5_MCPWM_MCOB_2_PORT,HYDRA_J6_5_MCPWM_MCOB_2_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_29_MCOB2}
};

PFCfgGpio pfHydraMCPWMAbortGpioCfg = {HYDRA_EXP_7_MCPWM_MCABORT_PORT,HYDRA_EXP_7_MCPWM_MCABORT_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_21_MCABORT};

PFCfgGpio pfHydraMCPWMMCIGpioCfg[3] = 
{
	{HYDRA_EXP_6_MCPWM_MCI_0_PORT,HYDRA_EXP_6_MCPWM_MCI_0_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_20_MCI0},
	{HYDRA_EXP_8_MCPWM_MCI_1_PORT,HYDRA_EXP_8_MCPWM_MCI_1_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_23_MCI1},
	{HYDRA_EXP_9_MCPWM_MCI_2_PORT,HYDRA_EXP_9_MCPWM_MCI_2_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_24_MCI2}
};

PFCfgGpio pfHydraTIMER0CAPGpioCfg[2] = 
{
	{HYDRA_EXP_4_TIMER_0_CAP_0_PORT,HYDRA_EXP_4_TIMER_0_CAP_0_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_26_CAP0_0},	
	{HYDRA_EXP_10_TIMER_0_CAP_1_PORT,HYDRA_EXP_10_TIMER_0_CAP_1_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_27_CAP0_1}
};

PFCfgGpio pfHydraTIMER3CAPGpioCfg[2] =
{
	{HYDRA_J6_25_TIMER_3_CAP_0_PORT,HYDRA_J6_25_TIMER_3_CAP_0_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_23_CAP3_0},
	{HYDRA_J6_24_TIMER_3_CAP_1_PORT,HYDRA_J6_24_TIMER_3_CAP_1_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_24_CAP3_1}
};

PFCfgGpio pfHydraTIMER0MATGpioCfg[2] = 
{
	{HYDRA_EXP_5_TIMER_0_MAT_0_PORT,HYDRA_EXP_5_TIMER_0_MAT_0_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_28_MAT0_0},
	{HYDRA_J6_5_TIMER_0_MAT_1_PORT,HYDRA_J6_5_TIMER_0_MAT_1_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_29_MAT0_1}
};
		
PFCfgGpio pfHydraTIMER1MATGpioCfg = {HYDRA_EXP_3_TIMER_1_MAT_1_PORT,HYDRA_EXP_3_TIMER_1_MAT_1_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_25_MAT1_1 };
PFCfgGpio pfHydraTIMER2MATGpioCfg = {HYDRA_J6_19_TIMER_2_MAT_1_PORT,HYDRA_J6_19_TIMER_2_MAT_1_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_4_29_MAT2_1 };

PFCfgGpio pfHydraTIMER3MATGpioCfg[2] =
{
	{HYDRA_EXP_16_TIMER_3_MAT_0_PORT,HYDRA_EXP_16_TIMER_3_MAT_0_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_10_MAT3_0},
	{HYDRA_EXP_15_TIMER_3_MAT_1_PORT,HYDRA_EXP_15_TIMER_3_MAT_1_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_11_MAT3_1}
};

#endif	// #ifdef MCU_CHIP_lpc1768

