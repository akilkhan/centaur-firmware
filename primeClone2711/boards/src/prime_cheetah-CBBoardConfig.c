#include "prime_framework.h"
#ifdef MCU_CHIP_lpc1768
#include "prime_gpio.h"
#include "prime_cheetah-CbBoardConfig.h"

PFCfgGpio pfCheetahCbLEDGpioCfg[6] = 
{
	// LED LD1
	{CHEETAHCB_LED_LD1_PORT, CHEETAHCB_LED_LD1_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// LED LD2
	{CHEETAHCB_LED_LD2_PORT, CHEETAHCB_LED_LD2_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// LED LD3
	{CHEETAHCB_LED_LD3_PORT, CHEETAHCB_LED_LD3_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// LED LD4
	{CHEETAHCB_LED_LD4_PORT, CHEETAHCB_LED_LD4_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// LED LD5
	{CHEETAHCB_LED_LD5_PORT, CHEETAHCB_LED_LD5_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// LED LD6
	{CHEETAHCB_LED_LD6_PORT, CHEETAHCB_LED_LD6_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio}
};

PFCfgGpio pfCheetahCbPUSHBUTTONGpioCfg =	{CHEETAHCB_PUSHBUTTON_PB_PORT, CHEETAHCB_PUSHBUTTON_PB_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio};

PFCfgGpio pfCheetahCbUART0GpioCfg[2] = 
{
	//TX0
	{CHEETAHCB_UART_0_TX_PORT,CHEETAHCB_UART_0_TX_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_2_TXD0},
	//RX0
	{CHEETAHCB_UART_0_RX_PORT,CHEETAHCB_UART_0_RX_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_3_RXD0}
};

PFCfgGpio pfCheetahCbUART2GpioCfg[2] =
{
	//TX2
	{CHEETAHCB_J2_4_UART_2_TX_PORT,CHEETAHCB_J2_4_UART_2_TX_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_10_TXD2},
	//RX2
	{CHEETAHCB_J2_6_UART_2_RX_PORT,CHEETAHCB_J2_6_UART_2_RX_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_11_RXD2}
};

PFCfgGpio pfCheetahCbUART3GpioCfg[2] =
{
	//TX3
	{CHEETAHCB_J2_3_UART_3_TX_PORT,CHEETAHCB_J2_3_UART_3_TX_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_0_TXD3},
	//RX3
	{CHEETAHCB_J2_5_UART_3_RX_PORT,CHEETAHCB_J2_5_UART_3_RX_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_1_RXD3}
};


PFCfgGpio pfCheetahCbCAN1GpioCfg[2] =	
{
	//RD1
	{CHEETAHCB_EXP_13_CAN_1_RD_1_PORT,CHEETAHCB_EXP_13_CAN_1_RD_1_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_21_RD1},
	//TD1
	{CHEETAHCB_EXP_14_CAN_1_TD_1_PORT,CHEETAHCB_EXP_14_CAN_1_TD_1_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_22_TD1}
};

PFCfgGpio pfCheetahCbCAN2GpioCfg[2] =	
{
	//RD2
	{CHEETAHCB_T1_2_CAN_2_RD_2_PORT,CHEETAHCB_T1_2_CAN_2_RD_2_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_4_RD2},
	//TD1
	{CHEETAHCB_T1_1_CAN_2_TD_2_PORT,CHEETAHCB_T1_1_CAN_2_TD_2_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_5_TD2}
};

PFCfgGpio pfCheetahCbSSP0GpioCfg[4] =
{
	//MISO0
	{CHEETAHCB_J5_7_SSP_0_MISO_PORT,CHEETAHCB_J5_7_SSP_0_MISO_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_17_MISO0},
	//MOSI0
	{CHEETAHCB_J5_8_SSP_0_MOSI_PORT,CHEETAHCB_J5_8_SSP_0_MOSI_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_18_MOSI0},
	//SCK0
	{CHEETAHCB_J5_6_SSP_0_SCK_PORT,CHEETAHCB_J5_6_SSP_0_SCK_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_15_SCK0},
	//SSEL0
	{CHEETAHCB_J5_5_SSP_0_SSEL_PORT,CHEETAHCB_J5_5_SSP_0_SSEL_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_16_SSEL0}
};

PFCfgGpio pfCheetahCbSSP1GpioCfg[4] =
{
	//MISO1
	{CHEETAHCB_J7_7_SSP_1_MISO_PORT,CHEETAHCB_J7_7_SSP_1_MISO_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_8_MISO1},
	//MOSI1
	{CHEETAHCB_J7_8_SSP_1_MOSI_PORT,CHEETAHCB_J7_8_SSP_1_MOSI_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_9_MOSI1},
	//SCK1
	{CHEETAHCB_J7_6_SSP_1_SCK_PORT,CHEETAHCB_J7_6_SSP_1_SCK_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_7_SCK1},
	//SSEL1
	{CHEETAHCB_J7_5_SSP_1_SSEL_PORT,CHEETAHCB_J7_5_SSP_1_SSEL_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_6_SSEL1}
};


PFCfgGpio pfCheetahCbI2C0GpioCfg[2] = 
{	
	//SDA
	{CHEETAHCB_J4_15_I2C_0_SDA_PORT,CHEETAHCB_J4_15_I2C_0_SDA_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_27_SDA0},
	//SCL
	{CHEETAHCB_J4_16_I2C_0_SCL_PORT,CHEETAHCB_J4_16_I2C_0_SCL_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_28_SCL0}
};

PFCfgGpio pfCheetahCbI2C1GpioCfg[2] = 
{	
		//SDA
	{CHEETAHCB_J2_3_I2C_1_SDA_PORT,CHEETAHCB_J2_3_I2C_1_SDA_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_0_SDA1},
		//SCL
	{CHEETAHCB_J2_5_I2C_1_SCL_PORT,CHEETAHCB_J2_5_I2C_1_SCL_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_1_SCL1}
};

PFCfgGpio pfCheetahCbI2C2GpioCfg[2] =
{
		//SDA
	{CHEETAHCB_J2_4_I2C_2_SDA_PORT,CHEETAHCB_J2_4_I2C_2_SDA_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_10_SDA2},
		//SCL
	{CHEETAHCB_J2_6_I2C_2_SCL_PORT,CHEETAHCB_J2_6_I2C_2_SCL_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_11_SCL2}
};

#endif	// #ifdef MCU_CHIP_lpc1768
