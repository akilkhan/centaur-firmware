#include "prime_framework.h"
#ifdef MCU_CHIP_atmega2560
#include "prime_gpio.h"
#include "prime_andromedaBoardConfig.h"

PFCfgGpio pfAndromedaLEDGpioCfg[4] =	
{
	//	LED UL1
	{ANDROMEDA_LED_UL1_PORT, ANDROMEDA_LED_UL1_PIN, enGpioDirOutput, enGpioPinModePullUp},	
	
	//	LED UL2
	{ANDROMEDA_LED_UL2_PORT, ANDROMEDA_LED_UL2_PIN, enGpioDirOutput, enGpioPinModePullUp},	
		
	//	LED UL3
	{ANDROMEDA_LED_UL3_PORT, ANDROMEDA_LED_UL3_PIN, enGpioDirOutput, enGpioPinModePullUp},
	
	//	LED UL4
	{ANDROMEDA_LED_UL4_PORT, ANDROMEDA_LED_UL4_PIN, enGpioDirOutput, enGpioPinModePullUp}
};		
	
PFCfgGpio pfAndromedaPushButtonGpioCfg[4] =
{
	//	Push Button PB1
	{ANDROMEDA_PUSHBUTTON_PB1_PORT, ANDROMEDA_PUSHBUTTON_PB1_PIN, enGpioDirOutput, enGpioPinModePullUp},
	
	//	Push Button PB2
	{ANDROMEDA_PUSHBUTTON_PB2_PORT, ANDROMEDA_PUSHBUTTON_PB2_PIN, enGpioDirOutput, enGpioPinModePullUp},	

	//	Push Button PB3
	{ANDROMEDA_PUSHBUTTON_PB3_PORT, ANDROMEDA_PUSHBUTTON_PB3_PIN, enGpioDirOutput, enGpioPinModePullUp},
	
	// Push Button PB4
	{ANDROMEDA_PUSHBUTTON_PB3_PORT, ANDROMEDA_PUSHBUTTON_PB3_PIN, enGpioDirOutput, enGpioPinModePullUp}
};		

//	For Synchronous Master Mode of USART0
PFCfgGpio pfAndromedaUsart0MasterGpioCfg[3] =
{
	//	USART0 Rx
	{ANDROMEDA_J4_2_USART0_RXD0_PORT, ANDROMEDA_J4_2_USART0_RXD0_PIN, enGpioDirInput, enGpioPinModePullUp},
	
	//	USART0 Tx
	{ANDROMEDA_J4_3_USART0_TXD0_PORT, ANDROMEDA_J4_3_USART0_TXD0_PIN, enGpioDirOutput, enGpioPinModePullUp},
	
	//	USART0 XCK
	{ANDROMEDA_J4_4_USART0_XCK0_PORT, ANDROMEDA_J4_4_USART0_XCK0_PIN, enGpioDirOutput, enGpioPinModePullUp}
};

//	For Synchronous Slave Mode of USART0
PFCfgGpio pfAndromedaUsart0SlaveGpioCfg[3]  =
{
	//	USART0 Rx
	{ANDROMEDA_J4_2_USART0_RXD0_PORT, ANDROMEDA_J4_2_USART0_RXD0_PIN, enGpioDirInput, enGpioPinModePullUp},
	
	//	USART0 Tx
	{ANDROMEDA_J4_3_USART0_TXD0_PORT, ANDROMEDA_J4_3_USART0_TXD0_PIN, enGpioDirOutput, enGpioPinModePullUp},
	
	//	USART0 XCK
	{ANDROMEDA_J4_4_USART0_XCK0_PORT, ANDROMEDA_J4_4_USART0_XCK0_PIN, enGpioDirInput, enGpioPinModePullUp}
	
};

//	For Asynchronous Mode of USART0
PFCfgGpio pfAndromedaUart0GpioCfg[2]  =
{
	//	USART0 Rx
	{ANDROMEDA_J4_2_USART0_RXD0_PORT, ANDROMEDA_J4_2_USART0_RXD0_PIN, enGpioDirInput, enGpioPinModePullUp},
	
	//	USART0 Tx
	{ANDROMEDA_J4_3_USART0_TXD0_PORT, ANDROMEDA_J4_3_USART0_TXD0_PIN, enGpioDirOutput, enGpioPinModePullUp}
};	


//	For Asynchronous Mode of USART1
PFCfgGpio pfAndromedaUart1GpioCfg[2] =
{
	//	USART1 Rx
	{ANDROMEDA_EXP_17_USART1_RXD1_PORT, ANDROMEDA_EXP_17_USART1_RXD1_PIN, enGpioDirInput, enGpioPinModePullUp},
	
	//	USART1 Tx
	{ANDROMEDA_EXP_18_USART1_TXD1_PORT, ANDROMEDA_EXP_18_USART1_TXD1_PIN, enGpioDirOutput, enGpioPinModePullUp}
};	

//	For Asynchronous Mode of USART2
PFCfgGpio pfAndromedaUart2GpioCfg[2] =
{
	//	USART2 Rx
	{ANDROMEDA_J4_12_USART2_RXD2_PORT, ANDROMEDA_J4_12_USART2_RXD2_PIN, enGpioDirInput, enGpioPinModePullUp},
	
	//	USART2 Tx
	{ANDROMEDA_J4_13_USART2_TXD2_PORT, ANDROMEDA_J4_13_USART2_TXD2_PIN, enGpioDirOutput, enGpioPinModePullUp}
};	

PFCfgGpio pfAndromedaInt0GpioCfg = {ANDROMEDA_EXP_11_INT0_PORT, ANDROMEDA_EXP_11_INT0_PIN, enGpioDirInput, enGpioPinModePullUp};	
PFCfgGpio pfAndromedaInt1GpioCfg = {ANDROMEDA_EXP_12_INT1_PORT, ANDROMEDA_EXP_12_INT1_PIN, enGpioDirInput, enGpioPinModePullUp};
PFCfgGpio pfAndromedaInt2GpioCfg = {ANDROMEDA_EXP_17_INT2_PORT, ANDROMEDA_EXP_17_INT2_PIN, enGpioDirInput, enGpioPinModePullUp};
PFCfgGpio pfAndromedaInt3GpioCfg = {ANDROMEDA_EXP_18_INT3_PORT, ANDROMEDA_EXP_18_INT3_PIN, enGpioDirInput, enGpioPinModePullUp};	
PFCfgGpio pfAndromedaInt4GpioCfg = {ANDROMEDA_J4_6_INT4_PORT, ANDROMEDA_J4_6_INT4_PIN, enGpioDirInput, enGpioPinModePullUp};	
PFCfgGpio pfAndromedaInt5GpioCfg = {ANDROMEDA_J4_7_INT5_PORT, ANDROMEDA_J4_7_INT5_PIN, enGpioDirInput, enGpioPinModePullUp};	
PFCfgGpio pfAndromedaInt6GpioCfg = {ANDROMEDA_J4_8_INT6_PORT, ANDROMEDA_J4_8_INT6_PIN, enGpioDirInput, enGpioPinModePullUp};	
PFCfgGpio pfAndromedaInt7GpioCfg = {ANDROMEDA_EXP_10_INT7_PORT, ANDROMEDA_EXP_10_INT7_PIN, enGpioDirInput, enGpioPinModePullUp};	
	
//PFCfgGpio pfADC0GpioCfg = {J5_19_ADC0_PORT, J5_19_ADC0_PIN, enGpioDirInput, enGpioPinModePullUp};
PFCfgGpio pfAndromedaADC1GpioCfg = {ANDROMEDA_J5_18_ADC1_PORT, ANDROMEDA_J5_18_ADC1_PIN, enGpioDirInput, enGpioPinModePullUp};
PFCfgGpio pfAndromedaADC2GpioCfg = {ANDROMEDA_J5_17_ADC2_PORT, ANDROMEDA_J5_17_ADC2_PIN, enGpioDirInput, enGpioPinModePullUp};
PFCfgGpio pfAndromedaADC3GpioCfg = {ANDROMEDA_J5_16_ADC3_PORT, ANDROMEDA_J5_16_ADC3_PIN, enGpioDirInput, enGpioPinModePullUp};
PFCfgGpio pfAndromedaADC4GpioCfg = {ANDROMEDA_J5_15_ADC4_PORT, ANDROMEDA_J5_15_ADC4_PIN, enGpioDirInput, enGpioPinModePullUp};
PFCfgGpio pfAndromedaADC5GpioCfg = {ANDROMEDA_J5_14_ADC5_PORT, ANDROMEDA_J5_14_ADC5_PIN, enGpioDirInput, enGpioPinModePullUp};
PFCfgGpio pfAndromedaADC6GpioCfg = {ANDROMEDA_J5_13_ADC6_PORT, ANDROMEDA_J5_13_ADC6_PIN, enGpioDirInput, enGpioPinModePullUp};
PFCfgGpio pfAndromedaADC7GpioCfg = {ANDROMEDA_J5_12_ADC7_PORT, ANDROMEDA_J5_12_ADC7_PIN, enGpioDirInput, enGpioPinModePullUp};
	
PFCfgGpio pfAndromedaSPIMasterGpioCfg[4] = {
								{ANDROMEDA_EXP_9_SPI_MOSI_PORT, ANDROMEDA_EXP_9_SPI_MOSI_PIN, enGpioDirOutput, enGpioPinModePullUp},
								{ANDROMEDA_EXP_8_SPI_MISO_PORT, ANDROMEDA_EXP_8_SPI_MISO_PIN, enGpioDirInput, enGpioPinModePullUp},
								{ANDROMEDA_EXP_6_SPI_SCK_PORT, ANDROMEDA_EXP_6_SPI_SCK_PIN, enGpioDirOutput, enGpioPinModePullUp},
								{ANDROMEDA_SPI_SS_PORT, ANDROMEDA_SPI_SS_PIN, enGpioDirOutput, enGpioPinModePullUp}								
							} ;
PFCfgGpio pfAndromedaSPISlaveGpioCfg[4] = {
								{ANDROMEDA_EXP_9_SPI_MOSI_PORT, ANDROMEDA_EXP_9_SPI_MOSI_PIN, enGpioDirInput, enGpioPinModePullUp},
								{ANDROMEDA_EXP_8_SPI_MISO_PORT, ANDROMEDA_EXP_8_SPI_MISO_PIN, enGpioDirOutput, enGpioPinModePullUp},
								{ANDROMEDA_EXP_6_SPI_SCK_PORT, ANDROMEDA_EXP_6_SPI_SCK_PIN, enGpioDirInput, enGpioPinModePullUp},
								{ANDROMEDA_SPI_SS_PORT, ANDROMEDA_SPI_SS_PIN, enGpioDirInput, enGpioPinModePullUp}								
							} ;
	
	
PFCfgGpio pfAndromedaI2CGpioCfg[2] = {
								{ANDROMEDA_EXP_12_I2C_SDA_PORT, ANDROMEDA_EXP_12_I2C_SDA_PIN, enGpioDirInput, enGpioPinModePullUp},
								{ANDROMEDA_EXP_11_I2C_SCL_PORT, ANDROMEDA_EXP_11_I2C_SCL_PIN, enGpioDirOutput, enGpioPinModePullUp}
							};						

PFCfgGpio pfAndromedaAnlgCompGpioCfg[2] = {
								{ANDROMEDA_J4_4_ACOMP_AIN0_PORT, ANDROMEDA_J4_4_ACOMP_AIN0_PIN, enGpioDirInput, enGpioPinModePullUp},
								{ANDROMEDA_J4_5_ACOMP_AIN1_PORT, ANDROMEDA_J4_5_ACOMP_AIN1_PIN, enGpioDirInput, enGpioPinModePullUp}
							};
							
#endif	// #ifdef MCU_CHIP_atmega2560
