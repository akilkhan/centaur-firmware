#pragma once

#include "primeConfigMacros.h"
#include "prime_at90can128_regmap.h"

#define PF_LITTLE_ENDIAN

// ADC resolution in bits
#define PF_ADC_BIT_RESOLUTION				10

// ADC maximum conversion value
#define PF_ADC_MAX_VALUE					(1 << PF_ADC_BIT_RESOLUTION)

// Scaling factor for ADC value to voltage conversion to avoid truncation
#define PF_ADC_SCALING_FACTOR				1000

// Conversion factor for ADC value to voltage conversion
#define PF_ADC_CONVERSION_FACTOR(REF_VTG)	((REF_VTG * PF_ADC_SCALING_FACTOR) / PF_ADC_MAX_VALUE)

// Convert ADc value in millivolts
#define PF_ADC_CALC_VTG(ADC_VAL, REF_VTG)	((ADC_VAL * PF_ADC_CONVERSION_FACTOR(REF_VTG)) / PF_ADC_SCALING_FACTOR)

#define PF_EXT_INT_MAX_CALLBACK   			8