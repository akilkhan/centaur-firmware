#include "prime_framework.h"

#if (PF_USE_EINT2 == 1)

#include "prime_utils.h"
#include "prime_sysClk.h"
#if (EINT2_CH == GPIO_INT_CH)
#include "prime_gpio.h"
#endif  // #if (EINT2_CH == GPIO_INT_CH)
#include "prime_eint2.h"

static PFEnBoolean eint2Init = enBooleanFalse;
static PFcallback eint2CallbackList[EINT2_MAX_CALLBACK] = {0};
static PFEnBoolean eint2Enable = enBooleanFalse;
static PFCfgEint2 eint2Cfg;

#if (EINT2_CH == GPIO_INT_CH)
static PFEnBoolean gpioIntInit = enBooleanFalse;
static PFCfgGpioInt gpioIntList[GPIO_MAX_INTERRUPTS];
static PFEnBoolean gpioIntBusy[GPIO_MAX_INTERRUPTS] = {enBooleanFalse};
static PFEnBoolean gpioIntEnable = enBooleanFalse;
#endif  // #if (EINT2_CH == GPIO_INT_CH)


PFEnStatus pfEint2Open(PFpCfgEint2 config)
{
	PFdword loop;
#if (PF_EINT2_DEBUG == 1)
	CHECK_NULL_PTR(config);
	if(config->mode > 0x04)
	{
		return enStatusInvArgs;
	}
	if(config->callbackCount > EINT2_MAX_CALLBACK)
	{
		return enStatusInvArgs;
	}
#endif	// #if (PF_EINT2_DEBUG == 1)

	// disable interrupt before configuring
	NVIC_DisableIRQ(IRQ_NUM(EINT2_CHANNEL));
	
	// set interrupt mode and polarity
	PERIPH_SC->EXTMODE |= (config->mode >> 1) << EINT2_CH;
	PERIPH_SC->EXTPOLAR |= (config->mode & 0x01) << EINT2_CH;
	
	// set callbacks
	if(config->callbackList != 0)
	{
		for(loop = 0; loop < config->callbackCount; loop++)
		{
			eint2CallbackList[loop] = config->callbackList[loop];
		}
	}
	
	pfMemCopy(&eint2Cfg, config, sizeof(PFCfgEint2));
	eint2Init = enBooleanTrue;
	
	return enStatusSuccess;
}

PFEnStatus pfEint2Enable(void)
{
#if (PF_EINT2_DEBUG == 1)
	CHECK_DEV_INIT(eint2Init);
#endif	// #if (PF_EINT2_DEBUG == 1)

	NVIC_EnableIRQ(IRQ_NUM(EINT2_CHANNEL));
	eint2Enable = enBooleanTrue;
	return enStatusSuccess;	
}

PFEnStatus pfEint2Disable(void)
{
#if (PF_EINT2_DEBUG == 1)
	CHECK_DEV_INIT(eint2Init);
#endif	// #if (PF_EINT2_DEBUG == 1)
	
	NVIC_DisableIRQ(IRQ_NUM(EINT2_CHANNEL));
	eint2Enable = enBooleanFalse;
	return enStatusSuccess;	
}

PFEnStatus pfEint2AddCallback(PFcallback callback)
{
	PFEnStatus status = enStatusError;
	PFbyte index;

#if (PF_EINT2_DEBUG == 1)
	CHECK_DEV_INIT(eint2Init);
	CHECK_NULL_PTR(callback);
#endif	// #if (PF_EINT2_DEBUG == 1)
	
	if(eint2Enable == enBooleanTrue)
	{
		NVIC_DisableIRQ(IRQ_NUM(EINT2_CHANNEL));
	}
	
	for(index = 0; index < EINT2_MAX_CALLBACK; index++)
	{
		if(eint2CallbackList[index] == 0)
		{
			eint2CallbackList[index] = callback;
			status = enStatusSuccess;
			break;
		}
	}
	
	if(eint2Enable == enBooleanTrue)
	{
		NVIC_EnableIRQ(IRQ_NUM(EINT2_CHANNEL));
	}
	return status;
}

PFEnStatus pfEint2RemoveCallback(PFcallback callback)
{
	PFEnStatus status = enStatusError;
	PFbyte index;
#if (PF_EINT2_DEBUG == 1)
	CHECK_DEV_INIT(eint2Init);
	CHECK_NULL_PTR(callback);
#endif	// #if (PF_EINT2_DEBUG == 1)
	
	if(eint2Enable == enBooleanTrue)
    {
		NVIC_DisableIRQ(IRQ_NUM(EINT2_CHANNEL));
	}
	for(index = 0; index < EINT2_MAX_CALLBACK; index++)
	{
		if(eint2CallbackList[index] == callback)
		{
			eint2CallbackList[index] = 0;
			status = enStatusSuccess;
			break;
		}
	}
	
	if(eint2Enable == enBooleanTrue)
    {
		NVIC_EnableIRQ(IRQ_NUM(EINT2_CHANNEL));
	}
	return status;
}

PFEnStatus pfEint2Close(void)
{
	NVIC_DisableIRQ(IRQ_NUM(EINT2_CHANNEL));
	eint2Enable = enBooleanFalse;
	eint2Init = enBooleanFalse;
	return enStatusSuccess;
}

#if (EINT2_CH == GPIO_INT_CH)
PFEnStatus pfGpioIntOpen(PFbyte* id, PFpCfgGpioInt config)
{
	PFbyte index;
#if (PF_EINT2_DEBUG == 1)    
    CHECK_NULL_PTR(config);
    CHECK_NULL_PTR(id);
    if( (config->gpioPortNo != GPIO_PORT_0) && (config->gpioPortNo != GPIO_PORT_2) )
    {
        return enStatusInvArgs;
    }
#endif  // #if (PF_EINT2_DEBUG == 1)    
	for (index = 0; index < GPIO_MAX_INTERRUPTS; index++)
	{
		if(gpioIntBusy[index] == enBooleanFalse)
		{
			*id = index;
			gpioIntBusy[index] = enBooleanTrue;
			break;
		}
	}
	if(index == GPIO_MAX_INTERRUPTS)
	{
		return enStatusError;
	}

	pfMemCopy(&gpioIntList[index], config, sizeof(PFCfgGpioInt));
	
	switch(config->gpioPortNo)
	{
		case GPIO_PORT_0:
			if(config->mode & enGpioIntModeFallingEdge)
			{
				PERIPH_GPIOINT->IO0IntEnF |= config->gpioIntPin;
			}
			if(config->mode & enGpioIntModeRisingEdge)
			{
				PERIPH_GPIOINT->IO0IntEnR |= config->gpioIntPin;
			}
			break;
                
		case GPIO_PORT_2:
			if(config->mode & enGpioIntModeFallingEdge)
			{
				PERIPH_GPIOINT->IO2IntEnF |= config->gpioIntPin;
			}
			if(config->mode & enGpioIntModeRisingEdge)
			{
				PERIPH_GPIOINT->IO2IntEnR |= config->gpioIntPin;
			}
			break;    
	}

	gpioIntEnable = enBooleanTrue;
	if(eint2Enable != enBooleanTrue)
	{
		pfEint2Enable();
	}
	
	gpioIntInit = enBooleanTrue;
	return enStatusSuccess;
}

PFEnStatus pfGpioIntEnable(void)
{
#if (PF_EINT2_DEBUG == 1)
	CHECK_DEV_INIT(gpioIntInit);
#endif	// #if (PF_EINT2_DEBUG == 1)
	if(eint2Enable != enBooleanTrue)
        {
            pfEint2Enable();
        }
	gpioIntEnable = enBooleanTrue;
	return enStatusSuccess;	
}

PFEnStatus pfGpioIntDisable(void)
{
#if (PF_EINT2_DEBUG == 1)
	CHECK_DEV_INIT(gpioIntInit);
#endif	// #if (PF_EINT2_DEBUG == 1)
	if(eint2Enable != enBooleanTrue)
        {
            pfEint2Disable();
        }
        gpioIntEnable = enBooleanFalse;
	
	return enStatusSuccess;	
}

PFEnStatus pfGpioIntClose(PFbyte gpioIntID)
{
#if (PF_EINT2_DEBUG == 1)
	if(gpioIntID >= GPIO_MAX_INTERRUPTS)
        {
		return enStatusInvArgs;
        }
	if(gpioIntBusy[gpioIntID] == enBooleanFalse)
        {
		return enStatusInvArgs;
        }
#endif	// #if (PF_EINT2_DEBUG == 1)
	
        switch(gpioIntList[gpioIntID].gpioPortNo)
        {
            case GPIO_PORT_0:
                if(gpioIntList[gpioIntID].mode & enGpioIntModeFallingEdge)
                {
                    PERIPH_GPIOINT->IO0IntEnF &= ~gpioIntList[gpioIntID].gpioIntPin;
                }
                if(gpioIntList[gpioIntID].mode & enGpioIntModeRisingEdge)
                {
                    PERIPH_GPIOINT->IO0IntEnR &= ~gpioIntList[gpioIntID].gpioIntPin;
                }
                break;
                
            case GPIO_PORT_2:
                if(gpioIntList[gpioIntID].mode & enGpioIntModeFallingEdge)
                {
                    PERIPH_GPIOINT->IO2IntEnF &= ~gpioIntList[gpioIntID].gpioIntPin;
                }
                if(gpioIntList[gpioIntID].mode & enGpioIntModeRisingEdge)
                {
                    PERIPH_GPIOINT->IO2IntEnR &= ~gpioIntList[gpioIntID].gpioIntPin;
                }
                break;    
        }
        
	gpioIntBusy[gpioIntID] = enBooleanFalse;
	pfMemSet(&gpioIntList[gpioIntID],PF_NULL,sizeof(PFCfgGpioInt));
	
	return enStatusSuccess;
}

#endif  // #if (EINT2_CH == GPIO_INT_CH)

void EINT2_HANDLER(void)
{
	PFbyte index;
    #if (EINT2_CH == GPIO_INT_CH)
        if(gpioIntEnable == enBooleanTrue)
        {
            //Port0
            if(PERIPH_GPIOINT->IntStatus & BIT_MASK_0)
            {
		for(index = 0; index < GPIO_MAX_INTERRUPTS; index++)
		{
                    if( (gpioIntList[index].gpioPortNo == GPIO_PORT_0) && (gpioIntBusy[index] == enBooleanTrue) )
                    {
			if( (PERIPH_GPIOINT->IO0IntStatF & gpioIntList[index].gpioIntPin) || (PERIPH_GPIOINT->IO0IntStatR & gpioIntList[index].gpioIntPin) ) 
			{
				PERIPH_GPIOINT->IO0IntClr |= gpioIntList[index].gpioIntPin;
				if(gpioIntList[index].callback != 0)
                                {
					gpioIntList[index].callback();
                                }
			}
                    }    
		}
            }
            //Port2
            if(PERIPH_GPIOINT->IntStatus & BIT_MASK_2)
            {
		for(index = 0; index < GPIO_MAX_INTERRUPTS; index++)
		{
                    if( (gpioIntList[index].gpioPortNo == GPIO_PORT_2) && (gpioIntBusy[index] == enBooleanTrue) )
                    {
			if( (PERIPH_GPIOINT->IO2IntStatF & gpioIntList[index].gpioIntPin) || (PERIPH_GPIOINT->IO2IntStatR & gpioIntList[index].gpioIntPin) ) 
			{
				PERIPH_GPIOINT->IO2IntClr |= gpioIntList[index].gpioIntPin;
				if(gpioIntList[index].callback != 0)
                                {
					gpioIntList[index].callback();
                                }
			}
                    }    
		}
            }
        }
    #endif //GPIO Int
	// clear interrupt flag
	PERIPH_SC->EXTINT = (1 << EINT2_CH);
    if(eint2Enable == enBooleanTrue)
    {
        for(index = 0; index < EINT2_MAX_CALLBACK; index++)
        {
			if(eint2CallbackList[index] != 0)
			{
				eint2CallbackList[index]();
			}
        }
    }
}

#endif	// #if (PF_USE_EINT2 == 1)
