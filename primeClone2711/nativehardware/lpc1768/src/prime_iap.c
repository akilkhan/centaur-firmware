#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_sysclk.h"
#include "prime_iap.h"


//  IAP Command
typedef void (*pfIapOperation)(PFdword *cmd,PFdword *result);
pfIapOperation pfIapCall = 0;
PFdword cpuCoreClock;
//#define cpuCoreClock  (100000)    // CCLK in kHz

/** @addtogroup IAP_Public_Functions IAP Public Function
 * @ingroup IAP
 * @{
 */
 
void pfIapOpen(void) 
{
	pfIapCall = ((pfIapOperation) PF_IAP_ADDR);
	cpuCoreClock = pfSysGetCpuClock();
	cpuCoreClock /= 1000;
}

void pfIapClose(void)
{
	pfIapCall = 0;
	cpuCoreClock = 0;
}
 

/*********************************************************************//**
 * @brief        Get Sector Number
 *
 * @param[in]    adr	 Sector Address
 *
 * @return       Sector Number.
 *
 **********************************************************************/
 PFdword pfIapGetSecNum (PFdword adr)
{
    PFdword n;

    n = adr >> 12;                               //  4kB Sector
    if (n >= 0x10) {
      n = 0x0E + (n >> 3);                       // 32kB Sector
    } 

    return (n);                                  // Sector Number
}

/*********************************************************************//**
 * @brief        Prepare sector(s) for write operation
 *
 * @param[in]    start_sec	          The number of start sector
 * @param[in]    end_sec	          The number of end sector
 *
 * @return       enIapStatusCmdSuccess/BUSY/INVALID_SECTOR.
 *
 **********************************************************************/
PFEnIapStatus pfIapPrepareSector(PFdword start_sec, PFdword end_sec)
{
    PFIapCommand command;
    command.cmd    = enIapCmdPrepare;                    // Prepare Sector for Write
    command.param[0] = start_sec;                    // Start Sector
    command.param[1] = end_sec;                      // End Sector
    pfIapCall (&command.cmd, &command.status);        // Call IAP Command
    return (PFEnIapStatus)command.status;
}

/*********************************************************************//**
 * @brief     Copy RAM to Flash
 *
 * @param[in] dest	          destination buffer (in Flash memory).
 * @param[in] source	   source buffer (in RAM).
 * @param[in] size	          the write size.
 *
 * @return    enIapStatusCmdSuccess.
 *            SRC_ADDR_ERROR/DST_ADDR_ERROR
 *            SRC_ADDR_NOT_MAPPED/DST_ADDR_NOT_MAPPED
 *            COUNT_ERROR/SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION
 *            BUSY
 *
 **********************************************************************/
PFEnIapStatus pfIapCopyRamToFlash(PFbyte * dest, PFbyte* source, PFEnIapWriteSize size)
{
    PFdword sec;
    PFEnIapStatus status;
    PFIapCommand command;

    // Prepare sectors
    sec = pfIapGetSecNum((PFdword)dest);
    status = pfIapPrepareSector(sec, sec);
    if(status != enIapStatusCmdSuccess)
        return status;
   
    // write
    command.cmd    = enIapCmdCopyRamToFlash;             // Copy RAM to Flash
    command.param[0] = (PFdword)dest;                 // Destination Flash Address
    command.param[1] = (PFdword)source;               // Source RAM Address
    command.param[2] =  size;                          // Number of bytes
    command.param[3] =  cpuCoreClock;         // CCLK in kHz
    pfIapCall (&command.cmd, &command.status);              // Call IAP Command
	  
    return (PFEnIapStatus)command.status;             // Finished without Errors	  
}

/*********************************************************************//**
 * @brief        Erase sector(s)
 *
 * @param[in]    start_sec	   The number of start sector
 * @param[in]    end_sec	   The number of end sector
 *
 * @return       enIapStatusCmdSuccess.
 *               INVALID_SECTOR
 *               SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION
 *               BUSY
 *
 **********************************************************************/
PFEnIapStatus pfIapEraseSector(PFdword start_sec, PFdword end_sec)
{
    PFIapCommand command;
    PFEnIapStatus status;

    // Prepare sectors
    status = pfIapPrepareSector(start_sec, end_sec);
    if(status != enIapStatusCmdSuccess)
        return status;

    // Erase sectors
    command.cmd    = enIapCmdErase;                    // Prepare Sector for Write
    command.param[0] = start_sec;                  // Start Sector
    command.param[1] = end_sec;                    // End Sector
    command.param[2] =  cpuCoreClock ;                // CCLK in kHz
    pfIapCall (&command.cmd, &command.status);      // Call IAP Command
    return (PFEnIapStatus)command.status;  
}

/*********************************************************************//**
 * @brief        Blank check sector(s)
 *
 * @param[in]    start_sec	   The number of start sector
 * @param[in]    end_sec	   The number of end sector
 * @param[out]   first_nblank_loc  The offset of the first non-blank word
  * @param[out]  first_nblank_val  The value of the first non-blank word
 *
 * @return       enIapStatusCmdSuccess.
 *               INVALID_SECTOR
 *               enIapStatusSectorNotBlank
 *               BUSY
 *
 **********************************************************************/
PFEnIapStatus pfIapBlankCheckSector(PFdword start_sec, PFdword end_sec,
                                 PFdword *first_nblank_loc, 
				PFdword *first_nblank_val)
{
    PFIapCommand command;
	
    command.cmd    = enIapCmdBlankCheck;                // Prepare Sector for Write
    command.param[0] = start_sec;                    // Start Sector
    command.param[1] = end_sec;                      // End Sector
    pfIapCall (&command.cmd, &command.status);        // Call IAP Command

    if(command.status == enIapStatusSectorNotBlank)
    {
      // Update out value
      if(first_nblank_loc != NULL)
          *first_nblank_loc =  command.result[0];
      if(first_nblank_val != NULL)
          *first_nblank_val =  command.result[1];
    }

    return (PFEnIapStatus)command.status;
}

/*********************************************************************//**
 * @brief        Read part identification number
 *
 * @param[out]   partID  Part ID
 *
 * @return       enIapStatusCmdSuccess
 *
 **********************************************************************/
PFEnIapStatus pfIapReadPartId(PFdword *partID)
{
   PFIapCommand command;
   command.cmd = enIapCmdReadPartId;
   pfIapCall (&command.cmd, &command.status);        // Call IAP Command

   if(command.status == enIapStatusCmdSuccess)
   {
      if(partID != NULL)
         *partID = command.result[0];
   }

   return (PFEnIapStatus)command.status;
}

/*********************************************************************//**
 * @brief       Read boot code version. The version is interpreted as <major>.<minor>.
 *
 * @param[out]  major  The major
 * @param[out]  minor  The minor
 *
 * @return      enIapStatusCmdSuccess
 *
 **********************************************************************/
PFEnIapStatus pfIapReadBootCodeVer(PFbyte *major, PFbyte* minor)
{
   PFIapCommand command;
   command.cmd = enIapCmdReadBootVersion;
   pfIapCall (&command.cmd, &command.status);        // Call IAP Command

   if(command.status == enIapStatusCmdSuccess)
   {
      if(major != NULL)
         *major = (command.result[0] >> 8) & 0xFF;
      if(minor != NULL)
         *minor = (command.result[0]) & 0xFF;
   }

   return (PFEnIapStatus)command.status;
}

/*********************************************************************//**
 * @brief        Read Device serial number.
 *
 * @param[out]   uid   Serial number.
 *
 * @return       enIapStatusCmdSuccess
 *
 **********************************************************************/
PFEnIapStatus pfIapReadDeviceSerialNum(PFdword *uid)
{
   PFIapCommand command;
   command.cmd = enIapCmdReadSerialNum;
   pfIapCall (&command.cmd, &command.status);        // Call IAP Command

   if(command.status == enIapStatusCmdSuccess)
   {
      if(uid != NULL)
      {
        PFdword i = 0;
        for(i = 0; i < 4; i++)
           uid[i] =  command.result[i];
      }
   }

   return (PFEnIapStatus)command.status;
}

/*********************************************************************//**
 * @brief        compare the memory contents at two locations.
 *
 * @param[in]    addr1   The address of the 1st buffer (in RAM/Flash).
 * @param[in]    addr2   The address of the 2nd buffer (in RAM/Flash).
 * @param[in]    size      Number of bytes to be compared; should be a multiple of 4.
 *
 * @return 	     enIapStatusCmdSuccess
 *               COMPARE_ERROR
 *               COUNT_ERROR (Byte count is not a multiple of 4)
 *               ADDR_ERROR
 *               ADDR_NOT_MAPPED
 *
 **********************************************************************/
PFEnIapStatus pfIapCompare(PFbyte *addr1, PFbyte *addr2, PFdword size)
{
   PFIapCommand command;
   command.cmd = enIapCmdCompare;
   command.param[0] = (PFdword)addr1;
   command.param[1] = (PFdword)addr2;
   command.param[2] = size;
   pfIapCall (&command.cmd, &command.status);        // Call IAP Command

   return (PFEnIapStatus)command.status;
}

/*********************************************************************//**
 * @brief        Re-invoke ISP.
 *
 * @param[in]    None.
 *
 * @return       None.
 *
 **********************************************************************/
void pfIapInvokeIsp(void)
{
   PFIapCommand command;
   command.cmd = enIapCmdReinvokeIsp;
   pfIapCall (&command.cmd, &command.status);        // Call IAP Command
}


/**
 * @}
 */
 

