#include "prime_framework.h"

#if (PF_USE_SPI0 == 1)

#include "prime_utils.h"
#include "prime_sysClk.h"
#include "prime_gpio.h"
#include "prime_spi0.h"


#if(SPI0_USE_FIFO != 0)
#include "prime_fifo.h"
	#warning SPI0 FIFO is enabled for interrupt based communication
	#if( (SPI0_BUFFER_SIZE == 0) || ((SPI0_BUFFER_SIZE & (SPI0_BUFFER_SIZE - 1)) != 0) )
		#error SPI0_BUFFER_SIZE cannot be zero. SPI0_BUFFER_SIZE should be power of 2
	#endif
#endif

static PFEnBoolean spi0ChInit = enBooleanFalse;		// initialize flag for the channel
static PFEnBoolean spi0ChBusy = enBooleanFalse;		// busy flag for the channel
static PFEnBoolean spi0ChMaster;						// master/slave mode
static PFbyte spi0ChInt = 0;						// interrupt flag for the channel
#if(SPI0_USE_FIFO != 0)
#define SPI0_STATE_IDLE		0
#define SPI0_STATE_READ		1
#define SPI0_STATE_WRITE		2
static PFbyte spi0ChState = SPI0_STATE_IDLE;					// read write flag	
static PFbyte spi0RxBuffer[SPI0_BUFFER_SIZE];		// SPI0 transmit buffer
static PFbyte spi0TxBuffer[SPI0_BUFFER_SIZE];		// SPI0 receive buffer
static PFFifo spi0TxFifo;							// SPI0 transmit fifo structure
static PFFifo spi0RxFifo;							// SPI0 receive fifo structure
#else	
static PFcallback spi0Callback = 0;					// callback for the channel interrupt
#endif	// #if(SPI0_USE_FIFO != 0)
static PFEnBoolean spi0RegDevice[SPI0_MAX_DEVICE_SUPPORTED] = {enBooleanFalse};	
static PFGpioPortPin spi0ChipSelect[SPI0_MAX_DEVICE_SUPPORTED] = {{0, 0}};
static PFEnBoolean spi0ChipSelectAssert[SPI0_MAX_DEVICE_SUPPORTED] = {enBooleanFalse};	
static PFbyte spi0RegDeviceCount = 0;
static PFCfgSpi0 spi0Cfg;
	
PFEnStatus pfSpi0Open(PFpCfgSpi0 config)
{
	PFdword baudrateVal = 0;
#if (PF_SPI0_DEBUG == 1)
        CHECK_NULL_PTR(config);
	if(config->clkDiv > 3)
        {
		return enStatusInvArgs;
        }
	if(config->mode > 3)
        {
		return enStatusInvArgs;
        }
#if(SPI0_USE_FIFO == 0)
	if( (config->interrupt != enSpi0IntNone) && (config->callback == PF_NULL_PTR) )
        {
		return enStatusInvArgs;
        }
#endif	// #if(SPI0_USE_FIFO == 0)
#endif	// #if (PF_SPI0_DEBUG == 1)
	// power on 
	POWER_ON(SPI0_CH);
	// set PCLK divider
	pfSysSetPclkDiv(PCLK_DIV(SPI0_CH), config->clkDiv);
		
	// set datasize
	SPI0_CHANNEL->CR0 &= 0xFFFFFFF0;
	SPI0_CHANNEL->CR0 |= config->datasize;
	
	// set mode
	SPI0_CHANNEL->CR0 &= ~(0x03 << 6);
	SPI0_CHANNEL->CR0 |= config->mode << 6;
	// set master/slave
	if(config->masterMode == enBooleanTrue)
	{
		SPI0_CHANNEL->CR1 &= INV_BIT_MASK_2;
	}
	else
	{
		SPI0_CHANNEL->CR1 |= BIT_MASK_2;
	}
	// set baudrate
	baudrateVal = pfSysGetPclk(PCLK_DIV(SPI0_CH)) / config->baudrate;
	SPI0_CHANNEL->CPSR = baudrateVal;
	
#if(SPI0_USE_FIFO != 0)
	// initialize fifo 
	pfFifoInit(&spi0TxFifo, spi0TxBuffer, SPI0_BUFFER_SIZE);
	pfFifoInit(&spi0RxFifo, spi0RxBuffer, SPI0_BUFFER_SIZE);
#else	
	// set callback
	spi0Callback = config->callback;
#endif	// #if(SPI0_USE_FIFO != 0)
	// set interrupts
	if(config->interrupt != enSpi0IntNone)
	{
		spi0ChInt = config->interrupt;
		SPI0_CHANNEL->IMSC = config->interrupt;
		NVIC_EnableIRQ((IRQn_Type)IRQ_NUM(SPI0_CH));
	}	
	
	// enable SPI0 channel
	SPI0_CHANNEL->CR1 |= BIT_MASK_1;
	
	spi0ChMaster = config->masterMode;
        pfMemCopy(&spi0Cfg, config, sizeof(PFCfgSpi0));
	spi0ChInit = enBooleanTrue;
	return enStatusSuccess;
}

void pfSpi0Close(void)
{
	POWER_OFF(SPI0_CH);
	spi0ChInit = enBooleanFalse;
}

PFEnStatus pfSpi0RegisterDevice(PFbyte* id, PFpGpioPortPin chipSelect)
{
	PFbyte index;
#if (PF_SPI0_DEBUG == 1)
        CHECK_DEV_INIT(spi0ChInit);
        CHECK_NULL_PTR(id);
#endif	// #if (PF_SPI0_DEBUG == 1)
	if(spi0RegDeviceCount >= SPI0_MAX_DEVICE_SUPPORTED)
	{
		return enStatusNoMem;
	}
	for(index = 0; index < SPI0_MAX_DEVICE_SUPPORTED; index++)
	{
		if(spi0RegDevice[index] == enBooleanFalse)
		{
			spi0RegDevice[index] = enBooleanTrue;
			spi0RegDeviceCount++;
			spi0ChipSelect[index].port = chipSelect->port;
			spi0ChipSelect[index].pin = chipSelect->pin;
			*id = index;
			pfGpioPinsSet(chipSelect->port, chipSelect->pin);
			return enStatusSuccess;
		}
	}
	return enStatusError;
}

PFEnStatus pfSpi0UnregisterDevice(PFbyte* id)
{
#if (PF_SPI0_DEBUG == 1)
	CHECK_DEV_INIT(spi0ChInit);
	CHECK_NULL_PTR(id);
	if(*id >= SPI0_MAX_DEVICE_SUPPORTED)
	{
		return enStatusInvArgs;
	}
#endif	// #if (PF_SPI0_DEBUG == 1)
	
	spi0RegDevice[*id] = enBooleanFalse;
	spi0RegDeviceCount--;
	return enStatusSuccess;
}

PFEnStatus pfSpi0ChipSelect(PFbyte* id, PFbyte pinStatus)
{
#if (PF_SPI0_DEBUG == 1)
	CHECK_DEV_INIT(spi0ChInit);
	CHECK_NULL_PTR(id);
	if(*id >= SPI0_MAX_DEVICE_SUPPORTED)
	{
		return enStatusInvArgs;
	}
	if(spi0RegDevice[*id] != enBooleanTrue)
	{
		return enStatusError;
	}
#endif	// #if (PF_SPI0_DEBUG == 1)	
	
	if(pinStatus == 0)
	{
		if(spi0ChBusy == enBooleanTrue)
		{
			return enStatusBusy;
		}
		pfGpioPinsClear(spi0ChipSelect[*id].port, spi0ChipSelect[*id].pin);
		spi0ChipSelectAssert[*id] = enBooleanTrue;	
		spi0ChBusy = enBooleanTrue;
	}
	else
	{
		pfGpioPinsSet(spi0ChipSelect[*id].port, spi0ChipSelect[*id].pin);
		spi0ChipSelectAssert[*id] = enBooleanFalse;	
		spi0ChBusy = enBooleanFalse;
	}
	
	return enStatusSuccess;
}

PFEnStatus pfSpi0ExchangeByte(PFbyte *id, PFword data, PFword* rxData)
{
#if (PF_SPI0_DEBUG == 1)
	CHECK_DEV_INIT(spi0ChInit);
	CHECK_NULL_PTR(id);
	if(*id >= SPI0_MAX_DEVICE_SUPPORTED)
	{
		return enStatusInvArgs;
	}
#endif	// #if (PF_SPI0_DEBUG == 1)
// 	if(spi0ChipSelectAssert[*id] != enBooleanTrue)
// 	{
// 		return enStatusError;
// 	}
	
	// wait if transmit FIFO is full
	while( (SPI0_CHANNEL->SR & (1 << 1)) != (1 << 1) );
	SPI0_CHANNEL->DR = data;
	// wait till data is transmitted
	while( (SPI0_CHANNEL->SR & (1 << 4)) == (1 << 4) );
	*rxData = SPI0_CHANNEL->DR;
	return enStatusSuccess;
}

PFEnStatus pfSpi0Write(PFbyte* id, PFbyte* data, PFdword size)
{
	PFdword index;
	PFword dummyRead;
#if (PF_SPI0_DEBUG == 1)
	CHECK_DEV_INIT(spi0ChInit);
	CHECK_NULL_PTR(id);
	CHECK_NULL_PTR(data);
	if(*id >= SPI0_MAX_DEVICE_SUPPORTED)
	{
		return enStatusInvArgs;
	}
#endif	// #if (PF_SPI0_DEBUG == 1)
	if(spi0ChipSelectAssert[*id] != enBooleanTrue)
	{
		return enStatusError;
	}
	
	// if fifo is enabled, push data into the fifo
	if(spi0ChInt != enSpi0IntNone)
	{
	#if(SPI0_USE_FIFO != 0)
		// set state
		spi0ChState = SPI0_STATE_WRITE;
		
		for(index = 0; index < size; index++)
		{
			pfFifoPush(&spi0TxFifo, *(data + index));
		}
		while( (SPI0_CHANNEL->SR & (1 << 2)) != 0 )
		{
			dummyRead = SPI0_CHANNEL->DR;
		}
		SPI0_CHANNEL->DR = pfFifoPop(&spi0TxFifo);
	#else
		SPI0_CHANNEL->DR = *data;
	#endif	// #if(SPI0_USE_FIFO != 0)	
	}
	else
	{
		// for polling based transmission, write each byte one by one to data register
		for(index = 0; index < size; index++)
		{
			SPI0_CHANNEL->DR = *(data + index);
			while( (SPI0_CHANNEL->SR & (1 << 4)) == (1 << 4) );
			dummyRead = SPI0_CHANNEL->DR;
		}
	}
	
	return enStatusSuccess;
}

PFEnStatus pfSpi0Read(PFbyte* id, PFbyte* data, PFdword size, PFdword* readBytes)
{
	PFdword index;
#if (PF_SPI0_DEBUG == 1)
	CHECK_DEV_INIT(spi0ChInit);
	CHECK_NULL_PTR(id);
	CHECK_NULL_PTR(data);
	CHECK_NULL_PTR(readBytes);
	if(*id >= SPI0_MAX_DEVICE_SUPPORTED)
	{
		return enStatusInvArgs;
	}
	if(spi0RegDevice[*id] != enBooleanTrue)
	{
		return enStatusError;
	}
#endif	// #if (PF_SPI0_DEBUG == 1)
	// reset read byte count
	*readBytes = 0;
	
	if(spi0ChInt != enSpi0IntNone)
	{
	// if fifo is to be used, push data into the fifo	
	#if(SPI0_USE_FIFO != 0)
		// set state
		spi0ChState = SPI0_STATE_WRITE;
		
		if(spi0ChMaster == enBooleanTrue)
		{
			// for master mode, push dummy data in buffer
			for(index = 0; index < size; index++)
			{
				pfFifoPush(&spi0TxFifo, 0);
			}
			SPI0_CHANNEL->DR = pfFifoPop(&spi0TxFifo);
			// wait for all bytes to transmit
			while(pfFifoIsEmpty(&spi0TxFifo) != enBooleanTrue);
		}
		// read receive fifo
		for(index = 0; index < size; index++)
		{
			if(pfFifoIsEmpty(&spi0RxFifo) == enBooleanTrue)
				break;

			*(data + index) = pfFifoPop(&spi0RxFifo);
			*readBytes += 1;
		}
		spi0ChState = SPI0_STATE_IDLE;
	#else
		if(spi0ChMaster == enBooleanTrue)
		{
			SPI0_CHANNEL->DR = *data;
		}
		else
		{
			return enStatusUnknown;
		}
	#endif	// #if(SPI0_USE_FIFO != 0)	
	}
	else
	{
		// for polling based transmission, write each byte one by one to data register
		for(index = 0; index < size; index++)
		{
			SPI0_CHANNEL->DR = 0;
			while( (SPI0_CHANNEL->SR & (1 << 4)) == (1 << 4) );
			*(data + index) = SPI0_CHANNEL->DR;
			*readBytes += 1;
		}
	}
	
	return enStatusSuccess;
}

PFdword pfSpi0GetIntStatus(void)
{
	return SPI0_CHANNEL->MIS;
}

PFEnStatus pfSpi0ClearIntStatus(PFdword intStatus)
{
#if (PF_SPI0_DEBUG == 1)
	CHECK_DEV_INIT(spi0ChInit);
#endif	// #if (PF_SPI0_DEBUG == 1)
	SPI0_CHANNEL->MIS = intStatus;
	return enStatusSuccess;
}

#if(SPI0_USE_FIFO != 0)
PFEnStatus pfSpi0GetRxBufferSize(PFdword* size)
{
#if (PF_SPI0_DEBUG == 1)
	CHECK_DEV_INIT(spi0ChInit);
    CHECK_NULL_PTR(size);
#endif	// #if (PF_SPI0_DEBUG == 1)    
    *size = pfFifoLength(&spi0RxFifo);
    return enStatusSuccess;
}

PFEnStatus pfSpi0GetRxBufferCount(PFdword* count)
{
#if (PF_SPI0_DEBUG == 1)
	CHECK_DEV_INIT(spi0ChInit);
        CHECK_NULL_PTR(count);
#endif	// #if (PF_SPI3_DEBUG == 1)    
    *count = spi0RxFifo.count;
    return enStatusSuccess;
}

PFEnStatus pfSpi0RxBufferFlush(void)
{
#if (PF_SPI0_DEBUG == 1)
	CHECK_DEV_INIT(spi0ChInit);
#endif	// #if (PF_SPI0_DEBUG == 1)    
    pfFifoFlush(&spi0RxFifo);
    return enStatusSuccess;
}

PFEnStatus pfSpi0GetTxBufferSize(PFdword* size)
{
#if (PF_SPI0_DEBUG == 1)
	CHECK_DEV_INIT(spi0ChInit);
    CHECK_NULL_PTR(size);
#endif	// #if (PF_SPI0_DEBUG == 1)    
    *size = pfFifoLength(&spi0TxFifo);
    return enStatusSuccess;
}

PFEnStatus pfSpi0GetTxBufferCount(PFdword* count)
{
#if (PF_SPI0_DEBUG == 1)
	CHECK_DEV_INIT(spi0ChInit);
    CHECK_NULL_PTR(count);
#endif	// #if (PF_SPI0_DEBUG == 1)    
    *count = spi0TxFifo.count;
    return enStatusSuccess;
}

PFEnStatus pfSpi0TxBufferFlush(void)
{
#if (PF_SPI0_DEBUG == 1)
	CHECK_DEV_INIT(spi0ChInit);
#endif	// #if (PF_SPI0_DEBUG == 1)    
    pfFifoFlush(&spi0TxFifo);
    return enStatusSuccess;
}
#endif	// #if(SPI0_USE_FIFO != 0)

void SPI0_INT_HANDLER(void)
{
	PFword dummyRead;
#if(SPI0_USE_FIFO != 0)
// 	if(spi0ChState == SPI0_STATE_IDLE)
// 		return;
	// for write operation
	if(spi0ChState == SPI0_STATE_WRITE)
	{
		dummyRead = SPI0_CHANNEL->DR;
		if(pfFifoIsEmpty(&spi0TxFifo) == enBooleanTrue)
		{
			spi0ChState = SPI0_STATE_IDLE;
			return;
		}
		else
			SPI0_CHANNEL->DR = pfFifoPop(&spi0TxFifo);
	}
	else		// for read operation
	{
		if(spi0ChMaster == enBooleanTrue)
		{
			pfFifoPush(&spi0RxFifo, SPI0_CHANNEL->DR);
			if( pfFifoIsEmpty(&spi0TxFifo) != enBooleanTrue )
			{
				SPI0_CHANNEL->DR = pfFifoPop(&spi0TxFifo);
			}
		}
		else		// slave mode
		{
			while( (SPI0_CHANNEL->SR & (1 << 2)) != 0 )
			{
				pfFifoPush(&spi0RxFifo, SPI0_CHANNEL->DR);
				//SPI0_CHANNEL->DR = 0;
			}
		}
	}
#else	
	if(spi0Callback != PF_NULL_PTR)
	{
		spi0Callback();
		return;
	}
#endif	// #if(SPI0_USE_FIFO != 0)	
}

#endif	// #if (PF_USE_SPI0 == 1)
