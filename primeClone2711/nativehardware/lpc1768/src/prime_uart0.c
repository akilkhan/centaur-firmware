#include "prime_framework.h"

#if (PF_USE_UART0 == 1)

#include "prime_utils.h"
#include "prime_sysClk.h"
#include "prime_uart0.h"


#if(UART0_USE_FIFO != 0)
#include "prime_fifo.h"
	#warning UART0 FIFO is enabled for interrupt based communication
	#if( (UART0_BUFFER_SIZE == 0) || ((UART0_BUFFER_SIZE & (UART0_BUFFER_SIZE - 1)) != 0) )
		#error UART0_BUFFER_SIZE cannot be zero. UART0_BUFFER_SIZE should be power of 2
	#endif
#endif	// #if(UART0_USE_FIFO != 0)


/*
// Lookup table for clock dividers according baudrates.
static const PFUart0BaudrateDiv baudrateDividerLookup[] = 
{
	{514, 4, 15}, 		// 2400
	{257, 4, 15},		// 4800
	{92, 10, 13},		// 9600
	{46, 10, 13},		// 19200
	{37, 1, 10},		// 38400
	{19, 3, 7},		// 57600
	{10, 5, 14},		// 115200
};
 */ 
// Lookup tables for clock dividers according baudrates.
static const PFdword uart0BaudValue[] = {2400, 4800, 9600, 19200, 38400, 57600, 115200};
//static const PFdword uart0BaudPrescale[] = {514, 257, 92, 46, 37, 19, 10};
//static const PFbyte uart0BaudFractDiv[] = {4, 4, 10, 10, 1, 3, 5};
//static const PFbyte uart0BaudFractMul[] = {15, 15, 13, 13, 10, 7, 14};

static PFEnBoolean uart0ChInit = enBooleanFalse;		// initialize flag for the channel
static PFEnBoolean uart0ChBusy = enBooleanFalse;		// busy flag for the channel
static PFbyte uart0ChInt;							// interrupt flag for the channel
#if(UART0_USE_FIFO != 0)
static PFbyte uart0RxBuffer[UART0_BUFFER_SIZE];	// UART0 transmit buffer
static PFbyte uart0TxBuffer[UART0_BUFFER_SIZE];	// UART0 receive buffer
static PFFifo uart0TxFifo;							// UART0 transmit fifo structure
static PFFifo uart0RxFifo;							// UART0 receive fifo structure
#else	
static PFcallback uart0TxCallback = 0;					// transmit callback for the channel
static PFcallback uart0RxCallback = 0;					// receive callback for the channel
#endif	// #if(UART0_USE_FIFO != 0)
static PFCfgUart0 uart0Cfg;

static struct fract_div_entry
{
	PFfloat fr;
	PFbyte	div;
	PFbyte	mul;

} fract_divs[] =
{
	{ 1.000, 0, 1 }, 	{ 1.067, 1, 15 }, 	{ 1.071, 1, 14 }, 	{ 1.077, 1, 13 },
	{ 1.083, 1, 12 }, 	{ 1.091, 1, 11 }, 	{ 1.100, 1, 10 }, 	{ 1.111, 1, 9 },
	{ 1.125, 1, 8 }, 	{ 1.133, 2, 15 }, 	{ 1.143, 1, 7 }, 	{ 1.154, 2, 13 },
	{ 1.167, 1, 6 }, 	{ 1.182, 2, 11 }, 	{ 1.200, 1, 5 }, 	{ 1.214, 3, 14 },
	{ 1.222, 2, 9 }, 	{ 1.231, 3, 13 }, 	{ 1.250, 1, 4 }, 	{ 1.267, 4, 15 },
	{ 1.273, 3, 11 }, 	{ 1.286, 2, 7 }, 	{ 1.300, 3, 10 }, 	{ 1.308, 4, 13 },
	{ 1.333, 1, 3 }, 	{ 1.357, 5, 14 }, 	{ 1.364, 4, 11 }, 	{ 1.375, 3, 8 },
	{ 1.385, 5, 13 }, 	{ 1.400, 2, 5 }, 	{ 1.417, 5, 12 }, 	{ 1.429, 3, 7 },
	{ 1.444, 4, 9 }, 	{ 1.455, 5, 11 }, 	{ 1.462, 6, 13 }, 	{ 1.467, 7, 15 },
	{ 1.500, 1, 2 }, 	{ 1.533, 8, 15 }, 	{ 1.538, 7, 13 }, 	{ 1.545, 6, 11 },
	{ 1.556, 5, 9 }, 	{ 1.571, 4, 7 }, 	{ 1.583, 7, 12 }, 	{ 1.600, 3, 5 },
	{ 1.615, 8, 13 }, 	{ 1.625, 5, 8 }, 	{ 1.636, 7, 11 }, 	{ 1.643, 9, 14 },
	{ 1.667, 2, 3 }, 	{ 1.692, 9, 13 }, 	{ 1.700, 7, 10 }, 	{ 1.714, 5, 7 },
	{ 1.727, 8, 11 }, 	{ 1.733, 11, 15 }, 	{ 1.750, 3, 4 }, 	{ 1.769, 10, 13 },
	{ 1.778, 7, 9 }, 	{ 1.786, 11, 14 }, 	{ 1.800, 4, 5 }, 	{ 1.818, 9, 11 },
	{ 1.833, 5, 6 }, 	{ 1.846, 11, 13 }, 	{ 1.857, 6, 7 }, 	{ 1.867, 13, 15 },
	{ 1.875, 7, 8 }, 	{ 1.889, 8, 9 }, 	{ 1.900, 9, 10 }, 	{ 1.909, 10, 11 },
	{ 1.917, 11, 12 }, 	{ 1.923, 12, 13 },	{ 1.929, 13, 14 },	{ 1.933, 14, 15 },
};

PFdword pfUart0BaudrateCalc(PFdword pclk, PFdword baud, PFbyte *mul, PFbyte *div)
{
    PFfloat fr_est, curr_fr;
    uint16_t dl;
    uint32_t i;
	if( pclk % (16 * baud) == 0 )
	{
		*mul = 1;
		*div = 0;
		return (pclk / (16 * baud));
	}

	fr_est = 1.5;
	while(1)
	{
		dl = pclk / (16 * baud * fr_est);
		curr_fr = (PFfloat)pclk / (PFfloat)(16 * baud * dl);
		if(curr_fr <= 1.1)
                {
			fr_est += 0.1;
                }
		else if(curr_fr >= 1.9)
                {
			fr_est -= 0.1;
		}
                else
		{
			fr_est = curr_fr;
			break;
		}
	}

	for(i = 0; i < (sizeof(fract_divs)/sizeof(struct fract_div_entry)); i++)
	{
		if( fr_est <= fract_divs[i].fr )
		{
			if( i == 0 )
			{
				*mul = fract_divs[i].mul;
				*div = fract_divs[i].div;
			}
			else if( (fract_divs[i].fr - fr_est) <= (fr_est - fract_divs[i-1].fr) )
			{
				*mul = fract_divs[i].mul;
				*div = fract_divs[i].div;
			}
			else
			{
				*mul = fract_divs[i-1].mul;
				*div = fract_divs[i-1].div;
			}
			return dl;
		}
	}

	*mul = 0;
	*div = 0;

	return 0;
}

PFEnStatus pfUart0Open(PFpCfgUart0 config)
{
	PFdword temp;
	PFdword uart0Pclk, baudDl;
	PFbyte baudFractMul, baudFractDiv;
#if (PF_UART0_DEBUG == 1)
	// Validate config pointer
	CHECK_NULL_PTR(config);
	if(config->clkDiv > 3)
	{
		return enStatusInvArgs;
	}
	if(config->databits > 3)
	{
		return enStatusInvArgs;
	}
#if(UART0_USE_FIFO == 0)
	if( ((config->interrupts & enUart0IntRx) != 0) && (config->receiveCallback == PF_NULL) )
	{
		return enStatusInvArgs;
	}
	if( ((config->interrupts & enUart0IntTx) != 0) && (config->transmitCallback == PF_NULL) )
	{
		return enStatusInvArgs;
	}
#endif	// #if(UART0_USE_FIFO == 0)
#endif	// #if (PF_UART0_DEBUG == 1)	

	// power on UART0 peripheral
	POWER_ON(UART0_CH);
	// set peripheral clock divider
	pfSysSetPclkDiv(PCLK_DIV(UART0_CH), config->clkDiv);
	
	// configure UART0 channel
	temp = (config->databits & 0x03) | ((config->stopBits & 0x01) << 2) | ((config->parity & 0x07) << 3) | (BIT_MASK_7);
	UART0_CHANNEL->LCR = temp;					// set databits, stop bits and parity and enable divisor latch
	
	// set baudrate
        uart0Pclk = pfSysGetPclk(PCLK_DIV(UART0_CH));
        baudDl = pfUart0BaudrateCalc(uart0Pclk, uart0BaudValue[config->baudrate], &baudFractMul, &baudFractDiv);
        if(baudDl == 0)
        {
            return enStatusNotSupported;
        }
        UART0_CHANNEL->DLL = baudDl & 0xFF;
	UART0_CHANNEL->DLM = (baudDl >> 8) & 0xFF;
	UART0_CHANNEL->FDR = ((baudFractDiv & 0x0F) | ((baudFractMul << 4) & 0xF0));
        
	//UART0_CHANNEL->DLL = uart0BaudPrescale[config->baudrate] & 0xFF;
	//UART0_CHANNEL->DLM = (uart0BaudPrescale[config->baudrate] >> 8) & 0xFF;
	//UART0_CHANNEL->FDR = ((uart0BaudFractDiv[config->baudrate] & 0x0F) | ((uart0BaudFractMul[config->baudrate] << 4) & 0xF0));
	
	// disable divisor latch
	UART0_CHANNEL->LCR &= INV_BIT_MASK_7;

#if(UART0_USE_FIFO != 0)
	// initialize fifo 
	pfFifoInit(&uart0TxFifo, uart0TxBuffer, UART0_BUFFER_SIZE);
	pfFifoInit(&uart0RxFifo, uart0RxBuffer, UART0_BUFFER_SIZE);
#else	
	// set transmit and receive user callbacks
	uart0TxCallback = config->transmitCallback;
	uart0RxCallback = config->receiveCallback;
#endif	// #if(UART0_USE_FIFO != 0)
	
	// set UART0 interrupts
	if(config->interrupts != enUart0IntNone)
	{
		UART0_CHANNEL->IER = config->interrupts;
		uart0ChInt = config->interrupts;
		
		NVIC_EnableIRQ(IRQ_NUM(UART0_CH));
		uart0ChInt |= 0x80;
	}
	
	pfMemCopy(&uart0Cfg, config, sizeof(PFCfgUart0));
	// set initialize flag and clear busy flag
	uart0ChInit = enBooleanTrue;
	uart0ChBusy = enBooleanFalse;
	return enStatusSuccess;
}

void pfUart0Close(void)
{
	uart0ChInit = enBooleanFalse;
	uart0ChBusy = enBooleanFalse;
	// power off UART0 peripheral
	POWER_OFF(UART0_CH);
}

PFEnStatus pfUart0WriteByte(PFbyte data)
{
#if (PF_UART0_DEBUG == 1) 	
	// check if the channel is initialized
	CHECK_DEV_INIT(uart0ChInit);
#endif	// #if (PF_UART0_DEBUG == 1)	
	// check if the channel is busy
	if(uart0ChBusy != enBooleanFalse)
	{
		return enStatusBusy;
	}
	UART0_CHANNEL->THR = data;
	while((UART0_CHANNEL->LSR & (BIT_MASK_5)) == 0);
	return enStatusSuccess;
}

PFEnStatus pfUart0Write(PFbyte* data, PFdword size)
{
	PFdword index;
#if (PF_UART0_DEBUG == 1)	
	// check if the channel is initialized
	CHECK_DEV_INIT(uart0ChInit);
        CHECK_NULL_PTR(data);
#endif	// #if (PF_UART0_DEBUG == 1)
	// check if the channel is busy
	if(uart0ChBusy != enBooleanFalse)
	{
		return enStatusBusy;
	}
	// set channel busy	
	uart0ChBusy = enBooleanTrue;
	// if fifo is to be used push data into the fifo
	if((uart0ChInt & enUart0IntTx) != 0)
	{
	#if(UART0_USE_FIFO != 0)
		for(index = 0; index < size; index++)
		{
			pfFifoPush(&uart0TxFifo, *(data + index));
		}
		UART0_CHANNEL->THR = pfFifoPop(&uart0TxFifo);
	#else
		UART0_CHANNEL->THR = *data;
	#endif	// #if(UART0_USE_FIFO != 0)	
		//UART0_CHANNEL->IER = uart0ChInt & (0x7F);
	}
	else
	{
		// for polling based transmission, write each byte one by one to data register
		for(index = 0; index < size; index++)
		{
			UART0_CHANNEL->THR = *(data + index);
			while((UART0_CHANNEL->LSR & (BIT_MASK_5)) == 0);
		}
	}
	
	uart0ChBusy = enBooleanFalse;
	return enStatusSuccess;
}

PFEnStatus pfUart0WriteString(PFbyte* data)
{
	PFdword index;
#if (PF_UART0_DEBUG == 1)	
	// check if the channel is initialized
	CHECK_DEV_INIT(uart0ChInit);
        CHECK_NULL_PTR(data);
#endif	// #if (PF_UART0_DEBUG == 1)
	// check if the channel is busy
	if(uart0ChBusy != enBooleanFalse)
	{
		return enStatusBusy;
	}
	// set channel busy	
	uart0ChBusy = enBooleanTrue;
	// if fifo is to be used push data into the fifo
	if((uart0ChInt & enUart0IntTx) != 0)
	{
	#if(UART0_USE_FIFO != 0)
		for(index = 0; *(data + index) != 0; index++)
		{
			pfFifoPush(&uart0TxFifo, *(data + index));
		}
		UART0_CHANNEL->THR = pfFifoPop(&uart0TxFifo);
	#else
		UART0_CHANNEL->THR = *data;
	#endif	// #if(UART0_USE_FIFO != 0)	
		//UART0_CHANNEL->IER = uart0ChInt & (0x7F);
	}
	else
	{
		// for polling based transmission, write each byte one by one to data register
		for(index = 0; *(data + index) != 0; index++)
		{
			UART0_CHANNEL->THR = *(data + index);
			while((UART0_CHANNEL->LSR & (BIT_MASK_5)) == 0);
		}
	}
	
	uart0ChBusy = enBooleanFalse;
	return enStatusSuccess;
}

PFEnStatus pfUart0ReadByte(PFbyte* rxByte)
{
	PFbyte rdByte;
#if (PF_UART0_DEBUG == 1)
	CHECK_DEV_INIT(uart0ChInit);
    CHECK_NULL_PTR(rxByte);
#endif	// #if (PF_UART0_DEBUG == 1)	
	if(uart0ChBusy != enBooleanFalse)
	{
		return enStatusBusy;
	}	
	while((UART0_CHANNEL->LSR & 0x01) == 0);
	rdByte = UART0_CHANNEL->RBR;
	
	*rxByte = rdByte;
	return enStatusSuccess;
}

PFEnStatus pfUart0Read(PFbyte* data, PFdword size, PFdword* readBytes)
{
	PFdword index;
#if (PF_UART0_DEBUG == 1)
	CHECK_DEV_INIT(uart0ChInit);
        CHECK_NULL_PTR(data);
        CHECK_NULL_PTR(readBytes);
#endif	// #if (PF_UART0_DEBUG == 1)	
	// check if channel is busy
	if(uart0ChBusy != enBooleanFalse)
	{
		return enStatusBusy;
	}
	// set channel busy	
	uart0ChBusy = enBooleanTrue;
	// reset read byte count
	*readBytes = 0;
	
	if((uart0ChInt & enUart0IntRx) != 0)
	{
	#if(UART0_USE_FIFO != 0)
		for(index = 0; index < size; index++)
		{
			if(pfFifoIsEmpty(&uart0RxFifo) == enBooleanTrue)
			{
				break;
			}
			*(data + index) = pfFifoPop(&uart0RxFifo);
			*readBytes += 1;
		}
	#else
		uart0ChBusy = enBooleanFalse;
		{
			return enStatusUnknown;
		}
	#endif	// #if(UART0_USE_FIFO != 0)	
	}
	else
	{
		for(index = 0; index < size; index++)
		{
			while((UART0_CHANNEL->LSR & 0x01) == 0);
			*(data + index) = UART0_CHANNEL->RBR;
			*readBytes += 1;
		}
	}
	uart0ChBusy = enBooleanFalse;
	return enStatusSuccess;
}

#if(UART0_USE_FIFO != 0)
PFEnStatus pfUart0GetRxBufferSize(PFdword* size)
{
#if (PF_UART3_DEBUG == 1)
	CHECK_DEV_INIT(uart0ChInit);
    CHECK_NULL_PTR(size);
#endif	// #if (PF_UART0_DEBUG == 1)    
    *size = pfFifoLength(&uart0RxFifo);
    return enStatusSuccess;
}

PFEnStatus pfUart0GetRxBufferCount(PFdword* count)
{
#if (PF_UART0_DEBUG == 1)
	CHECK_DEV_INIT(uart0ChInit);
        CHECK_NULL_PTR(count);
#endif	// #if (PF_UART3_DEBUG == 1)    
    *count = uart0RxFifo.count;
    return enStatusSuccess;
}

PFEnStatus pfUart0RxBufferFlush(void)
{
#if (PF_UART0_DEBUG == 1)
	CHECK_DEV_INIT(uart0ChInit);
#endif	// #if (PF_UART0_DEBUG == 1)    
    pfFifoFlush(&uart0RxFifo);
    return enStatusSuccess;
}

PFEnStatus pfUart0GetTxBufferSize(PFdword* size)
{
#if (PF_UART0_DEBUG == 1)
	CHECK_DEV_INIT(uart0ChInit);
    CHECK_NULL_PTR(size);
#endif	// #if (PF_UART0_DEBUG == 1)    
    *size = pfFifoLength(&uart0TxFifo);
    return enStatusSuccess;
}

PFEnStatus pfUart0GetTxBufferCount(PFdword* count)
{
#if (PF_UART0_DEBUG == 1)
	CHECK_DEV_INIT(uart0ChInit);
    CHECK_NULL_PTR(count);
#endif	// #if (PF_UART0_DEBUG == 1)    
    *count = uart0TxFifo.count;
    return enStatusSuccess;
}

PFEnStatus pfUart0TxBufferFlush(void)
{
#if (PF_UART0_DEBUG == 1)
	CHECK_DEV_INIT(uart0ChInit);
#endif	// #if (PF_UART0_DEBUG == 1)    
    pfFifoFlush(&uart0TxFifo);
    return enStatusSuccess;
}
#endif	// #if(UART0_USE_FIFO != 0)

void UART0_INT_HANDLER(void)
{
	PFbyte rxByte;
	PFdword intStat = UART0_CHANNEL->IIR;	//pfUart0GetIntStatus();

	if((intStat & 0x04) != 0)		// Data received interrupt
	{
	#if(UART0_USE_FIFO != 0)
		rxByte = UART0_CHANNEL->RBR;
		pfFifoPush(&uart0RxFifo, rxByte);
	#else
		if(uart0RxCallback != 0)
		{
			uart0RxCallback();
		}
		return;
	#endif		// #if(UART0_USE_FIFO != 0)
	}

	if((intStat & 0x02) != 0)		// THR Reg empty interrypt
	{
	#if(UART0_USE_FIFO != 0)
		if(pfFifoIsEmpty(&uart0TxFifo) != enBooleanTrue)
		{
			UART0_CHANNEL->THR = pfFifoPop(&uart0TxFifo);
		}
	#else	
		if(uart0TxCallback != 0)
		{
			uart0TxCallback();
		}
		return;
	#endif		// #if(UART0_USE_FIFO != 0)	
	}
}

#endif	// #if (PF_USE_UART0 == 1)
