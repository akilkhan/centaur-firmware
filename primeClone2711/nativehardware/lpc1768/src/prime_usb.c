#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_usb.h"

static PFEnBoolean usbDeviceInit = enBooleanFalse;		// initialize flag for usb device
static PFEnBoolean usbHostInit = enBooleanFalse;		// initialize flag for usb host
static PFEnBoolean usbDeviceBusy = enBooleanFalse;		// busy flag for the channel
static PFcallback usbDeviceCallback = 0;
static PFcallback usbHostCallback = 0;
static PFEnUsbMode usbChMode = enUsbModeIdle;

#define USB_RET_OK()		return enStatusSuccess

PFEnStatus pfUsbOpen(void)
{
#if (PF_USB_DEBUG == 1)
	// parameter sanity check
#endif	// #if (PF_USB_DEBUG == 1)

	// power on USB peripheral
	POWER_ON(USB_CH);

#if (PF_USB_DEVICE_ENABLE == 1)	
	// Enable USB device controller clock and AHB clock
	USB_CHANNEL->USBClkCtrl = BIT_MASK_1 | BIT_MASK_4;
	// Wait till clock is activated
	while(USB_CHANNEL->USBClkSt != (BIT_MASK_1 | BIT_MASK_4));
#endif // #if (PF_USB_DEVICE_ENABLE == 1)	
	usbDeviceInit = enBooleanTrue;
	USB_RET_OK();
}

void pfUsbClose(void)
{
	// power off USB peripheral
	POWER_OFF(USB_CH);
	pfUsbIntDisable();
}


void pfUsbSetMode(PFEnUsbMode usbMode)
{
	usbChMode = usbMode;
}

PFEnUsbMode pfUsbGetMode(void)
{
	return usbChMode;
}

PFEnStatus pfUsbIntEnable(void)
{
	NVIC_EnableIRQ(IRQ_NUM(USB_CH));
	
	USB_RET_OK();
}

PFEnStatus pfUsbIntDisable(void)
{
	NVIC_DisableIRQ(IRQ_NUM(USB_CH));
	
	USB_RET_OK();
}

PFEnStatus pfUsbSetDeviceCallback(PFcallback deviceCallback)
{
	if(deviceCallback == 0)
	{
		return enStatusInvArgs;
	}
	usbDeviceCallback = deviceCallback;
	
	USB_RET_OK();
}

PFEnStatus pfUsbSetHostCallback(PFcallback hostCallback)
{
	if(hostCallback == 0)
	{
		return enStatusInvArgs;
	}
	usbHostCallback = hostCallback;
	
	USB_RET_OK();
}

PFEnStatus pfUsbDeviceClockEnable(void)
{
	// Enable USB device controller clock and AHB clock
	USB_CHANNEL->USBClkCtrl = BIT_MASK_1 | BIT_MASK_4;
	// Wait till clock is activated
	while(USB_CHANNEL->USBClkSt != (BIT_MASK_1 | BIT_MASK_4));
	
	USB_RET_OK();
}

PFEnStatus pfUsbEnableDeviceInterrupts(PFdword devInt)
{
	USB_CHANNEL->USBDevIntEn = devInt;
	USB_RET_OK();
}

PFEnStatus pfUsbEnableEpInterrupts(PFdword epInt)
{
	USB_CHANNEL->USBEpIntEn	= epInt;
	USB_RET_OK();
}

PFEnStatus pfUsbClearDeviceInterrupts(PFdword devInt)
{
	USB_CHANNEL->USBDevIntClr = devInt;
	USB_RET_OK();
}

PFEnStatus pfUsbClearEpInterrupts(PFdword epInt)
{
	USB_CHANNEL->USBEpIntClr = epInt;
	USB_RET_OK();
}

PFEnStatus pfUsbSetEpInterruptPriority(PFdword epIntPriority)
{
	USB_CHANNEL->USBEpIntPri = epIntPriority;
	USB_RET_OK();
}

PFEnStatus pfUsbEnableDmaInterrupts(PFdword dmaInterrupts)
{
	USB_CHANNEL->USBDMAIntEn = dmaInterrupts;
	USB_RET_OK();
}

void pfUsbSoftconnect(PFEnBoolean connectStatus)
{
	PFbyte devCon;
#if(PF_USB_DEVICE_ENABLE == 1)
	if(connectStatus == enBooleanTrue)
	{
		devCon = USB_DEV_CON;
	}
	else
	{
		devCon = 0;
	}
	
	if(usbChMode == enUsbModeDevice)
	{
		pfUsbSIEWriteCommamdData(USB_CMD_SET_DEV_STAT, USB_DAT_WR_BYTE(devCon));
	}
#endif	// #if(PF_USB_DEVICE_ENABLE == 1)			
}

#if 0
PFEnStatus pfUsbDeviceReset(void)
{
	uint32_t n;

	USB_CHANNEL->USBEpInd = 0;
	USB_CHANNEL->USBMaxPSize = USB_Device_ControlEndpointSize;
	USB_CHANNEL->USBEpInd = 1;
	USB_CHANNEL->USBMaxPSize = USB_Device_ControlEndpointSize;
	while ((USB_CHANNEL->USBDevIntSt & USB_DEV_INT_EP_RLZED) == 0);

	/* Slave Register */
	//pfUsbSetEpInterrupts(0);
	USB_CHANNEL->USBEpIntEn		= 0;
	//pfUsbSetDeviceInterrupts(USB_DEV_INT_STAT | USB_DEV_INT_EP_SLOW | USB_DEV_INT_ERR);
	USB_CHANNEL->USBDevIntEn	= (USB_DEV_INT_STAT | USB_DEV_INT_EP_SLOW | USB_DEV_INT_ERR);

	USB_CHANNEL->USBEpIntClr	= 0xFFFFFFFF;
	USB_CHANNEL->USBDevIntClr	= 0xFFFFFFFF;
	USB_CHANNEL->USBEpIntPri	= 0;

	/* DMA registers */
	USB_CHANNEL->USBEpDMADis	= 0xFFFFFFFF;
	USB_CHANNEL->USBDMARClr		= 0xFFFFFFFF;
	USB_CHANNEL->USBEoTIntClr	= 0xFFFFFFFF;
	USB_CHANNEL->USBNDDRIntClr	= 0xFFFFFFFF;
	USB_CHANNEL->USBSysErrIntClr = 0xFFFFFFFF;

	USB_CHANNEL->USBDMAIntEn  = (USB_DMA_INT_EOT | USB_DMA_INT_NDD_REQ | USB_DMA_INT_SYS_ERR );
	USB_CHANNEL->USBUDCAH   = (uint32_t) UDCA;
	for (n = 0; n < USED_PHYSICAL_ENDPOINTS; n++) {
		UDCA[n] = 0;
	}
}
#endif
/*
 *	Serial Interface Engine API
 */
void pfUsbSIEWriteCommamd(PFdword cmd) 
{
	USB_CHANNEL->USBDevIntClr = USB_DEV_INT_CCEMTY;
	USB_CHANNEL->USBCmdCode = cmd;
	while ((USB_CHANNEL->USBDevIntSt & USB_DEV_INT_CCEMTY) == 0);
}

void pfUsbSIEWriteCommamdData (PFdword cmd, PFdword val) 
{
	// Write command
	USB_CHANNEL->USBDevIntClr = USB_DEV_INT_CCEMTY;
	USB_CHANNEL->USBCmdCode = cmd;
	while ((USB_CHANNEL->USBDevIntSt & USB_DEV_INT_CCEMTY) == 0);
	
	// Write data
	USB_CHANNEL->USBDevIntClr = USB_DEV_INT_CCEMTY;
	USB_CHANNEL->USBCmdCode = val;
	while ((USB_CHANNEL->USBDevIntSt & USB_DEV_INT_CCEMTY) == 0);
}

PFdword pfUsbSIEReadCommandData(PFdword cmd) 
{
	USB_CHANNEL->USBDevIntClr = USB_DEV_INT_CCEMTY | USB_DEV_INT_CDFULL;
	USB_CHANNEL->USBCmdCode = cmd;
	while ((USB_CHANNEL->USBDevIntSt & USB_DEV_INT_CDFULL) == 0);
  	return (USB_CHANNEL->USBCmdData);
}

/*
 *			Endpoint API
 */
void pfUsbRealizeEndpoint(PFbyte phyEpNum, PFdword maxPacketSize)
{
	USB_CHANNEL->USBReEp |= (1 << phyEpNum); 			// Realize endpoint

	USB_CHANNEL->USBEpInd = phyEpNum;					// Endpoint Index
	USB_CHANNEL->USBMaxPSize = maxPacketSize & 0x3ff;	// Max Packet Size

	while ((USB_CHANNEL->USBDevIntSt & USB_DEV_INT_EP_RLZED) == 0);
	USB_CHANNEL->USBDevIntClr = USB_DEV_INT_EP_RLZED;
}

/* 
 * 		USB interrupt handler			
 */
void USB_INT_HANDLER(void)
{
#if(PF_USB_HOST_ENABLE == 1)
	if(usbChMode == enUsbModeHost)
	{
		if(usbHostCallback != 0)
		{
			usbHostCallback();
		}
	}
#endif	// #if(PF_USB_HOST_ENABLE == 1)
	
#if(PF_USB_DEVICE_ENABLE == 1)
	if(usbChMode == enUsbModeDevice)
	{
		if(usbDeviceCallback != 0)
		{
			usbDeviceCallback();
		}
	}
#endif	// #if(PF_USB_DEVICE_ENABLE == 1)
}


