#include "prime_framework.h"

#if (PF_USE_UART2 == 1)

#include "prime_utils.h"
#include "prime_sysClk.h"
#include "prime_uart2.h"


#if(UART2_USE_FIFO != 0)
#include "prime_fifo.h"
	#warning UART2 FIFO is enabled for interrupt based communication
	#if( (UART2_BUFFER_SIZE == 0) || ((UART2_BUFFER_SIZE & (UART2_BUFFER_SIZE - 1)) != 0) )
		#error UART2_BUFFER_SIZE cannot be zero. UART2_BUFFER_SIZE should be power of 2
	#endif
#endif	// #if(UART2_USE_FIFO != 0)


/*
// Lookup table for clock dividers according baudrates.
static const PFUart2BaudrateDiv baudrateDividerLookup[] = 
{
	{514, 4, 15}, 		// 2400
	{257, 4, 15},		// 4800
	{92, 10, 13},		// 9600
	{46, 10, 13},		// 19200
	{37, 1, 10},		// 38400
	{19, 3, 7},			// 57600
	{10, 5, 14},		// 115200
};
 */ 
// Lookup tables for clock dividers according baudrates.
static const PFdword uart2BaudValue[] = {2400, 4800, 9600, 19200, 38400, 57600, 115200};
//static const PFdword uart2BaudPrescale[] = {514, 257, 92, 46, 37, 19, 10};
//static const PFbyte uart2BaudFractDiv[] = {4, 4, 10, 10, 1, 3, 5};
//static const PFbyte uart2BaudFractMul[] = {15, 15, 13, 13, 10, 7, 14};

static PFEnBoolean uart2ChInit = enBooleanFalse;		// initialize flag for the channel
static PFEnBoolean uart2ChBusy = enBooleanFalse;		// busy flag for the channel
static PFbyte uart2ChInt;								// interrupt flag for the channel
#if(UART2_USE_FIFO != 0)
static PFbyte uart2RxBuffer[UART2_BUFFER_SIZE];			// UART2 transmit buffer
static PFbyte uart2TxBuffer[UART2_BUFFER_SIZE];			// UART2 receive buffer
static PFFifo uart2TxFifo;								// UART2 transmit fifo structure
static PFFifo uart2RxFifo;								// UART2 receive fifo structure
#else	
static PFcallback uart2TxCallback = 0;					// transmit callback for the channel
static PFcallback uart2RxCallback = 0;					// receive callback for the channel
#endif	// #if(UART2_USE_FIFO != 0)
static PFCfgUart2 uart2Cfg;

static struct fract_div_entry
{
	PFfloat fr;
	PFbyte	div;
	PFbyte	mul;

} fract_divs[] =
{
	{ 1.000, 0, 1 }, 	{ 1.067, 1, 15 }, 	{ 1.071, 1, 14 }, 	{ 1.077, 1, 13 },
	{ 1.083, 1, 12 }, 	{ 1.091, 1, 11 }, 	{ 1.100, 1, 10 }, 	{ 1.111, 1, 9 },
	{ 1.125, 1, 8 }, 	{ 1.133, 2, 15 }, 	{ 1.143, 1, 7 }, 	{ 1.154, 2, 13 },
	{ 1.167, 1, 6 }, 	{ 1.182, 2, 11 }, 	{ 1.200, 1, 5 }, 	{ 1.214, 3, 14 },
	{ 1.222, 2, 9 }, 	{ 1.231, 3, 13 }, 	{ 1.250, 1, 4 }, 	{ 1.267, 4, 15 },
	{ 1.273, 3, 11 }, 	{ 1.286, 2, 7 }, 	{ 1.300, 3, 10 }, 	{ 1.308, 4, 13 },
	{ 1.333, 1, 3 }, 	{ 1.357, 5, 14 }, 	{ 1.364, 4, 11 }, 	{ 1.375, 3, 8 },
	{ 1.385, 5, 13 }, 	{ 1.400, 2, 5 }, 	{ 1.417, 5, 12 }, 	{ 1.429, 3, 7 },
	{ 1.444, 4, 9 }, 	{ 1.455, 5, 11 }, 	{ 1.462, 6, 13 }, 	{ 1.467, 7, 15 },
	{ 1.500, 1, 2 }, 	{ 1.533, 8, 15 }, 	{ 1.538, 7, 13 }, 	{ 1.545, 6, 11 },
	{ 1.556, 5, 9 }, 	{ 1.571, 4, 7 }, 	{ 1.583, 7, 12 }, 	{ 1.600, 3, 5 },
	{ 1.615, 8, 13 }, 	{ 1.625, 5, 8 }, 	{ 1.636, 7, 11 }, 	{ 1.643, 9, 14 },
	{ 1.667, 2, 3 }, 	{ 1.692, 9, 13 }, 	{ 1.700, 7, 10 }, 	{ 1.714, 5, 7 },
	{ 1.727, 8, 11 }, 	{ 1.733, 11, 15 }, 	{ 1.750, 3, 4 }, 	{ 1.769, 10, 13 },
	{ 1.778, 7, 9 }, 	{ 1.786, 11, 14 }, 	{ 1.800, 4, 5 }, 	{ 1.818, 9, 11 },
	{ 1.833, 5, 6 }, 	{ 1.846, 11, 13 }, 	{ 1.857, 6, 7 }, 	{ 1.867, 13, 15 },
	{ 1.875, 7, 8 }, 	{ 1.889, 8, 9 }, 	{ 1.900, 9, 10 }, 	{ 1.909, 10, 11 },
	{ 1.917, 11, 12 }, 	{ 1.923, 12, 13 },	{ 1.929, 13, 14 },	{ 1.933, 14, 15 },
};

PFdword pfUart2BaudrateCalc(PFdword pclk, PFdword baud, PFbyte *mul, PFbyte *div)
{
    PFfloat fr_est, curr_fr;
    uint16_t dl;
    uint32_t i;
	if( pclk % (16 * baud) == 0 )
	{
		*mul = 1;
		*div = 0;
		return (pclk / (16 * baud));
	}

	fr_est = 1.5;
	while(1)
	{
		dl = pclk / (16 * baud * fr_est);
		curr_fr = (PFfloat)pclk / (PFfloat)(16 * baud * dl);
		if(curr_fr <= 1.1)
                {
			fr_est += 0.1;
                }
		else if(curr_fr >= 1.9)
                {
			fr_est -= 0.1;
		}
                else
		{
			fr_est = curr_fr;
			break;
		}
	}

	for(i = 0; i < (sizeof(fract_divs)/sizeof(struct fract_div_entry)); i++)
	{
		if( fr_est <= fract_divs[i].fr )
		{
			if( i == 0 )
			{
				*mul = fract_divs[i].mul;
				*div = fract_divs[i].div;
			}
			else if( (fract_divs[i].fr - fr_est) <= (fr_est - fract_divs[i-1].fr) )
			{
				*mul = fract_divs[i].mul;
				*div = fract_divs[i].div;
			}
			else
			{
				*mul = fract_divs[i-1].mul;
				*div = fract_divs[i-1].div;
			}
			return dl;
		}
	}

	*mul = 0;
	*div = 0;

	return 0;
}

PFEnStatus pfUart2Open(PFpCfgUart2 config)
{
	PFdword temp;
	PFdword uart2Pclk, baudDl;
	PFbyte baudFractMul, baudFractDiv;
#if (PF_UART2_DEBUG == 1)
	// Validate config pointer
	CHECK_NULL_PTR(config);
	if(config->clkDiv > 3)
	{
		return enStatusInvArgs;
	}
	if(config->databits > 3)
	{
		return enStatusInvArgs;
	}
#if(UART2_USE_FIFO == 0)
	if( ((config->interrupts & enUart2IntRx) != 0) && (config->receiveCallback == PF_NULL) )
	{
		return enStatusInvArgs;
	}
	if( ((config->interrupts & enUart2IntTx) != 0) && (config->transmitCallback == PF_NULL) )
	{
		return enStatusInvArgs;
	}
#endif	// #if(UART2_USE_FIFO == 0)
#endif	// #if (PF_UART2_DEBUG == 1)	

	// power on UART2 peripheral
	POWER_ON(UART2_CH);
	// set peripheral clock divider
	pfSysSetPclkDiv(PCLK_DIV(UART2_CH), config->clkDiv);
	
	// configure UART2 channel
	temp = (config->databits & 0x03) | ((config->stopBits & 0x01) << 2) | ((config->parity & 0x07) << 3) | (BIT_MASK_7);
	UART2_CHANNEL->LCR = temp;					// set databits, stop bits and parity and enable divisor latch
	
	// set baudrate
        uart2Pclk = pfSysGetPclk(PCLK_DIV(UART2_CH));
        baudDl = pfUart2BaudrateCalc(uart2Pclk, uart2BaudValue[config->baudrate], &baudFractMul, &baudFractDiv);
        if(baudDl == 0)
        {
            return enStatusNotSupported;
        }
        UART2_CHANNEL->DLL = baudDl & 0xFF;
	UART2_CHANNEL->DLM = (baudDl >> 8) & 0xFF;
	UART2_CHANNEL->FDR = ((baudFractDiv & 0x0F) | ((baudFractMul << 4) & 0xF0));
        
	//UART2_CHANNEL->DLL = uart2BaudPrescale[config->baudrate] & 0xFF;
	//UART2_CHANNEL->DLM = (uart2BaudPrescale[config->baudrate] >> 8) & 0xFF;
	//UART2_CHANNEL->FDR = ((uart2BaudFractDiv[config->baudrate] & 0x0F) | ((uart2BaudFractMul[config->baudrate] << 4) & 0xF0));
	
	// disable divisor latch
	UART2_CHANNEL->LCR &= INV_BIT_MASK_7;

#if(UART2_USE_FIFO != 0)
	// initialize fifo 
	pfFifoInit(&uart2TxFifo, uart2TxBuffer, UART2_BUFFER_SIZE);
	pfFifoInit(&uart2RxFifo, uart2RxBuffer, UART2_BUFFER_SIZE);
#else	
	// set transmit and receive user callbacks
	uart2TxCallback = config->transmitCallback;
	uart2RxCallback = config->receiveCallback;
#endif	// #if(UART2_USE_FIFO != 0)
	
	// set UART2 interrupts
	if(config->interrupts != enUart2IntNone)
	{
		UART2_CHANNEL->IER = config->interrupts;
		uart2ChInt = config->interrupts;
		
		NVIC_EnableIRQ(IRQ_NUM(UART2_CH));
		uart2ChInt |= 0x80;
	}
	
	pfMemCopy(&uart2Cfg, config, sizeof(PFCfgUart2));
	// set initialize flag and clear busy flag
	uart2ChInit = enBooleanTrue;
	uart2ChBusy = enBooleanFalse;
	return enStatusSuccess;
}

void pfUart2Close(void)
{
	uart2ChInit = enBooleanFalse;
	uart2ChBusy = enBooleanFalse;
	// power off UART2 peripheral
	POWER_OFF(UART2_CH);
}

PFEnStatus pfUart2WriteByte(PFbyte data)
{
#if (PF_UART2_DEBUG == 1) 	
	// check if the channel is initialized
	CHECK_DEV_INIT(uart2ChInit);
#endif	// #if (PF_UART2_DEBUG == 1)	
	// check if the channel is busy
	if(uart2ChBusy != enBooleanFalse)
	{
		return enStatusBusy;
	}
	UART2_CHANNEL->THR = data;
	while((UART2_CHANNEL->LSR & (BIT_MASK_5)) == 0);
	return enStatusSuccess;
}

PFEnStatus pfUart2Write(PFbyte* data, PFdword size)
{
	PFdword index;
#if (PF_UART2_DEBUG == 1)	
	// check if the channel is initialized
	CHECK_DEV_INIT(uart2ChInit);
        CHECK_NULL_PTR(data);
#endif	// #if (PF_UART2_DEBUG == 1)
	// check if the channel is busy
	if(uart2ChBusy != enBooleanFalse)
	{
		return enStatusBusy;
	}
	// set channel busy	
	uart2ChBusy = enBooleanTrue;
	// if fifo is to be used push data into the fifo
	if((uart2ChInt & enUart2IntTx) != 0)
	{
	#if(UART2_USE_FIFO != 0)
		for(index = 0; index < size; index++)
		{
			pfFifoPush(&uart2TxFifo, *(data + index));
		}
		UART2_CHANNEL->THR = pfFifoPop(&uart2TxFifo);
	#else
		UART2_CHANNEL->THR = *data;
	#endif	// #if(UART2_USE_FIFO != 0)	
		//UART2_CHANNEL->IER = uart2ChInt & (0x7F);
	}
	else
	{
		// for polling based transmission, write each byte one by one to data register
		for(index = 0; index < size; index++)
		{
			UART2_CHANNEL->THR = *(data + index);
			while((UART2_CHANNEL->LSR & (BIT_MASK_5)) == 0);
		}
	}
	
	uart2ChBusy = enBooleanFalse;
	return enStatusSuccess;
}

PFEnStatus pfUart2WriteString(PFbyte* data)
{
	PFdword index;
#if (PF_UART2_DEBUG == 1)	
	// check if the channel is initialized
	CHECK_DEV_INIT(uart2ChInit);
        CHECK_NULL_PTR(data);
#endif	// #if (PF_UART2_DEBUG == 1)
	// check if the channel is busy
	if(uart2ChBusy != enBooleanFalse)
	{
		return enStatusBusy;
	}
	// set channel busy	
	uart2ChBusy = enBooleanTrue;
	// if fifo is to be used push data into the fifo
	if((uart2ChInt & enUart2IntTx) != 0)
	{
	#if(UART2_USE_FIFO != 0)
		for(index = 0; *(data + index) != 0; index++)
		{
			pfFifoPush(&uart2TxFifo, *(data + index));
		}
		UART2_CHANNEL->THR = pfFifoPop(&uart2TxFifo);
	#else
		UART2_CHANNEL->THR = *data;
	#endif	// #if(UART2_USE_FIFO != 0)	
		//UART2_CHANNEL->IER = uart2ChInt & (0x7F);
	}
	else
	{
		// for polling based transmission, write each byte one by one to data register
		for(index = 0; *(data + index) != 0; index++)
		{
			UART2_CHANNEL->THR = *(data + index);
			while((UART2_CHANNEL->LSR & (BIT_MASK_5)) == 0);
		}
	}
	
	uart2ChBusy = enBooleanFalse;
	return enStatusSuccess;
}

PFEnStatus pfUart2ReadByte(PFbyte* rxByte)
{
	PFbyte rdByte;
#if (PF_UART2_DEBUG == 1)
	CHECK_DEV_INIT(uart2ChInit);
    CHECK_NULL_PTR(rxByte);
#endif	// #if (PF_UART2_DEBUG == 1)	
	if(uart2ChBusy != enBooleanFalse)
	{
		return enStatusBusy;
	}	
	while((UART2_CHANNEL->LSR & 0x01) == 0);
	rdByte = UART2_CHANNEL->RBR;
	
	*rxByte = rdByte;
	return enStatusSuccess;
}

PFEnStatus pfUart2Read(PFbyte* data, PFdword size, PFdword* readBytes)
{
	PFdword index;
#if (PF_UART2_DEBUG == 1)
	CHECK_DEV_INIT(uart2ChInit);
        CHECK_NULL_PTR(data);
        CHECK_NULL_PTR(readBytes);
#endif	// #if (PF_UART2_DEBUG == 1)	
	// check if channel is busy
	if(uart2ChBusy != enBooleanFalse)
	{
		return enStatusBusy;
	}
	// set channel busy	
	uart2ChBusy = enBooleanTrue;
	// reset read byte count
	*readBytes = 0;
	
	if((uart2ChInt & enUart2IntRx) != 0)
	{
	#if(UART2_USE_FIFO != 0)
		for(index = 0; index < size; index++)
		{
			if(pfFifoIsEmpty(&uart2RxFifo) == enBooleanTrue)
			{
				break;
			}
			*(data + index) = pfFifoPop(&uart2RxFifo);
			*readBytes += 1;
		}
	#else
		uart2ChBusy = enBooleanFalse;
		{
			return enStatusUnknown;
		}
	#endif	// #if(UART2_USE_FIFO != 0)	
	}
	else
	{
		for(index = 0; index < size; index++)
		{
			while((UART2_CHANNEL->LSR & 0x01) == 0);
			*(data + index) = UART2_CHANNEL->RBR;
			*readBytes += 1;
		}
	}
	uart2ChBusy = enBooleanFalse;
	return enStatusSuccess;
}

#if(UART2_USE_FIFO != 0)
PFEnStatus pfUart2GetRxBufferSize(PFdword* size)
{
#if (PF_UART3_DEBUG == 1)
	CHECK_DEV_INIT(uart2ChInit);
    CHECK_NULL_PTR(size);
#endif	// #if (PF_UART2_DEBUG == 1)    
    *size = pfFifoLength(&uart2RxFifo);
    return enStatusSuccess;
}

PFEnStatus pfUart2GetRxBufferCount(PFdword* count)
{
#if (PF_UART2_DEBUG == 1)
	CHECK_DEV_INIT(uart2ChInit);
        CHECK_NULL_PTR(count);
#endif	// #if (PF_UART3_DEBUG == 1)    
    *count = uart2RxFifo.count;
    return enStatusSuccess;
}

PFEnStatus pfUart2RxBufferFlush(void)
{
#if (PF_UART2_DEBUG == 1)
	CHECK_DEV_INIT(uart2ChInit);
#endif	// #if (PF_UART2_DEBUG == 1)    
    pfFifoFlush(&uart2RxFifo);
    return enStatusSuccess;
}

PFEnStatus pfUart2GetTxBufferSize(PFdword* size)
{
#if (PF_UART2_DEBUG == 1)
	CHECK_DEV_INIT(uart2ChInit);
    CHECK_NULL_PTR(size);
#endif	// #if (PF_UART2_DEBUG == 1)    
    *size = pfFifoLength(&uart2TxFifo);
    return enStatusSuccess;
}

PFEnStatus pfUart2GetTxBufferCount(PFdword* count)
{
#if (PF_UART2_DEBUG == 1)
	CHECK_DEV_INIT(uart2ChInit);
    CHECK_NULL_PTR(count);
#endif	// #if (PF_UART2_DEBUG == 1)    
    *count = uart2TxFifo.count;
    return enStatusSuccess;
}

PFEnStatus pfUart2TxBufferFlush(void)
{
#if (PF_UART2_DEBUG == 1)
	CHECK_DEV_INIT(uart2ChInit);
#endif	// #if (PF_UART2_DEBUG == 1)    
    pfFifoFlush(&uart2TxFifo);
    return enStatusSuccess;
}

#endif	// #if(UART2_USE_FIFO != 0)

void UART2_INT_HANDLER(void)
{
	PFbyte rxByte;
	PFdword intStat = UART2_CHANNEL->IIR;	//pfUart2GetIntStatus();

	if((intStat & 0x04) != 0)		// Data received interrupt
	{
	#if(UART2_USE_FIFO != 0)
		rxByte = UART2_CHANNEL->RBR;
		pfFifoPush(&uart2RxFifo, rxByte);
	#else
		if(uart2RxCallback != 0)
		{
			uart2RxCallback();
		}
		return;
	#endif		// #if(UART2_USE_FIFO != 0)
	}

	if((intStat & 0x02) != 0)		// THR Reg empty interrypt
	{
	#if(UART2_USE_FIFO != 0)
		if(pfFifoIsEmpty(&uart2TxFifo) != enBooleanTrue)
		{
			UART2_CHANNEL->THR = pfFifoPop(&uart2TxFifo);
		}
	#else	
		if(uart2TxCallback != 0)
		{
			uart2TxCallback();
		}
		return;
	#endif		// #if(UART2_USE_FIFO != 0)	
	}
}

#endif	// #if (PF_USE_UART2 == 1)
