#include "prime_framework.h"

#if (PF_USE_ADC == 1)				

#include "prime_utils.h"
#include "prime_sysClk.h"
#include "prime_adc.h"

static PFEnBoolean adcChInit = enBooleanFalse;
static PFcallback adcChCallback = 0;
static PFCfgAdc adcCfg;

PFEnStatus pfAdcOpen(PFpCfgAdc config)
{
#if (PF_ADC_DEBUG == 1)
	CHECK_NULL_PTR(config);
	if(config->clkDiv > 3)
	{
		return enStatusInvArgs;
	}
	if( (config->chInterrupt != 0) && (config->callback == 0) )
	{
		return enStatusInvArgs;	
	}
#endif	// #if (PF_ADC_DEBUG == 1)	
	
	// power on ADC peripheral
	POWER_ON(ADC_CH);
	// set peripheral clock divider
	pfSysSetPclkDiv(PCLK_DIV(ADC_CH), config->clkDiv);
	
	ADC_CHANNEL->ADCR = 0;
	// ADC clock prescaler
	ADC_CHANNEL->ADCR |= (config->prescaler-1) << 8;
	
	// set conversion mode and channel
	if(config->burstMode == enBooleanTrue)
	{
		ADC_CHANNEL->ADCR |= BIT_MASK_16;
		// ADC channel selection
		ADC_CHANNEL->ADCR |= config->burstModeChannels;
	}
	else
	{
		ADC_CHANNEL->ADCR |= 1 << config->singleModeChannel;
		ADC_CHANNEL->ADCR |= config->conversionMode << 24;
	}
	
	// Enable ADC interrupt
	if(config->chInterrupt != 0)
	{
		// set callback
		adcChCallback = config->callback;
		ADC_CHANNEL->ADINTEN = config->chInterrupt;
		NVIC_EnableIRQ(IRQ_NUM(ADC_CH));
	}
	else
	{
		NVIC_DisableIRQ(IRQ_NUM(ADC_CH));
	}
	
	// Disable power down mode
	ADC_CHANNEL->ADCR |= BIT_MASK_21;
	
	pfMemCopy(&adcCfg, config, sizeof(PFCfgAdc));
	adcChInit = enBooleanTrue;
	
	return enStatusSuccess;
}

PFEnStatus pfAdcPowerDown(PFEnBoolean mode)
{
#if (PF_ADC_DEBUG == 1)
	CHECK_DEV_INIT(adcChInit);
#endif	// #if (PF_ADC_DEBUG == 1)
	if(mode == enBooleanTrue)
	{
		ADC_CHANNEL->ADCR &= ~(BIT_MASK_21);
	}
	else
	{
		ADC_CHANNEL->ADCR |= BIT_MASK_21;
	}
	
	return enStatusSuccess;
}

PFEnStatus pfAdcEnableChInterrupt(PFEnAdcChannel channel)
{
#if (PF_ADC_DEBUG == 1)
    CHECK_DEV_INIT(adcChInit);
#endif	// #if (PF_ADC_DEBUG == 1)
	ADC_CHANNEL->ADINTEN |= 1 << channel;
	NVIC_EnableIRQ(IRQ_NUM(ADC_CH));
	return enStatusSuccess;
}

PFEnStatus pfAdcDisableChInterrupt(PFEnAdcChannel channel)
{
#if (PF_ADC_DEBUG == 1)
	CHECK_DEV_INIT(adcChInit);
#endif	// #if (PF_ADC_DEBUG == 1)	
	ADC_CHANNEL->ADINTEN &= ~(1 << channel);
	return enStatusSuccess;
}

PFEnBoolean pfAdcIsIntEnabled(PFEnAdcChannel channel)
{
	if( (ADC_CHANNEL->ADINTEN & (1 << channel)) != 0 )
	{
		return enBooleanTrue;
	}
	else
	{
		return enBooleanFalse;
	}
}

PFEnStatus pfAdcStartBurstConversion(PFbyte channel)
{
#if (PF_ADC_DEBUG == 1)
	CHECK_DEV_INIT(adcChInit);
#endif	// #if (PF_ADC_DEBUG == 1)	
	
	// disable software conversion mode
	ADC_CHANNEL->ADCR &= ~(0x07 << 24);
	
	// set channels for burst conversion
	ADC_CHANNEL->ADCR &= ~(0x0F);
	ADC_CHANNEL->ADCR |= channel;
	
	// enable burst mode
	ADC_CHANNEL->ADCR |= (BIT_MASK_16);
	return enStatusSuccess;
}

PFword pfAdcSetConversionMode(PFEnAdcStart mode)
{
#if (PF_ADC_DEBUG == 1)
	CHECK_DEV_INIT(adcChInit);
#endif	// #if (PF_ADC_DEBUG == 1)
	// clear burst mode if set
	ADC_CHANNEL->ADCR &= ~(BIT_MASK_16);
	// set conversion mode
	ADC_CHANNEL->ADCR &= ~(0x07 << 24);
	ADC_CHANNEL->ADCR |= mode << 24;
	
	return enStatusSuccess;
}

PFEnStatus pfAdcSingleConversion(PFEnAdcChannel channel, PFword* value)
{
#if (PF_ADC_DEBUG == 1)
	CHECK_DEV_INIT(adcChInit);
        if( (value == 0) || (channel > 7) )
        {
            return enStatusInvArgs;
        }
#endif	// #if (PF_ADC_DEBUG == 1)	
	// clear channel number and conversion mode
	ADC_CHANNEL->ADCR &= ~(0x07 << 24);
	ADC_CHANNEL->ADCR &= ~(0x0F);
	
	// set channel number
	ADC_CHANNEL->ADCR |= (1 << channel);
	// start conversion
	ADC_CHANNEL->ADCR |= (enAdcStartNow << 24);
	
	while((ADC_CHANNEL->ADGDR & (BIT_MASK_31)) == 0);
	*value = ADC_CHANNEL->ADGDR >> 4;
	
	return enStatusSuccess;
}

PFEnStatus pfAdcGetVoltageSingleConversion(PFbyte channel, PFdword* milliVolt)
{
	PFword adcVal;
#if (PF_ADC_DEBUG == 1)
	CHECK_DEV_INIT(adcChInit);
        if(milliVolt == 0)
        {
            return enStatusInvArgs;
        }
#endif	// #if (PF_ADC_DEBUG == 1)	
	// clear channel number and conversion mode
	ADC_CHANNEL->ADCR &= ~(0x07 << 24);
	ADC_CHANNEL->ADCR &= ~(0x0F);
	
	// set channel number
	ADC_CHANNEL->ADCR |= (1 << channel);
	// start conversion
	ADC_CHANNEL->ADCR |= (enAdcStartNow << 24);
	
	while((ADC_CHANNEL->ADGDR & (BIT_MASK_31)) == 0);
	adcVal = ADC_CHANNEL->ADGDR >> 4;
	*milliVolt = (adcVal * 805)/1000;
	
	return enStatusSuccess;
}

PFEnStatus pfAdcGetLastChValue(PFbyte channel, PFword* value)
{
#if (PF_ADC_DEBUG == 1)
	CHECK_DEV_INIT(adcChInit);
        if(value == 0)
        {
            return enStatusInvArgs;
        }
	if(channel > 7)	
        {
		return enStatusInvArgs;
        }
#endif	// #if (PF_ADC_DEBUG == 1)	
	
	*value = ADC_CHANNEL->ADDR[channel] >> 4;

	return enStatusSuccess;
}

PFEnStatus pfAdcGetLastGlobalValue(PFword* value)
{
#if (PF_ADC_DEBUG == 1)
	CHECK_DEV_INIT(adcChInit);
        if(value == 0)
        {
            return enStatusInvArgs;
        }
#endif	// #if (PF_ADC_DEBUG == 1)	
	
	*value = ADC_CHANNEL->ADGDR >> 4;
	return enStatusSuccess;
}

PFEnStatus pfAdcClose(void)
{
#if (PF_ADC_DEBUG == 1)
	CHECK_DEV_INIT(adcChInit);
#endif	// #if (PF_ADC_DEBUG == 1)
	// set conversion start mode to none
	ADC_CHANNEL->ADCR &= ~(0x07 << 24);
	// enable power down mode
	ADC_CHANNEL->ADCR &= ~(BIT_MASK_21);
	// power off ADC peripheral
	POWER_OFF(ADC_CH);
	
	adcChInit = enBooleanFalse;
	
	return enStatusSuccess;
}

void ADC_INT_HANDLER(void)
{
	if(adcChCallback != 0)
	{
		adcChCallback();
	}
}

#endif	// #if (PF_USE_ADC == 1)
