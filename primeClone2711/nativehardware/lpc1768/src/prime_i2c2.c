#include "prime_framework.h"

#if (PF_USE_I2C2 == 1)

#include "prime_utils.h"
#include "prime_sysClk.h"
#include "prime_i2c2.h"

#if(I2C2_USE_FIFO != 0)
#include "prime_fifo.h"
	#warning I2C2 FIFO is enabled for interrupt based communication
	#if( (I2C2_BUFFER_SIZE == 0) || ((I2C2_BUFFER_SIZE & (I2C2_BUFFER_SIZE - 1)) != 0) )
		#error I2C2_BUFFER_SIZE cannot be zero. I2C2_BUFFER_SIZE should be power of 2
	#endif
#endif	// #if(I2C2_USE_FIFO != 0)

// I2C2 control bits	
#define I2C2_AA_FLAG				0x04			
#define I2C2_INT_FLAG				0x08
#define I2C2_STOP_FLAG				0x10
#define I2C2_START_FLAG				0x20
#define I2C2_EN_FLAG				0x40	

#define I2C2_SET_CTRL_BIT(BIT)		I2C2_CHANNEL->I2CONSET = BIT
#define I2C2_CLR_CTRL_BIT(BIT)		I2C2_CHANNEL->I2CONCLR = BIT
#define I2C2_GET_STATE()				(I2C2_CHANNEL->I2STAT >> 3)	
		
#define I2C2_SET_AA()				I2C2_SET_CTRL_BIT(I2C2_AA_FLAG)
#define I2C2_CLR_AA()				I2C2_CLR_CTRL_BIT(I2C2_AA_FLAG)
	
#define I2C2_SET_START()				I2C2_SET_CTRL_BIT(I2C2_START_FLAG)
#define I2C2_CLR_START()				I2C2_CLR_CTRL_BIT(I2C2_START_FLAG)

#define I2C2_SET_STOP()				I2C2_SET_CTRL_BIT(I2C2_STOP_FLAG)
#define I2C2_CLR_STOP()				I2C2_CLR_CTRL_BIT(I2C2_STOP_FLAG)	

#define I2C2_CLR_INT()				I2C2_CLR_CTRL_BIT(I2C2_INT_FLAG)
#define I2C2_CALL_SLAVE(ADDR, RW)	I2C2_CHANNEL->I2DAT = ADDR | RW


// State codes for I2C2 communication
// Master mode
#define I2C2_M_START					0x08
#define I2C2_M_REP_START				0x10
#define I2C2_M_ARB_LOST				0x38
// Master transmitter
#define I2C2_M_SLA_W_TX_ACK			0x18
#define I2C2_M_SLA_W_TX_NACK			0x20
#define I2C2_M_DATA_TX_ACK			0x28
#define I2C2_M_DATA_TX_NACK			0x30
// Master receiver
#define I2C2_M_SLA_R_ACK				0x40
#define I2C2_M_SLA_R_NACK			0x48
#define I2C2_M_DATA_RX_ACK			0x50
#define I2C2_M_DATA_RX_NACK			0x58

// Slave receiver
#define I2C2_S_SLA_W_RX_ACK			0x60
#define I2C2_S_ARB_LOST_SLA_W_RX		0x68
#define I2C2_S_GEN_CALL_RX_ACK		0x70								
#define	I2C2_S_ARB_LOST_GC_RX		0x78
#define I2C2_S_SLA_W_DATA_RX_ACK		0x80
#define I2C2_S_SLA_W_DATA_RX_NACK	0x88
#define I2C2_S_GC_DATA_RX_ACK		0x90
#define I2C2_S_GC_DATA_RX_NACK		0x98
#define I2C2_S_STOP_REP_START		0xA0
// Slave trasmiter
#define I2C2_S_SLA_R_RX_ACK			0xA8
#define I2C2_S_ARB_LOST_SLA_R_RX		0xB0
#define I2C2_S_DATA_TX_ACK			0xB8
#define I2C2_S_DATA_TX_NACK			0xC0
#define I2C2_S_LAST_DATA_TX_NACK		0xC8

static PFEnBoolean i2c2ChError = enBooleanFalse;		// transmission error flag
static PFbyte i2c2ChRWFlag = 0;						// read/write flag
static PFbyte i2c2SlaveAddr = 0;						// slave addr to call
static PFdword i2c2BytesToRead = 0;					// bytes to read from slave
static PFdword i2c2BytesRead = 0;					// bytes actually read from I2C2 bus
static PFEnBoolean i2c2ChInit = enBooleanFalse;		// initalize flag for the channel
static PFEnBoolean i2c2ChBusy = enBooleanFalse;		// busy flag for the channel
static PFbyte i2c2ChInt;								// interrupt flag for the channel
#if(I2C2_USE_FIFO != 0)
static PFbyte i2c2RxBuffer[I2C2_BUFFER_SIZE];			// I2C2 transmit buffer
static PFbyte i2c2TxBuffer[I2C2_BUFFER_SIZE];			// I2C2 receive buffer
static PFFifo i2c2TxFifo;							// I2C2 transmit fifo structure
static PFFifo i2c2RxFifo;							// I2C2 receive fifo structure
#else	
static PFcallback i2c2Callback = 0;					// transmit callback for the channel
#endif	// #if(I2C2_USE_FIFO != 0)
PFCfgI2c2 i2c2Cfg;	
	

PFEnStatus pfI2c2Open(PFpCfgI2c2 config)
{
	PFdword tHigh = 0, tLow = 0, tTot = 0;
#if (PF_I2C2_DEBUG == 1)
	// Validate config pointer
	CHECK_NULL_PTR(config);
	if( (config->clkDiv > 3) || (config->baudrate > I2C2_MAX_BAUDRATE) )
	{
		return enStatusInvArgs;
	}
#if(I2C2_USE_FIFO == 0)
	CHECK_NULL_PTR(config->callback);
#endif	
#endif	// #if (PF_I2C2_DEBUG == 1)	
	
	// power on I2C2 module
	POWER_ON(I2C2_CH);
	// set peripheral clock
	pfSysSetPclkDiv(PCLK_DIV(I2C2_CH), config->clkDiv);
	
	// set baudrate and duty cycle
	tTot = pfSysGetPclk(PCLK_DIV(I2C2_CH)) / config->baudrate;
	tHigh = (tTot * config->dutyCycle) / 100;
	tLow = tTot - tHigh;
	I2C2_CHANNEL->I2SCLH = tHigh;
	I2C2_CHANNEL->I2SCLL = tLow;
	
	// set own I2C2 addresses
	if(config->enableGenCall == enBooleanTrue)
	{
		config->ownAddress[0] |= 0x01;
	}
	I2C2_CHANNEL->I2ADR0 = config->ownAddress[0];
	I2C2_CHANNEL->I2ADR1 = config->ownAddress[1];
	I2C2_CHANNEL->I2ADR2 = config->ownAddress[2];
	I2C2_CHANNEL->I2ADR3 = config->ownAddress[3];
	
	// set address masks
	I2C2_CHANNEL->I2MASK0 = config->addrMask[0];
	I2C2_CHANNEL->I2MASK1 = config->addrMask[1];
	I2C2_CHANNEL->I2MASK2 = config->addrMask[2];
	I2C2_CHANNEL->I2MASK3 = config->addrMask[3];
	
	// set ack enable
	if(config->enableAck)
	{
		I2C2_SET_AA();
	}
	
	// initialize FIFO
#if(I2C2_USE_FIFO != 0)
	pfFifoInit(&i2c2TxFifo, i2c2TxBuffer, I2C2_BUFFER_SIZE);
	pfFifoInit(&i2c2RxFifo, i2c2RxBuffer, I2C2_BUFFER_SIZE);
#else
	i2c2Callback = config->callback;
#endif	// #if(I2C2_USE_FIFO != 0)

	if(config->intEnable)
	{
		i2c2ChInt = enBooleanTrue;
		NVIC_EnableIRQ(IRQ_NUM(I2C2_CH));
	}
	
	// enable I2C2 interface
	I2C2_SET_CTRL_BIT(I2C2_EN_FLAG);
	
	pfMemCopy(&i2c2Cfg, config, sizeof(PFCfgI2c2));
	i2c2ChInit = enBooleanTrue;
	return enStatusSuccess;
}	

PFEnStatus pfI2c2Close(void)
{
#if (PF_I2C2_DEBUG == 1)
	CHECK_DEV_INIT(i2c2ChInit);
#endif	// #if (PF_I2C2_DEBUG == 1)	
	I2C2_CLR_CTRL_BIT(I2C2_EN_FLAG);
	POWER_OFF(I2C2_CH);
	i2c2ChInit = enBooleanFalse;
	return enStatusSuccess;
}

PFEnStatus pfI2c2IntEnable(void)
{
#if (PF_I2C2_DEBUG == 1)
	CHECK_DEV_INIT(i2c2ChInit);
#endif	// #if (PF_I2C2_DEBUG == 1)	
	i2c2ChInt = enBooleanTrue;
	NVIC_EnableIRQ(IRQ_NUM(I2C2_CH));
	return enStatusSuccess;
}

PFEnStatus pfI2c2IntDisable(void)
{
#if (PF_I2C2_DEBUG == 1)
	CHECK_DEV_INIT(i2c2ChInit);
#endif	// #if (PF_I2C2_DEBUG == 1)	
	NVIC_DisableIRQ(IRQ_NUM(I2C2_CH));
	i2c2ChInt = enBooleanFalse;
	return enStatusSuccess;
}

PFEnStatus pfI2c2SetControlFlag(PFbyte flag)
{
#if (PF_I2C2_DEBUG == 1)
	CHECK_DEV_INIT(i2c2ChInit);
#endif	// #if (PF_I2C2_DEBUG == 1)	
	I2C2_SET_CTRL_BIT(flag);
	return enStatusSuccess;
}

PFEnStatus pfI2c2ClearControlFlag(PFbyte flag)
{
#if (PF_I2C2_DEBUG == 1)
	CHECK_DEV_INIT(i2c2ChInit);
#endif	// #if (PF_I2C2_DEBUG == 1)	
	I2C2_CLR_CTRL_BIT(flag);
	return enStatusSuccess;
}

PFEnStatus pfI2c2GetState(PFbyte* state)
{
#if (PF_I2C2_DEBUG == 1)
	CHECK_DEV_INIT(i2c2ChInit);
	CHECK_NULL_PTR(state);
#endif	// #if (PF_I2C2_DEBUG == 1)	
	*state = I2C2_GET_STATE();
	return enStatusSuccess;
}


PFEnStatus pfI2c2Start( void )
{

#if (PF_I2C2_DEBUG == 1)
	CHECK_DEV_INIT(i2c2ChInit);

	if(i2c2ChBusy == enBooleanTrue)
		return enStatusBusy;	
#endif	// #if (PF_I2C2_DEBUG == 1)	
	
	I2C2_SET_START();
	return enStatusSuccess ;
}
PFEnStatus pfI2c2Stop( void )
{

#if (PF_I2C2_DEBUG == 1)
	CHECK_DEV_INIT(i2c2ChInit);
#endif	// #if (PF_I2C2_DEBUG == 1)

	I2C2_SET_STOP();
	I2C2_CLR_INT();
	return enStatusSuccess ;
}


static PFEnStatus pfDataProcessing(PFbyte* data, PFword size)
{
	volatile PFbyte  status;
	PFword index=0;
	 
	while(1)
	{
		
		while(!(I2C2_CHANNEL->I2CONSET & I2C2_INT_FLAG));
		status = I2C2_CHANNEL->I2STAT;
		switch(status)
		{
//
// Master mode
//
				case I2C2_M_START:
				case I2C2_M_REP_START:
					I2C2_CALL_SLAVE(i2c2SlaveAddr, i2c2ChRWFlag);
					I2C2_CLR_START();
					break;
//
// Master transmitter
//
				case I2C2_M_SLA_W_TX_ACK:
				case I2C2_M_DATA_TX_ACK:
					if( index < size )
					{
							I2C2_CHANNEL->I2DAT = *(data + index);
							index++;
					}
					else
					{
						i2c2ChBusy = enBooleanFalse;
						return enStatusSuccess ;
					}										
					break;

//
// Master transmitter error conditions
//
				case I2C2_M_ARB_LOST:
				case I2C2_M_SLA_W_TX_NACK:
				case I2C2_M_DATA_TX_NACK:
					i2c2ChBusy = enBooleanFalse;
					I2C2_SET_STOP();
					return enStatusError ;
					//break;
//
// Master receiver
//
				case I2C2_M_SLA_R_NACK:
					i2c2ChBusy = enBooleanFalse;
					return enStatusError ;
					//break;
			
				case I2C2_M_SLA_R_ACK:
					// do nothing
					break;

				case I2C2_M_DATA_RX_ACK:
					*(data + index) = I2C2_CHANNEL->I2DAT;
					index++;
					i2c2BytesRead++;

					if(i2c2BytesRead >= i2c2BytesToRead -1)
					{
						I2C2_CLR_AA();
						return enStatusSuccess ;
					}
					break;


				case I2C2_M_DATA_RX_NACK:
					*(data + index) = I2C2_CHANNEL->I2DAT;
					i2c2BytesRead++;
					index++;
					i2c2ChBusy = enBooleanFalse;
					return enStatusSuccess ;
					//break;

//
// Slave receiver
//
				case I2C2_S_SLA_W_RX_ACK:
				case I2C2_S_ARB_LOST_SLA_W_RX:
				case I2C2_S_GEN_CALL_RX_ACK:
				case I2C2_S_ARB_LOST_GC_RX:
				case I2C2_S_STOP_REP_START:
					break;

				case I2C2_S_SLA_W_DATA_RX_ACK:
				case I2C2_S_GC_DATA_RX_ACK:
				case I2C2_S_SLA_W_DATA_RX_NACK:
				case I2C2_S_GC_DATA_RX_NACK:
					*(data + index) = I2C2_CHANNEL->I2DAT;
					index++;
					break;
			
//
// Slave transmitter
//
				case I2C2_S_SLA_R_RX_ACK:
				case I2C2_S_ARB_LOST_SLA_R_RX:
				case I2C2_S_DATA_TX_ACK:
					if( index < size )
					{
							I2C2_CHANNEL->I2DAT = *(data + index);
							index++;
					}
					else
					{
						I2C2_CHANNEL->I2DAT = 0;
						return enStatusSuccess ;
					}
					break;

				case I2C2_S_DATA_TX_NACK:
				case I2C2_S_LAST_DATA_TX_NACK:
					i2c2ChBusy = enBooleanFalse;
					return enStatusSuccess ;
					//break;

				default:
					i2c2ChBusy = enBooleanFalse;
					return enStatusError;							
					//	break;
			}
			
		I2C2_CLR_INT();
	}
}


PFEnStatus pfI2c2Write(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFdword size)
{
	PFdword index;
	PFEnStatus status;
	
#if (PF_I2C2_DEBUG == 1)
	CHECK_DEV_INIT(i2c2ChInit);
	CHECK_NULL_PTR(data);
	if(size == 0)
	{
			return enStatusInvArgs;
	}
	if(i2c2ChBusy == enBooleanTrue)
		return enStatusBusy;	
#endif	// #if (PF_I2C2_DEBUG == 1)	

	i2c2ChBusy = enBooleanTrue;
	i2c2SlaveAddr = slaveAddr;
	i2c2ChRWFlag = 0;	
	
	// USING INTERRUPT ======================================================================================
	if( i2c2ChInt != enBooleanFalse)           
	{	
#if(I2C2_USE_FIFO != 0)
			
		for(index = 0; index < size; index++)
		{
			pfFifoPush(&i2c2TxFifo, *(data + index));
		}
		if(master == enBooleanTrue)
		{
			I2C2_SET_START();
		}
		else
		{
			I2C2_SET_AA();
		}
		while(i2c2ChBusy == enBooleanTrue);
		
		if(i2c2ChError == enBooleanTrue)
		{
			i2c2ChError = enBooleanFalse;
			return enStatusError;
		}
#else
		return enStatusNotSupported;
#endif	// #if(I2C2_USE_FIFO != 0)	
	}
	
	// USING POLLING ======================================================================================
	else
	{
		status = pfDataProcessing(data,size);	
		if(status != enStatusSuccess)
				return status;
		
	}
	return enStatusSuccess;
}

PFEnStatus pfI2c2Read(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFdword size, PFdword* readBytes)
{
	PFdword index = 0;
	PFEnStatus status;
	
#if (PF_I2C2_DEBUG == 1)
	CHECK_DEV_INIT(i2c2ChInit);
	CHECK_NULL_PTR(data);
	CHECK_NULL_PTR(readBytes);
	if(size == 0)
	{
		return enStatusInvArgs;
	}
	if(i2c2ChBusy == enBooleanTrue)
		return enStatusBusy;	
#endif	// #if (PF_I2C2_DEBUG == 1)	

	
	i2c2ChBusy = enBooleanTrue;
	i2c2SlaveAddr = slaveAddr;
	i2c2ChRWFlag = 1;		
	i2c2BytesToRead = size;
	
	// USING INTERRUPT ======================================================================================
	if( i2c2ChInt != enBooleanFalse)           
	{	
#if(I2C2_USE_FIFO != 0)
			if(master != enBooleanTrue)
			{
				for(index = 0; index < size; index++)
				{
					if(pfFifoIsEmpty(&i2c2RxFifo) == enBooleanTrue)
						break;
					*(data + index) = pfFifoPop(&i2c2RxFifo);
				}
				*readBytes = index;
				return enStatusSuccess;
			}
			
			if(size > 1)
			{
				I2C2_SET_AA();
			}
			else
			{
				I2C2_CLR_AA();
			}
			I2C2_SET_START();
			
			while(i2c2ChBusy == enBooleanTrue);
		
			I2C2_CLR_AA();
			
			for(index = 0; index < i2c2BytesRead; index++)
			{
				*(data + index) = pfFifoPop(&i2c2RxFifo);
			}
			*readBytes = i2c2BytesRead;
			i2c2BytesRead = 0;
			
			if(i2c2ChError == enBooleanTrue)
			{
				i2c2ChError = enBooleanFalse;
				return enStatusError;
			}
#endif	// #if(I2C2_USE_FIFO != 0)	

	}
	
	// USING POLLING ======================================================================================
	else
	{
		status = pfDataProcessing(data,size);	
		if(status != enStatusSuccess)
				return status;
		
		I2C2_CLR_AA();		
		*readBytes = i2c2BytesRead;
		i2c2BytesRead = 0;
	}			
	return enStatusSuccess;
}

#if(I2C2_USE_FIFO != 0)

PFEnStatus pfI2c2GetRxBufferCount(PFdword* count)
{
#if (PF_I2C2_DEBUG == 1)
	CHECK_DEV_INIT(i2c2ChInit);
	CHECK_NULL_PTR(count);
#endif	// #if (PF_I2C2_DEBUG == 1)	
	*count = i2c2RxFifo.count;
	return enStatusSuccess;
}

PFEnStatus pfI2c2TxBufferFlush(void)
{
#if (PF_I2C2_DEBUG == 1)
	CHECK_DEV_INIT(i2c2ChInit);
#endif	// #if (PF_I2C2_DEBUG == 1)	
	pfFifoFlush(&i2c2TxFifo);
	return enStatusSuccess;
}

PFEnStatus pfI2c2RxBufferFlush(void)
{
#if (PF_I2C2_DEBUG == 1)
	CHECK_DEV_INIT(i2c2ChInit);
#endif	// #if (PF_I2C2_DEBUG == 1)	
	pfFifoFlush(&i2c2RxFifo);
	return enStatusSuccess;
}
#endif	// #if(I2C2_USE_FIFO != 0)	

void I2C2_INT_HANDLER(void)
{
#if(I2C2_USE_FIFO != 0)
	PFbyte status = I2C2_CHANNEL->I2STAT;
	switch(status)
	{
		//
		// Master mode
		//
		case I2C2_M_START:
		case I2C2_M_REP_START:
			I2C2_CALL_SLAVE(i2c2SlaveAddr, i2c2ChRWFlag);
			I2C2_CLR_START();
			break;
		//
		// Master transmitter
		//
		case I2C2_M_SLA_W_TX_ACK:
		case I2C2_M_DATA_TX_ACK:
			if(pfFifoIsEmpty(&i2c2TxFifo) != enBooleanTrue)
			{
				I2C2_CHANNEL->I2DAT = pfFifoPop(&i2c2TxFifo);
			}
			else
			{
				i2c2ChError= enBooleanFalse;
				i2c2ChBusy = enBooleanFalse;
				I2C2_SET_STOP();
			}
			break;
		
		//
		// Master transmitter error conditions
		//
		case I2C2_M_ARB_LOST:
		case I2C2_M_SLA_W_TX_NACK:
		case I2C2_M_DATA_TX_NACK:
			i2c2ChError= enBooleanTrue;
			i2c2ChBusy = enBooleanFalse;
			I2C2_SET_STOP();
			break;
		
		//
		// Master reciever
		//
		case I2C2_M_SLA_R_NACK:
			i2c2ChError= enBooleanTrue;
			i2c2ChBusy = enBooleanFalse;
			I2C2_SET_STOP();
			break;
		
		case I2C2_M_SLA_R_ACK:
			// do nothing
			break;

		case I2C2_M_DATA_RX_ACK:
			pfFifoPush(&i2c2RxFifo, I2C2_CHANNEL->I2DAT);
			i2c2BytesRead++;
			if(i2c2BytesRead >= i2c2BytesToRead -1)
			{
				I2C2_CLR_AA();
			}
			break;

		case I2C2_M_DATA_RX_NACK:
			pfFifoPush(&i2c2RxFifo, I2C2_CHANNEL->I2DAT);
			i2c2BytesRead++;
			i2c2ChError= enBooleanFalse;
			i2c2ChBusy = enBooleanFalse;
			I2C2_SET_STOP();
			break;
		
		//
		// Slave receiver
		//
		case I2C2_S_SLA_W_RX_ACK:
		case I2C2_S_ARB_LOST_SLA_W_RX:
		case I2C2_S_GEN_CALL_RX_ACK:
		case I2C2_S_ARB_LOST_GC_RX:
		case I2C2_S_STOP_REP_START:
			break;

		case I2C2_S_SLA_W_DATA_RX_ACK:
		case I2C2_S_GC_DATA_RX_ACK:
		case I2C2_S_SLA_W_DATA_RX_NACK:
		case I2C2_S_GC_DATA_RX_NACK:
			pfFifoPush(&i2c2RxFifo, I2C2_CHANNEL->I2DAT);
			break;
		
		//
		// Slave trasmitter
		//
		case I2C2_S_SLA_R_RX_ACK:
		case I2C2_S_ARB_LOST_SLA_R_RX:
		case I2C2_S_DATA_TX_ACK:
			if(pfFifoIsEmpty(&i2c2TxFifo) != enBooleanTrue)
			{
				I2C2_CHANNEL->I2DAT = pfFifoPop(&i2c2TxFifo);
			}
			else
			{
				I2C2_CHANNEL->I2DAT = 0;
			}
			break;

		case I2C2_S_DATA_TX_NACK:
		case I2C2_S_LAST_DATA_TX_NACK:
			i2c2ChError= enBooleanFalse;
			i2c2ChBusy = enBooleanFalse;
			break;

		default:
			i2c2ChError= enBooleanTrue;
			i2c2ChBusy = enBooleanFalse;
			break;
	}

	I2C2_CLR_INT();
#else
	if(i2c2Callback != 0)
	{
		i2c2Callback();
	}
#endif	// #if(I2C2_USE_FIFO != 0)
}

#endif	// #if (PF_USE_I2C2 == 1)

