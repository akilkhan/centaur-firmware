#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_sysClk.h"

static PFCfgClk clkConfig;

static void pfPllFeedSequence(void)
{
	PERIPH_SC->PLL0FEED = 0xAA;
	PERIPH_SC->PLL0FEED = 0x55;
}

PFEnStatus pfSysSetCpuClock(PFpCfgClk config)
{
	PFdword pllPreDiv, pllMul, pllDiv, temp, loop, preDivStatus = 0;
	PFEnStatus status;
#if (PF_SYSCLK_DEBUG == 1)		
        CHECK_NULL_PTR(config);
	if(config->pllClkSrc > 2)
        {
		return enStatusInvArgs;
        }
	if(config->cpuFreqHz > 100000000)
        {
		return enStatusInvArgs;
        }
#endif	// #if (PF_SYSCLK_DEBUG == 1)

	pfMemCopy(&clkConfig, config, sizeof(PFCfgClk));

	pllDiv = (300*1000000) / config->cpuFreqHz;
	temp = ((config->cpuFreqHz * pllDiv) / (2 * config->oscFreq));
        
	for(loop = temp; loop <= (config->oscFreq/1000000); loop++)
	{
		if( ((config->oscFreq/1000000) % loop) == 0 )
		{
			pllPreDiv = loop;
			preDivStatus = 1;
			break;
		}
	}
	if(!preDivStatus)
	{
		return enStatusError;
	}
	
	pllMul = ((config->cpuFreqHz * pllDiv * pllPreDiv) / 24) / 1000000;	
	
	status = pfPllConfig(pllPreDiv ,pllMul ,pllDiv, config->pllClkSrc);
	return status;
}

PFdword pfSysGetCpuClock (void)
{
	PFdword pllPreDiv, pllMul, pllDiv, srcClkFreq, freq, clkSrc;
	
	pllMul = (PERIPH_SC->PLL0STAT & 0x7FFF) + 1;
	pllPreDiv = ((PERIPH_SC->PLL0STAT >> 16) & 0xFF) + 1;
	pllDiv = (PERIPH_SC->CCLKCFG & 0xFF)+ 1;
	clkSrc = PERIPH_SC->CLKSRCSEL & 0x03;			

	switch (clkSrc) 
	{
                default:                          	// Reserved
		case 0:                           	// Internal RC oscillator
                        srcClkFreq = 4000000;
                	break;
		
                case 1:                           	// Main oscillator
			srcClkFreq = clkConfig.oscFreq;
                        break;

                case 2:                           	// RTC oscillator
			srcClkFreq = 32768;
                        break;
	}	

        if ( ((PERIPH_SC->PLL0STAT >> 24) & 3) == 3 ) 	// PLL0 connected 
	{
            freq = ( (srcClkFreq / pllPreDiv) * (2 * pllMul) ) / pllDiv;
        }
        else
        {
            freq = (srcClkFreq / pllDiv);
        }
        return freq;
}


PFEnStatus pfPllConfig(PFdword pllPreDivider,PFdword pllMultiplier,PFdword cpuClkDivider,PFdword pllClkSrc)
{
	PFdword pllStatus;
#if (PF_SYSCLK_DEBUG == 1)
	if(pllClkSrc > 2)
        {
		return enStatusInvArgs;
        }
#endif	// #if (PF_SYSCLK_DEBUG == 1)

	pllStatus = PERIPH_SC->PLL0STAT;
	PERIPH_SC->SCS = 0x20;							// Enable main oscillator
	while ((PERIPH_SC->SCS & BIT_MASK_6) == 0);			// Wait for Oscillator to be ready
  
	if(pllStatus & BIT_MASK_25)							// if PLL is connected, disconnect it
	{
		//Disconnect PLL
		PERIPH_SC->PLL0CON &= ~0x01;					
		pfPllFeedSequence();
	}
	if(pllStatus & BIT_MASK_24)							// if PLL is enabled, disable it
	{
		//Disable PLL
		PERIPH_SC->PLL0CON &= ~0x02;
		pfPllFeedSequence();
	}
	PERIPH_SC->CLKSRCSEL = pllClkSrc;				// Select main oscillator as clock source for PLL

	PERIPH_SC->PLL0CFG = ((pllPreDivider-1) << 16) | (pllMultiplier-1);	
	pfPllFeedSequence();

	PERIPH_SC->PLL0CON = 0x01;						// Enable PLL
	pfPllFeedSequence();

	PERIPH_SC->CCLKCFG = cpuClkDivider-1;			// CPU clock divider
	
	PERIPH_SC->PCLKSEL0 = 0x00;     				// Peripheral Clock Selection CCLK / 4
	PERIPH_SC->PCLKSEL1 = 0x00;

	while( (PERIPH_SC->PLL0STAT & BIT_MASK_26) == 0 );	// Wait for PLL to achieve lock
	
	PERIPH_SC->PLL0CON = 0x03;						// Connect PLL
	pfPllFeedSequence();		

	//LPC_SC->CLKOUTCFG =  0x00;

	pllStatus = PERIPH_SC->PLL0STAT;
	pllStatus = 0;
	
	return enStatusSuccess;
}

// Sets the PLL and CPU clock divider to run the CPU at 100MHZ with 12MHz external crystal
void pfSysClockInit_100(void)
{
	unsigned long pllStatus;

	pllStatus = PERIPH_SC->PLL0STAT;

	PERIPH_SC->SCS = 0x20;							// Enable main oscillator
   	while ((PERIPH_SC->SCS & (1<<6)) == 0);		// Wait for Oscillator to be ready
  
	if(pllStatus & (1<<25))						// if PLL is connected, disconnect it
	{
		//Disconnect PLL
		PERIPH_SC->PLL0CON &= ~0x01;					
		pfPllFeedSequence();
	}

	if(pllStatus & (1<<24))						// if PLL is enabled, disable it
	{
		//Disable PLL
		PERIPH_SC->PLL0CON &= ~0x02;
		pfPllFeedSequence();
	}

	PERIPH_SC->CLKSRCSEL = 0x01;					// Select main oscillator as clock source for PLL
#if (F_CPU == 60000000)
	PERIPH_SC->PLL0CFG = (0<<16) | 19;		// PLL divider  = 1		PLL multiplier = 20
#else
#if (F_CPU == 100000000)
	PERIPH_SC->PLL0CFG = (11 << 16) | 149;		// PLL divider  = 11 + 1		PLL multiplier = 149 + 1
#endif	// #if (F_CPU == 100000000)
#endif	// #if (F_CPU == 60000000)
	pfPllFeedSequence();

	PERIPH_SC->PLL0CON = 0x01;						// Enable PLL
	pfPllFeedSequence();

#if (F_CPU == 60000000)
	PERIPH_SC->CCLKCFG = 0x07;						// CPU clock divider: 8 - 1 = 7
#else
#if (F_CPU == 100000000)
	PERIPH_SC->CCLKCFG = 0x02;						// CPU clock divider: 2 + 1
#endif	// #if (F_CPU == 100000000)
#endif	// #if (F_CPU == 60000000)
	
	PERIPH_SC->PCLKSEL0 = 0x00;     				// Peripheral Clock Selection CCLK / 4
  	PERIPH_SC->PCLKSEL1 = 0x00;

	while( (PERIPH_SC->PLL0STAT & (1<<26)) == 0 );	// Wait for PLL to achieve lock
	
	PERIPH_SC->PLL0CON = 0x03;						// Connect PLL
	pfPllFeedSequence();		

	//LPC_SC->CLKOUTCFG =  0x00;

	pllStatus = PERIPH_SC->PLL0STAT;
	pllStatus = 0;
}


void pfSysSetPclkDiv(PFdword peripheral, PFdword clock)
{
	if(peripheral > 30)
	{
		PERIPH_SC->PCLKSEL1 &= ~(0x03 << (peripheral-32));
		PERIPH_SC->PCLKSEL1 |= (clock << (peripheral-32));
	}
	else
	{
		PERIPH_SC->PCLKSEL0 &= ~(0x03 << peripheral);
		PERIPH_SC->PCLKSEL0 |= (clock << peripheral);
	}
}

PFdword pfSysGetPclk(PFdword peripheral)
{
	const PFbyte pclkDiv[] = {4, 1, 2, 8};
	PFdword divIndex;
	PFdword freq = pfSysGetCpuClock();

	if(peripheral > 30)
	{
		divIndex = PERIPH_SC->PCLKSEL1 & (0x03 << (peripheral-32));
		divIndex = divIndex >> (peripheral-32);
	}
	else
	{
		divIndex = PERIPH_SC->PCLKSEL0 & (0x03 << (peripheral));
		divIndex = divIndex >> (peripheral);
	}
	
	freq = freq / pclkDiv[divIndex];
	return freq;
}
