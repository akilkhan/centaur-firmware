/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework USB common APIs for LPC17xx.
 * 
 *
 * Review status: NO
 *
 */
#pragma once

/**
 * \defgroup PF_USBCOMMON_API USB COMMON API
 * @{
 */ 
 
/** USB configuration macros 		*/
#define USB_CH				USB
#define USB_CHANNEL			PERIPH(USB_CH)
#define USB_INT_HANDLER		INT_HANDLER(USB_CH) 

typedef enum
{
	enUsbModeIdle = 0,
	enUsbModeHost,
	enUsbModeDevice,
	enUsbModeOTG
}PFEnUsbMode;

typedef struct
{
	int a;
}PFCfgUsb;

typedef PFCfgUsb*	PFpCfgUsb;


PFEnStatus pfUsbOpen(void);

void pfUsbClose(void);

void pfUsbSetMode(PFEnUsbMode usbMode);

PFEnUsbMode pfUsbGetMode(void);

PFEnStatus pfUsbIntEnable(void);

PFEnStatus pfUsbIntDisable(void);

PFEnStatus pfUsbSetDeviceCallback(PFcallback deviceCallback);

PFEnStatus pfUsbSetHostCallback(PFcallback hostCallback);

PFEnStatus pfUsbDeviceClockEnable(void);

PFEnStatus pfUsbEnableDeviceInterrupts(PFdword devInt);

PFEnStatus pfUsbEnableEpInterrupts(PFdword epInt);

PFEnStatus pfUsbClearDeviceInterrupts(PFdword devInt);

PFEnStatus pfUsbClearEpInterrupts(PFdword epInt);

PFEnStatus pfUsbSetEpInterruptPriority(PFdword epIntPriority);

PFEnStatus pfUsbEnableDmaInterrupts(PFdword dmaInterrupts);

void pfUsbSoftconnect(PFEnBoolean connectStatus);

//PFEnStatus pfUsbDeviceReset(void);

void pfUsbSIEWriteCommamd(PFdword cmd) ;

void pfUsbSIEWriteCommamdData(PFdword cmd, PFdword val) ;

PFdword pfUsbSIEReadCommandData(PFdword cmd);

void pfUsbRealizeEndpoint(PFbyte phyEpNum, PFdword maxPacketSize);




/** @} */

