#include "prime_framework.h"
#if (PF_USE_WDT == 1)
#include "prime_wdt.h"

#define PRIME_WDT_WDTCSR_WDTIF			((PFdword)(1<<7))

static PFCfgWatchdog watchdogConfigStruct={0};
static PFbyte initflag=0;

 PFEnStatus pfWatchdogOpen(PFpCfgWatchdog config)
{ 
	PFbyte temp = 0;
#ifdef PF_WDT_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	} 
	if(config->mode == enWDTintrReset)
	{
	 if(config->callback ==0)
	 {
		return enStatusInvArgs;
	 }
	}	 
#endif

	asm("push r0");
	if(config->mode == 2 )
	{
		temp = 0x08;		
	}
	else
	{
		temp = 0x48;
	}
	if(config->timeout == 8)
	temp |= (1<<5);
	else if(config->timeout == 9 )
	temp |= ((1<<5)|(1<<0)); 
	else
	temp |= config->timeout;
	asm("mov r0, r24");
	__asm__ __volatile__("ldi r24,0x00");
	__asm__ __volatile__("ldi r25,0x18");
	__asm__ __volatile__ ("wdr");
	__asm__ __volatile__("ldi r16,0x00");
	__asm__ __volatile__("sts 0x0054, r16");
    __asm__ __volatile__("sts 0x0060, r25");
	__asm__ __volatile__("sts 0x0060, r0");
    asm("pop r0");
	pfMemCopy(&watchdogConfigStruct,config,sizeof(PFCfgWatchdog ));
	initflag = 1;
	return enStatusSuccess;
}

PFEnStatus pfWatchdogStart(void)
{
	PFbyte temp = 0;
#ifdef PF_WDT_DEBUG
		if(initflag == 0)
		{
			return enStatusNotConfigured
		}
#endif 	

	asm("push r0");
	if(watchdogConfigStruct.mode == 2 )
	{
		temp = 0x08;
	}
	else
	{
		temp = 0x48;
	}
	if(watchdogConfigStruct.timeout == 8)
	temp |= (1<<5);
	else if(watchdogConfigStruct.timeout == 9 )
	temp |= ((1<<5)|(1<<0));
	else
	temp |= watchdogConfigStruct.timeout;
	asm("mov r0, r24");
	__asm__ __volatile__("ldi r24,0x00");
	__asm__ __volatile__("ldi r25,0x18");
	__asm__ __volatile__ ("wdr");
	__asm__ __volatile__("ldi r16,0x00");
	__asm__ __volatile__("sts 0x0054, r16");
	__asm__ __volatile__("sts 0x0060, r25");
	__asm__ __volatile__("sts 0x0060, r0");
	 asm("pop r0");
	return enStatusSuccess;
}

PFEnStatus pfWatchdogClose(void)
{
#ifdef PF_WDT_DEBUG
	if(initflag == 0)
	{
		return enStatusNotConfigured
	}
#endif
	__asm__ __volatile__("ldi r24,0x00");
	__asm__ __volatile__("ldi r25,0x18");
	__asm__ __volatile__("sts 0x0060, r24");
	initflag=0;
	return enStatusSuccess;
}

PFEnStatus pfWatchdogChkIntrFlag(PFEnBoolean *flagStatus)
{
#ifdef PF_WDT_DEBUG
	if(initflag == 0)
	{
		return enStatusNotConfigured
	}
#endif
	if(((PERIPH_WDT->PFWDTCSR & PRIME_WDT_WDTCSR_WDTIF) >>7) == 0x01)
	{
		*flagStatus = enBooleanTrue;	
	}
	else
	{
		*flagStatus = enBooleanFalse;
	}
	return enStatusSuccess;
}

PFEnStatus pfWatchdogUpdateTimeout(PFbyte timeout)
{
	PFbyte temp = 0;
#ifdef PF_WDT_DEBUG
	if(initflag == 0)
	{
		return enStatusNotConfigured
	}
#endif
	asm("push r0");
	if(watchdogConfigStruct.mode == 2 )
	{
		temp = 0x08;
	}
	else
	{
		temp = 0x48;
	}
	if(timeout == 8)
	temp |= (1<<5);
	else if(timeout == 9 )
	temp |= ((1<<5)|(1<<0));
	else
	temp |= timeout;
	asm("mov r0, r24");
	__asm__ __volatile__("ldi r24,0x00");
	__asm__ __volatile__("ldi r25,0x18");
	__asm__ __volatile__ ("wdr");
	__asm__ __volatile__("ldi r16,0x00");
	__asm__ __volatile__("sts 0x0054, r16");
	__asm__ __volatile__("sts 0x0060, r25");
	__asm__ __volatile__("sts 0x0060, r0");
		asm("pop r0");
		return enStatusSuccess;
}

PFEnStatus pfWatchdogStop(void)
{
	 #ifdef PF_WDT_DEBUG
	 if(initflag == 0)
	 {
		 return enStatusNotConfigured
	 }
	 #endif
	 __asm__ __volatile__("ldi r24,0x00");
	 __asm__ __volatile__("ldi r25,0x18");
	 __asm__ __volatile__("sts 0x0060, r24");
	 return enStatusSuccess;
}
 
 
void INT_HANDLER(WDT_VECT)(void)
{
	if(watchdogConfigStruct.callback != 0)
	{
		watchdogConfigStruct.callback();
	}
	return;	
}	
#endif