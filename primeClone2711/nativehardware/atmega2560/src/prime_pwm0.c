#include "prime_framework.h"
#if(PF_USE_PWM0 == 1)	
	#include "prime_pwm0.h"
	
	
static PFcallback pwm0CmpMatchACallback;
static PFcallback pwm0CmpMatchBCallback;
static PFcallback pwm0OverFlowCallback;
static PFbyte pwm0InitFlag=0;
static PFbyte pwm0SettingVal=0;
static PFbyte pwm0Value=0;


PFEnStatus pfPwm0Open(PFpCfgPwm0 config)
{
#ifdef PF_PWM0_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
    if( (config->pwm0Mode<0) || ((config->pwm0Mode > enTimerCtcMode)))
	{
		return enStatusInvArgs;
	}
	
	if( (config->exMatchActionA < 0) || (config->exMatchActionA > 3) )
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionB < 0) || (config->exMatchActionB > 3) )
	{
		return enStatusInvArgs;
	}
	if(config->cmpMatchACallback ==0)
	{
		return enStatusInvArgs;
	}
	if(config->cmpMatchBCallback ==0)
	{
		return enStatusInvArgs;
	}
	if(config->overFlowCallback ==0)
	{
		return enStatusInvArgs;
	}	
#endif

	if((config->pwm0Mode& 0x01)!=0)
	{
		PWM0_CHANNEL->TCCRA |=config->pwm0Mode;
		PWM0_CHANNEL->TCCRB |=(config->pwm0Mode &0x04)<<1;
	}
	
	PWM0_CHANNEL->OCRA =config->matchValueA;
	PWM0_CHANNEL->OCRB =config->matchValueB;
	
	if(config->exMatchActionA !=enPwm0ExtMatchCtrlNone)
	{
		PWM0_CHANNEL->TCCRA |=config->exMatchActionA <<6;
		GPIO_PORTB->DDR |=(1<<7);
	}		
	if(config->exMatchActionB!=enPwm0ExtMatchCtrlNone)
	{
		PWM0_CHANNEL->TCCRA |=config->exMatchActionB <<4;
		GPIO_PORTG->DDR |=(1<<5);
	}		
	if(config->cmpMatchACallback !=0)
	{
		pwm0CmpMatchACallback =config->cmpMatchACallback;
	}	
	if(config->cmpMatchBCallback !=0)
	{
		pwm0CmpMatchBCallback =config->cmpMatchBCallback;
	}	
	if(config->overFlowCallback !=0)
	{
		pwm0OverFlowCallback =config->overFlowCallback;
	}
	if(config->clockSource >=0 && config->clockSource <=7)
	{
		PWM0_CHANNEL->TCCRB |= config->clockSource;
	}
	if(config->interrupt !=enPwm0IntNone)
	{
		//	PWM0_CHANNEL->TIMSK |=config.interrupt;
		TIMSK_INT->TIMER0 |= config->interrupt;	
	}	
	PWM0_CHANNEL->TCNT = 0;
	pwm0InitFlag=1;
	return enStatusSuccess;
	
}

PFEnStatus pfPwm0Close(void)
{
#ifdef PFPWM0_DEBUG
	if(pwm0InitFlag ==0)
	{
		return enStatusNotConfigured;
	}
#endif
	PWM0_CHANNEL->TCCRB = 0x00;
	PWM0_CHANNEL->TCNT = 0;
	return enStatusSuccess;
		
}

PFEnStatus pfPwm0Stop(void)
{
#ifdef PFPWM0_DEBUG
	if((pwm0InitFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif
	pwm0SettingVal=PWM0_CHANNEL->TCCRB;
	pwm0Value=PWM0_CHANNEL->TCNT;
	PWM0_CHANNEL->TCCRB = 0x00;
	return enStatusSuccess;

}

PFEnStatus pfPwm0Start(void)
{
	PFbyte val;
	
#ifdef PFPWM0_DEBUG
	if((pwm0InitFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif	
	if(pwm0SettingVal!=0)
	{
		PWM0_CHANNEL->TCCRB = pwm0SettingVal;
		PWM0_CHANNEL->TCNT = pwm0Value;
	}
	else
	{
		val =PWM0_CHANNEL->TCCRB;
		PWM0_CHANNEL->TCCRB = 0x00;
		PWM0_CHANNEL->TCCRB =val;
		PWM0_CHANNEL->TCNT = 0;
	}
	return	enStatusSuccess;	
	
}
PFEnStatus pfPwm0Reset(void)
{
#ifdef PFPWM0_DEBUG
	if((pwm0InitFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif
	PWM0_CHANNEL->TCNT = 0;
	return enStatusSuccess;
	
}
PFEnStatus pfPwm0ReadCount(PFdword *data)
{
#ifdef PFPWM0_DEBUG
	if((pwm0InitFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif
	*data =PWM0_CHANNEL->TCNT;
	return enStatusSuccess;
	
}

//ISR(TIMER0_COMPA_vect)
void INT_HANDLER(TIMER0_COMPA)(void)
//INT_HANDLER(TIMER0_COMPA)
{
	if(pwm0CmpMatchACallback !=0)
	pwm0CmpMatchACallback();
}


//ISR(TIMER0_COMPB_vect)
void INT_HANDLER(TIMER0_COMPB)(void)
//INT_HANDLER(TIMER0_COMPB)
{
	if(pwm0CmpMatchBCallback !=0)
	pwm0CmpMatchBCallback();
}


////ISR(TIMER0_OVF_vect)
void INT_HANDLER(TIMER0_OVF)(void)
//INT_HANDLER(TIMER0_OVF)
{
	if(pwm0OverFlowCallback !=0)
	pwm0OverFlowCallback();
}


PFEnStatus pfPwm0UpdateMatchRegister(PFbyte regNum, PFdword regVal)
{
	switch(regNum)
	{
		case 0:
		PWM0_CHANNEL->OCRA =regVal;
		break;
		
		case 1:
		PWM0_CHANNEL->OCRB =regVal;
		break;
		
		default:
			return enStatusInvArgs;
	}
	
	return enStatusSuccess;
}

#endif		// #if(PF_USE_PWM0)
