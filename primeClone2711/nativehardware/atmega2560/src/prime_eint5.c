#include "prime_framework.h"
#if (PF_USE_EINT5 == 1)
#include "prime_eint5.h"


static PFcallback eint5CallBackList[PF_EXT_INT_MAX_CALLBACK]={0};
static PFbyte eint5MaxIntCount=0;
static PFbyte eint5InitFlag = 0;


PFEnStatus pfEint5Open(PFpCfgEint5 config)
{
	PFbyte index;
#ifdef PF_EINT5_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
	if((config->mode > 3)|| (config->mode < enIntModeLowLevel))
	{
		return enStatusInvArgs;
	}
	if(config->callbackList == 0)
	{
		return enStatusInvArgs;
	}
	if(config->maxCallbacks < 0)
	{
		return enStatusInvArgs;
	}
#endif

	if(config->mode <=3)
	{
		EXTINT->EICRB |=config->mode <<(EINT5_CH-3);
	}	
	
	eint5MaxIntCount=config->maxCallbacks ;
// 	if(eint5MaxIntCount >0)
// 	eint5CallBackList =(PFcallback*)malloc(sizeof(PFcallback)*eint5MaxIntCount);
// 	else
// 	return enStatusNotSupported;
		
	for(index=0;index<eint5MaxIntCount;index++)
	{
		if(config->callbackList!=0 )
		eint5CallBackList[index]=config->callbackList[index];
	}
	 eint5InitFlag = enBooleanTrue ;
	
	return enStatusSuccess;
	
}


PFEnStatus pfEint5Enable(void)
{
#ifdef PF_EINT5_DEBUG
	if(eint5InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK |= (1<<EINT5_CH);
	return enStatusSuccess;
}


PFEnStatus pfEint5Disable(void)
{
#ifdef PF_EINT5_DEBUG
	if(eint5InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK &=~(1<<EINT5_CH);
	return enStatusSuccess;
}



PFEnStatus pfEint5AddCallback(PFcallback callback)
{
	PFbyte index=0;
#ifdef PF_EINT5_DEBUG
	if(eint5InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
	if(callback == 0)
	{
		return enStatusInvArgs;
	}
#endif	
	for(index=0;index<=eint5MaxIntCount;index++)
	{
		if (eint5CallBackList[index]==0)
		{
			eint5CallBackList[index]=callback;
			break;
		}
	}
	
	return enStatusSuccess;
}

PFEnStatus pfEint5RemoveCallback(PFcallback callback)
{
	PFbyte index=0;
#ifdef PF_EINT5_DEBUG
	if(eint5InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	
	for(index=0;index<=eint5MaxIntCount;index++)
	{
		if (eint5CallBackList[index]==callback)
		{
			eint5CallBackList[index]=0;
			break;
		}
	}
	return enStatusSuccess;
	
}


PFEnStatus pfEint5Close(void)
{
#ifdef PF_EINT5_DEBUG
	if(eint5InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK &=~(1<<EINT5_CH);
	//free(eint5CallBackList);
	return enStatusSuccess;
}

void INT_HANDLER(EINT5_CHANNEL)(void)
{
	PFbyte index=0;
	
	for(index=0;index<=eint5MaxIntCount;index++)
	{
		if(eint5CallBackList[index] !=0)
		eint5CallBackList[index]();
		
	}
}

#endif		//#if (PF_USE_EINT5 == 1)
