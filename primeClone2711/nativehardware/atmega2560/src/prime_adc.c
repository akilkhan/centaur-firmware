#include "prime_framework.h"
#if (PF_USE_ADC == 1)
#include "prime_adc.h"

static PFEnStatus adcInitFlag=0;
static PFcallback adcCallBack=0;
static PFbyte adcLeftAdjust=0;
//static PFword value;
static PFdword adcRefVtg;
static PFCfgAdc adcCfg; 

PFEnStatus pfAdcOpen(PFpCfgAdc config)
{
#ifdef PF_ADC_DEBUG
	if (config->channel >15)
	{
		return enStatusInvArgs;	
	}
	
	if(config->refVoltage >3)
	{
		return enStatusInvArgs;
	}
	if(config->conversionMode > enAdcTimer1CapturEvent)
	{
		return enStatusInvArgs;
	}	
	if(config->prescaler <= enAdcPrescaleBy128)
	{
		return enStatusInvArgs;
	}
	if( config->interrupt !=0 && config->callback ==0 )
	{
		return enStatusNotConfigured;
	}	
#endif

	switch(adcCfg.refVoltage)
	{
		case enAdcArefSelect:
			adcRefVtg = adcCfg.arefVoltage;
			break;
		case enAdcAvccSelect:
			adcRefVtg = 5000;
			break;
		case enAdcInternal1_1Select:
			adcRefVtg = 1100;
			break;
		case enAdcInternal2_6Select:
			adcRefVtg = 2560;
			break;
		default:
			return enStatusInvArgs;
	}
	
	if (config->channel <= enAdcChannel_15)
	{
		if (config->channel >enAdcChannel_7)
		{
			GPIO_PORTK->PORT =~(1<<(config->channel -8));
			ADC_CHANNEL->ADCSRB |=(BIT_3); //setting the mux5 pin
			ADC_CHANNEL->ADMUX |= (config->channel -8);
		}
		GPIO_PORTF->PORT = ~(1<<config->channel);
		ADC_CHANNEL->ADMUX |= config->channel;
		
	} 
	
	
	if(config->refVoltage <=3)
	{
		ADC_CHANNEL->ADMUX |=config->refVoltage <<6;//refernce volt
	}
	
	
	if(config->resultLeftAdjust !=enBooleanFalse)
	{
		ADC_CHANNEL->ADMUX |= BIT_5; //ADLAR
		adcLeftAdjust=1;
	}	
	if(config->conversionMode >=enAdcNone)
	{	
		
		if(config->conversionMode > enAdcStartNow && config->conversionMode <= enAdcTimer1CapturEvent)
		{
			ADC_CHANNEL->ADCSRA |=(BIT_5);
			ADC_CHANNEL->ADCSRB |=(config->conversionMode -2);
			
		}
		else
			ADC_CHANNEL->ADCSRA &=~(BIT_5);

		
	}	
		
	if(config->prescaler <= enAdcPrescaleBy128)
	{
		ADC_CHANNEL->ADCSRA |= config->prescaler;
	}	
	
	if (config->interrupt != enBooleanFalse)
	{
		ADC_CHANNEL->ADCSRA |= config->interrupt <<BIT_3; // ADIE bit set
	}
	
	if(config->callback != 0)
	{
		adcCallBack =config->callback;
	}
	
	
	ADC_CHANNEL->ADCSRA |= (BIT_7); //ADEn pin set to enable ADC
	
	pfMemCopy(&adcCfg,config,sizeof(PFCfgAdc));
	adcInitFlag =1;
	
	return enStatusSuccess;	
	
}

PFEnStatus pfAdcSelectMode(PFEnAdcStart mode)
{

#ifdef PF_ADC_DEBUG
	if (adcInitFlag == 0)
	{
		return enStatusNotConfigured;
	}
	if((mode> enAdcTimer1CapturEvent) || (mode < enAdcNone))
	{
		return enStatusInvArgs;
	}	
#endif
	
	if(mode >=enAdcNone)
	{
		if(mode > enAdcStartNow && mode <= enAdcTimer1CapturEvent)
		{
			ADC_CHANNEL->ADCSRA |=(BIT_5); //ADATE bit
			ADC_CHANNEL->ADCSRB |=(mode -2);
		}
		else
		{
			ADC_CHANNEL->ADCSRA &=~(BIT_5);
		}			
	}
	return enStatusSuccess;
}



PFEnStatus pfAdcSelectChannel(PFEnAdcChannel channel)
{

#ifdef PF_ADC_DEBUG
	if (adcInitFlag == 0)
	{
		return enStatusNotConfigured;
	}
	if (channel > enAdcChannel_15)
	{
		return enStatusInvArgs;
	}	
#endif

	ADC_CHANNEL->ADCSRA &= ~(BIT_7); //ADEn pin set to disable ADC
	ADC_CHANNEL->ADMUX &= 0xf8;
			
	if (channel >enAdcChannel_7)
	{
		GPIO_PORTK->PORT =~(1<<(channel -8));
		ADC_CHANNEL->ADCSRB |=(BIT_3); //setting the mux5 pin
		ADC_CHANNEL->ADMUX |= (channel -8);
	}
	else
	{
		GPIO_PORTF->PORT = ~(1<<channel);
		ADC_CHANNEL->ADCSRB &=~(BIT_3);
		ADC_CHANNEL->ADMUX |= channel;
	}			
			
	ADC_CHANNEL->ADCSRA |= (BIT_7); //ADEn pin set to enable ADC
		
	return enStatusSuccess;
}
PFEnStatus pfAdcEnableInterrupt(void)
{

#ifdef PF_ADC_DEBUG
	if (adcInitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif

	if(adcInitFlag!=0)
	{
		ADC_CHANNEL->ADCSRA |= BIT_3;
	}
	return enStatusSuccess;	
}

PFEnStatus pfAdcDisableInterrupt(void)
{
#ifdef PF_ADC_DEBUG
	if (adcInitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	ADC_CHANNEL->ADCSRA |= ~(BIT_3);
	return enStatusSuccess;
}

PFEnStatus pfAdcStartBurstConversion(void)
{
#ifdef PF_ADC_DEBUG
	if (adcInitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	ADC_CHANNEL->ADCSRA |= (BIT_6);
	return enStatusSuccess;
}

PFEnStatus pfAdcSingleConversion(PFEnAdcChannel channel,PFword* value)
{
#ifdef PF_ADC_DEBUG
	if (adcInitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
	if (value == 0)
	{
		return enStatusInvArgs;
	}	
#endif

	pfAdcSelectChannel(channel);
	ADC_CHANNEL->ADCSRA &=~(BIT_5); //ADATE bit
	ADC_CHANNEL->ADCSRA |= (BIT_6); //ADSC bit to start conversion
	while(!(ADC_CHANNEL->ADCSRA &(BIT_4)));
	*value =ADC_CHANNEL->ADCL;
	*value |=ADC_CHANNEL->ADCH <<8;
	
	if(adcLeftAdjust != 0)
	{
		*value =*value>>6;
		return enStatusSuccess;
		
	}
	else
	{
		return enStatusSuccess;
	}
}

PFEnStatus pfAdcGetVoltageSingleConversion(PFbyte channel, PFdword* milliVolt)
{
	PFdword adcVal;
#ifdef PF_ADC_DEBUG
	if (adcInitFlag == 0)
	{
		return enStatusNotConfigured;
	}

	if(milliVolt == 0)
    {
        return enStatusInvArgs;
    }
#endif
		
	if(pfAdcSingleConversion(channel,&adcVal) == enStatusSuccess)
	{
		*milliVolt = PF_ADC_CALC_VTG(adcVal, adcRefVtg);
		return enStatusSuccess;
	}
	return enStatusError;
}

PFEnStatus pfAdcGetLastValue(PFword *value)
{
#ifdef PF_ADC_DEBUG
	if (adcInitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
	if (value == 0)
	{
		return enStatusInvArgs;
	}	
#endif
	
//	while(!(ADCSRA &(1<<BIT_3)));
				
	*value =ADC_CHANNEL->ADCL;
	*(value) |=ADC_CHANNEL->ADCH <<8;
	
	if(adcLeftAdjust != 0)
	{
		*value =*value>>6;
		return enStatusSuccess;
	}
	else
	{		
		return enStatusSuccess;
	}
	
}

PFEnStatus pfAdcClose(void)
{
#ifdef PF_ADC_DEBUG
	if (adcInitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	ADC_CHANNEL->ADCSRA &= ~(BIT_7); //ADEn pin set to enable ADC
	return enStatusSuccess;
}

void INT_HANDLER(ADC_VECT)(void)
{
	if(adcCallBack !=0)
	{
		adcCallBack() ;
	}	
}

#endif		//#if (PF_USE_ADC == 1)
