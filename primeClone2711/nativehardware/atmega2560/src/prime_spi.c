#include "prime_framework.h"

#if (PF_USE_SPI == 1)
#include "prime_fifo.h"
#include "prime_gpio.h"
#include "prime_spi.h"

#if(SPI_USE_FIFO != 0)
#warning SPI FIFO is enabled for interrupt based communication
#if( (SPI_BUFFER_SIZE == 0) || ((SPI_BUFFER_SIZE & (SPI_BUFFER_SIZE - 1)) != 0) )
#error SPI_BUFFER_SIZE cannot be zero. SPI_BUFFER_SIZE should be power of 2
#endif
#endif

static PFEnBoolean spiChInit = enBooleanFalse;		// initalize flag for the channel
static PFEnBoolean spiChBusy = enBooleanFalse;		// busy flag for the channel
static PFEnBoolean spiChMaster;						// master/slave mode
static PFbyte spiChInt = 0;						// interrupt flag for the channel

#define SPI_STATE_IDLE		0
#define SPI_STATE_READ		1
#define SPI_STATE_WRITE		2
static PFbyte spiChState = SPI_STATE_IDLE;					// read write flag

#if(SPI_USE_FIFO != 0)


static PFbyte spiRxBuffer[SPI_BUFFER_SIZE];		// SPI transmit buffer
static PFbyte spiTxBuffer[SPI_BUFFER_SIZE];		// SPI receive buffer
static PFFifo spiTxFifo;							// SPI transmit fifo structure
static PFFifo spiRxFifo;							// SPI receive fifo structure
#else
static PFcallback spiCallback = 0;					// callback for the channel interrupt
#endif	// #if(SPI_USE_FIFO != 0)

static PFEnBoolean spiRegDevice[SPI_MAX_DEVICE_SUPPORTED] = {enBooleanFalse};	
static PFGpioPortPin spiChipSelect[SPI_MAX_DEVICE_SUPPORTED] = {{0, 0}};
static PFEnBoolean spiChipSelectAssert[SPI_MAX_DEVICE_SUPPORTED] = {enBooleanFalse};	
static PFbyte spiRegDeviceCount = 0;

PFEnStatus pfSpiOpen(PFpCfgSpi config)
{
	unsigned char temp;
#ifdef PF_SPI_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
	if((config->baudRate > 7) && (config->baudRate <  enFreqDivideBy4))
	{
		return enStatusNotSupported;
	}	
	if((config->spiMode > 3)&& (config->spiMode < 0 ))
	{
		return enStatusNotSupported;
	}	
#if (SPI_USE_FIFO == 0)	
	if(config->spiCallback == 0)
	{
		return enStatusInvArgs;
	}	
#endif	// #if (SPI_USE_FIFO == 0)		
#endif	// PF_SPI_DEBUG

	
	temp =	config->baudRate & 0x04;
	if(temp !=0)
	{
		SPI_CHANNEL->SPCR |= config->baudRate;
		SPI_CHANNEL->SPSR |=(temp)>>2;
	}
	else
	{	
		SPI_CHANNEL->SPCR |= config->baudRate;
	}			
	 	
	if(config->masterMode !=enBooleanFalse)
	{ 	
		SPI_CHANNEL->SPCR |=config->masterMode <<4;
		GPIO_PORTB->DDR |= (1<<2) |(1<<1) | (1<<0);		// SCK, MOSI, SS
		GPIO_PORTB->DDR &= ~(1<<3);
	}
	else
	{
		GPIO_PORTB->DDR &=~ ((1<<2) |(1<<1) | (1<<0));		// SCK, MOSI, SS
		GPIO_PORTB->DDR |= (1<<3);
	}	
 	SPI_CHANNEL->SPCR |=config->spiMode << 2;
	
	#if(SPI_USE_FIFO != 0)
	// initialize fifo
	pfFifoInit(&spiTxFifo, spiTxBuffer, SPI_BUFFER_SIZE);
	pfFifoInit(&spiRxFifo, spiRxBuffer, SPI_BUFFER_SIZE);
	#else
	if(config->spiCallback != 0)
	{// set callback
		spiCallback = config->spiCallback;
	}		
	#endif	// #if(SPI_USE_FIFO != 0)
	// set interrupts
	if(config->interrupt !=enBooleanFalse)
	{
		SPI_CHANNEL->SPCR |=config->interrupt <<7;
		spiChInt = config->interrupt;
	}		
	// enable SPI channel
 	SPI_CHANNEL->SPCR |=(1<<6); //enable SPI
 	spiChMaster = config->masterMode;
 	spiChInit = enBooleanTrue;
	
	
	return enStatusSuccess;

}

PFEnStatus pfSpiRegisterDevice(PFbyte* id, PFpGpioPortPin chipSelect)
{
	PFbyte index;
#ifdef PF_SPI_DEBUG
	if(spiChInit != enBooleanTrue)
	{	
		return enStatusNotConfigured;
	}
	if(id == 0)
	{	
		return enStatusInvArgs;
	}	
#endif	// PF_SPI_DEBUG
	if(spiRegDeviceCount == SPI_MAX_DEVICE_SUPPORTED)
	{
		return enStatusNoMem;
	}	
	
	for(index = 0; index < SPI_MAX_DEVICE_SUPPORTED; index++)
	{
		if(spiRegDevice[index] == enBooleanFalse)
		{
			spiRegDevice[index] = enBooleanTrue;
			spiRegDeviceCount++;
			spiChipSelect[index].port = chipSelect->port;
			spiChipSelect[index].pin = chipSelect->pin;
			*id = index;
			return enStatusSuccess;
		}
	}
	
	return enStatusError;
}

PFEnStatus pfSpiUnregisterDevice(PFbyte* id)
{
#ifdef PF_SPI_DEBUG
	if(spiChInit != enBooleanTrue)
	{	
		return enStatusNotConfigured;
	}
	if(id == 0)
	{	
		return enStatusInvArgs;
	}	
#endif	// PF_SPI_DEBUG
	
	spiRegDevice[*id] = enBooleanFalse;
	spiRegDeviceCount--;
	return enStatusSuccess;
}

PFEnStatus pfSpiClose(void)
{
#ifdef PF_SPI_DEBUG
	if(spiChInit != enBooleanFalse)
	{
		return enStatusNotConfigured;
	}
	if(spiChBusy == enBooleanTrue)
	{
		return enStatusBusy;
	}	
#endif	// PF_SPI_DEBUG

	SPI_CHANNEL->SPCR |=(1<<6); //enable SPI
	spiChInit = enBooleanFalse;
	return enStatusSuccess;
}

PFEnStatus pfSpiChipSelect(PFbyte* id, PFbyte pinStatus)
{
	PFbyte index;
#ifdef PF_SPI_DEBUG
	if(spiChInit != enBooleanTrue)
	{	
		return enStatusNotConfigured;
	}
	if(id == 0)
	{	
		return enStatusInvArgs;
	}
	if(*id >= SPI_MAX_DEVICE_SUPPORTED)
	{	
		return enStatusInvArgs;
	}
	if(spiRegDevice[*id] != enBooleanTrue)
	{	
		return enStatusError;
	}	
#endif	// PF_SPI_DEBUG	
	
	if(pinStatus == 0)
	{
		for(index = 0; index < SPI_MAX_DEVICE_SUPPORTED; index++)
		{
			if(spiChipSelectAssert[index] == enBooleanTrue)
			{
				return enStatusBusy;
			}
		}
		
		pfGpioPinsClear(spiChipSelect[*id].port, spiChipSelect[*id].pin);
		spiChipSelectAssert[*id] = enBooleanTrue;	
	}
	else
	{
		pfGpioPinsSet(spiChipSelect[*id].port, spiChipSelect[*id].pin);
		spiChipSelectAssert[*id] = enBooleanFalse;	
	}
	
	return enStatusSuccess;
}


PFEnStatus pfSpiExchangeByte( PFbyte* id, PFbyte data, PFbyte* exchangevalue)
{
	volatile PFbyte rxData = 0;
#ifdef PF_SPI_DEBUG
	if(spiChInit != enBooleanFalse)
	{
		return enStatusNotConfigured;
	}
	if(spiChBusy == enBooleanTrue)
	{
		return enStatusBusy;
	}		
#endif	// PF_SPI_DEBUG
	if(spiChipSelectAssert[*id] != enBooleanTrue)
	{
		return enStatusError;
	}
	// wait if transmit FIFO is full
	SPI_CHANNEL->SPDR =data;
	while((SPI_CHANNEL->SPSR & (1<<7)) == 0); //the flag is set on transmission
	rxData =SPI_CHANNEL->SPDR;
	exchangevalue = rxData;
	return enStatusSuccess;
}

PFEnStatus pfSpiWrite(PFbyte* id,PFbyte* data, PFdword size)
{
	PFdword index;
	PFbyte dummyRead;
#ifdef PF_SPI_DEBUG
	if(spiChInit != enBooleanFalse)
	{
		return enStatusNotConfigured;
	}
	if(data == PF_NULL)
	{
		return enStatusInvArgs;
	}		
#endif	// PF_SPI_DEBUG
	// check if channel is busy
	if(spiChBusy == enBooleanTrue)
	{
		return enStatusBusy;
	}	
	if(spiChipSelectAssert[*id] != enBooleanTrue)
	{	
		return enStatusError;
	}
	// set channel busy
	spiChBusy = enBooleanTrue;
	// set state
	spiChState = SPI_STATE_WRITE;
	// if fifo is to be used push data into the fifo
	if((spiChInt & enBooleanTrue) != 0)
	{
		#if(SPI_USE_FIFO != 0)
		for(index = 0; index < size; index++)
		{
			pfFifoPush(&spiTxFifo, *(data + index));
		}
		SPI_CHANNEL->SPDR = pfFifoPop(&spiTxFifo);
		#else
		SPI_CHANNEL->SPDR = *data;
		#endif	// #if(SPI_USE_FIFO != 0)
	}
	else
	{
		// for polling based transmission, write each byte one by one to data register
		for(index = 0; index < size; index++)
		{
			SPI_CHANNEL->SPDR = *(data + index);
			while((SPI_CHANNEL->SPSR & (1<<7)) == 0); //the flag is set on transmission

			dummyRead = SPI_CHANNEL->SPDR;
		}
	}
	
	spiChBusy = enBooleanFalse;
	return enStatusSuccess;
}


PFEnStatus pfSpiRead(PFbyte* id,PFbyte* data, PFdword size, PFdword* readBytes)
{
	PFdword index;
#ifdef PF_SPI_DEBUG
	if(spiChInit != enBooleanFalse)
	{
		return enStatusNotConfigured;
	}
	if(data == 0)
	{
		return enStatusInvArgs;
	}	
#endif	// PF_SPI_DEBUG
	// check if channel is busy
	if(spiChBusy == enBooleanTrue)
	{
		return enStatusBusy;
	}	
	if(spiChipSelectAssert[*id] != enBooleanTrue)
	{	
		return enStatusError;
	}	
	// set channel busy
	spiChBusy = enBooleanTrue;
	// set state
	spiChState = SPI_STATE_WRITE;
	// reset read byte count
	*readBytes = 0;
	
	if(spiChInt != enBooleanFalse)
	{
		// if fifo is to be used, push data into the fifo
		#if(SPI_USE_FIFO != 0)
		if(spiChMaster == enBooleanTrue)
		{
			// for master mode, push dummy data in buffer
			for(index = 0; index < size; index++)
			{
				pfFifoPush(&spiTxFifo, 0);
			}
			SPI_CHANNEL->SPDR = pfFifoPop(&spiTxFifo);
			// wait for all bytes to transmit
			while(pfFifoIsEmpty(&spiTxFifo) != enBooleanTrue);
		}
		// read receive fifo
		for(index = 0; index < size; index++)
		{
			if(pfFifoIsEmpty(&spiRxFifo) == enBooleanTrue)
			break;

			*(data + index) = pfFifoPop(&spiRxFifo);
			*readBytes++;
		}
		#else
		if(spiChMaster == enBooleanTrue)
		{
			SPI_CHANNEL->SPDR= *data;
		}
		else
		{
			spiChBusy = enBooleanFalse;
			return enStatusUnknown;
		}
		#endif	// #if(SPI_USE_FIFO != 0)
	}
	else
	{
		// for polling based transmission, write each byte one by one to data register
		for(index = 0; index < size; index++)
		{
			SPI_CHANNEL->SPDR= 0;
			while((SPI_CHANNEL->SPSR & (1<<7)) == 0); //the flag is set on transmission
			*(data + index) = SPI_CHANNEL->SPDR;
			*readBytes++;
		}
	}
	spiChState = SPI_STATE_IDLE;
	spiChBusy = enBooleanFalse;
	return enStatusSuccess;
}

#if(SPI_USE_FIFO != 0)
PFEnStatus pfSpiGetRxBufferCount(PFdword* count)
{
#ifdef PF_SPI_DEBUG
	if(spiChInit != enBooleanFalse)
	{
		return enStatusNotConfigured;
	}
	if(count == 0)
	{
		return enStatusInvArgs;
	}	
#endif	// PF_SPI_DEBUG
	*count = spiRxFifo.count;
	return enStatusSuccess;
}

PFEnStatus pfSpiTxBufferFlush(void)
{
#ifdef PF_SPI_DEBUG
	if(spiChInit != enBooleanFalse)
	{
		return enStatusNotConfigured;
	}
#endif	// PF_SPI_DEBUG
	pfFifoFlush(&spiTxFifo);
	return enStatusSuccess;
}

PFEnStatus pfSpiRxBufferFlush(void)
{
#ifdef PF_SPI_DEBUG
	if(spiChInit != enBooleanFalse)
	{
		return enStatusNotConfigured;
	}
#endif	// PF_SPI_DEBUG
	pfFifoFlush(&spiRxFifo);
	return enStatusSuccess;
}
#endif	// #if(SPI_USE_FIFO != 0)

//ISR(SPI_STC_vect)
void INT_HANDLER(SPI)(void)
// INT_HANDLER(SPI_VECT)
{
	PFbyte dummyRead;
	#if(SPI_USE_FIFO != 0)
	if(spiChState == SPI_STATE_IDLE)
	return;
	// for write operation
	if(spiChState == SPI_STATE_WRITE)
	{
		if(pfFifoIsEmpty(&spiTxFifo) == enBooleanTrue)
		{
			spiChState = SPI_STATE_IDLE;
			return;
		}
		dummyRead = SPI_CHANNEL->SPDR;
		SPI_CHANNEL->SPDR = pfFifoPop(&spiTxFifo);
	}
	else		// for read operation
	{
		if(spiChMaster == enBooleanTrue)
		{
			pfFifoPush(&spiRxFifo, SPI_CHANNEL->SPDR);
			if( pfFifoIsEmpty(&spiTxFifo) != enBooleanTrue )
			SPI_CHANNEL->SPDR = pfFifoPop(&spiTxFifo);
		}
		else		// slave mode
		{
			pfFifoPush(&spiRxFifo, SPI_CHANNEL->SPDR);
			SPI_CHANNEL->SPDR = 0;
			
		}
		
	}
	
	
	
	#else
	if(spiCallback != 0)
	{
		spiCallback();
		return;
	}
	#endif	// #if(SPI_USE_FIFO != 0)
	
}

#endif		// #if (PF_USE_SPI == 1)
