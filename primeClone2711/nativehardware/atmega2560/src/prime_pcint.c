#include "prime_framework.h"
#if (PF_USE_PCINT == 1)
#include "prime_utils.h"
#include "prime_pcint.h"

static PFcallback pcintVect0CallBackList[8]={0};
static PFcallback pcintVect1CallBackList[8]={0};
static PFcallback pcintVect2CallBackList[8]={0};
static PFEnBoolean pciIntId[PCIINT_MAX_NO]={0};
static PFCfgPcint pciIntConfig[PCIINT_MAX_NO];
static PFbyte pcintMaxIntCount=0;

static PFbyte pcintInit=0;

PFEnStatus pfPcintOpen(PFbyte *id,PFpCfgPcint config,PFbyte count)
{
#ifdef PF_PCINT_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
	
	if(id == 0)
	{
		return enStatusInvArgs;
	}
	
	if(count >PCIINT_MAX_NO )
	{
		return enStatusInvArgs;
	}
#endif //PF_PCINT_DEBUG
	PFbyte index;
		
	for(index=0;index<count;index++)
	{
		if(pciIntId[index] == enBooleanFalse)
		{
			if(config[index].pcintChannel <=7)
			{
				PCINT->PCICR |= BIT_MASK_0;
			}
			else if( (config[index].pcintChannel >7) && (config[index].pcintChannel <=15))
			{
				PCINT->PCICR |= BIT_MASK_1;
			}
			else 
			{
				PCINT->PCICR |= BIT_MASK_2;
			}
			
			
			if((config[index].pcintCallback!=0 )&& (config[index].pcintChannel <=7))
			{
				pcintVect0CallBackList[index]=config[index].pcintCallback;
			}
			else if((config[index].pcintCallback!=0 )&& ( (config[index].pcintChannel >7) && (config[index].pcintChannel <=15)))
			{
				pcintVect1CallBackList[index]=config[index].pcintCallback;
			}
			else
			{
				pcintVect2CallBackList[index]=config[index].pcintCallback;
			}
			
			id[index] = index;
			pciIntId[index] = enBooleanTrue;
			pfMemCopy(&pciIntConfig[index],&config[index],sizeof(PFCfgPcint));
		}
		
	}
	
	pcintInit =1;
	return enStatusSuccess;
}

PFEnStatus pfPcintEnable(PFbyte id)
{
#ifdef PF_PCINT_DEBUG
	if(pcintInit == 0)
	{
		return enStatusNotConfigured;
	}
	if(id >PCIINT_MAX_NO)
	{
		return enStatusNotConfigured;
	}
#endif //PF_PCINT_DEBUG
	if(pcintInit !=0)
	{
		if(pciIntConfig[id].pcintChannel <=7)
		{
			PCINT->PCMSK0 |= (1<<(pciIntConfig[id].pcintChannel)) ;
		}
		else if( (pciIntConfig[id].pcintChannel >7) && (pciIntConfig[id].pcintChannel <=15))
		{
			PCINT->PCMSK1 |= (1<<(pciIntConfig[id].pcintChannel -8));
		}
		else
		{
			PCINT->PCMSK2 |= (1<<(pciIntConfig[id].pcintChannel -16));
		}		
		return enStatusSuccess;
	}
	return enStatusError;
}


PFEnStatus pfPcintDisable(PFbyte id)
{
#ifdef PF_PCINT_DEBUG
	if(pcintInit == 0)
	{
		return enStatusNotConfigured;
	}
	
	if(id >PCIINT_MAX_NO)
	{
		return enStatusNotConfigured;
	}
#endif //PF_PCINT_DEBUG
	if(pcintInit !=0)
	{
		if(pciIntConfig[id].pcintChannel <=7)
		{
			PCINT->PCMSK0 &= ~(1<<(pciIntConfig[id].pcintChannel)) ;
		}
		else if( (pciIntConfig[id].pcintChannel >7) && (pciIntConfig[id].pcintChannel <=15))
		{
			PCINT->PCMSK1 &= ~(1<<(pciIntConfig[id].pcintChannel -8));
		}
		else
		{
			PCINT->PCMSK2 &= ~(1<<(pciIntConfig[id].pcintChannel -16));
		}
		return enStatusSuccess;
	}
	return enStatusError;
}


/*
PFEnStatus pfPcintAddCallback(PFcallback callback)
{
	PFbyte index=0;
#ifdef PF_PCINT_DEBUG
	if(pcintInit == 0)
	{
		return enStatusNotConfigured;
	}
#endif	
	for(index=0;index<=pcintMaxIntCount;index++)
	{
		if (pcintCallBackList[index]==0)
		{
			pcintCallBackList[index]=callback;
			break;
		}
	}
	
	return enStatusSuccess;
}

PFEnStatus pfPcintRemoveCallback(PFcallback callback)
{
	PFbyte index=0;
#ifdef PF_PCINT_DEBUG
	if(pcintInit == 0)
	{
		return enStatusNotConfigured;
	}
#endif	
	for(index=0;index<=pcintMaxIntCount;index++)
	{
		if (pcintCallBackList[index]==callback)
		{
			pcintCallBackList[index]=0;
			break;
		}
	}
	return enStatusSuccess;
	
}
*/
PFEnStatus pfPCIntClose(PFbyte id)
{
	PFEnStatus status;
#ifdef PF_PCINT_DEBUG
	if(pcintInit == 0)
	{
		return enStatusNotConfigured;
	}
	if(id >PCIINT_MAX_NO)
	{
		return enStatusNotConfigured;
	}
#endif //PF_PCINT_DEBUG
	
	status = pfPcintDisable(id);
	
	return status;
}

void INT_HANDLER(PCINTR0)(void)
{
	PFbyte index=0;
	
	for(index=0;index<=pcintMaxIntCount;index++)
	{
		if(pcintVect0CallBackList[index] !=0)
		pcintVect0CallBackList[index]();
		
	}
}

void INT_HANDLER(PCINTR1)(void)
{
	PFbyte index=0;
	
	for(index=0;index<=pcintMaxIntCount;index++)
	{
		if(pcintVect1CallBackList[index] !=0)
		pcintVect1CallBackList[index]();
		
	}
}
void INT_HANDLER(PCINTR2)(void)
{
	PFbyte index=0;
	
	for(index=0;index<=pcintMaxIntCount;index++)
	{
		if(pcintVect2CallBackList[index] !=0)
		pcintVect1CallBackList[index]();
		
	}
}
#endif	//#if (PF_USE_PCINT == 1)
