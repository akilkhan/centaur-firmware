#include "prime_framework.h"
#include "prime_pwm.h"
static PFcallback pwmCmpMatchACallback;
static PFcallback pwmCmpMatchBCallback;
static PFcallback pwmOverFlowCallback;
static PFbyte pwmInitFlag=0;
static PFbyte pwmSettingVal=0;
static PFbyte pwmValue=0;


PFEnStatus pfPwmOpen(PFpCfgPwm config)
{
#ifdef PFPWM_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
    if( (config->pwmMode<0) || ((config->pwmMode > enTimerCtcMode)))
	{
		return enStatusInvArgs;
	}
	
	if( (config->exMatchActionA < 0) || (config->exMatchActionA > 3) )
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionB < 0) || (config->exMatchActionB > 3) )
	{
		return enStatusInvArgs;
	}
	if(config->cmpMatchACallback ==0)
	{
		return enStatusInvArgs;
	}
	if(config->cmpMatchBCallback ==0)
	{
		return enStatusInvArgs;
	}
	if(config->overFlowCallback ==0)
	{
		return enStatusInvArgs;
	}	
#endif

	if((config->pwmMode& 0x01)!=0)
	{
		PWM_CHANNEL->TCCRA |=config->pwmMode;
		PWM_CHANNEL->TCCRB |=(config->pwmMode &0x04)<<1;
	}
	
	PWM_CHANNEL->OCRA =config->matchValueA;
	PWM_CHANNEL->OCRB =config->matchValueB;
	
	if(config->exMatchActionA !=enPwmExtMatchCtrlNone)
	{
		PWM_CHANNEL->TCCRA |=config->exMatchActionA <<6;
		GPIO_PORTB->DDR |=(1<<7);
	}		
	if(config->exMatchActionB!=enPwmExtMatchCtrlNone)
	{
		PWM_CHANNEL->TCCRA |=config->exMatchActionB <<4;
		GPIO_PORTG->DDR |=(1<<5);
	}		
	if(config->cmpMatchACallback !=0)
	{
		pwmCmpMatchACallback =config->cmpMatchACallback;
	}	
	if(config->cmpMatchBCallback !=0)
	{
		pwmCmpMatchBCallback =config->cmpMatchBCallback;
	}	
	if(config->overFlowCallback !=0)
	{
		pwmOverFlowCallback =config->overFlowCallback;
	}
	if(config->clockSource >=0 && config->clockSource <=7)
	{
		PWM_CHANNEL->TCCRB |= config->clockSource;
	}
	if(config->interrupt !=enPwmIntNone)
	{
		//	PWM_CHANNEL->TIMSK |=config.interrupt;
		TIMSK_INT->TIMER0 |= config->interrupt;	
	}	
	PWM_CHANNEL->TCNT = 0;
	pwmInitFlag=1;
	return enStatusSuccess;
	
}

PFEnStatus pfPwmClose(void)
{
#ifdef PFPWM_DEBUG
	if(pwmInitFlag ==0)
	{
		return enStatusNotConfigured;
	}
#endif
	PWM_CHANNEL->TCCRB = 0x00;
	PWM_CHANNEL->TCNT = 0;
	return enStatusSuccess;
		
}

PFEnStatus pfPwmStop(void)
{
#ifdef PFPWM_DEBUG
	if((pwmInitFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif
	pwmSettingVal=PWM_CHANNEL->TCCRB;
	pwmValue=PWM_CHANNEL->TCNT;
	PWM_CHANNEL->TCCRB = 0x00;
	return enStatusSuccess;

}

PFEnStatus pfPwmStart(void)
{
	PFbyte val;
	
#ifdef PFPWM_DEBUG
	if((pwmInitFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif	
	if(pwmSettingVal!=0)
	{
		PWM_CHANNEL->TCCRB = pwmSettingVal;
		PWM_CHANNEL->TCNT = pwmValue;
	}
	else
	{
		val =PWM_CHANNEL->TCCRB;
		PWM_CHANNEL->TCCRB = 0x00;
		PWM_CHANNEL->TCCRB =val;
		PWM_CHANNEL->TCNT = 0;
	}
	return	enStatusSuccess;	
	
}
PFEnStatus pfPwmReset(void)
{
#ifdef PFPWM_DEBUG
	if((pwmInitFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif
	PWM_CHANNEL->TCNT = 0;
	return enStatusSuccess;
	
}
PFbyte pfPwmReadCount(PFbyte *data)
{
#ifdef PFPWM_DEBUG
	if((pwmInitFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif
	*data =PWM_CHANNEL->TCNT;
	return enStatusSuccess;
	
}

//ISR(TIMER0_COMPA_vect)
void INT_HANDLER(TIMER0_COMPA)(void)
//INT_HANDLER(TIMER0_COMPA)
{
	if(pwmCmpMatchACallback !=0)
	pwmCmpMatchACallback();
}


//ISR(TIMER0_COMPB_vect)
void INT_HANDLER(TIMER0_COMPB)(void)
//INT_HANDLER(TIMER0_COMPB)
{
	if(pwmCmpMatchBCallback !=0)
	pwmCmpMatchBCallback();
}


////ISR(TIMER0_OVF_vect)
void INT_HANDLER(TIMER0_OVF)(void)
//INT_HANDLER(TIMER0_OVF)
{
	if(pwmOverFlowCallback !=0)
	pwmOverFlowCallback();
}


PFEnStatus pfPwmUpdateMatchRegister(PFbyte regNum, PFdword regVal)
{
	switch(regNum)
	{
		case 0:
		PWM_CHANNEL->OCRA =regVal;
		break;
		
		case 1:
		PWM_CHANNEL->OCRB =regVal;
		break;
		
		default:
			return enStatusInvArgs;
	}
	
	return enStatusSuccess;
}
