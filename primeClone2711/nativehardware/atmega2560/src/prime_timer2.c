#include "prime_framework.h"
#if (PF_USE_TIMER2 == 1)
#include "prime_timer2.h"

static PFEnStatus timer2InitFlag=0;
static PFcallback timer2CmpMatchACallback;
static PFcallback timer2CmpMatchBCallback;
static PFcallback timer2OverFlowCallback;

PFEnStatus pfTimer2Open(PFpCfgTimer2 config)
{
#ifdef PF_TIMER2_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
    if(config->timer2Mode > enTimer2CtcMode)
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionA < 0) || (config->exMatchActionA > 3) )
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionB < 0) || (config->exMatchActionB > 3) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER2_INT_MATCHREG_A) != 0) && (config->cmpMatchACallback == 0) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER2_INT_MATCHREG_B) != 0) && (config->cmpMatchBCallback == 0) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER2_INT_OVERFLOW) != 0) && (config->overFlowCallback == 0) )
	{
		return enStatusInvArgs;
	}		
#endif
	
	if(config->timer2Mode !=0)
	{
		TIMER2_CHANNEL->TCCRA |=config->timer2Mode;
	}	
	TIMER2_CHANNEL->OCRA =config->matchValueA;
	TIMER2_CHANNEL->OCRB =config->matchValueB;
	
	//TIMSK(TIMER2_CHANNEL) = config->interrupt;
	if(config->exMatchActionA !=enTimer2ExtMatchCtrlNone)
	{
		TIMER2_CHANNEL->TCCRA |=config->exMatchActionA << 6;
	}
		
	if(config->exMatchActionB!=enTimer2ExtMatchCtrlNone)
	{
		TIMER2_CHANNEL->TCCRA |=config->exMatchActionB << 4;
	}
		
	if(config->cmpMatchACallback !=0)
	{
		timer2CmpMatchACallback =config->cmpMatchACallback;
	}	
	if(config->cmpMatchBCallback !=0)
	{
		timer2CmpMatchBCallback =config->cmpMatchBCallback;
	}	
	if(config->overFlowCallback !=0)
	{
		timer2OverFlowCallback =config->overFlowCallback;
	}
	if(config->interrupt != TIMER2_INT_NONE)
	{
		TIMSK_INT->TIMER2 = config->interrupt;
	}
	if(config->clockSource <= 7)
	{
		TIMER2_CHANNEL->TCCRB |= config->clockSource;
	}
	
	
	timer2InitFlag = enBooleanTrue;

	return enStatusSuccess;
	
}

PFEnStatus pfTimer2Close(void)
{
#ifdef PF_TIMER2_DEBUG
    if(timer2InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif

	TIMER2_CHANNEL->TCCRB = 0x00;
	TIMER2_CHANNEL->TCNT = 0;
	return enStatusSuccess;
	
}

PFEnStatus pfTimer2Start(void)
{
	PFbyte val;
#ifdef PF_TIMER2_DEBUG
    if(timer2InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif

	val=TIMER2_CHANNEL->TCCRB;
	TIMER2_CHANNEL->TCCRB = 0x00;
	TIMER2_CHANNEL->TCCRB |= val;
	TIMER2_CHANNEL->TCNT = 0;
	return enStatusSuccess;
}
PFEnStatus pfTimer2Reset(void)
{
#ifdef PF_TIMER2_DEBUG
    if(timer2InitFlag == 0)
	{
		return enStatusNotConfigured;
	}		
#endif

	TIMER2_CHANNEL->TCNT = 0;
	return enStatusSuccess;
}
PFEnStatus pfTimer2ReadCount(PFbyte* data)
{
#ifdef PF_TIMER2_DEBUG
    if(timer2InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	*data =TIMER2_CHANNEL->TCNT;
	return enStatusSuccess;
}

PFEnStatus pfTimer2UpdateMatchRegister(PFbyte regNum, PFdword regVal)
{
#ifdef PF_TIMER2_DEBUG
    if(timer2InitFlag == 0)
	{
		return enStatusNotConfigured;
	}		
#endif
	switch(regNum)
	{
		case 0:	
				TIMER2_CHANNEL->OCRA =regVal;
				break;
		
		case 1:
				TIMER2_CHANNEL->OCRB =regVal;
				break;
	}
	return enStatusSuccess;
}

PFEnStatus pfTimer2IntEnable(PFbyte interrupt)
{
#ifdef PF_TIMER2_DEBUG
    if(interrupt && (~TIMER2_INT_ALL))
	{
		return enStatusInvArgs;
	}
#endif
	TIMSK_INT->TIMER2 |= interrupt;
	return enStatusSuccess;	
}

PFEnStatus pfTimer2IntDisable(PFbyte interrupt)
{
#ifdef PF_TIMER2_DEBUG
    if(interrupt && (~TIMER2_INT_ALL))
	{
		return enStatusInvArgs;
	}
#endif
	TIMSK_INT->TIMER2 &= (~interrupt);
	return enStatusSuccess;	
}

void INT_HANDLER(TIMER2_COMPA)(void)
{
	if(timer2CmpMatchACallback !=0)
	timer2CmpMatchACallback();
}


void INT_HANDLER(TIMER2_COMPB)(void)
{
	if(timer2CmpMatchBCallback !=0)
	timer2CmpMatchBCallback();
}

void INT_HANDLER(TIMER2_OVF)(void)
{
	if(timer2OverFlowCallback !=0)
	timer2OverFlowCallback();
}
#endif		//#if (PF_USE_TIMER2 == 1)