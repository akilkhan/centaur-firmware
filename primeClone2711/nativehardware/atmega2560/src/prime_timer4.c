#include "prime_framework.h"
#if (PF_USE_TIMER4 == 1)
#include "prime_timer4.h"

static PFEnStatus timer4InitFlag=0;
static PFcallback timer4cmpMatchACallback;
static PFcallback timer4cmpMatchBCallback;
static PFcallback timer4cmpMatchCCallback;
static PFcallback timer4inputCaptureCallback;
static PFcallback timer4overFlowCallback;

PFEnStatus pfTimer4Open(PFpCfgTimer4 config)
{
#ifdef PF_TIMER4_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}		
    if( (config->timer4Mode != 0x08) && (config->timer4Mode != 0x18) )
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionA < 0) || (config->exMatchActionA > 3) )
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionB < 0) || (config->exMatchActionB > 3) )
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionC < 0) || (config->exMatchActionC> 3) )
	{
		return enStatusInvArgs;
	}		

		if( ((config->interrupt & TIMER4_INT_MATCHREG_A) != 0) && (config->cmpMatchACallback == 0) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER4_INT_MATCHREG_B) != 0) && (config->cmpMatchBCallback == 0) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER4_INT_MATCHREG_C) != 0) && (config->cmpMatchCCallback == 0) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER4_INT_OVERFLOW) != 0) && (config->overFlowCallback == 0) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER4_INT_INPUTCAPTURE) != 0) && (config->inputCaptureCallback == 0) )
	{
		return enStatusInvArgs;
	}
#endif

// 	TIMER4_CHANNEL->TCCRA = 0;
// 	TIMER4_CHANNEL->TCCRB = 0;
// 	
	if(config->timer4Mode)
	{
		TIMER4_CHANNEL->TCCRB = config->timer4Mode;
	}	
	TIMER4_CHANNEL->OCRA = config->matchValueA;
	TIMER4_CHANNEL->OCRB = config->matchValueB;
	TIMER4_CHANNEL->OCRC = config->matchValueC;
	
	if(config->inputCaptureRisingEdge)
	{
		TIMER4_CHANNEL->TCCRB |= 1<<6;
	}	
	if(Timer4InputCaptureNoiseCanceler__)
	{
		TIMER4_CHANNEL->TCCRB |= 1<<7;
	}	
	if(config->exMatchActionA != enTimer4ExtMatchCtrlNone)
	{
		TIMER4_CHANNEL->TCCRA = config->exMatchActionA << 6;
	}	
	if(config->exMatchActionB!=enTimer4ExtMatchCtrlNone)
	{
		TIMER4_CHANNEL->TCCRA |=config->exMatchActionB << 4;
	}	
	if(config->exMatchActionC!=enTimer4ExtMatchCtrlNone)
	{
		TIMER4_CHANNEL->TCCRA |=config->exMatchActionC << 2;
	}	
	if(config->cmpMatchACallback !=0)
	{
		timer4cmpMatchACallback =config->cmpMatchACallback;
	}	
	if(config->cmpMatchBCallback !=0)
	{
		timer4cmpMatchBCallback =config->cmpMatchBCallback;
	}	
	if(config->cmpMatchCCallback !=0)
	{
		timer4cmpMatchCCallback =config->cmpMatchCCallback;
	}	
	if(config->inputCaptureCallback !=0)
	{
		timer4inputCaptureCallback =config->inputCaptureCallback;
	}	
	if(config->overFlowCallback !=0)
	{
		timer4overFlowCallback =config->overFlowCallback;
	}
	if( config->interrupt != TIMER4_INT_NONE )
	{
		TIMSK_INT->TIMER4 = config->interrupt;
	}		
	if(config->clockSource <= 7)
	{
		TIMER4_CHANNEL->TCCRB |= config->clockSource;
	}		

    timer4InitFlag=1;

	return enStatusSuccess;
	
}

PFEnStatus pfTimer4Close(void)
{
#ifdef PF_TIMER4_DEBUG
    if(timer4InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	TIMER4_CHANNEL->TCCRB = 0x00;
	TIMER4_CHANNEL->TCNT = 0;
	
	return enStatusSuccess;
}

PFEnStatus pfTimer4Start(void)
{
#ifdef PF_TIMER4_DEBUG
	if(timer4InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	PFbyte val;
	val=TIMER4_CHANNEL->TCCRB;
	TIMER4_CHANNEL->TCCRB = 0x00;
	TIMER4_CHANNEL->TCCRB |= val;
	TIMER4_CHANNEL->TCNT = 0;
	
	return enStatusSuccess;
}
PFEnStatus pfTimer4Reset(void)
{
#ifdef PF_TIMER4_DEBUG
	if(timer4InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	TIMER4_CHANNEL->TCNT = 0;
	return enStatusSuccess;
}

PFEnStatus pfTimer4ReadCount(PFdword* count)
{
#ifdef PF_TIMER4_DEBUG
	if(timer4InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
	if(count == 0)
	{
		return enStatusInvArgs;
	}
#endif
	*count = TIMER4_CHANNEL->TCNT;
	return enStatusSuccess;
}

PFEnStatus pfTimer4ReadCaptureCount(PFdword* count)
{
#ifdef PF_TIMER4_DEBUG
	if(timer4InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
	if(count == 0)
	{
		return enStatusInvArgs;
	}
#endif
	*count = TIMER4_CHANNEL->ICR;
	return enStatusSuccess;
}

PFEnStatus pfTimer4UpdateMatchRegister(PFbyte regNum, PFword regVal)
{
#ifdef PF_TIMER4_DEBUG
	if(timer4InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
	if( (regNum < 'A') || (regNum > 'C') )
	{
		return enStatusNotConfigured;
	}	
#endif
	switch(regNum)
	{
		case 'A':	
				  TIMER4_CHANNEL->OCRA =regVal;
				  break;
		
		case 'B':
				  TIMER4_CHANNEL->OCRB =regVal;
				  break;
				
		case 'C':
				  TIMER4_CHANNEL->OCRC =regVal;
				  break;
	}
	return enStatusSuccess;
}

PFEnStatus pfTimer4IntEnable(PFbyte interrupt)
{
#ifdef PF_TIMER4_DEBUG
    if(interrupt && (~TIMER4_INT_ALL))
	{
		return enStatusInvArgs;
	}
#endif
	TIMSK_INT->TIMER4 |= interrupt;
	return enStatusSuccess;	
}

PFEnStatus pfTimer4IntDisable(PFbyte interrupt)
{
#ifdef PF_TIMER4_DEBUG
    if(interrupt && (~TIMER4_INT_ALL))
	{
		return enStatusInvArgs;
	}
#endif
	TIMSK_INT->TIMER4 &= (~interrupt);
	return enStatusSuccess;	
}

void INT_HANDLER(TIMER4_COMPA)(void)
{
	if(timer4cmpMatchACallback !=0)
	timer4cmpMatchACallback();
}

void INT_HANDLER(TIMER4_COMPB)(void)
{
	if(timer4cmpMatchBCallback !=0)
	timer4cmpMatchBCallback();
}

void INT_HANDLER(TIMER4_COMPC)(void)
{
	if(timer4cmpMatchCCallback !=0)
	timer4cmpMatchCCallback();
}

void INT_HANDLER(TIMER4_CAPT)(void)
{
	if(timer4inputCaptureCallback !=0)
	timer4inputCaptureCallback();
}

void INT_HANDLER(TIMER4_OVF)(void)
{
	if(timer4overFlowCallback !=0)
	timer4overFlowCallback();
}

#endif		// #if (PF_USE_TIMER4 == 1)
