#include "prime_framework.h"
#if (PF_USE_EINT6 == 1)
#include "prime_eint6.h"


static PFcallback eint6CallBackList[PF_EXT_INT_MAX_CALLBACK]={0};
static PFbyte eint6MaxIntCount=0;
static PFbyte eint6InitFlag = 0;


PFEnStatus pfEint6Open(PFpCfgEint6 config)
{
	PFbyte index;
#ifdef PF_EINT6_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
	if((config->mode > 3)|| (config->mode < enIntModeLowLevel))
	{
		return enStatusInvArgs;
	}
	if(config->callbackList == 0)
	{
		return enStatusInvArgs;
	}
	if(config->maxCallbacks < 0)
	{
		return enStatusInvArgs;
	}
#endif

	if(config->mode <=3)
	{
		EXTINT->EICRB |=config->mode <<(EINT6_CH-3);
	}	
	
	eint6MaxIntCount=config->maxCallbacks ;
// 	if(eint6MaxIntCount >0)
// 	eint6CallBackList =(PFcallback*)malloc(sizeof(PFcallback)*eint6MaxIntCount);
// 	else
// 	return enStatusNotSupported;
		
	for(index=0;index<eint6MaxIntCount;index++)
	{
		if(config->callbackList!=0 )
		eint6CallBackList[index]=config->callbackList[index];
	}
	 eint6InitFlag = enBooleanTrue ;
	
	return enStatusSuccess;
	
}


PFEnStatus pfEint6Enable(void)
{
#ifdef PF_EINT6_DEBUG
	if(eint6InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK |= (1<<EINT6_CH);
	return enStatusSuccess;
}


PFEnStatus pfEint6Disable(void)
{
#ifdef PF_EINT6_DEBUG
	if(eint6InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK &=~(1<<EINT6_CH);
	return enStatusSuccess;
}



PFEnStatus pfEint6AddCallback(PFcallback callback)
{
	PFbyte index=0;
#ifdef PF_EINT6_DEBUG
	if(eint6InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
	if(callback == 0)
	{
		return enStatusInvArgs;
	}
#endif	
	for(index=0;index<=eint6MaxIntCount;index++)
	{
		if (eint6CallBackList[index]==0)
		{
			eint6CallBackList[index]=callback;
			break;
		}
	}
	
	return enStatusSuccess;
}

PFEnStatus pfEint6RemoveCallback(PFcallback callback)
{
	PFbyte index=0;
#ifdef PF_EINT6_DEBUG
	if(eint6InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	
	for(index=0;index<=eint6MaxIntCount;index++)
	{
		if (eint6CallBackList[index]==callback)
		{
			eint6CallBackList[index]=0;
			break;
		}
	}
	return enStatusSuccess;
	
}


PFEnStatus pfEint6Close(void)
{
#ifdef PF_EINT6_DEBUG
	if(eint6InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK &=~(1<<EINT6_CH);
	//free(eint6CallBackList);
	return enStatusSuccess;
}

void INT_HANDLER(EINT6_CHANNEL)(void)
{
	PFbyte index=0;
	
	for(index=0;index<=eint6MaxIntCount;index++)
	{
		if(eint6CallBackList[index] !=0)
		eint6CallBackList[index]();
		
	}
}
#endif		//#if (PF_USE_EINT6 == 1)


