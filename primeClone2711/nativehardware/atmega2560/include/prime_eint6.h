/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework External Interrupt driver for ATmega2560.
 *
 * 
 * Review status: NO
 *
 */
#pragma once

/**
 * \defgroup PF_EXT_INT_API EXT INT API
 * @{
 */ 

/**		Enumeration for External Interrupt mode	*/

#define EINT6_CH				6
#define EINT6_CHANNEL			PF_CONCAT(EINT, EINT6_CH)

typedef enum{
	enIntModeLowLevel = 0,		/**< The low level of EINT6 generates an interrupt request						*/	
	enIntModToggle,				/**< Any edge of EINT6 generates asynchronously an interrupt request			*/	
	enIntModeFallingEdge,		/**< The falling edge of EINT6 generates asynchronously an interrupt request	*/	
	enIntModeRisingEdge			/**< The rising edge of EINT6 generates asynchronously an interrupt request		*/	
}PFEnEint6Mode;

/**		External interrupt	configure Structure	*/
typedef struct
{
	PFEnEint6Mode	mode;								/**< External interrupt mode									*/
	PFdword			maxCallbacks;						/**< Maximum number of callbacks allowed for tailchaining		*/
	PFcallback* callbackList;			/**< Pointer to array of callbacks to attach to interrupt		*/
}PFCfgEint6;

typedef PFCfgEint6* PFpCfgEint6;

/**
 * The function configures and enables External Interrupt with given settings.
 
 * \param config configuration structure which contains the settings for the external interrupt to be used.

 * \return External Interrupt status.
 */
PFEnStatus pfEint6Open(PFpCfgEint6 config);

/**
 * The function enables External Interrupt 
 *
 * \return External Interrupt status.
 */
PFEnStatus pfEint6Enable(void);

/**
 * The function disables External Interrupt 
 *
 * \return External Interrupt status.
 */
PFEnStatus pfEint6Disable(void);

/**
 * The function adds a callback to callback list if tailchaining is enabled.
 *
 * \param callback callback function to add in the callback list.
 *
 * \return add callback status
 */
PFEnStatus pfEint6AddCallback(PFcallback callback);

/**
 * The function removes the specified callback from callback list.
 *
 * \param callback callback function to add in the callback list.
 *
 * \return remove callback status
 */
PFEnStatus pfEint6RemoveCallback(PFcallback callback);

/**
 * The function disables External Interrupt .
 *
 * \return External Interrupt status.
 */
PFEnStatus pfEint6Close(void);


/** @} */
