/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework UART2 driver for ATmega2560.
 *
 * 
 * Review status: NO
 *
 */
#pragma once

#define  UART2_USE_FIFO 1

#define UART2_CH				UART2
#define UART2_CHANNEL		PERIPH(UART2_CH)

#if(UART2_USE_FIFO != 0)
	#define  UART2_BUFFER_SIZE 64
#endif	//#if(UART2_USE_FIFO != 0)

typedef enum
{
	enUart2Baudrate_2400 = 0,		/**< UART2 baudrate 2400				*/
	enUart2Baudrate_4800,			/**< UART2 baudrate 4800				*/
	enUart2Baudrate_9600,			/**< UART2 baudrate 9600				*/
	enUart2Baudrate_19200,			/**< UART2 baudrate 19200			*/
	enUart2Baudrate_38400,			/**< UART2 baudrate 38400			*/
	enUart2Baudrate_57600,			/**< UART2 baudrate 57600			*/
	enUart2Baudrate_115200			/**< UART2 baudrate 115200			*/
}PFEnUart2Baudrate;

/**		Enumeration for parity setting for the Uart2 channel		*/
typedef enum
{
	enUart2ParityNone = 0,			/**< Selecting No parity for communication for Uart2	*/
	enUart2ParityEven,				/**< Selecting Even parity for communication  for Uart2*/
	enUart2parityOdd				/**< Selecting Odd parity for communication  for Uart2	*/
}PFEnUart2Parity;

/**		Enumeration for number of stop bits to use for Uart2 channel		*/
typedef enum
{
	enUart2StopBits_1 = 0,			/**< Selecting Number of Stop Bits = 1  for Uart2	*/
	enUart2StopBits_2,				/**< Selecting Number of Stop Bits = 2  for Uart2	*/
}PFEnUart2StopBits;

typedef enum
{
	enUart2DataBits_5=0x00,			/**< Selecting Number of Date Bits = 5  for Uart2	*/
	enUart2DataBits_6=0x01,			/**< Selecting Number of Date Bits = 6  for Uart2*/
	enUart2DataBits_7=0x02,			/**< Selecting Number of Date Bits = 7  for Uart2	*/
	enUart2DataBits_8=0x03,			/**< Selecting Number of Date Bits = 8 	for Uart2*/
	enUart2DataBits_9=0x07			/**< Selecting Number of Date Bits = 9  for Uart2	*/
}PFEnUart2DataBits;



/**		Enumeration for interrupts to enable for the Uart2 channel		*/
typedef enum
{
	enUart2IntNone,					/**< Interrupts dissabled  for Uart2*/
	enUart2IntTx=0x04,				/**< Transmission Interrupts are enabled  for Uart2*/
	enUart2IntRx=0x08,				/**< Reception Interrupts are enabled  for Uart2*/
	enUart2IntTxRx=0x0c				/**< Transmission as well as reception Interrupts are enabled for Uart2*/
}PFEnUart2Interrupt;


typedef enum
{
	enUart2ModeTx=0x01,				/**<  Uart2 in transmission mode */
 	enUart2ModeRx,					/**<  Uart2 in reception mode */
	enUart2ModeTxRx					/**<  Uart2 in transmission as well as reception mode */
}PFEnUart2Mode;

/**		Uart2  configuration structure			*/
typedef struct
{
	PFEnUart2Mode		mode;			/**<   receiver or transmitter mode */
	PFEnUart2Baudrate 	baudrate;		/**<   Set baudrate for channel */
	PFEnUart2Parity 		parity;		/**<   Parity to be used for communication */
	PFEnUart2StopBits 	stopBits;		/**<   Number of stop bits to be used */
	PFEnUart2DataBits 	dataBits;		/**<   Number of stop bits to be used */
	PFEnUart2Interrupt interrupts;		/**<   Interrupts to enable for the channel */
#if(Uart2_USE_FIFO == 0)
	PFcallback		transmitCallback;
	PFcallback		receiveCallback;
#endif
}PFCfgUart2;
typedef PFCfgUart2* PFpCfgUart2;


/**
Description: The function initializes the port with given settings.

Parameters:
config - configuration structure which contains the settings for the communication channel to be used.

Return:	ENStatus. Returns UART2 initialization status.
*/
PFEnStatus pfUart2Open(PFpCfgUart2 config);


/**
Description: Turn offs the UART2 channel

Return:	ENStatus. Returns UART2 initialization status.
 */
PFEnStatus pfUart2Close(void);


/**
Description: The function writes one byte to UART2 data register

Parameters:
channel - UART2 channel to be used for communication.
data - 8 bit data to write to the UART2 channel.

Return:	ENStatus. Returns UART2 write status.
*/
PFEnStatus pfUart2WriteByte(PFbyte data);

/**
Description: 	The function sends multiple bytes on UART2 channel. 
				If transmit interrupt is enabled, the function will enqueue the data in transmit FIFO.
				Otherwise it will wait in the function and send each byte by polling the line status.

Parameters:
channel - UART2 channel to be used for communication.
data - Unsigned char pointer to the data to be sent.
size - Total number of bytes to send.

Return:	ENStatus. Returns UART2 write status.
*/
PFEnStatus pfUart2Write(PFbyte* data, PFdword size);

/**
 * The function reads one byte from UART2 channel.
 * If receive interrupt is enabled, the function will read the byte from receive FIFO.
 * Otherwise it will wait in the function for one byte to receive by polling the line status.
 *
 * \return read byte from UART2 channel.
 */
PFEnStatus pfUart2ReadByte(PFbyte* data);

/**
 * The function reads one byte from UART2 channel.
 * If receive interrupt is enabled, the function will read the byte from receive FIFO.
 * Otherwise it will wait in the function for one byte to receive by polling the line status.
 *
 * \param data Unsigned char pointer to the buffer where the read data should be loaded.
 * \param size Total number of bytes to read.
 * \return UART2 read status.
 */
PFEnStatus pfUart2Read(PFbyte* data, PFdword size, PFdword* readBytes);

#if(UART2_USE_FIFO != 0)
/**
 * Returns the number of bytes received in UART2 buffer.
 *
 * \return number of bytes received in UART2 buffer.
 */
PFEnStatus pfUart2GetRxBufferCount(PFdword* count);

/**
 * This function empties the transmit buffer.
 */
PFEnStatus pfUart2TxBufferFlush(void);

/**
 * This function empties the receive buffer.
 */
PFEnStatus pfUart2RxBufferFlush(void);

#endif

