/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework PWM0 driver for AT90CAN128.
 *
 * 
 * Review status: NO
 *
 */
 
 #pragma once
/**
 * \defgroup PF_TIMER_API Timer API
 * @{
 */ 

#define PWM2_CH				TIMER0
#define PWM2_CHANNEL			PERIPH(PWM2_CH)

#if(( PWM2_TIMER !=TIMER0) ||(PWM2_TIMER !=TIMER2))
	#error "PWM2 is available only on TIMER0 and TIMER2 for this controller "
#endif
  
 /** Enumeration for pwm modes			*/
typedef enum
{
	enPwm2Noclock = 0,				/**< No clock source is selected and the pwm is disabled	*/
	enPwm2NoprescaledClk,			/**< clock source is selected and the pwm is not pre-scaled	*/
	enPwm2ClkDivideby8,				/**< Clock source frequency is divided by 8	*/
	enPwm2ClkDivideby64,			/**< Clock source frequency is divided by 8	*/
	enPwm2ClkDivideby256,			/**< Clock source frequency is divided by 8	*/
	enPwm2ClkDivideby1024,			/**< Clock source frequency is divided by 8	*/
	enPwm2ExtClkFallingEdge,		/**<  pwm with external source on falling edge 	*/
	enPwm2ExtClkRisingEdge			/**<  pwm with external source on falling edge 	*/
}PFEnPwm2Clocksource;


/** Enumeration for external match control		*/
typedef enum
{
	enPwm2ExtMatchCtrlNone = 0,				/**< Do nothing on count match						*/
	enPwm2ExtMatchCtrlTogglePin,			/**< Toggle match pin								*/
	enPwm2ExtMatchCtrlClearPin,				/**< Clear match pin								*/
	enPwm2ExtMatchCtrlSetPin				/**< Set match pin									*/
}PFEnPwm2ExtMatchCtrl;

typedef enum
{
	enPwm2PhaseCrctMode=0x01,		/**<	Phase correct Mode 	*/
	enPwm2FastMode=0x03,			/**<	Fast Mode	*/  
}PFEnPwm2Mode;

/**	No interrupt	*/
#define PWM2_INT_NONE			0x00
/**	interrupt on overflow	*/
#define PWM2_INT_OVERFLOW		0x01
/**	interrupt on matching with the value in OCR0A 	*/
#define PWM2_INT_MATCHREG_A	0x02
/**	interrupt on matching with the value in OCR0B 	*/
#define PWM2_INT_MATCHREG_B	0x04
/**	interrupt on matching with the value in OCR0A ,OCR0B and overflow 	*/
#define PWM2_INT_ALL			0x07

/**		Timer configure structure		*/
typedef struct
{
	PFEnPwm2Clocksource	    clockSource;		/**< Select clock source				*/
	PFEnPwm2Mode			pwm2Mode;			/** PWM2 mode*/
	PFbyte				    matchValueA;		/**< Match register A compare value							*/
	PFEnPwm2ExtMatchCtrl 	exMatchActionA;		/**< match pin control on count match 				*/ 
	PFbyte			     	interrupt;			/**< To enable or disable pwm interrupt			*/
	PFcallback			    cmpMatchACallback;	/**< Callback function for pwm ISR				*/
	PFcallback			    overFlowCallback;
}PFCfgPwm2;

/** Pinter to PFCfgTiemr structure		*/
typedef PFCfgPwm2* PPpCfgPwm2;

/**
 * Intialized pwm with given parameters
 * 
 * \param config pwm configuration structure
 * 
 * \return pwm intialization status
 */
PFEnStatus pfPwm2Open(PPpCfgPwm2 config);

/**
 * Stops pwm operation and turn offs the pwm module
 * 
 * \return pwm turn off operation status
 */
PFEnStatus pfPwm2Close(void);

/**
 * Starts pwm operation
 * 
 * \return pwm start status
 */
PFEnStatus pfPwm2Start(void);

/**
 * Stops pwm operation
 * 
 * \return pwm stop status
 */
PFEnStatus pfPwm2Stop(void);

/**
 * Resets the pwm operation. Timer will start counting from zero again.
 * 
 * \return pwm reset status
 */
PFEnStatus pfPwm2Reset(void);

/**
 * Returns the pwm count
 * \param  data pointer to the read value
 * \return pwm count
 */
PFEnStatus pfPwm2ReadCount(PFbyte *data);



/** @} */

