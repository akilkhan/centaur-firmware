/**
 *
 *                              Copyright (c) 2013
 *                      PhiRobotics Technologies Pvt Ltd
 *               Vedant Commercial Complex, Vartak Nagar, Thane(w),
 *                           Maharashtra-400606, India
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework External Interrupt driver for LPC17xx.
 *
 * 
 * Review status: NO
 *
 */
#pragma once

/**
 * \defgroup PF_EXT_INT_API EXT INT API
 * @{
 */ 

/**		Enumeration for External Interrupt mode	*/
#define EXTINT_MAX_NO		8
#define EXT_INT_CH			5
#define EXT_INT_CHANNEL		PF_CONCAT(EINT, EXT_INT_CH)
//#define EXT_INT_HANDLER		INT_HANDLER(EXT_INT_CHANNEL)

typedef enum{
	enIntModeLowLevel = 0,
	enIntModToggle,
	enIntModeFallingEdge,	
	enIntModeRisingEdge	
}PFEnExtIntMode;

/**		External interrupt	configure Structure	*/
/**		External interrupt	configure Structure	*/
typedef struct
{
	PFEnExtIntMode	mode;								/**< External interrupt mode									*/
	PFdword			maxCallbacks;						/**< Maximum number of callbacks allowed for tailchaining		*/
	PFcallback* callbackList;			/**< Pointer to array of callbacks to attach to interrupt		*/
}PFCfgExtInt;

typedef PFCfgExtInt* PPFCfgExtInt;

/**
 * The function configures and enables External Interrupt with given settings.
 
 * \param config configuration structure which contains the settings for the external interrupt to be used.

 * \return External Interrupt status.
 */
PFbyte extIntOpen(PPFCfgExtInt config);

/**
 * The function enables External Interrupt 
 *
 * \return External Interrupt status.
 */
PFEnStatus extIntEnable(void);

/**
 * The function disables External Interrupt 
 *
 * \return External Interrupt status.
 */
PFEnStatus extIntDisable(void);

/**
 * The function adds a callback to callback list if tailchaining is enabled.
 *
 * \param callback callback function to add in the callback list.
 *
 * \return add callback status
 */
PFEnStatus extIntAddCallback(PFcallback callback);

/**
 * The function removes the specified callback from callback list.
 *
 * \param callback callback function to add in the callback list.
 *
 * \return remove callback status
 */
PFEnStatus extIntRemoveCallback(PFcallback callback);

/**
 * The function disables External Interrupt .
 *
 * \return External Interrupt status.
 */
PFEnStatus extIntClose(void);


/** @} */
