/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework PWM0 driver for AT90CAN128.
 *
 * 
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_TIMER_API Timer API
 * @{
 */ 

#define PWM0_CH				TIMER0
#define PWM0_CHANNEL		PERIPH(PWM0_CH)

 #if(( PWM0_TIMER !=TIMER0) ||(PWM0_TIMER !=TIMER2))
	#error "PWM0 is available only on TIMER0 and TIMER2 for this controller "
 #endif
 
 

  
	
/** Enumeration for timer modes			*/
typedef enum
{
	enPwm0Noclock = 0,			/**No clock source is selected and the timer is disabled	*/
	enPwm0NoprescaledClk,		/**< clock source is selected and the timer is not pre-scaled	*/
	enPwm0ClkDivideby8,			/**< Clock source freqency is divided by 8	*/
	enPwm0ClkDivideby64,		/**< Clock source freqency is divided by 8	*/
	enPwm0ClkDivideby256,		/**< Clock source freqency is divided by 8	*/
	enPwm0ClkDivideby1024,		/**< Clock source freqency is divided by 8	*/
	enPwm0ExtClkFallingEdge,	/**<  timer with external source on falling edge 	*/
	enPwm0ExtClkRisingEdge		/**<  timer with external source on falling edge 	*/
}PFEnPwm0Clocksource;


/** Enumeration for external match control		*/
typedef enum
{
	enPwm0ExtMatchCtrlNone = 0,				/**< Do nothing on count match						*/
	enPwm0ExtMatchCtrlTogglePin,			/**< Toggle match pin								*/
	enPwm0ExtMatchCtrlClearPin,				/**< Clear match pin								*/
	enPwm0ExtMatchCtrlSetPin				/**< Set match pin									*/
}PFEnPwm0ExtMatchCtrl;

typedef enum
{
	enPwm0PhaseCrctMode=0x01,	/**<	Phase correct Mode 	*/
	enPwm0FastMode=0x03,			/**<	Fast Mode	*/  
}PFEnPwm0Mode;

/**	No interrupt	*/
#define PWM0_INT_NONE			0x00
/**	interrupt on overflow	*/
#define PWM0_INT_OVERFLOW		0x01
/**	interrupt on matching with the value in OCR0A 	*/
#define PWM0_INT_MATCHREG_A	0x02
/**	interrupt on matching with the value in OCR0B 	*/
#define PWM0_INT_MATCHREG_B	0x04
/**	interrupt on matching with the value in OCR0A ,OCR0B and overflow 	*/
#define PWM0_INT_ALL			0x07

/**		Timer configure structure		*/
typedef struct
{
	PFEnPwm0Clocksource	    clockSource;		/**< Select clock source				*/
	PFEnPwm0Mode			pwm0Mode;			/** PWM0 mode*/
	PFbyte				    matchValueA;		/**< Match register A compare value							*/
	PFEnPwm0ExtMatchCtrl 	exMatchActionA;	/**< match pin control on count match 				*/ 
	PFbyte			     	interrupt;			/**< To enable or disable timer interrupt			*/
	PFcallback			    cmpMatchACallback;	/**< Callback function for timer ISR				*/
	PFcallback			    overFlowCallback;
}PFCfgPwm0;

/** Pinter to PFCfgTiemr structure		*/
typedef PFCfgPwm0* PFpCfgPwm0;

/**
 * Intialized timer with given parameters
 * 
 * \param config timer configuration structure
 * 
 * \return timer intialization status
 */
PFEnStatus pfPwm0Open(PPFCfgPwm0 config);

/**
 * Stops timer operation and turn offs the timer module
 * 
 * \return timer turn off operation status
 */
PFEnStatus pfPwm0Close(void);

/**
 * Starts timer operation
 * 
 * \return timer start status
 */
PFEnStatus pfPwm0Start(void);

/**
 * Stops timer operation
 * 
 * \return timer stop status
 */
PFEnStatus pfPwm0Stop(void);

/**
 * Resets the timer operation. Timer will start counting from zero again.
 * 
 * \return timer reset status
 */
PFEnStatus pfPwm0Reset(void);


/**
 * Returns the timer count
 * 
 * \param  data pointer to the read value
 * \return timer count
 */
PFEnStatus pfPwm0ReadCount(PFbyte *data);



/** @} */

