#include "prime_framework.h"
#if (PF_USE_EINT3 == 1)
#include "prime_eint3.h"


static PFcallback eint3CallBackList[PF_EXT_INT_MAX_CALLBACK]={0};
static PFbyte eint3MaxIntCount=0;
static PFbyte eint3InitFlag = 0;


PFEnStatus pfEint3Open(PFpCfgEint3 config)
{
	PFbyte index;
#ifdef PF_EINT3_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
	if((config->mode > 3)|| (config->mode < enIntModeLowLevel))
	{
		return enStatusInvArgs;
	}
	if(config->callbackList == 0)
	{
		return enStatusInvArgs;
	}
	if(config->maxCallbacks < 0)
	{
		return enStatusInvArgs;
	}
#endif

	if(config->mode <=3)
	{
		EXTINT->EICRA |=config->mode <<EINT3_CH ;
	}	
	
	eint3MaxIntCount=config->maxCallbacks ;
// 	if(eint3MaxIntCount >0)
// 	eint3CallBackList =(PFcallback*)malloc(sizeof(PFcallback)*eint3MaxIntCount);
// 	else
// 	return enStatusNotSupported;
		
	for(index=0;index<eint3MaxIntCount;index++)
	{
		if(config->callbackList!=0 )
		eint3CallBackList[index]=config->callbackList[index];
	}
	 eint3InitFlag = enBooleanTrue ;
	
	return enStatusSuccess;
	
}


PFEnStatus pfEint3Enable(void)
{
#ifdef PF_EINT3_DEBUG
	if(eint3InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK |= (1<<EINT3_CH);
	return enStatusSuccess;
}


PFEnStatus pfEint3Disable(void)
{
#ifdef PF_EINT3_DEBUG
	if(eint3InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK &=~(1<<EINT3_CH);
	return enStatusSuccess;
}



PFEnStatus pfEint3AddCallback(PFcallback callback)
{
	PFbyte index=0;
#ifdef PF_EINT3_DEBUG
	if(eint3InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
	if(callback == 0)
	{
		return enStatusInvArgs;
	}
#endif	
	for(index=0;index<=eint3MaxIntCount;index++)
	{
		if (eint3CallBackList[index]==0)
		{
			eint3CallBackList[index]=callback;
			break;
		}
	}
	
	return enStatusSuccess;
}

PFEnStatus pfEint3RemoveCallback(PFcallback callback)
{
	PFbyte index=0;
#ifdef PF_EINT3_DEBUG
	if(eint3InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	
	for(index=0;index<=eint3MaxIntCount;index++)
	{
		if (eint3CallBackList[index]==callback)
		{
			eint3CallBackList[index]=0;
			break;
		}
	}
	return enStatusSuccess;
	
}


PFEnStatus pfEint3Close(void)
{
#ifdef PF_EINT3_DEBUG
	if(eint3InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK &=~(1<<EINT3_CH);
	//free(eint3CallBackList);
	return enStatusSuccess;
}

void INT_HANDLER(EINT3_CHANNEL)(void)
{
	PFbyte index=0;
	
	for(index=0;index<=eint3MaxIntCount;index++)
	{
		if(eint3CallBackList[index] !=0)
		eint3CallBackList[index]();
		
	}
}
#endif		//#if (PF_USE_EINT3 == 1)

