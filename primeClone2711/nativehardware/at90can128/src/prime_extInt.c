#include "prime_framework.h"
#include "prime_extint.h"
// #include <avr/io.h>
// #include <avr/interrupt.h>


static PFcallback extintcallBackList[EXTINT_MAX_NO]={0};
static PFbyte extintmaxIntCount=0;
static PFbyte extintinitFlag = 0;


PFEnStatus extIntrOpen(PPFCfgExtInt config)
{
	PFbyte index;
#ifdef PFEXTINT_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
	if((config->mode > 3)|| (config->mode < enIntModeLowLevel))
	{
		return enStatusInvArgs;
	}
	if(config->callBackList == 0)
	{
		return enStatusInvArgs;
	}
	if(config->maxCallbacks < 0)
	{
		return enStatusInvArgs;
	}
#endif

	if(config->mode <=3)
	{
		if(EXT_INT_CH <=3)
		EXTINT->EICRA |=config->mode <<EXT_INT_CH ;
		else
		EXTINT->EICRB |=config->mode <<(EXT_INT_CH-3);
	}	
	
	extintmaxIntCount=config->maxCallbacks ;
// 	if(extintmaxIntCount >0)
// 	extintcallBackList =(PFcallback*)malloc(sizeof(PFcallback)*extintmaxIntCount);
// 	else
// 	return enStatusNotSupported;
		
	for(index=0;index<extintmaxIntCount;index++)
	{
		if(config->callBackList!=0 )
		extintcallBackList[index]=config->callBackList[index];
	}
	 extintinitFlag = enBooleanTrue ;
	
	return enStatusSuccess;
	
}


PFEnStatus extIntEnable(void)
{
#ifdef PFEXTINT_DEBUG
	if(extintinitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK |= (1<<EXT_INT_CH);
	return enStatusSuccess;
}


PFEnStatus extIntDisable(void)
{
#ifdef PFEXTINT_DEBUG
	if(extintinitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK &=~(1<<EXT_INT_CH);
	return enStatusSuccess;
}



PFEnStatus extIntAddCallback(PFcallback callback)
{
	PFbyte index=0;
#ifdef PFEXTINT_DEBUG
	if(extintinitFlag == 0)
	{
		return enStatusNotConfigured;
	}
	if(callback == 0)
	{
		return enStatusInvArgs;
	}
#endif	
	for(index=0;index<=extintmaxIntCount;index++)
	{
		if (extintcallBackList[index]==0)
		{
			extintcallBackList[index]=callback;
			break;
		}
	}
	
	return enStatusSuccess;
}

PFEnStatus extIntRemoveCallback(PFcallback callback)
{
	PFbyte index=0;
#ifdef PFEXTINT_DEBUG
	if(extintinitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	
	for(index=0;index<=extintmaxIntCount;index++)
	{
		if (extintcallBackList[index]==callback)
		{
			extintcallBackList[index]=0;
			break;
		}
	}
	return enStatusSuccess;
	
}


PFEnStatus extIntClose(void)
{
#ifdef PFEXTINT_DEBUG
	if(extintinitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK &=~(1<<EXT_INT_CH);
	//free(extintcallBackList);
	return enStatusSuccess;
}

void INT_HANDLER(EXT_INT_CHANNEL)(void)
//ISR(INT5_vect)
{
	PFbyte index=0;
	
	for(index=0;index<=extintmaxIntCount;index++)
	{
		if(extintcallBackList[index] !=0)
		extintcallBackList[index]();
		
	}
	
	
	
}


