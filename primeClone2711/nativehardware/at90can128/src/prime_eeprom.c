#include "prime_framework.h"
#include "prime_eeprom.h"

PFbyte eeprominitFlag=0;

PFEnStatus pfEEpromOpen(PFpCfgEeprom config)
{
#ifdef PF_EEPROM_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
	if(config->mode > 2)
	{
		return enStatusInvArgs;
	}	
#endif
	
	EEPROM_CHANNEL->EECR= (config->mode << 4);
	
	
	eeprominitFlag =1;	
	return enStatusSuccess;
}

PFEnStatus pfEEpromRead(PFword address ,PFbyte *data,PFbyte size)
{
	PFword loop;
	
#ifdef PF_EEPROM_DEBUG
	if(eeprominitFlag ==0)
	{
		return enStatusNotConfigured;
	}	
#endif
	/* Wait for completion of previous write */
	while(EEPROM_CHANNEL->EECR & (BIT_1));

	for(loop =0 ;loop <size ; loop++)
	{
	/* Set up address register */
		EEPROM_CHANNEL->EEARL = address;
		EEPROM_CHANNEL->EEARH = (address >> 8);
	
		/* Start EEPROM_CHANNEL read by writing EERE */
		EEPROM_CHANNEL->EECR |= (BIT_0);
		/* Return data from Data Register */
		data[loop] = EEPROM_CHANNEL->EEDR;
		address++;
	}	
	
	return enStatusSuccess  ;
}

PFEnStatus pfEEpromWriteByte(PFword address, PFbyte ucData)
{
	PFbyte readByte, mpeByte, peByte;
	PFbyte* eecrPtr;
#ifdef PF_EEPROM_DEBUG
	if(eeprominitFlag ==0)
	{
		return enStatusNotConfigured;
	}	
#endif	
	/* Wait for completion of previous write */
	while(EEPROM_CHANNEL->EECR & (BIT_1));
	/* Set up address and Data Registers */
	EEPROM_CHANNEL->EEARL = address;
	EEPROM_CHANNEL->EEARH = address >>8;
	EEPROM_CHANNEL->EEDR = ucData;
	/* Write logical one to EEMPE */
	//EEPROM->EECR |= (BIT_2);
	
// 	readByte = EEPROM->EECR;
// 	eecrPtr = &EEPROM->EECR;
// 	mpeByte = readByte | BIT_2;
// 	peByte = mpeByte | BIT_1;
// 	__asm("OUT 0x3F,mpeByte");
// 	__asm("OUT 0x3F,peByte");
// 	*eecrPtr = mpeByte;
// 	*eecrPtr = peByte;
	/* Start eeprom write by setting EEPE */
	//EEPROM->EECR |= (BIT_1);

/* Following lines are added to ensure the setting of bit EEPE within 
	4 clk cycles after writing bit EEMPE 
	EECR register has address 0x1f	*/

	__asm("sbi 0x1F,2");
	__asm("sbi 0x1F,1");

	
	return enStatusSuccess;
}


PFEnStatus pfEEpromWrite(PFword address, PFbyte* ucData,PFword size)
{
	PFword loop;
	PFEnStatus ret=enStatusSuccess;
#ifdef PF_EEPROM_DEBUG
	if(size <= 0)
	{
		return enStatusInvArgs;
	}	
#endif
	
	for(loop =0 ;((loop <size)&&(ret == enStatusSuccess)) ; loop++)
	{
		ret= pfEEpromWriteByte( address++, ucData[loop]);
	}
	return ret;
}


PFEnStatus pfEEpromClose(void)
{
#ifdef PF_EEPROM_DEBUG
	if(eeprominitFlag ==0)
	{
		return enStatusNotConfigured;
	}	
#endif
	EEPROM_CHANNEL->EECR &= ~((1<<BIT_2)|(1<<BIT_0));
	return enStatusSuccess;
}
