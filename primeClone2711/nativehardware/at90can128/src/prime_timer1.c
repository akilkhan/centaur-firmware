#include "prime_framework.h"
#if (PF_USE_TIMER1 == 1)
#include "prime_timer1.h"

static PFEnStatus timer1InitFlag=0;
static PFcallback timer1cmpMatchACallback;
static PFcallback timer1cmpMatchBCallback;
static PFcallback timer1cmpMatchCCallback;
static PFcallback timer1inputCaptureCallback;
static PFcallback timer1overFlowCallback;

PFEnStatus pfTimer1Open(PFpCfgTimer1 config)
{
#ifdef PF_TIMER1_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}		
    if( (config->timer1Mode != 0x08) && (config->timer1Mode != 0x18) )
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionA < 0) || (config->exMatchActionA > 3) )
	{
		return enStatusInvArgs;
	}
	if(  (config->exMatchActionB < 0) || (config->exMatchActionB > 3) )
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionC < 0) || (config->exMatchActionC > 3) )
	{
		return enStatusInvArgs;
	}		
		
	if( ((config->interrupt & TIMER1_INT_MATCHREG_A) != 0) && (config->cmpMatchACallback == 0) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER1_INT_MATCHREG_B) != 0) && (config->cmpMatchBCallback == 0) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER1_INT_MATCHREG_C) != 0) && (config->cmpMatchCCallback == 0) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER1_INT_OVERFLOW) != 0) && (config->overFlowCallback == 0) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER1_INT_INPUTCAPTURE) != 0) && (config->inputCaptureCallback == 0) )
	{
		return enStatusInvArgs;
	}
	
#endif
	if(config->timer1Mode)
	{
		TIMER1_CHANNEL->TCCRB = config->timer1Mode;
	}	
	TIMER1_CHANNEL->OCRA = config->matchValueA;
	TIMER1_CHANNEL->OCRB = config->matchValueB;
	TIMER1_CHANNEL->OCRC = config->matchValueC;
	
	if(config->inputCaptureRisingEdge)
	{
		TIMER1_CHANNEL->TCCRB |= 1<<6;
	}	
	if(Timer1InputCaptureNoiseCanceler__)
	{
		TIMER1_CHANNEL->TCCRB |= 1<<7;
	}	
	if(config->exMatchActionA != enTimer1ExtMatchCtrlNone)
	{
		TIMER1_CHANNEL->TCCRA = config->exMatchActionA << 6;
	}	
	if(config->exMatchActionB!=enTimer1ExtMatchCtrlNone)
	{
		TIMER1_CHANNEL->TCCRA |=config->exMatchActionB << 4;
	}	
	if(config->exMatchActionC!=enTimer1ExtMatchCtrlNone)
	{
		TIMER1_CHANNEL->TCCRA |=config->exMatchActionC << 2;
	}	
	if(config->cmpMatchACallback !=0)
	{
		timer1cmpMatchACallback =config->cmpMatchACallback;
	}	
	if(config->cmpMatchBCallback !=0)
	{
		timer1cmpMatchBCallback =config->cmpMatchBCallback;
	}	
	if(config->cmpMatchCCallback !=0)
	{
		timer1cmpMatchCCallback =config->cmpMatchCCallback;
	}	
	if(config->inputCaptureCallback !=0)
	{
		timer1inputCaptureCallback =config->inputCaptureCallback;
	}	
	if(config->overFlowCallback !=0)
	{
		timer1overFlowCallback =config->overFlowCallback;
	}
	if( config->interrupt != TIMER1_INT_NONE )
	{
		//TIMER1_CHANNEL->TIMSK = config.interrupt;
		TIMSK_INT->TIMER1 = config->interrupt;
	}		
	if(config->clockSource <= 7)
	{
		TIMER1_CHANNEL->TCCRB |= config->clockSource;
	}		

    timer1InitFlag=1;

	return enStatusSuccess;
	
}

PFEnStatus pfTimer1Close(void)
{
#ifdef PF_TIMER1_DEBUG
    if(timer1InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	TIMER1_CHANNEL->TCCRB = 0x00;
	TIMER1_CHANNEL->TCNT = 0;
	
	return enStatusSuccess;
}

PFEnStatus pfTimer1Start(void)
{
#ifdef PF_TIMER1_DEBUG
	if(timer1InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	PFbyte val;
	val=TIMER1_CHANNEL->TCCRB;
	TIMER1_CHANNEL->TCCRB = 0x00;
	TIMER1_CHANNEL->TCCRB |= val;
	TIMER1_CHANNEL->TCNT = 0;
	
	return enStatusSuccess;
}
PFEnStatus pfTimer1Reset(void)
{
#ifdef PF_TIMER1_DEBUG
	if(timer1InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	TIMER1_CHANNEL->TCNT = 0;
	return enStatusSuccess;
}

PFEnStatus pfTimer1ReadCount(PFdword* count)
{
#ifdef PF_TIMER1_DEBUG
	if(timer1InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
	if(count == 0)
	{
		return enStatusInvArgs;
	}
#endif
	*count = TIMER1_CHANNEL->TCNT;
	return enStatusSuccess;
}

PFEnStatus pfTimer1ReadCaptureCount(PFdword* count)
{
#ifdef PF_TIMER1_DEBUG
	if(timer1InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
	if(count == 0)
	{
		return enStatusInvArgs;
	}
#endif
	*count = TIMER1_CHANNEL->ICR;
	return enStatusSuccess;
}

PFEnStatus pfTimer1UpdateMatchRegister(PFbyte regNum, PFword regVal)
{
#ifdef PF_TIMER1_DEBUG
	if(timer1InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
	if( (regNum < 'A') || (regNum > 'C') )
	{
		return enStatusNotConfigured;
	}	
#endif
	switch(regNum)
	{
		case 'A':	
				  TIMER1_CHANNEL->OCRA =regVal;
				  break;
		
		case 'B':
				  TIMER1_CHANNEL->OCRB =regVal;
				  break;
				
		case 'C':
				  TIMER1_CHANNEL->OCRC =regVal;
				  break;
	}
	return enStatusSuccess;
}

PFEnStatus pfTimer1IntEnable(PFbyte interrupt)
{
#ifdef PF_TIMER1_DEBUG
    if(interrupt && (~TIMER1_INT_ALL))
	{
		return enStatusInvArgs;
	}
#endif
	TIMSK_INT->TIMER1 |= interrupt;
	return enStatusSuccess;	
}

PFEnStatus pfTimer1IntDisable(PFbyte interrupt)
{
#ifdef PF_TIMER1_DEBUG
    if(interrupt && (~TIMER1_INT_ALL))
	{
		return enStatusInvArgs;
	}
#endif
	TIMSK_INT->TIMER1 &= (~interrupt);
	return enStatusSuccess;	
}


void INT_HANDLER(TIMER1_COMPA)(void)
{
	if(timer1cmpMatchACallback !=0)
	timer1cmpMatchACallback();
}

void INT_HANDLER(TIMER1_COMPB)(void)
{
	if(timer1cmpMatchBCallback !=0)
	timer1cmpMatchBCallback();
}

void INT_HANDLER(TIMER1_COMPC)(void)
{
	if(timer1cmpMatchCCallback !=0)
	timer1cmpMatchCCallback();
}

void INT_HANDLER(TIMER1_CAPT)(void)
{
	if(timer1inputCaptureCallback !=0)
	timer1inputCaptureCallback();
}

void INT_HANDLER(TIMER1_OVF)(void)
{
	if(timer1overFlowCallback !=0)
	timer1overFlowCallback();
}
#endif		//#if (PF_USE_TIMER1 == 1)
