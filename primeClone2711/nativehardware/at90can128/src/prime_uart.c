/*
 * uart.c
 *
 * Created: 20-12-2013 12:43:42
 *  Author: Admin
 */ 


/************************************************************************/
/*    Prime Uart Library                                                */
/************************************************************************/

// #include "uart.h"
// #include "pfUartConfig.h"
// #include "prime_fifo.h"
// //#include  "prime_framework.h" 

#include "prime_framework.h"
#include "prime_fifo.h"
#include "prime_uart.h"

#if(UART_USE_FIFO != 0)
	#warning UART FIFO is enabled for interrupt based communication
	#if( (UART_BUFFER_SIZE == 0) || ((UART_BUFFER_SIZE & (UART_BUFFER_SIZE - 1)) != 0) )
		#error UART_BUFFER_SIZE cannot be zero. UART_BUFFER_SIZE should be power of 2
	#endif
#endif

static PFword baudArray[]={383,191,95,63,47,31,23,15,11,7,3};

static PFEnBoolean uartChInit;						// initalize flag for the channel
static PFEnBoolean uartChBusy;						// busy flag for the channel
static PFbyte uartChInt;
	
#if(UART_USE_FIFO != 0)
static PFbyte uartRxBuffer[UART_BUFFER_SIZE];	// UART transmit buffer
static PFbyte uartTxBuffer[UART_BUFFER_SIZE];	// UART receive buffer
static PFFifo uartTxFifo;							// UART transmit fifo structure
static PFFifo uartRxFifo;							// UART receive fifo structure
#else	
static PFcallback uartTxCallback;					// transmit callback for the channel
static PFcallback uartRxCallback;					// receive callback for the channel
#endif	// #if(UART_USE_FIFO != 0)


PFEnStatus pfUartOpen(PPFCfgUart config)
{	
#ifdef PFUART_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
	if ((config->mode) >0x03)
	{
		return enStatusNotSupported;
	}
	if ((config->parity) > 0x02)
	{
		return enStatusNotSupported;
	}
	if ((config->stopBits) !=enUartStopBits_1)
	{
		return enStatusNotSupported;
	}	
#if(UART_USE_FIFO != 0)
	if((config->transmitCallback) ==0)
	{
		return enStatusInvArgs;
	}
	if((config->receiveCallback) ==0)
	{
		return enStatusInvArgs;
	}	
#endif
#endif

	
	UART_CHANNEL->UBRRH = (baudArray[(config->baudrate)] >> 8);
	UART_CHANNEL->UBRRL = baudArray[(config->baudrate)];
	if ((config->mode) <=0x03)
	{
		UART_CHANNEL->UCSRB |= ((config->mode)<<3);
	} 
				
	if ((config->parity) !=enUartParityNone)
	{
		if ((config->parity) !=enUartParityEven)
		{
			UART_CHANNEL->UCSRC = ((1<<5)|(1<<4));
		} 
		else
		{
			UART_CHANNEL->UCSRC = ((1<<5)|(0<<4));
		}
	} 
	
				
	if ((config->stopBits) !=enUartStopBits_1)
	{
		UART_CHANNEL->UCSRC |=(1<<3);
	}
 
	if ((config->interrupts) != enUartIntNone)
	{
		UART_CHANNEL->UCSRB |=(config->interrupts) <<4;
		uartChInt =(config->interrupts);
	} 
				
	if(((config->dataBits) & 0x07) !=0x07)
	{	
		UART_CHANNEL->UCSRC |= config->dataBits <<1;
	}
	else
	{	
		UART_CHANNEL->UCSRC |= (config->dataBits)<<1;
		UART_CHANNEL->UCSRB |= (1<<2);
	}
		
#if(UART_USE_FIFO != 0)
	// initialize fifo 
	pfFifoInit(&uartTxFifo, uartTxBuffer, UART_BUFFER_SIZE);
	pfFifoInit(&uartRxFifo, uartRxBuffer, UART_BUFFER_SIZE);
#else	
	// set transmit and receive user callbacks
	if((config->transmitCallback) !=0)
	{
		uartTxCallback = (config->transmitCallback);
	}
	if((config->receiveCallback) !=0)
	{
		uartRxCallback = (config->receiveCallback);
	}	
#endif	// #if(UART_USE_FIFO != 0)
	uartChInit =  enBooleanTrue;
	return enStatusSuccess;

}
			
/** ---------------------------------------------------*/
PFEnStatus pfUartWriteByte(PFbyte data)
{

#ifdef PFUART_DEBUG	
	// check if the channel is initialized
	if(uartChInit !=  enBooleanTrue)
	{	
		return enStatusNotConfigured;
	}	
#endif	// _DEBUG_

	UART_CHANNEL->UDR =data;
	while(!(UART_CHANNEL->UCSRA &(1<<5)));
	return enStatusSuccess;
}

PFEnStatus pfUartReadByte(PFbyte* read_byte)
{

#ifdef PFUART_DEBUG	
	// check if the channel is initialized
	if(uartChInit !=  enBooleanTrue)
	{	
		return enStatusNotConfigured;
	}	
#endif	// PFUART_DEBUG

	while(!(UART_CHANNEL->UCSRA &(1<<7)));
	read_byte = UART_CHANNEL->UDR;
	return enStatusSuccess;
}

PFEnStatus pfUartWrite(PFbyte* data, PFdword size)
{
	PFdword index;
#ifdef PFUART_DEBUG	
	// check if the channel is initialized
	if(uartChInit !=  enBooleanTrue)
	{
		return enStatusNotConfigured;
	}		
#endif	// PFUART_DEBUG
	
	// check if the channel is busy
	if(uartChBusy != enBooleanFalse)
	{
		return enStatusBusy;
	}		
	
	// set channel busy	
	uartChBusy = enBooleanTrue;
	
	// if fifo is to be used push data into the fifo
	if((uartChInt & enUartIntTx) != 0)
	{
	#if(UART_USE_FIFO != 0)
		for(index = 0; index < size; index++)
		{
			pfFifoPush(&uartTxFifo, *(data + index));
		}
		UART_CHANNEL->UDR = pfFifoPop(&uartTxFifo);
	#else
		UART_CHANNEL->UDR = *data;
	#endif	// #if(UART_USE_FIFO != 0)	
		//LPC_UART0->IER = uartChInt & (0x7F);
	}
	else
	{
		// for polling based transmission, write each byte one by one to data register
		for(index = 0; index < size; index++)
		{
			UART_CHANNEL->UDR = *(data + index);
			while(!(UART_CHANNEL->UCSRA &(1<<5)));

		}
	}
	
	uartChBusy = enBooleanFalse;
	return enStatusSuccess;
}




PFEnStatus pfUartRead(PFbyte* data, PFdword size,PFdword* readBytes)
{
	PFdword index;
#ifdef PFUART_DEBUG
	if(uartChInit !=  enBooleanTrue)
	{	
		return enStatusNotConfigured;
	}	
#endif	// PFUART_DEBUG
	
	if(uartChBusy != enBooleanFalse)
	{	
		return enStatusBusy;
	}		
	uartChBusy = enBooleanTrue;
	
	if((uartChInt & enUartIntRx) != 0)
	{
	#if(UART_USE_FIFO != 0)
		for(index = 0; index < size; index++)
		{
			if(pfFifoIsEmpty(&uartRxFifo) == enBooleanTrue)
				break;
			
			*(data + index) = pfFifoPop(&uartRxFifo);
				*readBytes += 1;
		}
	#endif	// #if(UART_USE_FIFO != 0)	
	}
	else
	{
		for(index = 0; index < size; index++)
		{
			while(!(UART_CHANNEL->UCSRA &(1<<7)));
			*(data + index) = UART_CHANNEL->UDR;
				*readBytes++;
		}
	}
	uartChBusy = enBooleanFalse;
	return enStatusSuccess;
}


PFEnStatus pfUartClose(void)
{

#ifdef PFUART_DEBUG	
	// check if the channel is initialized
	if(uartChInit !=  enBooleanTrue)
		return enStatusNotConfigured;
#endif	// PFUART_DEBUG
	UART_CHANNEL->UCSRB |=(enUartModeTxRx)<<3;
	uartChInit = enBooleanFalse;
	return enStatusSuccess;
}

#if(UART_USE_FIFO != 0)
PFEnStatus pfUartGetRxBufferCount(PFdword* buffer_count)
{
	buffer_count = uartRxFifo.count;
	return enStatusSuccess;
}

void pfUartTxBufferFlush(void)
{
	pfFifoFlush(&uartTxFifo);
}

void pfUartRxBufferFlush(void)
{

	pfFifoFlush(&uartRxFifo);
}
#endif	// #if(UART_USE_FIFO != 0)





//ISR(USART0_RX_vect)

void INT_HANDLER(USART0_RX)(void)
//INT_HANDLER(USART0_RX)
{
	PFbyte rxByte;
	#if(UART_USE_FIFO != 0)
		rxByte = UART_CHANNEL->UDR;
		pfFifoPush(&uartRxFifo, rxByte);
	#else
		if(uartRxCallback != 0)
		{
			uartRxCallback();
		}
		return;
	#endif	
}


//ISR(USART0_TX_vect)
void INT_HANDLER(USART0_TX)(void)
//INT_HANDLER(USART0_TX)
{
	#if(UART_USE_FIFO != 0)
		if(pfFifoIsEmpty(&uartTxFifo) != enBooleanTrue)
		{
			UART_CHANNEL->UDR = pfFifoPop(&uartTxFifo);
		}
		
	#else	
		if(uartTxCallback != 0)
		{
			uartTxCallback();
		}
		return;
	#endif		//
}
