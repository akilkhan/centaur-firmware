/*
 * pfPwm.c
 *
 * Created: 27-12-2013 16:17:36
 *  Author: Admin
 */ 

#include "prime_framework.h"
#include "prime_pwm.h"
// #include <avr/io.h>
// #include <avr/interrupt.h>
//cmpMatchACallback cmpMatchBCallback overFlowCallback
static PFcallback pwmcmpMatchACallback;
static PFcallback pwmoverFlowCallback;
static PFbyte pwminitFlag=0;
static PFbyte pwmstartFlag=0;
static PFbyte pwmstartFlag=0;
static PFbyte pwmValue=0;


PFEnStatus pfPwmOpen(PPFCfgPwm config)
{
#ifdef PFPWM_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
    if( (config->pwmMode<0) || ((config->pwmMode > enPwmFastMode)))
	{
		return enStatusInvArgs;
	}
	
	if( (config->exMatchActionA < 0) || (config->exMatchActionA > 3) )
	{
		return enStatusInvArgs;
	}
	
	if(config->cmpMatchACallback ==0)
	{
		return enStatusInvArgs;
	}
	
	if(config->overFlowCallback ==0)
	{
		return enStatusInvArgs;
	}	
#endif

	if((config->pwmMode& 0x01)==0x01)
	{
		PWM_CHANNEL->TCCRA |=(1<<3);
	}
	else if((config->pwmMode& 0x03)==0x03)
	{
		PWM_CHANNEL->TCCRA |=((1<<3) |(1<<6));
	}
	
	PWM_CHANNEL->OCRA =config->matchValueA;
	
	
	if(config->exMatchActionA !=enPwmExtMatchCtrlNone)
	{
		PWM_CHANNEL->TCCRA |=config->exMatchActionA <<4;
	//	GPIO_PORTB->DDR |=(1<<7);
	}		
	if(config->cmpMatchACallback !=0)
	{
		pwmcmpMatchACallback =config->cmpMatchACallback;
	}	
		
	if(config->overFlowCallback !=0)
	{
		pwmoverFlowCallback =config->overFlowCallback;
	}
	if(config->clockSource >=0 && config->clockSource <=7)
	{
		PWM_CHANNEL->TCCRA |= config->clockSource;
	}
	if(config->interrupt !=enPwmIntNone)
	{
		//	PWM_CHANNEL->TIMSK |=config.interrupt;
		TIMSK_INT->TIMER0 |= config->interrupt;	
	}	
	PWM_CHANNEL->TCNT = 0;
	pwminitFlag=1;
	return enStatusSuccess;
	
}

PFEnStatus pfPwmClose(void)
{
#ifdef PFPWM_DEBUG
	if(pwminitFlag ==0)
	{
		return enStatusNotConfigured;
	}
#endif
	PWM_CHANNEL->TCCRA = 0x00;
	PWM_CHANNEL->TCNT = 0;
	return enStatusSuccess;
		
}

PFEnStatus pfPwmStop(void)
{
#ifdef PFPWM_DEBUG
	if((pwminitFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif
	pwmstartFlag=PWM_CHANNEL->TCCRA;
	pwmValue=PWM_CHANNEL->TCNT;
	PWM_CHANNEL->TCCRA = 0x00;
	return enStatusSuccess;

}

PFEnStatus pfPwmStart(void)
{
	PFbyte val;
	
#ifdef PFPWM_DEBUG
	if((pwminitFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif	
	if(pwmstartFlag!=0)
	{
		PWM_CHANNEL->TCCRA = pwmstartFlag;
		PWM_CHANNEL->TCNT = pwmValue;
	}
	else
	{
		val =PWM_CHANNEL->TCCRA;
		PWM_CHANNEL->TCCRA = 0x00;
		PWM_CHANNEL->TCCRA =val;
		PWM_CHANNEL->TCNT = 0;
	}
	return	enStatusSuccess;	
	
}
PFEnStatus pfPwmReset(void)
{
#ifdef PFPWM_DEBUG
	if((pwminitFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif
	PWM_CHANNEL->TCNT = 0;
	return enStatusSuccess;
	
}
PFEnStatus pfPwmReadCount(PFbyte *data)
{
#ifdef PFPWM_DEBUG
	if((pwminitFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif
	*data =PWM_CHANNEL->TCNT;
	return enStatusSuccess;
	
}

//ISR(TIMER0_COMPA_vect)
void INT_HANDLER(TIMER0_COMPA)(void)
//INT_HANDLER(TIMER0_COMPA)
{
	if(pwmcmpMatchACallback !=0)
	pwmcmpMatchACallback();
}


////ISR(TIMER0_OVF_vect)
void INT_HANDLER(TIMER0_OVF)(void)
//INT_HANDLER(TIMER0_OVF)
{
	if(pwmoverFlowCallback !=0)
	pwmoverFlowCallback();
}


PFEnStatus pfPwmUpdateMatchRegister(PFbyte regNum, PFdword regVal)
{
	switch(regNum)
	{
		case 0:
		PWM_CHANNEL->OCRA =regVal;
		break;
		
		default:
			return enStatusInvArgs;
	}
	
	return enStatusSuccess;
}
