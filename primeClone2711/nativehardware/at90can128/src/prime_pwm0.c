#include "prime_framework.h"
#if(PF_USE_PWM0 == 1)	
	#include "prime_pwm0.h"

static PFcallback pwm0cmpMatchACallback;
static PFcallback pwm0overFlowCallback;
static PFbyte pwm0initFlag=0;
static PFbyte pwm0startFlag=0;
static PFbyte pwm0Value=0;


PFEnStatus pfPwm0Open(PFpCfgPwm0 config)
{
#ifdef PF_PWM0_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
    if( (config->pwm0Mode<0) || ((config->pwm0Mode > enPwm0FastMode)))
	{
		return enStatusInvArgs;
	}
	
	if( (config->exMatchActionA < 0) || (config->exMatchActionA > 3) )
	{
		return enStatusInvArgs;
	}
	
	if(config->cmpMatchACallback ==0)
	{
		return enStatusInvArgs;
	}
	
	if(config->overFlowCallback ==0)
	{
		return enStatusInvArgs;
	}	
#endif	//PF_PWM0_DEBUG

	if((config->pwm0Mode& 0x01)==0x01)
	{
		PWM0_CHANNEL->TCCRA |=(1<<3);
	}
	else if((config->pwm0Mode& 0x03)==0x03)
	{
		PWM0_CHANNEL->TCCRA |=((1<<3) |(1<<6));
	}
	
	PWM0_CHANNEL->OCRA =config->matchValueA;
	
	
	if(config->exMatchActionA !=enPwm0ExtMatchCtrlNone)
	{
		PWM0_CHANNEL->TCCRA |=config->exMatchActionA <<4;
	//	GPIO_PORTB->DDR |=(1<<7);
	}		
	if(config->cmpMatchACallback !=0)
	{
		pwm0cmpMatchACallback =config->cmpMatchACallback;
	}	
		
	if(config->overFlowCallback !=0)
	{
		pwm0overFlowCallback =config->overFlowCallback;
	}
	if(config->clockSource >=0 && config->clockSource <=7)
	{
		PWM0_CHANNEL->TCCRA |= config->clockSource;
	}
	if(config->interrupt !=enPwm0IntNone)
	{
		//	PWM0_CHANNEL->TIMSK |=config.interrupt;
		TIMSK_INT->TIMER0 |= config->interrupt;	
	}	
	PWM0_CHANNEL->TCNT = 0;
	pwm0initFlag=1;
	return enStatusSuccess;
	
}

PFEnStatus pfPwm0Close(void)
{
#ifdef PF_PWM0_DEBUG
	if(pwm0initFlag ==0)
	{
		return enStatusNotConfigured;
	}
#endif	//PF_PWM0_DEBUG
	PWM0_CHANNEL->TCCRA = 0x00;
	PWM0_CHANNEL->TCNT = 0;
	return enStatusSuccess;
		
}

PFEnStatus pfPwm0Stop(void)
{
#ifdef PF_PWM0_DEBUG
	if((pwm0initFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif	//PF_PWM0_DEBUG
	pwm0startFlag=PWM0_CHANNEL->TCCRA;
	pwm0Value=PWM0_CHANNEL->TCNT;
	PWM0_CHANNEL->TCCRA = 0x00;
	return enStatusSuccess;

}

PFEnStatus pfPwm0Start(void)
{
	PFbyte val;
	
#ifdef PF_PWM0_DEBUG
	if((pwm0initFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif	//PF_PWM0_DEBUG	
	if(pwm0startFlag!=0)
	{
		PWM0_CHANNEL->TCCRA = pwm0startFlag;
		PWM0_CHANNEL->TCNT = pwm0Value;
	}
	else
	{
		val =PWM0_CHANNEL->TCCRA;
		PWM0_CHANNEL->TCCRA = 0x00;
		PWM0_CHANNEL->TCCRA =val;
		PWM0_CHANNEL->TCNT = 0;
	}
	return	enStatusSuccess;	
	
}
PFEnStatus pfPwm0Reset(void)
{
#ifdef PF_PWM0_DEBUG
	if((pwm0initFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif	//PF_PWM0_DEBUG
	PWM0_CHANNEL->TCNT = 0;
	return enStatusSuccess;
	
}
PFEnStatus pfPwm0ReadCount(PFbyte *data)
{
#ifdef PF_PWM0_DEBUG
	if((pwm0initFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif	//PF_PWM0_DEBUG
	*data =PWM0_CHANNEL->TCNT;
	return enStatusSuccess;
	
}

//ISR(TIMER0_COMPA_vect)
void INT_HANDLER(TIMER0_COMPA)(void)
//INT_HANDLER(TIMER0_COMPA)
{
	if(pwm0cmpMatchACallback !=0)
		pwm0cmpMatchACallback();
}


////ISR(TIMER0_OVF_vect)
void INT_HANDLER(TIMER0_OVF)(void)
//INT_HANDLER(TIMER0_OVF)
{
	if(pwm0overFlowCallback !=0)
		pwm0overFlowCallback();
}


PFEnStatus pfPwm0UpdateMatchRegister(PFbyte regNum, PFdword regVal)
{
	switch(regNum)
	{
		case 0:
			PWM0_CHANNEL->OCRA =regVal;
		break;
		
		default:
			return enStatusInvArgs;
	}
	
	return enStatusSuccess;
}
#endif		// #if (PF_USE_PWM0 == 1)
