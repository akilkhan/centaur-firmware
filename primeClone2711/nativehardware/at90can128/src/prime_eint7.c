#include "prime_framework.h"
#if (PF_USE_EINT7 == 1)
#include "prime_eint7.h"


static PFcallback eint7CallBackList[PF_EXT_INT_MAX_CALLBACK]={0};
static PFbyte eint7MaxIntCount=0;
static PFbyte eint7InitFlag = 0;


PFEnStatus pfEint7Open(PFpCfgEint7 config)
{
	PFbyte index;
#ifdef PF_EINT7_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
	if((config->mode > 3)|| (config->mode < enIntModeLowLevel))
	{
		return enStatusInvArgs;
	}
	if(config->callbackList == 0)
	{
		return enStatusInvArgs;
	}
	if(config->maxCallbacks < 0)
	{
		return enStatusInvArgs;
	}
#endif

	if(config->mode <=3)
	{
		EXTINT->EICRB |=config->mode <<(EINT7_CH-3);
	}	
	
	eint7MaxIntCount=config->maxCallbacks ;
// 	if(eint7MaxIntCount >0)
// 	eint7CallBackList =(PFcallback*)malloc(sizeof(PFcallback)*eint7MaxIntCount);
// 	else
// 	return enStatusNotSupported;
		
	for(index=0;index<eint7MaxIntCount;index++)
	{
		if(config->callbackList!=0 )
		eint7CallBackList[index]=config->callbackList[index];
	}
	 eint7InitFlag = enBooleanTrue ;
	
	return enStatusSuccess;
	
}


PFEnStatus pfEint7Enable(void)
{
#ifdef PF_EINT7_DEBUG
	if(eint7InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK |= (1<<EINT7_CH);
	return enStatusSuccess;
}


PFEnStatus pfEint7Disable(void)
{
#ifdef PF_EINT7_DEBUG
	if(eint7InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK &=~(1<<EINT7_CH);
	return enStatusSuccess;
}



PFEnStatus pfEint7AddCallback(PFcallback callback)
{
	PFbyte index=0;
#ifdef PF_EINT7_DEBUG
	if(eint7InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
	if(callback == 0)
	{
		return enStatusInvArgs;
	}
#endif	
	for(index=0;index<=eint7MaxIntCount;index++)
	{
		if (eint7CallBackList[index]==0)
		{
			eint7CallBackList[index]=callback;
			break;
		}
	}
	
	return enStatusSuccess;
}

PFEnStatus pfEint7RemoveCallback(PFcallback callback)
{
	PFbyte index=0;
#ifdef PF_EINT7_DEBUG
	if(eint7InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	
	for(index=0;index<=eint7MaxIntCount;index++)
	{
		if (eint7CallBackList[index]==callback)
		{
			eint7CallBackList[index]=0;
			break;
		}
	}
	return enStatusSuccess;
	
}


PFEnStatus pfEint7Close(void)
{
#ifdef PF_EINT7_DEBUG
	if(eint7InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK &=~(1<<EINT7_CH);
	//free(eint7CallBackList);
	return enStatusSuccess;
}

void INT_HANDLER(EINT7_CHANNEL)(void)
{
	PFbyte index=0;
	
	for(index=0;index<=eint7MaxIntCount;index++)
	{
		if(eint7CallBackList[index] !=0)
		eint7CallBackList[index]();
		
	}
}
#endif		//#if (PF_USE_EINT7 == 1)


