#include "prime_framework.h"
#if (PF_USE_CAN == 1)

#include "prime_fifo.h"
#include "prime_can.h"

#define CAN_MOB_SET_TX		 		0x40
#define CAN_MOB_SET_RX				0x80
#define CAN_MIN_TQ					0x07    // 7 = Minimum TQ in 1 bit of CAN bus time
#define CAN_MAX_TQ				    0x19    // 25 = Maximum TQ in 1 bit of CAN bus time
#define CAN_MIN_BRP					0x00    // 0 = Minimum baud rate pre-scaler clock
#define CAN_MAX_BRP					0x3F    // 63 = Maximum baud rate pre-scaler clock
#define CAN_MAX_SJW			        0x03    // 4 = Maximum Synchronization Jump Width. 4-1 = 3 actually


#define CAN_MESSAGE_SIZE	(sizeof(PFCanMessage))

static PFEnStatus pfCanWriteMessage(PFCanMessage* message);
static PFEnStatus pfCanReadMessage(PFCanMessage* message);
static PFEnStatus pfCanCheckTxFreeMob(PFbyte* count);
static PFEnBoolean canInit = enBooleanFalse;
static PFEnBoolean canIntFlag = enBooleanFalse;

static PFbyte mobStatus[CAN_MAX_MOBS_NUMBER] = {0};
static PFbyte canInterrupts = 0;	
static PFEnBoolean canTxInterrupt = enBooleanFalse;	
static PFEnBoolean canRxInterrupt = enBooleanFalse;
#if(CAN_USE_FIFO == 0)
static PFcallback canTxCallback = 0;
static PFcallback canRxCallback = 0;
#else
static PFbyte canTxBuffer[CAN_BUFFER_SIZE] = {0};
static PFbyte canRxBuffer[CAN_BUFFER_SIZE] = {0};
static PFFifo canTxFifo;
static PFFifo canRxFifo;
static void pfCanPushRxMessage(PFCanMessage* msg);
static PFEnStatus pfCanPopTxMessage(PFCanMessage* msg);
static PFEnStatus pfCanPopRxMessage(PFCanMessage* msg);
static void pfCanPushTxMessage(PFCanMessage* msg);

#endif	// #if(CAN_USE_FIFO == 0)
static PFcallback canErrCallback = 0;

static void pfCanSetBaudRate(PFdword CANBaudRate)
{
    PFbyte brp = 0;
    PFbyte TotalTscl; 		//Total of Tscl in a CAN bus bit time
    PFbyte Tscltemp; 		//nTQ temporally
	if ( (CANBaudRate != 0) && (CANBaudRate <= 1000000L) )
    {
		//Set the Bit Rate Prescale (BRP).
        //total Tscl = F_CPU / (2*(BRP+1)*F_CAN)
        do{
            TotalTscl = (PFbyte) ( 14745800 / ((PFdword)((brp + 1) << 1 ) * CANBaudRate) ) ;
		}while ((++brp <= CAN_MAX_BRP) && (TotalTscl > CAN_MAX_TQ));
		CAN_CHANNEL->CANBT1 = --brp;
		
		//For Phase segment 2. Total TQ / 4 to have 75% sampling point.
		Tscltemp = TotalTscl >> 2;
		CAN_CHANNEL->CANBT3  =  (Tscltemp-1) << 4;
				
		//For Phase segment 1. (rest of TQ) / 2
        TotalTscl = TotalTscl - Tscltemp;
        Tscltemp = TotalTscl >> 1;
        CAN_CHANNEL->CANBT3  |= ((Tscltemp - 1) << 1);
		
		//For Propagation segment.
        TotalTscl = TotalTscl - Tscltemp;
        CAN_CHANNEL->CANBT2 = ((TotalTscl - 2) << 1) ;    		// -2 because 1TQ for Sync segment and 1TQ offset in register.
	}
}

PFEnStatus pfCanOpen(PFpCfgCan config)
{
	PFbyte loop;
	PFbyte volatile *regIndex;
#ifdef PF_CAN_DEBUG_
	if((config == 0))
	{
		return enStatusInvArgs;
	}
	if((config->baudRate < 0) || (config->baudRate > 1000000))
	{
		return enStatusInvArgs;
	}
	
	if((config->intr > 7) || (config->intHandler == 0))
	{
		return enStatusInvArgs;
	}
#if(CAN_USE_FIFO == 0)
	if((config->txCallback == 0) || (config->rxCallback == 0));
		return enStatusInvArgs;
#endif	// #if(PF_CAN_USE_FIFO == 0)	

#endif // PF_CAN_DEBUG_

	// Reset CAN module
	//CANGCON = (1 << SWRES);
	
	CAN_CHANNEL->CANGCON = (1<<0);
	
	// Configure CAN baudrate as 100Khz
	pfCanSetBaudRate(config->baudrate);
	
	// Clear all MOBs
	for(loop = 0; loop < CAN_MAX_MOBS_NUMBER; loop++)
	{
		// Select MOB
		CAN_CHANNEL->CANPAGE = (loop << 4);
		
		// Clear MOB register
// 		CANSTMOB = 0;
// 		CANCDMOB = 0;
		for (regIndex = &CAN_CHANNEL->CANSTMOB; regIndex < &CAN_CHANNEL->CANSTML; regIndex++) 
		{ 
			*regIndex = 0x00;
		}
		for(regIndex = 0; regIndex < 8; regIndex++)
		{
			CAN_CHANNEL->CANMSG = 0;
		}
	}	
	
	// Configure MOBs
	for(loop = 0; loop < CAN_MAX_MOBS_NUMBER; loop++)
	{
		// Select MOB
		CAN_CHANNEL->CANPAGE = (loop << 4);
			
		// Configure MOB for reception
		if(loop < CAN_RX_MOB)
		{
			mobStatus[loop] = CAN_MOB_SET_RX;
		}
		else
		{
			mobStatus[loop] = 0;
		}	
		CAN_CHANNEL->CANCDMOB = mobStatus[loop];			
	}
	
	// Enable the interrupts of all MObs (0..14)
	CAN_CHANNEL->CANIE1 = 0x7F;        
	CAN_CHANNEL->CANIE2 = 0xFF; 	
	
#if(CAN_USE_FIFO == 0)
	canTxCallback = config->txCallback;
	canRxCallback = config->rxCallback;
#else
	pfFifoInit(&canRxFifo, canRxBuffer, CAN_BUFFER_SIZE);
	pfFifoInit(&canTxFifo, canTxBuffer, CAN_BUFFER_SIZE);
#endif	// #if(CAN_USE_FIFO == 0)

	if((config->interrupt)!=0)
	{
		if(config->errCallback !=0)
		canErrCallback = config->errCallback;
		
		CAN_CHANNEL->CANGIE = 0x80 | config->interrupt;
		canInterrupts = config->interrupt;
	}		
	

	// Enable the CAN bus controller
	CAN_CHANNEL->CANGCON |=  (1<<1);//ENASTB);              

	canInit	= enBooleanTrue;
	return enStatusSuccess;
}

PFEnStatus pfCanClose(void)
{
	// Reset CAN module
	CAN_CHANNEL->CANGCON = (1 << 0);
	canInit	= enBooleanFalse;
	
	return enStatusSuccess;
}

PFEnStatus pfCanCheckTxFreeMob(PFbyte* count)
{
	PFbyte index;
	for (index = 0; index < CAN_MAX_MOBS_NUMBER; index++)
	{
		if(mobStatus[index] != CAN_MOB_SET_RX)
		{
			if(index < 8)
			{
				if((CAN_CHANNEL->CANEN2 & (1 << index)) == 0)
					break;
			}
			else
			{
				if((CAN_CHANNEL->CANEN1 & (1 << (index-8))) == 0)
					break;
			}
		}
	}
	count = index;
	return enStatusSuccess;
}

PFEnStatus pfCanCheckRxMob(PFbyte* count)
{
	PFbyte index;
	for (index = 0; index < CAN_MAX_MOBS_NUMBER; index++)
	{
		CAN_CHANNEL->CANPAGE = index << 4;
// 		if((CANSTMOB & (1 << RXOK)) != 0)
// 			break;
		if(mobStatus[index] == CAN_MOB_SET_RX)
		{
			if(index < 8)
			{
				if((CAN_CHANNEL->CANEN2 & (1 << index)) == 0)
					break;
			}
			else
			{
				if((CAN_CHANNEL->CANEN1 & (1 << (index-8))) == 0)
					break;
			}
		}
	}
	count = index;
	return enStatusSuccess;
}

PFEnStatus pfCanWrite(PFCanMessage* message)
{
	PFEnStatus status;
	PFbyte loop;
#ifdef PF_CAN_DEBUG_
	if(canInit != enBooleanTrue)
		return enStatusNotConfigured;
#endif	// PF_CAN_DEBUG_			

	if((canInterrupts & enCanIntTx) != 0)
	{
		#if(CAN_USE_FIFO != 0)
			pfCanCheckTxFreeMob(&loop );
			if(loop >= CAN_MAX_MOBS_NUMBER)
			{
				pfCanPushTxMessage(message);
				return enStatusSuccess;
			}
		#endif	// #if(CAN_USE_FIFO != 0)	
		status = pfCanWriteMessage(message);
			return status;
	}
	
	status = pfCanWriteMessage(message);
	if(status != enStatusSuccess)
		return status;
	
	// Wait till message is transmitted
	while ( (CAN_CHANNEL->CANSTMOB & (1 << 6)) == 0);
	// Reset TXOK flag
	CAN_CHANNEL->CANSTMOB = 0;
	// Reset MOB used for transmission
	CAN_CHANNEL->CANCDMOB = 0;
	
	return enStatusSuccess;
}

static PFEnStatus pfCanWriteMessage(PFCanMessage* message)
{
	PFbyte loop, ctrlByte;
	pfCanCheckTxFreeMob(&loop);
	if(loop >= CAN_MAX_MOBS_NUMBER)
	{
		return enStatusError;
	}
	
	// Select MOB
	CAN_CHANNEL->CANPAGE = (loop << 4);
	// Disable MOB
	CAN_CHANNEL->CANCDMOB = 0;
	// Set message ID
	if(message->frameFormat == enCanFrameStandard)
	{
		CAN_CHANNEL->CANIDT4 = 0;
		CAN_CHANNEL->CANIDT3 = 0;
		// IDT[2:0]
		CAN_CHANNEL->CANIDT2 = (message->id & 0x007) << 5;
		//IDT[10:3]
		CAN_CHANNEL->CANIDT1 = (message->id & 0x7F8) >> 3;
	}
	else
	{
		// IDT[4:0]
		CAN_CHANNEL->CANIDT4 = (message->id & 0x0000001F) << 3;
		// IDT[12:5]
		CAN_CHANNEL->CANIDT3 = (message->id & 0x00001FE0) >> 5;
		// IDT[20:13]
		CAN_CHANNEL->CANIDT2 = (message->id & 0x001FE000) >> 13;
		// IDT[28:21]
		CAN_CHANNEL->CANIDT1 = (message->id & 0x1FE00000) >> 21;
	}
	
	// Check remote frame
	if(message->remoteFrame == enBooleanTrue)
	{
		CAN_CHANNEL->CANIDT4 |= (1 << 2);
	}
	else
	{
		CAN_CHANNEL->CANIDT4 &= ~(1 << 2);
	}
	
	// Set data
	if(message->length > 8)
		message->length = 8;
	
	CAN_CHANNEL->CANPAGE |= (1 << 3);		// Disable message auto increment index
	for(loop = 0; loop < message->length; loop++)
	{
		// Set data buffer index
		CAN_CHANNEL->CANPAGE &= ~(0x07);
		CAN_CHANNEL->CANPAGE |= loop;
		// Write data
		CAN_CHANNEL->CANMSG = message->data[loop];	
	}
	
	// Set length
	ctrlByte = message->length;			
	// Set frame format
	ctrlByte |= (message->frameFormat << 4);
	
	// Send message
	CAN_CHANNEL->CANCDMOB = CAN_MOB_SET_TX | ctrlByte;
	
	return enStatusSuccess;
}

PFEnStatus pfCanRead(PFCanMessage* message)
{
	PFEnStatus status;
#ifdef PF_CAN_DEBUG_
	if(canInit != enBooleanTrue)
		return enStatusNotConfigured;
#endif	// PF_CAN_DEBUG_			
	
	if((canInterrupts & enCanIntRx) != 0)
	{
	#if(CAN_USE_FIFO != 0)
		status = pfCanPopRxMessage(message);
		return status;
	#else
		return enStatusError;
	#endif
	}
		
	status = pfCanReadMessage(message);
	return status;
}

PFEnStatus pfCanReadMessage(PFCanMessage* message)
{
	PFbyte mobNum, index;
	pfCanCheckRxMob(&mobNum);
	if(mobNum >= CAN_MAX_MOBS_NUMBER)
		return enStatusError;
		
	CAN_CHANNEL->CANPAGE = (mobNum << 4);
	
	// Check the MOB receive status
	if((CAN_CHANNEL->CANSTMOB & (1 << 5)) == 0)
		return enStatusError;
	
	// Get frame format
	if((CAN_CHANNEL->CANCDMOB & (1 << 4)) == 0)
	{
		message->frameFormat = enCanFrameStandard;
	}
	else
	{
		message->frameFormat = enCanFrameExtended;
	}
	
	// Get ID
	if(message->frameFormat == enCanFrameStandard)
	{
		message->id	= (CAN_CHANNEL->CANIDT2 >> 5) | (CAN_CHANNEL->CANIDT1 << 3);	
	}
	else
	{
		message->id	= (CAN_CHANNEL->CANIDT4 >> 3) | (CAN_CHANNEL->CANIDT3 << 5) | (CAN_CHANNEL->CANIDT2 << 13) | (CAN_CHANNEL->CANIDT1 << 21);
	}
	
	// Get RTR tag
	if((CAN_CHANNEL->CANIDT4 & (1 << 2)) != 0)
	{
		message->remoteFrame = enBooleanTrue;
	}
	else
	{
		message->remoteFrame = enBooleanFalse;
	}
	
	// Get data length
	message->length = CAN_CHANNEL->CANCDMOB & 0x0F;
	
	// Get data
	if(message->remoteFrame == enBooleanFalse)
	{
		CAN_CHANNEL->CANPAGE |= (1 << 3);			// Disable auto increment data index
		for(index = 0; index < message->length; index++)
		{
			CAN_CHANNEL->CANPAGE &= ~(0x07);
			CAN_CHANNEL->CANPAGE |= index;
			message->data[index] = CAN_CHANNEL->CANMSG;
		}
	}	
	
	// Clear RXOK flag
	CAN_CHANNEL->CANSTMOB = 0;
	
	// Reinitialize MOB for reception
	CAN_CHANNEL->CANCDMOB = mobStatus[mobNum];
	
	return enStatusSuccess;
}

// PFEnBoolean pfCanCheckStatus(PFEnCanStatus param)
// {
// #ifdef _DEBUG_
// 	if(canInit != enBooleanTrue)
// 		return enStatusNotConfigured;
// #endif	// _DEBUG_			
// 
// 	return enStatusSuccess;
// 	
// }

PFEnStatus pfCanGetTxErrCounter(PFbyte* errCount)
{
#ifdef PF_CAN_DEBUG_
	if(canInit != enBooleanTrue)
		return enStatusNotConfigured;
#endif	// PF_CAN_DEBUG_			

	return enStatusSuccess;
	
}

PFEnStatus pfCanGetRxErrCounter(PFbyte* errCount)
{
#ifdef PF_CAN_DEBUG_
	if(canInit != enBooleanTrue)
		return enStatusNotConfigured;
#endif	// PF_CAN_DEBUG_			

	return enStatusSuccess;
	
}

#if(CAN_USE_FIFO != 0)
static void pfCanPushRxMessage(PFCanMessage* msg)
{
	PFbyte loop;
	PFbyte* msgPtr = (PFbyte*)msg;
	
	for(loop = 0; loop < CAN_MESSAGE_SIZE; loop++)
	{
		pfFifoPush(&canRxFifo, *(msgPtr + loop));
	}
}

static PFEnStatus pfCanPopRxMessage(PFCanMessage* msg)
{
	PFbyte loop;
	PFbyte* msgPtr = (PFbyte*)msg;
	
	if(pfFifoIsEmpty(&canRxFifo) == enBooleanTrue)
		return enStatusError;
	
	for(loop = 0; loop < CAN_MESSAGE_SIZE; loop++)
	{
		*(msgPtr + loop) = pfFifoPop(&canRxFifo);
	}
	
	return enStatusSuccess;
}

static void pfCanPushTxMessage(PFCanMessage* msg)
{
	PFbyte loop;
	PFbyte* msgPtr = (PFbyte*)msg;
	
	for(loop = 0; loop < CAN_MESSAGE_SIZE; loop++)
	{
		pfFifoPush(&canTxFifo, *(msgPtr + loop));
	}
}

static PFEnStatus pfCanPopTxMessage(PFCanMessage* msg)
{
	PFbyte loop;
	PFbyte* msgPtr = (PFbyte*)msg;
	
	if(pfFifoIsEmpty(&canTxFifo) == enBooleanTrue)
	return enStatusError;
	
	for(loop = 0; loop < CAN_MESSAGE_SIZE; loop++)
	{
		*(msgPtr + loop) = pfFifoPop(&canTxFifo);
	}
	
	return enStatusSuccess;
}

PFEnStatus pfCanGetRxBufferCount(PFdword* buffer_count)
{
	buffer_count = (canRxFifo.count/sizeof(PFCanMessage));
	return enStatusSuccess;
}

void pfCanTxBufferFlush(void)
{
	pfFifoFlush(&canTxFifo);
}

void pfCanRxBufferFlush(void)
{
	pfFifoFlush(&canRxFifo);
}
#endif	// #if(CAN_USE_FIFO != 0)

void INT_HANDLER(CANIT_vect)(void)
{
	PFbyte loop, mobStatus = 0;;
	PFCanMessage msg;
	
	for(loop =0;loop <8;loop++)
	{
		//Checking intrrupt status of each mob  
		if((CAN_CHANNEL->CANSIT2 & ( 1<< loop)))
		{
			// Selecting the mob
			CAN_CHANNEL->CANPAGE = loop << 4;
			mobStatus = CAN_CHANNEL->CANSTMOB;
			if(mobStatus &(1<<6))
			{
				CAN_CHANNEL->CANSTMOB = 0;
				CAN_CHANNEL->CANCDMOB = 0;
				#if(CAN_USE_FIFO != 0)
				if(pfFifoIsEmpty(&canTxFifo) == enBooleanFalse)
				{
					pfCanPopTxMessage(&msg);
					pfCanWriteMessage(&msg);
				}
				#else
				if(canTxCallback != 0)
				{
					canTxCallback();
				}
				#endif	// #if(CAN_USE_FIFO != 0)
				
			}
	
			if(mobStatus & (1<<5))
			{
				#if(CAN_USE_FIFO != 0)
				pfCanReadMessage(&msg);
				pfCanPushRxMessage(&msg);
				#else
				if(canRxCallback != 0)
				{
					canRxCallback();
				}
				#endif	// #if(CAN_USE_FIFO != 0)
		
				//CANSTMOB = 0;
			}
	
	
			if((mobStatus & 0x10) || (mobStatus & 0x08) ||(mobStatus & 0x04) ||(mobStatus & 0x02) || (mobStatus & 0x01))
			{
				canErrCallback();
				CAN_CHANNEL->CANSTMOB =0;
			}
			
		}
	}
	for(loop = 0;loop <7;loop++)		
	{
		if((CAN_CHANNEL->CANSIT1 & (1<< loop)))
		{
			// Selecting the mob
			CAN_CHANNEL->CANPAGE = ((loop+8) << 4);
			mobStatus = CAN_CHANNEL->CANSTMOB;
			if(mobStatus &(1<<6))
			{
				CAN_CHANNEL->CANSTMOB = 0;
				CAN_CHANNEL->CANCDMOB = 0;
				#if(CAN_USE_FIFO != 0)
				if(pfFifoIsEmpty(&canTxFifo) == enBooleanFalse)
				{
					pfCanPopTxMessage(&msg);
					pfCanWriteMessage(&msg);
				}
				#else
				if(canTxCallback != 0)
				{
					canTxCallback();
				}
				#endif	// #if(CAN_USE_FIFO != 0)
				
			}
	
			if(mobStatus &(1<<5))
			{
				#if(CAN_USE_FIFO != 0)
				pfCanReadMessage(&msg);
				pfCanPushRxMessage(&msg);
				#else
				if(canRxCallback != 0)
				{
					canRxCallback();
				}
				#endif	// #if(CAN_USE_FIFO != 0)
		
				//CANSTMOB = 0;
			}
	
	
			if((mobStatus & 0x10) || (mobStatus & 0x08) ||(mobStatus & 0x04) ||(mobStatus & 0x02) || (mobStatus & 0x01))
			{
				canErrCallback();
				CAN_CHANNEL->CANSTMOB =0;
			}
			
		}
	}
}

#endif		//#if (PF_USE_CAN == 1)