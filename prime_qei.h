/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework QEI driver for LPC17xx.
 *
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_QEI_API QEI API
 * @{
 */
 
/** Configuration macros for QEI		*/
#define QEI_CH						QEI
#define QEI_CHANNEL				PERIPH(QEI_CH)
#define QEI_INT_HANDLER		INT_HANDLER(QEI_CH)

/** QEI Interrupt values		*/

#define QEI_INT_INX						0x0001
#define QEI_INT_TIMER					0x0002
#define QEI_INT_VEL						0x0004
#define QEI_INT_DIR						0x0008
#define QEI_INT_ERR						0x0010
#define QEI_INT_ENCLK					0x0020
#define QEI_INT_POS0_MATCH		0x0040
#define QEI_INT_POS1_MATCH		0x0080
#define QEI_INT_POS2_MATCH		0x0100
#define QEI_INT_REV						0x0200
#define QEI_INT_POS0_REV			0x0400
#define QEI_INT_POS1_REV			0x0800
#define QEI_INT_POS2_REV			0x1000

/** QEI counter defines for resetting	*/

#define QEI_POS_COUNTER			0x01
#define QEI_RST_POS_INX			0x02
#define QEI_VEL_COUNTER			0x04
#define QEI_INX_COUNTER			0x08

typedef struct
{
	PFEnPclkDivider	clkDiv;		/**< Select QEI PCLK divider from pre-defined values 0-3 */
	PFbyte dirInv;						/**< 0- Do not invert direction; 1- Invert direction */
	PFbyte signalMode;				/**< 0- Quadrature decoder mode; 1- PhA act as direction signal and PhB act as clock signal */
	PFbyte captureMode;				/**< 0- 2x mode, only PhA edges are counted; 1- 4x mode, both PhA and PhB edges are counted */
	PFbyte inxInv;						/**< 0- Do not invert index signal; 1- Invert index signal */
	PFdword maxPosition;			/**< Maximum position count. Posistion counter is reset when reaches this count */
	PFdword posCompare[3];		/**< Position compare match registers. Interrupt can be generated when postion count reaches this value */
	PFdword indexCompare;			/**< Index compare match. Interrupt can be generated when index count reaches this value */
	PFdword velTimerCount;		/**< Timer count for velocity capture timer */
	PFdword velocityCompare;	/**< velocity compare match. Interrupt can be generated when velocity is less than this value */
	PFdword filter;						/**< Digital filter sample clock value. Encoder inputs must be stable over sample clock */
	PFdword interrupts;				/**< Bit packed value of the interrupts to enable */
	PFcallback indexCallback;				/**< Callback function if an Index pulse is detected									*/
	PFcallback velocityCallback;		/**< Callback function for Velocity register less than match register value			*/
	PFcallback errorCallback;				/**< Callback function for Encoder phase error			*/
	PFcallback encoderPulseCallback;		/**< Callback function for Encoder pulse detection		*/
	PFcallback posComp0Callback;		/**< Callback function for Position Compare Reg 0 match			*/
	PFcallback posComp1Callback;		/**< Callback function for Position Compare Reg 1 match			*/
	PFcallback posComp2Callback;		/**< Callback function for Position Compare Reg 2 match			*/
	PFcallback RevCallback;					/**< Callback function for Index Compare interrupt			*/
	PFcallback posComp0CombinedCallback; 	/**< Combined position 0 and revolution count interrupt			*/										
	PFcallback posComp1CombinedCallback;	/**< Combined position 1 and revolution count interrupt			*/
	PFcallback posComp2CombinedCallback;	/**< Combined position 2 and revolution count interrupt			*/
}PFCfgQei;

typedef PFCfgQei* PFpCfgQei;

typedef struct 
{
	PFdword position;
	PFdword velocity;
	PFdword index;
	PFbyte dir;
}PFCfgQeiParameters;

typedef PFCfgQeiParameters* PFpCfgQeiParameters;

PFEnStatus pfQeiOpen(PFpCfgQei config);
void pfQeiStop(void);
void pfQeiGetParameters(PFpCfgQeiParameters Param);
void pfQeiResetCounters(PFbyte counters);
PFdword pfQeiGetVelocity(void);
PFbyte pfQeiGetDirection(void);
PFdword pfQeiGetPosition(void);

/** @} */
