#include "prime_framework.h"
#include "prime_sysClk.h"
#include "prime_qei.h"

static void pfQeiGetIntStatus(PFword* status);
static void pfQeiClearInterrupt(PFword intClear);

PFdword qeiVelocity = 0;
PFbyte qeiDirection = 0;

static PFEnBoolean qeiInit = enBooleanFalse;
static PFCfgQei qeiCfg;
static PFcallback indexCallback = 0;				/**< Callback function if an Index pulse is detected									*/
static PFcallback velocityCallback = 0;		/**< Callback function for Velocity register less than match register value			*/
static PFcallback errorCallback = 0;				/**< Callback function for Encoder phase error			*/
static PFcallback encoderPulseCallback = 0;		/**< Callback function for Encoder pulse detection		*/
static PFcallback posComp0Callback = 0;		/**< Callback function for Position Compare Reg 0 match			*/
static PFcallback posComp1Callback = 0;		/**< Callback function for Position Compare Reg 1 match			*/
static PFcallback posComp2Callback = 0;		/**< Callback function for Position Compare Reg 2 match			*/
static PFcallback RevCallback = 0;		/**< Callback function for Index Compare Interrupt			*/
static PFcallback posComp0CombinedCallback = 0; 	/**< Combined position 0 and revolution count interrupt			*/										
static PFcallback posComp1CombinedCallback = 0;	/**< Combined position 1 and revolution count interrupt			*/
static PFcallback posComp2CombinedCallback = 0;	/**< Combined position 2 and revolution count interrupt			*/

PFEnStatus pfQeiOpen(PFpCfgQei config)
{
	#if (PF_QEI_DEBUG == 1)
        CHECK_NULL_PTR(config);
	if( (config->clkDiv > 3) || (config->callback == 0) )
        {
		return enStatusInvArgs;
        }
	#endif	// #if (PF_QEI_DEBUG == 1)	
	
	// power on RIT module
	POWER_ON(QEI_CH);					/* Power to QEI peripheral block */
	// set PCLK clock divider
	pfSysSetPclkDiv(PCLK_DIV(QEI_CH), config->clkDiv);

	// Set QEI configuration
	QEI_CHANNEL->QEICONF =(config->dirInv * 0x01) | (config->signalMode * 0x02) | (config->captureMode * 0x04) | (config->inxInv * 0x08);

	// Maximum position
	QEI_CHANNEL->QEIMAXPOS = config->maxPosition;

	// Position match registers
	QEI_CHANNEL->CMPOS0 = config->posCompare[0];
	QEI_CHANNEL->CMPOS1 = config->posCompare[1];
	QEI_CHANNEL->CMPOS2 = config->posCompare[2];

	// Index match register
	QEI_CHANNEL->INXCMP = config->indexCompare;

	// Velocity timer count
	QEI_CHANNEL->QEILOAD = config->velTimerCount;

	// Velocity match register
	QEI_CHANNEL->VELCOMP = config->velocityCompare;

	// Digital filter sample clock value
	QEI_CHANNEL->FILTER = config->filter;

	// QEI interrupts
	if(config->interrupts)
	{
		QEI_CHANNEL->QEISET = (config->interrupts & 0x1FFF);
		NVIC_EnableIRQ(IRQ_NUM(QEI_CH));
				
		indexCallback = config->indexCallback;
		velocityCallback = config->velocityCallback;
		errorCallback = config->errorCallback;
		encoderPulseCallback = config->encoderPulseCallback;
		posComp0Callback = config->posComp0Callback;
		posComp1Callback = config->posComp1Callback;
		posComp2Callback = config->posComp2Callback;
		RevCallback = config->RevCallback;
		posComp0CombinedCallback = config->posComp0CombinedCallback; 	
		posComp1CombinedCallback = config->posComp1CombinedCallback;
		posComp2CombinedCallback = config->posComp2CombinedCallback;
	}
	else
		NVIC_DisableIRQ(IRQ_NUM(QEI_CH));
	
	pfMemCopy(&qeiCfg, config, sizeof(PFCfgQei));
	qeiInit = enBooleanTrue;
	return enStatusSuccess;
}

void pfQeiStop(void)
{
	NVIC_DisableIRQ(IRQ_NUM(QEI_CH));
	POWER_OFF(QEI_CH);
	qeiInit = enBooleanFalse;
}

void pfQeiResetCounters(PFbyte counters)
{
	QEI_CHANNEL->QEICON = counters;
}

void pfQeiGetParameters(PFpCfgQeiParameters Param)
{
	Param->position = QEI_CHANNEL->QEIPOS;
	Param->velocity = QEI_CHANNEL->QEICAP;
	Param->index = QEI_CHANNEL->INXCNT;
	Param->dir = QEI_CHANNEL->QEISTAT & 0x01;
}

void pfQeiGetIntStatus(PFword* status)
{
	*status = QEI_CHANNEL->QEIINTSTAT;
}

void pfQeiClearInterrupt(PFword intClear)
{
	QEI_CHANNEL->QEICLR = (intClear & 0x1FFF);
}

PFdword pfQeiGetVelocity(void)
{
	return qeiVelocity;
}

PFbyte pfQeiGetDirection(void)
{
	return qeiDirection;
}

PFdword pfQeiGetPosition(void)
{
	return QEI_CHANNEL->QEIPOS;
}

void QEI_INT_HANDLER(void)
{
	PFword *intStatus;
	pfQeiGetIntStatus(intStatus);	

	switch(*intStatus)
	{
		case QEI_INT_INX: 			// Index pulse detected
				if(indexCallback != 0)
				{
							indexCallback();
				}
				break;

		case QEI_INT_TIMER:			// Velocity timer overflow
			qeiVelocity = QEI_CHANNEL->QEICAP;
			break;

		case QEI_INT_VEL:			// Captured velocity is less than match register value
			if(velocityCallback != 0)
				{
							velocityCallback();
				}					
			break;

		case QEI_INT_DIR:			// Motor direction changed
			qeiDirection = (QEI_CHANNEL->QEISTAT & 0x01);
			break;

		case QEI_INT_ERR:			// Encoder phase error
				if(errorCallback != 0)
				{
							errorCallback();
				}				
			break;

		case QEI_INT_ENCLK:			// Encoder pulse detected
				if(encoderPulseCallback != 0)
				{
							encoderPulseCallback();
				}			
			break;

		case QEI_INT_POS0_MATCH:	// Position Compare Reg 0 match occurred
				if(posComp0Callback != 0)
				{
							posComp0Callback();
				}		
			break;

		case QEI_INT_POS1_MATCH:	// Position Compare Reg 1 match occurred
			if(posComp1Callback != 0)
				{
							posComp1Callback();
				}	
			break;

		case QEI_INT_POS2_MATCH:	// Position Compare Reg 2 match occurred
				if(posComp2Callback != 0)
				{
							posComp2Callback();
				}	
			break;

		case QEI_INT_REV:			// Index Compare Reg match occurred
				if(RevCallback != 0)
				{
					RevCallback();
				}
			break;

		case QEI_INT_POS0_REV:		// INT_POS0 + INT_REV
				if(posComp0CombinedCallback != 0)
				{
					posComp0CombinedCallback();
				}
			break;

		case QEI_INT_POS1_REV:		// INT_POS1 + INT_REV
			if(posComp1CombinedCallback != 0)
				{
					posComp1CombinedCallback();
				}
			break;

		case QEI_INT_POS2_REV:		// INT_POS2 + INT_REV
			if(posComp2CombinedCallback != 0)
				{
					posComp2CombinedCallback();
				}
			break;
	}
	pfQeiClearInterrupt(*intStatus);
}

  

