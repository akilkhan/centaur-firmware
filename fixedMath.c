
#include "prime_framework.h"
#include "fixedMath.h"

PFEnStatus pfGetScaledUval(int64_t *res, PFdword num)
{
	if((num << BIT_PRECISION) < OVERFLOW_VAL_32B )
	{
		*res = (num << BIT_PRECISION) ;
		return enStatusSuccess;
	}
	else
	{
		return enStatusError;
	}
}
PFEnStatus pfGetScaledSval(int64_t *res, PFsdword num)
{
	*res = (num << BIT_PRECISION) ;
	return enStatusSuccess;
	
}

PFEnStatus pfFixedNum2Decimal(float *res, int64_t num )
{
	int32_t loop = 0, divFact = 1, intPart = 0, temp =0;
	float fraction = 0;
	int64_t multiplier = 10;
	
	intPart = (int64_t)num >> 16; 
	for(loop = (BIT_PRECISION - 1) ; loop >= 0 ; loop--)
	{
		temp = (((int64_t)num >> loop) & 0x01) ;
		if(temp != 0)
		{
			fraction += 1.0 / (1<<divFact);
		}
		divFact++;
		multiplier *= 10;
		
	}
	*res = intPart + fraction;
	return enStatusSuccess;
	
}

PFEnStatus pfDecimal2FixedNum(int64_t *res, pfFixedDataType* num )
{
	PFsdword loop = 0, tempFraction = 0, fraction = 0, temp =0;
	
	if(num->fractPart != 0)
	{
		for(loop = (BIT_PRECISION - 1) ; loop >=  0 ; loop--)
		{
			temp = (num->fractPart << 1) ;
			if(num->divFact != 0)
			{
				tempFraction = (temp / num->divFact) ;
			}
			else 
				return enStatusError;
			
			if(tempFraction >= 1)
			num->fractPart = temp - num->divFact ;
			else
			num->fractPart = temp ;
			
			fraction += (tempFraction << (loop)) ;
		}
		*res = (num->intPart << BIT_PRECISION) + fraction;
	}
	else
		*res = (num->intPart << BIT_PRECISION);
		
	return enStatusSuccess;
}
	

PFEnStatus pfFixedUmul32(int64_t *res, int64_t x, int64_t y)
{
	*res = ((uint64_t)x * y ) >> BIT_PRECISION; 
	if(*res < OVERFLOW_VAL_32B )
	return enStatusSuccess;
	else
	{
		return enStatusError;
	}
}

PFEnStatus pfFixedSmul32(int64_t *res, int64_t x, int64_t y)
{
	int64_t tempX = x, tempY = y; 
	if(x < 0)
	{
		tempX = -x;
	}
	if(y < 0)
	{
		tempY = -y;
	}
	*res = (BIT_MASK & ((int64_t)((int64_t)tempX * tempY ) >> (BIT_PRECISION))); 
	
	if(x < 0)
	{
		*res = -(*res);
	}
	if(y < 0)
	{
		*res = -(*res);
	}
	return enStatusSuccess;
}


PFEnStatus pfFixedUdiv32(int64_t *res, int64_t x, int64_t y)
{
	int64_t temp;
	temp = (int64_t)(x << BIT_PRECISION);
	*res = temp/y; 
	return enStatusSuccess;
}

PFEnStatus pfFixedSdiv32(int64_t *res, int64_t x, int64_t y)
{
	int64_t tempX = x, tempY = y; 
	int64_t temp = 0;
	if(x < 0)
		tempX = -x;
	if(y < 0)
		tempY = -y;
	
	temp = ((int64_t)tempX << BIT_PRECISION);
	if(tempY == 0)
		return enStatusError;
	*res = temp/tempY;
	
	if(x < 0)
		*res = -(*res);
	if(y < 0)
		*res = -(*res);
	return enStatusSuccess;
}

