/** CentaurMain.h */

#define 	ENABLE_PID_LOG						0
#define 	ENABLE_COMMAND_QUEUE			0
#define 	USE_COUNT_DIFF_FILTER			0
#define 	CENTAUR_STANDALONE_TEST		0
#define 	ENABLE_CENTAUR_PROFILING	0
#define 	FUTURE_EXPANSION					0

#if (ENABLE_PID_LOG == 1)
	#define STORED_DATA_SIZE					10L
#endif

#if (ENABLE_COMMAND_QUEUE == 1)
	#define CENTAUR_CMD_QUEUE_SIZE    64  /* Max number of cmd's that can be added to Queue */
#endif

#if (USE_COUNT_DIFF_FILTER == 1)
	#define COUNT_DIFF_MAX_0			200
	#define COUNT_DIFF_MAX_1			200
	#define COUNT_DIFF_MAX_2			200
	#define COUNT_DIFF_MAX_3			200
#endif

#if (ENABLE_COMMAND_QUEUE == 1)
/* Common structure to store commands to command queue */
typedef struct{
	PFbyte cmd;               /** Cmd number */
	PFbyte dir;               /** Direction of rotation */
	PFbyte status;            /** Status to be impose */
	float speed;              /** Speed of rotation */
	float acc;                /** Acceleration value */
	float dec;                /** Deceleration value */
}PFcentaurCmdQueue;
#endif
