#include "prime_framework.h"
#include "prime_centaurBoardConfig.h"
#include "a3941.h"
#include "prime_gpio.h"
#include "prime_sysClk.h"
#include "prime_spi0.h"
#include "prime_spi1.h"
#include "prime_uart0.h"
#include "prime_uart2.h"
#include "prime_pwm.h"
#include "prime_adc.h"
#include "prime_rit.h"
#include "prime_canCommon.h"
#include "prime_can1.h"
#include "prime_timer1.h"
#include "fixedMath.h"
#include "prime_pidHwconfig.h"
#include "prime_pid.h"
#include "prime_delay.h"
#include "prime_qei.h"

PFCfgGpio pfUART0GpioCfg[2] =
{
	//TX0
	{CENTAUR_UART_0_TX_PORT,CENTAUR_UART_0_TX_PIN,enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_2_TXD0},
	//RX0
	{CENTAUR_UART_0_RX_PORT,CENTAUR_UART_0_RX_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_3_RXD0}
};

PFCfgGpio pfCAN1GpioCfg[2] =
{
	//RD1
	{CENTAUR_RJ11_CAN_1_RD_PORT,CENTAUR_RJ11_CAN_1_RD_PIN,enGpioDirInput,enGpioPinModePullUp,enGpioOpenDrainDisable, GPIO_0_0_RD1},
	//TD1
	{CENTAUR_RJ11_CAN_1_TD_PORT,CENTAUR_RJ11_CAN_1_TD_PIN,enGpioDirOutput,enGpioPinModePullUp,enGpioOpenDrainDisable, GPIO_0_1_TD1},
};

PFCfgGpio pfSPI0GpioCfg[3] =
{
	//SPI0 Configuration
	//SPI  0 MISO
	{CENTAUR_J1_7_SSP0_MISO_PORT, CENTAUR_J1_7_SSP0_MISO_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_17_MISO0},
	// SPI 0 MOSI
	{CENTAUR_J1_6_SSP0_MOSI_PORT, CENTAUR_J1_6_SSP0_MOSI_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_18_MOSI0},
	// SPI 0 SCK
	{CENTAUR_J1_8_SSP0_SCK_PORT, CENTAUR_J1_8_SSP0_SCK_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_15_SCK0},
};

PFCfgGpio pfSPI1GpioCfg[3] =
{	
 	//SPI1 Configuration
	//SPI  1 MISO
	{CENTAUR_J1_13_SSP1_MISO_PORT, CENTAUR_J1_13_SSP1_MISO_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_8_MISO1},
	// SPI 1 MOSI
	{CENTAUR_J1_12_SSP1_MOSI_PORT, CENTAUR_J1_12_SSP1_MOSI_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_9_MOSI1},
	// SPI 1 SCK
	{CENTAUR_J1_14_SSP1_SCK_PORT, CENTAUR_J1_14_SSP1_SCK_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_7_SCK1},
};

PFCfgGpio pfQEIGpioCfg[] =
{
	// PHASE A
	{CENTAUR_QEI_4_QEI_PHA_PORT,CENTAUR_QEI_4_QEI_PHA_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_20_MCI0},
	// PHASE B
	{CENTAUR_QEI_3_QEI_PHB_PORT,CENTAUR_QEI_3_QEI_PHB_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_23_MCI1},
	// INDEX
	{CENTAUR_QEI_2_QEI_INDEX_PORT,CENTAUR_QEI_2_QEI_INDEX_PIN,enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_1_24_MCI2},
};

extern void allPIDTermUsed(void);
extern void canReceiveIsr(void);
extern void cheetahCbTestIsr(void);

void canTransmitIsr(void)
{
	return;
}

void canHandler(void)
{
	return;
}

PFCfgClk systemConfig = 
{
	100000000,
	12000000, 
	enPllClkSrcMainOSC
};

PFCfgUart0 uartConfig = 
{
	enPclkDiv_4, 			
	enUart0Baudrate_115200, 	
	enUart0Databits_8, 		
	enUart0ParityNone, 		
	enUart0StopBits_1, 
	enUart0IntRx
};

PFCfgRit ritConfig = 
{
	250,			// configure RIT for 10ms
	0x00,			// compare mask
	pfDelayTickUpdate,	// callback
	enPclkDiv_4,	// pclk divider
	enBooleanTrue,	// halt on break
	enBooleanTrue,	// reset on match
	
};

PFCfgCan1 canConfig = 
{
	canReceiveIsr,	// Receive Callback	
	canTransmitIsr,	// Transmit Callback
	1000000,		// baudrate
	canHandler,		// CAN Error callback
	enPclkDiv_1,	// pclk divider
	enCan1IntRx,	// interrupts
	
};

PFCfgSpi1 spiConfig =
{
	enPclkDiv_4,		// pclk divider
	enBooleanTrue,		// master mode
	enSpi1Databits_8,	// 8 databits
	enSpi1Mode_0,		// spi mode 0
	2000000,			// baudrate = 2MHz
	0,
	enSpi1IntNone		// No interrupt to enable
};

PFCfgSpi0 spi0Config =
{
	enPclkDiv_4,		// pclk divider
	enBooleanTrue,		// master mode
	enSpi0Databits_8,	// 8 databits
	enSpi0Mode_0,		// spi mode 0
	2000000,			// baudrate = 2MHz
	0,
	enSpi0IntNone
};

PFCfgPwm pwmConfig = 
{	
	5,				// prescaler
	enPclkDiv_4,	// pclk divider
	enPwmModePwm,	// Mode
	{enPwmChannelModeDisable, enPwmChannelModeDisable, enPwmChannelModeDisable, enPwmChannelModeSingleEdge, enPwmChannelModeSingleEdge, enPwmChannelModeDisable},	// channel mode
	{500, 0, 0, 0, 0, 500, 0},	// match reg value
	{enPwmMatchActNone, enPwmMatchActNone, enPwmMatchActNone, enPwmMatchActNone, enPwmMatchActNone, enPwmMatchActNone, enPwmMatchActNone},			// match action
	enBooleanFalse,
	0
};

PFCfgTimer1 timer1Config = 
{					
	25, 							// pre
	{5000,0, 0, 0}, 		// match reg
	{enTimer1MatchActResetInt, enTimer1MatchActNone, enTimer1MatchActNone, enTimer1MatchActNone},		// internal match action
	{enTimer1ExtMatchCtrlNone, enTimer1ExtMatchCtrlNone, enTimer1ExtMatchCtrlNone, enTimer1ExtMatchCtrlNone},	// external match control
	allPIDTermUsed, //call  back
	enPclkDiv_4, 					// pclk divider
	enTimer1ModeTimer,// timer mode	
	enBooleanTrue,	// interrupt
	
};

PFCfgGpio driverGpioPins[] = 
{	
	// RESET
	{A3941_RESET_PORT, A3941_RESET_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// PWMH
	{A3941_PWMH_PORT, A3941_PWMH_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_4_PWM1_5},
	// PWML
	{A3941_PWML_PORT, A3941_PWML_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_2_3_PWM1_4},
	// SR	
	{A3941_SR_PORT, A3941_SR_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// PHASE
	{A3941_PHASE_PORT, A3941_PHASE_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// FF1
	{A3941_FF1_PORT, A3941_FF1_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// FF2
	{A3941_FF2_PORT, A3941_FF2_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// FWF LED
	{A3941_LED_PORT, A3941_LED_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
};	

PFCfgQei qeiconfig = 
{
	enPclkDiv_1,		/**< Select QEI PCLK divider from pre-defined values 0-3 */
	1,			/**< Invert direction */
	0,			/**< 0- Quadrature decoder mode; 1- PhA act as direction signal and PhB act as clock signal */
	1,			/**< 0- 2x mode, only PhA edges are counted; 1- 4x mode, both PhA and PhB edges are counted */
	0,				/**< 0- Do not invert index signal; 1- Invert index signal */
	0xFFFFFFFF,		/**< Maximum position count. Posistion counter is reset when reaches this count */
	{0,0,0},		/**< Position compare match registers. Interrupt can be generated when postion count reaches this value */
	0,		/**< Index compare match. Interrupt can be generated when index count reaches this value */
	0,		/**< Timer count for velocity capture timer */
	0,	/**< velocity compare match. Interrupt can be generated when velocity is less than this value */
	1,				/**< Digital filter sample clock value. Encoder inputs must be stable over sample clock */
	0		/**< No Interrupts */
};

PFEnStatus boardInit(void)
{
	PFEnStatus status;	
	
	// system clock init
	pfSysSetCpuClock(&systemConfig);
	
	// GPIO init
	status = pfGpioInit(pfUART0GpioCfg, 2);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	
	status = pfGpioInit(pfQEIGpioCfg, 3);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	
	status = pfGpioInit(pfCAN1GpioCfg, 2);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	
	status = pfGpioInit(pfSPI0GpioCfg, 3);
	if(status != enStatusSuccess)
	{
		while(1);
	}

	status = pfGpioInit(pfSPI1GpioCfg, 3);
	if(status != enStatusSuccess)
	{
		while(1);
	}

	/* RIT init */
	status = pfRitOpen(&ritConfig);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	pfRitStart();
	
	/* UART init */
	status = pfUart0Open(&uartConfig);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	
	/* SPI init */
	status = pfSpi1Open(&spiConfig);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	
	status = pfSpi0Open(&spi0Config);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	
	status = pfTimer1Open(&timer1Config);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	
	// PWM init
	status = pfPwmOpen(&pwmConfig);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	
	status = pfQeiOpen(&qeiconfig);
	if(status != enStatusSuccess)
	{
		while(1);
	}

	status = pfCan1Open(&canConfig);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	pfPwmStart();
	pfUart0Write((PFbyte*)"\rPhi Robotics", 14);
	return enStatusSuccess;
}




